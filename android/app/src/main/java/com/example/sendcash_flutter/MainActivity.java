package com.example.sendcash_flutter;

import io.flutter.embedding.android.FlutterActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.android.FlutterFragmentActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.plugins.util.GeneratedPluginRegister;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterFragmentActivity {

    private static final String CHANNEL = "com.sendcash_flutter.channel";
    private static final String apiSecret = "SK-32N27QYH-6LJUUP6E-VNBRRJBA-X74QH7T6";
    private static BinaryMessenger flutterView;
    MethodChannel methodChannel;

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        GeneratedPluginRegister.registerGeneratedPlugins(flutterEngine);
        flutterView = flutterEngine.getDartExecutor().getBinaryMessenger();
        methodChannel = new MethodChannel(flutterView, CHANNEL);
        methodChannel.setMethodCallHandler(this::onMethodCall);
        getHashKey();

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
    }

    private void getHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private void onMethodCall(MethodCall call, MethodChannel.Result result) {
        if(call.method.equals("sign")) {
            List<String> arguments = (List<String>) call.arguments;
            result.success(computeSignature(arguments.get(0),arguments.get(1),arguments.get(2)));
        }
    }

    public static String computeSignature(String secretKey, String url, String reqData) {
        String data = url + reqData;
        try {
            Mac sha256Hmac = Mac.getInstance("HmacSHA256");
            SecretKeySpec key = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
            sha256Hmac.init(key);
            byte[] macData = sha256Hmac.doFinal(data.getBytes());
            String result = "";
            for (final byte element : macData){
                result += Integer.toString((element & 0xff) + 0x100, 16).substring(1);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

}

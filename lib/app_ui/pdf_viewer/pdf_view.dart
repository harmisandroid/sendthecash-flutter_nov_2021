import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/generated/i18n.dart';

class PdfView extends StatefulWidget {
  @override
  _PdfViewState createState() => _PdfViewState();
}

class _PdfViewState extends State<PdfView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Stack(
        children: [
          GAppbar(
            title: S.of(context).downloadPdf,
            height: 153.0,
          ),
          Container(
            margin: EdgeInsets.only(top: 150.0),
            padding: EdgeInsets.only(left: 20.0, right: 20.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(0.0, 0.0))
                ]),
            clipBehavior: Clip.antiAlias,
          ),
          Container(
            margin: EdgeInsets.only(top: 153),
            child: PDF(fitPolicy: FitPolicy.BOTH).cachedFromUrl(
              'https://shophabby.com//public//pdf//1641293259_930802.pdf',
              placeholder: (progress) => Center(child: Text('$progress %')),
              errorWidget: (error) => Center(child: Text(error.toString())),
            ),
          )
        ],
      ),
    );
  }
}

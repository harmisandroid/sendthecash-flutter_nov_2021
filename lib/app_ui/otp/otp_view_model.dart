import 'dart:convert';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:sendcash_flutter/app_ui/change_password/change_password_view.dart';
import 'package:sendcash_flutter/app_ui/otp/otp_view.dart';
import 'package:sendcash_flutter/app_ui/upload_documents/upload_documents_view.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/constant.dart';

class OtpViewModel with ChangeNotifier {
  BuildContext mContext;
  AppPreferences appPreferences = AppPreferences();
  ApiServices apiServices = ApiServices();
  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();
  LoginDetails loginDetails;
  String userId;
  String mFrom;

  Future<void> attachContext(BuildContext context, LoginDetails mLoginDetails,
      String firebaseId,String from) async {
    mContext = context;
    userId = firebaseId;
    loginDetails = mLoginDetails;
    mFrom = from;
  }



  /*-------------------------------------- phone verification --------------------------*/


  void sendOtp(
      {Map<String, dynamic> otpParams,
      String mobileNumber,
      String phoneCode,
      String otpCode}) async {
    CommonUtils.showProgressDialog(mContext);
    String response = await apiServices.sendOtp(
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("Failed to send otp");
        },
        mapParams: otpParams);
    if (response != null) {
      Map<String, dynamic> mapResponse = Map();
      mapResponse = jsonDecode(response);
      if (mapResponse['reference_id'] != null &&
          mapResponse['status']['code'] == 290) {
        insertOtpToFiresbase(
            mobileNumber: mobileNumber, phoneCode: phoneCode, otpCode: otpCode);
      } else {
        CommonUtils.hideProgressDialog(mContext);
        CommonUtils.showRedToastMessage("Failed to send otp");
      }
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showRedToastMessage("Oops something went wrong");
    }
  }

  Future<void> insertOtpToFiresbase(
      {String mobileNumber, String phoneCode, String otpCode}) async {
    Map<String, dynamic> mapDynamic = Map();
    mapDynamic[DbKey.MOBILE_NO] = mobileNumber;
    mapDynamic[DbKey.PHONE_CODE] = phoneCode;
    mapDynamic[DbKey.OTP] = otpCode;
    await firebaseDatabaseHelper.insertFirebaseOtp(
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("failed to add otp firebase");
        },
        onSuccess: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage("Otp send successfully!");
        },
        userdata: mapDynamic,
        onDocumentAdded: (documentRefernce) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage("Otp send successfully!");
        });
  }

  Future<void> verifyOtp(Map<String, dynamic> otpParams, from) async {
    CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.verifyOtp(
        userdata: otpParams,
        onSuccess: (QuerySnapshot<Map<String, dynamic>> querySnapshot) {
          CommonUtils.hideProgressDialog(mContext);
          if (querySnapshot.size > 0) {
            CommonUtils.showGreenToastMessage("Otp Verified success");
            if (from == AppConstants.SCREEN_FORGOT_PASSWORD) {
              Navigator.push(mContext, CupertinoPageRoute(builder: (context) {
                return ChangePasswordView(
                  firebaseUserId: userId,
                  loginDetails: loginDetails,
                  from:AppConstants.SCREEN_FORGOT_PASSWORD,
                );
              }));
            } else if (from == AppConstants.SCREEN_REGISTER || from == AppConstants.SCREEN_LOGIN || from == AppConstants.SCREEN_WELCOME) {
             updatePhoneVerifiedTrue();
            }
          } else {
            CommonUtils.showRedToastMessage(S.of(mContext).invalidOtp);
          }
        },
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage(error.toString());
        });
  }

  /*------------------- Email verification --------------------*/

  Future<void> sendOtpToEmail({otp, email}) async {
    CommonUtils.showProgressDialog(mContext);
    String response = await apiServices.sendOtpToEmail(
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
        },
        otp: otp,
        email: email);
    if (response == "Success") {
      CommonUtils.showGreenToastMessage(
          "otp sent to email address successfully");
      insertEmailotpToFirebase(emailAddress: email, otp: otp);
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showRedToastMessage("Failed to send email address");
    }
  }

  Future<void> insertEmailotpToFirebase(
      {String emailAddress, String otp}) async {
    Map<String, dynamic> emailParams = Map();
    emailParams[DbKey.EMAIL] = emailAddress;
    emailParams[DbKey.OTP] = otp;
    await firebaseDatabaseHelper.insertEmailFirebaseOtp(
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("failed to add otp firebase");
        },
        onSuccess: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage("Otp send successfully!");
        },
        userdata: emailParams,
        onDocumentAdded: (documentRefernce) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage("Otp send successfully!");
        });
  }

  Future<void> verifyEmailOtp(Map<String, dynamic> otpParams, from) async {
    CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.verifyEmailOtp(
        userdata: otpParams,
        onSuccess: (QuerySnapshot<Map<String, dynamic>> querySnapshot) async {
          CommonUtils.hideProgressDialog(mContext);
          if (querySnapshot.size > 0) {
            CommonUtils.showGreenToastMessage("Otp Verified success");
            if (from == AppConstants.SCREEN_FORGOT_PASSWORD) {
              Navigator.push(mContext, CupertinoPageRoute(builder: (context) {
                return ChangePasswordView(
                  firebaseUserId: userId,
                  loginDetails: loginDetails,
                  from: AppConstants.SCREEN_FORGOT_PASSWORD,
                );
              }));
            } else if(from == AppConstants.SCREEN_PROFILE) {
              loginDetails.isEmailVerified = true;
              await appPreferences.setLoginDetails(jsonEncode(loginDetails));
              Navigator.pop(mContext,true);
            }else{
              Navigator.push(mContext, CupertinoPageRoute(builder: (context) {
                return UploadDocumentsView();
              }));
            }
          } else {
            CommonUtils.showRedToastMessage(S.of(mContext).invalidOtp);
          }
        },
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage(error.toString());
        });
  }


  /*-----------------verify phone from regsiter screen---------------*/

  void updatePhoneVerifiedTrue() async {
    CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.setPhoneVerifiedTrue(
        isPhoneVerified: true,
        userId: userId,
        onError: (error, stackTrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("failed to set email verify true");
        },
        onSuccess: (value) async {
          CommonUtils.hideProgressDialog(mContext);
          loginDetails.isPhoneVerified = true;
          await appPreferences.setLoginDetails(jsonEncode(loginDetails));
          if(mFrom != null && mFrom == AppConstants.SCREEN_PROFILE){
            Navigator.pop(mContext,true);
          }else{
            Navigator.pushAndRemoveUntil(mContext, CupertinoPageRoute(builder:(context){
              return UploadDocumentsView();
            }), (route) => false);
          }

        });
  }
}

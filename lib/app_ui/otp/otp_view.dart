import 'dart:async';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/otp/countdown_timer.dart';
import 'package:sendcash_flutter/app_ui/otp/otp_view_model.dart';
import 'package:sendcash_flutter/app_ui/upload_documents/upload_documents_view.dart';
import 'package:sendcash_flutter/common_widgets/appbar/back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/buttons/Buttons.dart';
import 'package:sendcash_flutter/common_widgets/logo/custom_logo.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class OTPView extends StatefulWidget {
  String mobileNumber;
  String emailAddress;
  String phoneCode;
  LoginDetails loginDetails;
  String firebaseUserId;
  String from;

  OTPView(
      {Key key,
      this.mobileNumber,
      this.phoneCode,
      this.emailAddress,
      this.loginDetails,
      this.firebaseUserId,
      this.from})
      : super(key: key);

  @override
  _OTPViewState createState() => _OTPViewState();
}

class _OTPViewState extends State<OTPView> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController textEditingController = TextEditingController();
  StreamController<ErrorAnimationType> errorController;
  bool hasError = false;
  String otpCode = "";
  final formKey = GlobalKey<FormState>();
  OtpViewModel mViewModel;
  bool isResendActivated = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context, widget.loginDetails, widget.firebaseUserId,widget.from);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<OtpViewModel>(context);
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
              CommonColors.gradientColorTop,
              CommonColors.gradientColorBottom
            ])),
        child: Scaffold(
          key: scaffoldKey,
          backgroundColor: Colors.transparent,
          appBar: BackAppbar(
            title: S.of(context).otp,
          ),
          body: SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: 100),
                    child: Image.asset(
                      LocalImages.applogotext,
                      width: 240,
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: 40),
                    child: Text(
                      S.of(context).verifyPhone,
                      style: CommonStyle.getAppFont(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: 25, left: 30, right: 30),
                    child: Text(
                      widget.emailAddress == null
                          ? S.of(context).verifyHint +
                              widget.phoneCode +
                              widget.mobileNumber
                          : S.of(context).emailHint + " ${widget.emailAddress}",
                      textAlign: TextAlign.center,
                      style: CommonStyle.getAppFont(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.normal),
                    ),
                  ),
                  Form(
                    key: formKey,
                    child: Padding(
                        padding: const EdgeInsets.only(
                            left: 60.0, right: 60.0, top: 20.0),
                        child: PinCodeTextField(
                          appContext: context,
                          pastedTextStyle: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                          length: 4,
                          textStyle: CommonStyle.getAppFont(
                              color: Colors.white,
                              fontSize: 22.0,
                              fontWeight: FontWeight.normal),
                          obscureText: false,
                          blinkWhenObscuring: true,
                          animationType: AnimationType.fade,
                          validator: (v) {},
                          pinTheme: PinTheme(
                            shape: PinCodeFieldShape.circle,
                            fieldHeight: 55,
                            fieldWidth: 55,
                            activeFillColor:
                                CommonColors.shadowColor.withOpacity(.5),
                            inactiveColor:
                                CommonColors.shadowColor.withOpacity(.5),
                            selectedFillColor:
                                CommonColors.shadowColor.withOpacity(.5),
                            inactiveFillColor:
                                CommonColors.shadowColor.withOpacity(.5),
                            activeColor:
                                CommonColors.shadowColor.withOpacity(.5),
                            selectedColor: Colors.white,
                          ),
                          cursorColor: Colors.white,
                          /*animationDuration: Duration(milliseconds: 300),*/
                          enableActiveFill: true,
                          errorAnimationController: errorController,
                          controller: textEditingController,
                          keyboardType: TextInputType.number,
                          onCompleted: (v) {
                            printf("Completed");
                            otpCode = v;
                          },
                          onChanged: (value) {
                            printf(value);
                            setState(() {
                              otpCode = value;
                            });
                          },
                          beforeTextPaste: (text) {
                            printf("Allowing to paste $text");
                            //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                            //but you can show anything you want here, like your pop up saying wrong paste format or etc
                            otpCode = text;
                            return true;
                          },
                        )),
                  ),
                  CountDownTimer(
                    secondsRemaining: 60,
                    whenTimeExpires: () {
                      setState(() {
                        isResendActivated = true;
                      });
                    },
                    countDownTimerStyle: CommonStyle.getAppFont(
                        color: Colors.white, fontSize: 20.0),
                  ),
                  Visibility(
                    child: InkWell(
                      onTap: () {
                        String code = getRandomCodeFourDigit();
                        Map<String, dynamic> sendOtpMap = Map();
                        sendOtpMap['phone_number'] =
                            widget.phoneCode.replaceAll("+", "") +
                                widget.mobileNumber;
                        sendOtpMap['message'] =
                            "Your code is ${code.toString()}";
                        sendOtpMap['message_type'] = "OTP";
                        if (widget.emailAddress == null) {
                          mViewModel.sendOtp(
                              otpCode: code,
                              mobileNumber: widget.mobileNumber,
                              otpParams: sendOtpMap,
                              phoneCode: widget.phoneCode);
                        } else {
                          mViewModel.sendOtpToEmail(
                            otp: code,
                            email: widget.emailAddress,
                          );
                        }
                      },
                      child: Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(left: 30, right: 30),
                        child: Text(
                          S.of(context).recentCode,
                          textAlign: TextAlign.center,
                          style: CommonStyle.getAppFont(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                    visible: isResendActivated,
                  ),
                  Visibility(
                    child: Container(
                      height: 1,
                      width: 80,
                      color: Colors.white,
                    ),
                    visible: isResendActivated,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20.0, right: 20.0),
                    child: Buttons(
                      text: S.of(context).verify,
                      onClick: () async {
                        if (otpCode.isNotEmpty) {
                          if(widget.emailAddress == null){
                            Map<String, dynamic> otpParams = Map();
                            otpParams[DbKey.OTP] = otpCode;
                            otpParams[DbKey.MOBILE_NO] = widget.mobileNumber;
                            mViewModel.verifyOtp(otpParams, widget.from);
                          }else{
                            Map<String, dynamic> otpParams = Map();
                            otpParams[DbKey.OTP] = otpCode;
                            otpParams[DbKey.EMAIL] = widget.mobileNumber;
                            mViewModel.verifyEmailOtp(otpParams, widget.from);
                          }

                        } else {
                          CommonUtils.showRedToastMessage(
                              "Please enter received otp");
                        }
                      },
                    ),
                  )
                ]),
          ),
        ));
  }
}

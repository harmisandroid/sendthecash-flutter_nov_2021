import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_picker/country_picker.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import 'package:local_auth/local_auth.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/app/app_model.dart';
import 'package:sendcash_flutter/app_ui/home/home_view.dart';
import 'package:sendcash_flutter/app_ui/login/login_view.dart';
import 'package:sendcash_flutter/app_ui/upload_documents/upload_documents_view.dart';
import 'package:sendcash_flutter/app_ui/welcome/welcome_view.dart';
import 'package:sendcash_flutter/common_widgets/bottomnavbar/bottom_navigation_view.dart';
import 'package:sendcash_flutter/common_widgets/social_row/social_row.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/linked_account_details.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/services/wyre_params.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:http/http.dart' as http;
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';
import 'package:twitter_login/entity/auth_result.dart';
import 'package:twitter_login/twitter_login.dart';
// import 'package:twitter_login/twitter_login.dart';

class LinkedAccountsViewModel with ChangeNotifier {
  AppPreferences appPreferences = new AppPreferences();
  LoginDetails loginDetails;
  BuildContext mContext;
  final facebookLogin = FacebookLogin();
  final LocalAuthentication localAuth = LocalAuthentication();
  ApiServices apiServices = ApiServices();
  final firebaseAuth = FirebaseAuth.instance;
  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();
  String firebaseUserId;
  List<LinkedAccountDetails> accountList = [];
  bool isEmptyList = false;

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    accountList.clear();
    isEmptyList = false;
    firebaseUserId = await appPreferences.getUserFirebaseId();
    getLinkedAccounts();
  }

  //user social register

  Future<void> googleLogin() async {
    GoogleSignIn _googleSignIn = GoogleSignIn(
      scopes: ['email'],
    );
    try {
      GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
      if (googleSignInAccount != null) {
        addToLinkedAccount(mapData: {
          DbKey.USERNAME: googleSignInAccount.displayName,
          DbKey.EMAIL: googleSignInAccount.email,
          DbKey.USER_SOCIAL_ID: googleSignInAccount.id,
          DbKey.PROFILE_PIC: googleSignInAccount.photoUrl,
          DbKey.LOGIN_TYPE: AppConstants.LOGIN_TYPE_GOOGLE,
          DbKey.USER_ID: firebaseUserId,
        });
        _googleSignIn.signOut();
      } else {
        CommonUtils.showRedToastMessage("google login failed");
      }
    } catch (error) {
      CommonUtils.showRedToastMessage("google login failed" + error.toString());
    }
  }

  Future<void> twitterLogin() async {
    final twitterLogin = TwitterLogin(
      apiKey: AppConstants.TWITTER_API_KEY,
      apiSecretKey: AppConstants.TWITTER_API_SECRET,
      redirectURI: AppConstants.TWITTER_CALLBACK_URL,
    );

    AuthResult authResult = await twitterLogin.loginV2();
    printf("ScreenName "+ authResult.user.screenName);
    switch (authResult.status) {
      case TwitterLoginStatus.loggedIn:
        addToLinkedAccount(mapData: {
          DbKey.USERNAME: authResult.user.name,
          DbKey.EMAIL: "",
          DbKey.USER_SOCIAL_ID: authResult.user.id,
          DbKey.PROFILE_PIC: authResult.user.thumbnailImage,
          DbKey.LOGIN_TYPE: AppConstants.LOGIN_TYPE_TWITTER,
          DbKey.USER_ID: firebaseUserId,
          DbKey.USER_SOCIAL_NAME: authResult.user.screenName,
        });
        break;
      case TwitterLoginStatus.cancelledByUser:
        // cancel
        break;
      case TwitterLoginStatus.error:
        // error
        printf("Twitter  Error => " + authResult.errorMessage);
        break;
    }
  }

  Future<void> fbLogin() async {
    final result = await facebookLogin.logIn(['email', 'public_profile']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        getProfileData(result.accessToken.token);
        break;
      case FacebookLoginStatus.cancelledByUser:
        CommonUtils.showRedToastMessage("Login cancelled ");
        break;
      case FacebookLoginStatus.error:
        CommonUtils.showRedToastMessage("Login failed " + result.errorMessage);
        break;
    }
  }

  void getProfileData(String token) async {
    CommonUtils.showProgressDialog(mContext);
    var url = Uri.parse('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${token}');
    final graphResponse = await http.get(url);
    CommonUtils.hideProgressDialog(mContext);
    final profile = jsonDecode(graphResponse.body);
    printf("Facebook data " + graphResponse.body);
    String profilePic = "http://graph.facebook.com/" +
        profile['id'] +
        "/picture?type=large&redirect=true&width=500&height=500";

    addToLinkedAccount(mapData: {
      DbKey.USERNAME: profile['first_name'] + " " + profile['last_name'],
      DbKey.EMAIL: profile['email'] ?? "",
      DbKey.USER_SOCIAL_ID: profile['id'],
      DbKey.PROFILE_PIC: profilePic,
      DbKey.LOGIN_TYPE: AppConstants.LOGIN_TYPE_FACEBOOK,
      DbKey.USER_ID: firebaseUserId,
    });
    facebookLogin.logOut();
  }

  void addToLinkedAccount({Map<String, dynamic> mapData}) async {
    mapData[DbKey.USER_PROFILE_LINK] = "https://twitter.com/elonmusk?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor";
    CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.addLinkedAccounts(
        data: mapData,
        alreadyExists: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage(S.of(mContext).accountAlreadyLinked);
        },
        onSuccess: (documentReference) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage(S.of(mContext).accountLinkedSuccessfully);
          accountList.clear();
          getLinkedAccounts();
        },
        onError: (object, tracktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("Failed to add  linked accounts");
        });
  }

  void showAccountDialog() {
    showDialog(
        barrierDismissible: true,
        useSafeArea: true,
        context: mContext,
        builder: (context) {
          return Dialog(
            insetPadding: EdgeInsets.all(20.0),
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: Container(
              height: 150.0,
              padding: EdgeInsets.all(10.0),
              color: CommonColors.primaryColor,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    S.of(context).continueWith,
                    style: CommonStyle.getAppFont(
                        color: Colors.white,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                  ),
                  getRowWidget(
                    onClicked: (String clickType) {
                      if (clickType == AppConstants.CLICK_TYPE_FACEBOOK) {
                        fbLogin();
                      } else if (clickType == AppConstants.CLICK_TYPE_GOOGLE) {
                        googleLogin();
                      } else {
                        twitterLogin();
                      }
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget getRowWidget({Function(String) onClicked}) {
    return Container(
      height: 100.0,
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
         /* getSocialButton(
              logo: LocalImages.facebook,
              onClick: () {
                if (onClicked != null) {
                  onClicked(AppConstants.CLICK_TYPE_FACEBOOK);
                }
              }),*/
          getSocialButton(
              logo: LocalImages.twitter,
              onClick: () {
                if (onClicked != null) {
                  onClicked(AppConstants.CLICK_TYPE_TWITTER);
                }
              }),
          getSocialButton(
              logo: LocalImages.google,
              onClick: () {
                if (onClicked != null) {
                  onClicked(AppConstants.CLICK_TYPE_GOOGLE);
                }
              }),
        ],
      ),
    );
  }

  Widget getSocialButton({String logo, Function onClick}) {
    return InkWell(
      onTap: onClick,
      child: Container(
        height: 40,
        width: 75,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(30)),
          color: CommonColors.shadowcolor.withOpacity(.5),
        ),
        child: Center(
          child: Image.asset(
            logo,
            height: 24,
            width: 24,
          ),
        ),
      ),
    );
  }

  void getLinkedAccounts() async {
    CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.getLinkedAccounts(
        userId: firebaseUserId,
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage(error.toString());
        },
        onSuccess: (QuerySnapshot<Map<String, dynamic>> querySnapshot) {
          CommonUtils.hideProgressDialog(mContext);
          if (querySnapshot.size > 0) {
            isEmptyList = false;
           querySnapshot.docs.forEach((element) {
             accountList.add(LinkedAccountDetails.fromJson(element.data()));
           });
          } else {
            isEmptyList = true;
          }
          notifyListeners();
        });
  }
}

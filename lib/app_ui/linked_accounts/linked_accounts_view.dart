import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/linked_accounts/linked_accounts_view_model.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/gradient_button/gradientbutton.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/linked_account_details.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/text_style.dart';
import 'package:url_launcher/url_launcher.dart';

class LinkedAccountsView extends StatefulWidget {
  @override
  _LinkedAccountsViewState createState() => _LinkedAccountsViewState();
}

class _LinkedAccountsViewState extends State<LinkedAccountsView> {
  LinkedAccountsViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<LinkedAccountsViewModel>(context);
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      bottomNavigationBar: Container(
        height: 100.0 + MediaQuery.of(context).padding.bottom,
        margin: EdgeInsets.symmetric(horizontal: 20.0),
        alignment: Alignment.center,
        child: GradientButton(
          height: 45.0,
          btnText: S.of(context).linkAccount,
          onClick: () {
            mViewModel.showAccountDialog();
          },
        ),
      ),
      body: Stack(
        children: [
          GAppbar(
            title: S.of(context).linkedAccounts,
            height: 153.0,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 150.0),
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(10.0, 0.0))
                ]),
            child: Container(
              child: mViewModel.isEmptyList
                  ? Center(
                child: Text(
                  "You have not linked any accounts",
                  style: CommonStyle.getAppFont(
                      fontWeight: FontWeight.w500,
                      fontSize: 16.0,
                      color: CommonColors.getHintTextStyle(context)),
                ),
              )
                  : ListView.separated(
                      itemCount: mViewModel.accountList.length,
                      shrinkWrap: true,
                      padding: EdgeInsets.all(0.0),
                      separatorBuilder: (context, index) {
                        return Container(
                          margin: EdgeInsets.symmetric(vertical: 10.0),
                          width: MediaQuery.of(context).size.width,
                          height: 1.0,
                          color: CommonColors.getListDeviderColor(context),
                        );
                      },
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () async {
                            if(mViewModel.accountList[index].loginType == AppConstants.LOGIN_TYPE_TWITTER){
                              await launch("https://twitter.com/"+mViewModel.accountList[index].twitterUserName);
                            }
                          },
                          child: getLinkedAccountItem(mViewModel.accountList[index]),
                        );
                      }),
            ),
          )
        ],
      ),
    );
  }

  Widget getLinkedAccountItem(LinkedAccountDetails linkedAccountDetails) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircleAvatar(
            backgroundImage: NetworkImage(linkedAccountDetails.profilePic),
          ),
          SizedBox(
            width: 10.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                linkedAccountDetails.username,
                style: CommonStyle.getAppFont(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500,
                    color: CommonColors.getWhiteTextColor(context)),
              ),
              SizedBox(
                height: 5.0,
              ),
              Text(
                linkedAccountDetails.email.isEmpty
                    ? "NA"
                    : linkedAccountDetails.email,
                style: CommonStyle.getAppFont(
                    fontSize: 16.0,
                    fontWeight: FontWeight.normal,
                    color:
                        CommonColors.getLightWhiteTextColor(context)),
              )
            ],
          )
        ],
      ),
    );
  }
}

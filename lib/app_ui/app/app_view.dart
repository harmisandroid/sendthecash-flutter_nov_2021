import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/add_bank_account/add_bank_account_view_model.dart';
import 'package:sendcash_flutter/app_ui/add_credit_card/add_credit_card_view_model.dart';
import 'package:sendcash_flutter/app_ui/add_funds/add_funds_view_model.dart';
import 'package:sendcash_flutter/app_ui/bank_accounts/bank_accounts_view_model.dart';
import 'package:sendcash_flutter/app_ui/change_password/change_password_view_model.dart';
import 'package:sendcash_flutter/app_ui/currency_rates/currency_rates_view_model.dart';
import 'package:sendcash_flutter/app_ui/edit_profile/edit_profile_view_model.dart';
import 'package:sendcash_flutter/app_ui/forgot_password/forgot_password_view_model.dart';
import 'package:sendcash_flutter/app_ui/home/home_view_model.dart';
import 'package:sendcash_flutter/app_ui/linked_accounts/linked_accounts_view_model.dart';
import 'package:sendcash_flutter/app_ui/my_qr/qr_view_model.dart';
import 'package:sendcash_flutter/app_ui/otp/otp_view_model.dart';
import 'package:sendcash_flutter/app_ui/profile/profile_view_model.dart';
import 'package:sendcash_flutter/app_ui/send_view/send_view_model.dart';
import 'package:sendcash_flutter/app_ui/setting/setting_view_model.dart';
import 'package:sendcash_flutter/app_ui/side_menu/side_menu_view_model.dart';
import 'package:sendcash_flutter/app_ui/splash/splash_view.dart';
import 'package:sendcash_flutter/app_ui/splash/splash_view_model.dart';
import 'package:sendcash_flutter/app_ui/support/support_view_model.dart';
import 'package:sendcash_flutter/app_ui/transaction/transaction_view_model.dart';
import 'package:sendcash_flutter/app_ui/transfer_funds/transfer_funds_view_model.dart';
import 'package:sendcash_flutter/app_ui/upload_documents/upload_documents_view_model.dart';
import 'package:sendcash_flutter/app_ui/wallet/wallet_view_model.dart';
import 'package:sendcash_flutter/app_ui/welcome/user_view_model.dart';
import 'package:sendcash_flutter/app_ui/withdraw/withdraw_view_model.dart';
import 'package:sendcash_flutter/common_widgets/bottomnavbar/bottom_nav_view_model.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'app_model.dart';

class AppView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppViewState();
  }
}

final GlobalKey<NavigatorState> mainNavigatorKey = GlobalKey<NavigatorState>();
final routeObserver = RouteObserver<PageRoute>();

class AppViewState extends State<AppView> /*with WidgetsBindingObserver*/ {
  static BuildContext appContext;
  final _app = AppModel();
  AppPreferences appPreferences = new AppPreferences();
  GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  StreamSubscription<ConnectivityResult> subscription;

  var isLaunch = true;

  final darkThemeData = ThemeData(
      fontFamily: AppConstants.FONT_FAMILY,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      scaffoldBackgroundColor: DarkThemeColors.scaffoldWhiteBack,
      shadowColor: DarkThemeColors.shadowColor,
      primaryColor: CommonColors.primaryColor,
      inputDecorationTheme: InputDecorationTheme(
          border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 1.0))),
      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        backgroundColor: DarkThemeColors.bottomNavColor,
      ),
      backgroundColor: DarkThemeColors.backgroundColor);

  final lightThemeData = ThemeData(
    fontFamily: AppConstants.FONT_FAMILY,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    scaffoldBackgroundColor: CommonColors.scaffoldWhiteBack,
    shadowColor: Colors.white,
    primaryColor: CommonColors.primaryColor,

    backgroundColor: CommonColors.backgroundColor,
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: CommonColors.bottomNavColor,
    ),
    inputDecorationTheme: InputDecorationTheme(
        border: OutlineInputBorder(
            borderSide:
                BorderSide(color: CommonColors.fieldBorder, width: 1.0))),
  );

  @override
  void initState() {
    super.initState();
   registerConnectivity();
   Future.delayed(Duration.zero,(){
     _app.getExhangeRates();
   });
  }

  void registerConnectivity(){
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (!isLaunch) {
        if (result == ConnectivityResult.wifi ||
            result == ConnectivityResult.mobile) {

          Navigator.pop(mainNavigatorKey.currentContext);
        } else {
          // Navigator.push(
          //   mainNavigatorKey.currentContext,
          //   MaterialPageRoute(builder: (context) => NoInternetView()),
          // );
        }
      } else {
        isLaunch = false;
        if (result == ConnectivityResult.wifi ||
            result == ConnectivityResult.mobile) {
        } else {
          // Navigator.push(
          //   mainNavigatorKey.currentContext,
          //   MaterialPageRoute(builder: (context) => NoInternetView()),
          // );
        }
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    appContext = context;
    return ChangeNotifierProvider<AppModel>.value(
      value: _app,
      child: Consumer<AppModel>(
        builder: (context, value, child) {
          value.isLoading = false;
          if (value.isLoading) {
            return Container(
              color: Theme.of(context).backgroundColor,
            );
          }
          return MultiProvider(
            providers: [
              ChangeNotifierProvider<SplashViewModel>(
                  create: (_) => SplashViewModel()),

              ChangeNotifierProvider<UserViewModel>(
                  create: (_) => UserViewModel()),
              ChangeNotifierProvider<BottomNavBarViewModel>(
                  create: (_) => BottomNavBarViewModel()),
              ChangeNotifierProvider<AddFundsViewModel>(
                  create: (_) => AddFundsViewModel()),
              ChangeNotifierProvider<TransferFundsViewModel>(
                  create: (_) => TransferFundsViewModel()),
              ChangeNotifierProvider<HomeViewModel>(
                  create: (_) => HomeViewModel()),
              ChangeNotifierProvider<SideMenuViewModel>(
                  create: (_) => SideMenuViewModel()),
              ChangeNotifierProvider<ProfileViewModel>(
                  create: (_) => ProfileViewModel()),
              ChangeNotifierProvider<UploadDocumentsViewModel>(
                  create: (_) => UploadDocumentsViewModel()),
              ChangeNotifierProvider<CurrencyRatesViewModel>(
                  create: (_) => CurrencyRatesViewModel()),
              ChangeNotifierProvider<EditProfileViewModel>(
                  create: (_) => EditProfileViewModel()),
              ChangeNotifierProvider<TransactionViewModel>(
                  create: (_) => TransactionViewModel()),
              ChangeNotifierProvider<ChangePasswordViewModel>(
                  create: (_) => ChangePasswordViewModel()),
              ChangeNotifierProvider<SettingViewModel>(
                  create: (_) => SettingViewModel()),
              ChangeNotifierProvider<AddBankAccountViewModel>(
                  create: (_) => AddBankAccountViewModel()),
              ChangeNotifierProvider<AddCreditCardViewModel>(
                  create: (_) => AddCreditCardViewModel()),
              ChangeNotifierProvider<WithdrawViewModel>(
                  create: (_) => WithdrawViewModel()),
              ChangeNotifierProvider<SupportViewModel>(
                  create: (_) => SupportViewModel()),
              ChangeNotifierProvider<LinkedAccountsViewModel>(
                  create: (_) => LinkedAccountsViewModel()),

              ChangeNotifierProvider<ForgotPasswordViewModel>(
                  create: (_) => ForgotPasswordViewModel()),
              ChangeNotifierProvider<OtpViewModel>(
                  create: (_) => OtpViewModel()),
              ChangeNotifierProvider<MyQRCodeViewModel>(
                  create: (_) => MyQRCodeViewModel()),
              ChangeNotifierProvider<SendViewModel>(
                  create: (_) => SendViewModel()),
              ChangeNotifierProvider<WalletViewModel>(
                  create: (_) => WalletViewModel()),
              ChangeNotifierProvider<BankAccountsViewModel>(
                  create: (_) => BankAccountsViewModel()),
            ],
            child: MaterialApp(
              builder: (context, child) {
                return MediaQuery(
                  child: child,
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
                );
              },
              navigatorKey: mainNavigatorKey,
              navigatorObservers: [routeObserver],
              debugShowCheckedModeBanner: false,
              color: CommonColors.primaryColor,
              locale: new Locale(Provider.of<AppModel>(context).locale, ""),
              localizationsDelegates: [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
              localeListResolutionCallback: S.delegate.listResolution(
                  fallback: const Locale(AppConstants.LANGUAGE_ENGLISH, '')),

              theme: lightThemeData,
              darkTheme: darkThemeData,
              themeMode: Provider.of<AppModel>(context, listen: false).darkTheme
                  ? ThemeMode.dark
                  : ThemeMode.light,
              home: SplashView(),
            ),
          );
        },
      ),
    );
  }
}

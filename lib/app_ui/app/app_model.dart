import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/globals.dart';


class AppModel with ChangeNotifier {
  Map<String, dynamic> appConfig;
  bool isLoading = true;
  String message;
  bool darkTheme = false;
  String locale = AppConstants.LANGUAGE_ENGLISH;
  String localeEngligsh = AppConstants.LANGUAGE_ENGLISH;
  static var scaffoldKey;
  LoginDetails loginDetails;
  AppPreferences appPreferences = new AppPreferences();
  ApiServices apiServices  = ApiServices();
  // Services _services = new Services();

  /* common lists used in app*/

  Map<String, String> data = Map();

  void changeLanguage() async {
    String locale = await appPreferences.getLanguageCode();
    if (CommonUtils.isEmpty(locale)) {
      appPreferences.setLanguageCode(this.locale);
      locale = this.locale;
    }
    this.locale = locale;
    notifyListeners();
  }

  void updateTheme(bool theme) {
    darkTheme = theme;
    notifyListeners();
  }

  void applyChanges() {
    notifyListeners();
  }

  Future<void> getExhangeRates() async {
    final response = await apiServices.getExhangeRates(onFailed: () {
      CommonUtils.showRedToastMessage("Failed to get data");
    });

    if (response != null) {
      Map<String, dynamic> mapResponse = jsonDecode(response) as Map;
      Globals.rateMap = mapResponse;
    }else{
      CommonUtils.showRedToastMessage("Failed to get data");
    }
  }


}

class App {
  Map<String, dynamic> appConfig;
  App(this.appConfig);
}

import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_picker/country_picker.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import 'package:local_auth/local_auth.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/app/app_model.dart';
import 'package:sendcash_flutter/app_ui/otp/otp_view.dart';
import 'package:sendcash_flutter/app_ui/upload_documents/upload_documents_view.dart';
import 'package:sendcash_flutter/common_widgets/bottomnavbar/bottom_navigation_view.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/params/create_account.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/services/wyre_params.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:http/http.dart' as http;
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:twitter_login/entity/auth_result.dart';
import 'package:twitter_login/twitter_login.dart';

class UserViewModel with ChangeNotifier {
  AppPreferences appPreferences = new AppPreferences();
  LoginDetails loginDetails;
  BuildContext mContext;
  final facebookLogin = FacebookLogin();
  final LocalAuthentication localAuth = LocalAuthentication();
  bool canCheckBiometrics = false;
  List<BiometricType> _availableBiometrics;
  String _authorized = 'Not Authorized';
  bool _isAuthenticating = false;
  final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  String deviceId = "";
  bool isEmailExists = false, isMobileExists = false;
  Country selectedCountry,selectedPhoneCountry;
  String phoneCode = "+1";
  bool isSwitched = false;
  ApiServices apiServices = ApiServices();
  final firebaseAuth = FirebaseAuth.instance;
  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();
  String selectedDay, selectedMonth, selectedYear;
  String mScreenName;
  DateTime selectedBirthdate;

  void attachContext(BuildContext context, String screenName) {
    mContext = context;
    isEmailExists = false;
    isMobileExists = false;
    mScreenName = screenName;
    selectedBirthdate = null;
    if (selectedCountry == null) {
      phoneCode = "+1";
      selectedCountry = Country(
          phoneCode: "+1",
          countryCode: "US",
          e164Sc: 1223,
          geographic: true,
          level: 1524,
          name: "United States",
          example: "",
          displayName: "USA",
          displayNameNoCountryCode: "No",
          e164Key: "");
      selectedPhoneCountry = selectedCountry;
    }
    biometricAuth();
    readDeviceInfo();
  }

  /************************  FINGERPRINT LOGIN METHODS *****************************/

  Future<void> biometricAuth() async {
    canCheckBiometrics = await localAuth.canCheckBiometrics;
    notifyListeners();
    if (canCheckBiometrics) {
      _getAvailableBiometrics();
    } else {
      printf("bio matric not available");
    }
  }

  Future<void> _getAvailableBiometrics() async {
    List<BiometricType> availableBiometrics = [];
    try {
      availableBiometrics = await localAuth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      availableBiometrics = <BiometricType>[];
      printf(e);
    }
    _availableBiometrics = availableBiometrics;
    notifyListeners();
  }

  Future<void> authenticateWithBiometrics() async {
    bool authenticated = false;
    try {
      authenticated = await localAuth.authenticate(
          localizedReason:
              'Scan your fingerprint (or face or whatever) to authenticate',
          useErrorDialogs: true,
          sensitiveTransaction: true,
          stickyAuth: false,
          biometricOnly: true);
      if (authenticated) {
        String firebaseId = await appPreferences.getUserFirebaseId();
        if (firebaseId != null) {
          CommonUtils.showProgressDialog(mContext);
          getUserDetails(
              userId: firebaseId, isRegister: false, isSocial: false);
        } else {
          CommonUtils.showRedToastMessage(
              S.of(mContext).pleaseLoginOneWithEmailPassword);
        }
      }
    } on PlatformException catch (e) {
      printf(e);
      return;
    }

    final String message = authenticated ? 'Authorized' : 'Not Authorized';
    _authorized = message;
  }

  /********************* USER SOCIAL LOGIN **********************/

  Future<void> googleLogin() async {
    GoogleSignIn _googleSignIn = GoogleSignIn(
      scopes: ['email'],
    );
    try {
      GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
      if (googleSignInAccount != null) {
        CommonUtils.showProgressDialog(mContext);
        checkSocialUserAlreadyExists(
            firstName: googleSignInAccount.displayName.split(" ")[0],
            lastName: googleSignInAccount.displayName.split(" ")[1],
            loginType: AppConstants.LOGIN_TYPE_GOOGLE,
            email: googleSignInAccount.email,
            profilePic: googleSignInAccount.photoUrl,
            socialId: googleSignInAccount.id);
        _googleSignIn.signOut();
      } else {
        CommonUtils.showRedToastMessage("google login failed");
      }
    } catch (error) {
      CommonUtils.showRedToastMessage("google login failed" + error.toString());
    }
  }

  Future<void> twitterLogin() async {
    final twitterLogin = TwitterLogin(
      apiKey: AppConstants.TWITTER_API_KEY,
      apiSecretKey: AppConstants.TWITTER_API_SECRET,
      redirectURI: AppConstants.TWITTER_CALLBACK_URL,
    );

    AuthResult authResult = await twitterLogin.loginV2();
    switch (authResult.status) {
      case TwitterLoginStatus.loggedIn:
        CommonUtils.showProgressDialog(mContext);
        checkSocialUserAlreadyExists(
            email: "",
            socialId: authResult.user.id.toString(),
            firstName: authResult.user.name,
            lastName: authResult.user.screenName,
            profilePic: authResult.user.thumbnailImage,
            loginType: AppConstants.LOGIN_TYPE_TWITTER);
        break;
      case TwitterLoginStatus.cancelledByUser:
        // cancel
        break;
      case TwitterLoginStatus.error:
        // error
        printf("Twitter  Error => " + authResult.errorMessage);
        break;
    }
  }

  Future<void> fbLogin() async {
    final result = await facebookLogin.logIn(['email', 'public_profile']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        getProfileData(result.accessToken.token);
        break;
      case FacebookLoginStatus.cancelledByUser:
        CommonUtils.showRedToastMessage("Login cancelled ");
        break;
      case FacebookLoginStatus.error:
        CommonUtils.showRedToastMessage("Login failed " + result.errorMessage);
        break;
    }
  }

  void getProfileData(String token) async {
    CommonUtils.showProgressDialog(mContext);
    var url = Uri.parse(
        'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${token}');
    final graphResponse = await http.get(url);
    CommonUtils.hideProgressDialog(mContext);
    final profile = jsonDecode(graphResponse.body);
    printf("Facebook data " + profile['email']);
    String profilePic = "http://graph.facebook.com/" +
        profile['id'] +
        "/picture?type=large&redirect=true&width=500&height=500";
    CommonUtils.showProgressDialog(mContext);
    checkSocialUserAlreadyExists(
        email: profile['email'],
        firstName: profile['first_name'],
        lastName: profile['last_name'],
        profilePic: profilePic,
        socialId: profile['id'].toString(),
        loginType: AppConstants.LOGIN_TYPE_FACEBOOK);
    facebookLogin.logOut();
  }

  /************* CHECK USER EXISTS *******************/

  void checkSocialUserAlreadyExists(
      {firstName, lastName, email, profilePic, loginType, socialId}) async {
    Map<String, dynamic> userData = Map();
    userData[DbKey.FIRST_NAME] = firstName;
    userData[DbKey.LAST_NAME] = lastName;
    userData[DbKey.USERNAME] = "";
    userData[DbKey.USER_SOCIAL_ID] = socialId ?? "";
    userData[DbKey.EMAIL] = email;
    userData[DbKey.PHONE_CODE] = "";
    userData[DbKey.MOBILE_NO] = "";
    userData[DbKey.PASSWORD] = "";
    userData[DbKey.BIRTHDATE] = "";
    userData[DbKey.IS_EMAIL_VERIFIED] = true;
    userData[DbKey.IS_PHONE_VERIFIED] = false;
    userData[DbKey.SSN] = "";
    userData[DbKey.PROFILE_PIC] = profilePic;
    userData[DbKey.LOGIN_TYPE] = loginType;
    userData[DbKey.ADDRESS] = {
      DbKey.ADDRESS_LINE_1: "",
      DbKey.ADDRESS_LINE_2: "",
      DbKey.CITY: "",
      DbKey.STATE: "",
      DbKey.ZIPCODE: "",
      DbKey.COUNTRY: "",
    };

    userData[DbKey.USER_APP_SETTINGS] = {
      DbKey.IS_NOTIFICATION_ON: true,
      DbKey.IS_DARK_THEME: false,
      DbKey.DEFAULT_CURRENCY: "USD",
      DbKey.CURRENCY_SYMBOL: "\$"
    };

    await firebaseDatabaseHelper.chedkUserExists(
        onError: (error, stacktrace) {
          Fluttertoast.showToast(
              msg: "failed to get userdetails " + error.toString());
        },
        onSuccess: (querysnapshot) async {
          if (querysnapshot.size > 0) {
            getUserDetails(
                userId: querysnapshot.docs[0].id,
                isRegister: false,
                isSocial: true,
                userData: userData);
          } else {
            registerSocialUserToFirebase(userData, true);
          }
        },
        userdata: userData);
  }

  void registerSocialUserToFirebase(
      Map<String, dynamic> userdata, bool isSocial) async {
    DateTime currentDateTime = DateTime.now();
    DateFormat dateFormat = DateFormat("dd/MM/yyyy HH:mm:ss");
    DateFormat passwordDateFormat = DateFormat("dd/MM/yyyy HH:mm:ss");
    DateFormat birthDateFormat = DateFormat("yyyy-MM-dd");
    userdata[DbKey.CREATED_AT] = dateFormat.format(currentDateTime);
    userdata[DbKey.UPDATED_AT] = dateFormat.format(currentDateTime);
    userdata[DbKey.CHANGE_PASSWORD_DATE] = passwordDateFormat.format(currentDateTime);
    if(selectedBirthdate != null){
      userdata[DbKey.BIRTHDATE] = birthDateFormat.format(selectedBirthdate);
    }

    await firebaseDatabaseHelper.createUser(
        userdata: userdata,
        onSuccess: (documentReference) {
          getUserDetails(
              userId: documentReference.id,
              isRegister: true,
              isSocial: isSocial,
              userData: userdata);
        },
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          Fluttertoast.showToast(
              msg: "Failed to register user " + error.toString());
        });
  }

  /********************** GET USER DETAILS  *****************/

  void getUserDetails(
      {userId,
      bool isRegister,
      bool isSocial,
      Map<String, dynamic> userData}) async {
    await firebaseDatabaseHelper.getUserDetails(
        userId: userId,
        onSuccess: (documentReference) async {
          await firebaseDatabaseHelper.updateUserIdToDevice(
              userId: userId,
              onError: (error, stacktrace) {
                CommonUtils.hideProgressDialog(mContext);
                Fluttertoast.showToast(
                    msg: "Failed to register user id  to device " +
                        error.toString());
              },
              onSuccess: (querysnapshot) async {
                Map<String, dynamic> data =
                    documentReference.data() as Map<String, dynamic>;
                loginDetails = LoginDetails.fromJson(data);
                await appPreferences.setLoginDetails(jsonEncode(data));
                await appPreferences.setUserFirebaseId(userId);
                // add social accounts to firebase
                if (isSocial) {
                  await addToLinkedAccount(mapData: {
                    DbKey.USERNAME:
                        loginDetails.firstName + " " + loginDetails.lastName,
                    DbKey.EMAIL: (loginDetails.email == null ||
                            loginDetails.email.isEmpty)
                        ? ""
                        : loginDetails.email,
                    DbKey.USER_SOCIAL_ID: loginDetails.socialId,
                    DbKey.PROFILE_PIC: loginDetails.profilePic,
                    DbKey.LOGIN_TYPE: loginDetails.loginType,
                    DbKey.USER_ID: userId,
                  });
                }

                if (isRegister ||
                    (loginDetails.walletId == null ||
                        loginDetails.walletId.isEmpty)) {
                  await createWalletApi(userId: userId);
                } else {
                  await getWalletDetails(walletId: loginDetails.walletId);
                }

                if (isRegister ||
                    (loginDetails.sendwyreAccountId == null ||
                        loginDetails.sendwyreAccountId.isEmpty)) {
                  if (userData == null) {
                    userData = loginDetails.toJson();
                  }

                  createSendwyreAccount(
                      email: userData[DbKey.EMAIL],
                      phone: userData[DbKey.MOBILE_NO],
                      dateOfBirth: userData[DbKey.BIRTHDATE],
                      name: userData[DbKey.FIRST_NAME] +
                          " " +
                          userData[DbKey.LAST_NAME],
                      postalCode: userData[DbKey.ADDRESS][DbKey.ZIPCODE],
                      street1: userData[DbKey.ADDRESS][DbKey.ADDRESS_LINE_1],
                      street2: userData[DbKey.ADDRESS][DbKey.ADDRESS_LINE_2],
                      state: userData[DbKey.ADDRESS][DbKey.STATE],
                      city: userData[DbKey.ADDRESS][DbKey.CITY],
                      ssn: userData[DbKey.SSN],
                      phoneCode: userData[DbKey.PHONE_CODE],
                      userId: userId);
                } else {
                  getSendwyreAccountDetails(
                      accountId: loginDetails.sendwyreAccountId);
                }
              },
              deviceId: deviceId);
        },
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          Fluttertoast.showToast(
              msg: "Failed to get user details " + error.toString());
        });
  }

  Future<void> readDeviceInfo() async {
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidDeviceInfo =
            await deviceInfoPlugin.androidInfo;
        deviceId = androidDeviceInfo.id;
      } else if (Platform.isIOS) {
        IosDeviceInfo iosDeviceInfo = await deviceInfoPlugin.iosInfo;
        deviceId = iosDeviceInfo.identifierForVendor;
      }
    } on PlatformException {
      Fluttertoast.showToast(msg: "Failed to get device data");
    }
  }

  Future<void> storeIsKeepLogin(bool value) async {
    isSwitched = value;
    await appPreferences.setKeepLoggedIn(true);
    notifyListeners();
  }

  void navigateUser() {
    if (loginDetails.userAppSettings != null) {
      if (loginDetails.userAppSettings.isDarkTheme != null) {
        Provider.of<AppModel>(mContext, listen: false)
            .updateTheme(loginDetails.userAppSettings.isDarkTheme);
      }
      if (loginDetails.userAppSettings.defaultCurrency != null &&
          loginDetails.userAppSettings.defaultCurrency.isNotEmpty) {
        Globals.currencySymbol = loginDetails.userAppSettings.currencySymbol;
        Globals.currencyName = loginDetails.userAppSettings.defaultCurrency;
      }
    }
    if (loginDetails.loginType == AppConstants.LOGIN_TYPE_NORMAL &&
        loginDetails.isPhoneVerified != null &&
        !loginDetails.isPhoneVerified) {
      // first verify phone number
      String lMobileNumber = loginDetails.mobileNo;
      String lPhoecode = loginDetails.phoneCode;
      sendOtp(
        mobileNumber: lMobileNumber,
        phoneCode: lPhoecode
      );
    } else if (loginDetails.profilePic == null ||
        loginDetails.profilePic.isEmpty ||
        loginDetails.frontIdImage == null ||
        loginDetails.frontIdImage.isEmpty ||
        loginDetails.backIdImage == null ||
        loginDetails.backIdImage.isEmpty) {
      Navigator.pushAndRemoveUntil(mContext,
          CupertinoPageRoute(builder: (context) {
        return UploadDocumentsView(
          from: AppConstants.SCREEN_LOGIN,
        );
      }), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(mContext,
          CupertinoPageRoute(builder: (context) {
        return BottomNavigationView();
      }), (route) => false);
    }
  }

  /*------------------ CREATE ,UPDATE, GET WALLET DETAILS APIS ----------------------------*/

  Future<void> createWalletApi({String userId}) async {
    String response = await apiServices.createWallet(
        params: getWalletParams(userId),
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("Failed to create wallet");
        });
    if (response != null) {
      Map<String, dynamic> mapJson = jsonDecode(response) as Map;
      if(mapJson['id'] != null){
        loginDetails.walletId = mapJson["id"];
        await appPreferences.setLoginDetails(jsonEncode(loginDetails));
        await appPreferences.setWalletDetails(response);
        updateWalletIdToFirebase(
            userId: userId, mapParams: {DbKey.WALLET_ID: mapJson["id"]});
      }

    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showRedToastMessage("failed to create wallet");
    }
  }

  String getWalletParams(name) {
    Map<String, dynamic> mapParams = Map();
    mapParams[WyreParams.NAME] = name;
    mapParams[WyreParams.CALLBACK_URL] = "https://sendthecash.com";
    mapParams[WyreParams.WALLET_TYPE] = "DEFAULT";
    mapParams[WyreParams.NOTES] = " Wallet creation for id ${name}";
    printf("params => " + jsonEncode(mapParams));
    return jsonEncode(mapParams);
  }

  Future<void> updateWalletIdToFirebase({
    Map<String, dynamic> mapParams,
    String userId,
  }) async {
    await firebaseDatabaseHelper.updateUserProfile(
        userId: userId,
        onSuccess: (value) {

        },
        onError: (error, stackTrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("Failed to update wallet id");
        },
        userdata: mapParams);
  }

  Future<void> getWalletDetails({String walletId}) async {
    String response = await apiServices.getWalletDetails(
        walletId: walletId,
        onFailed: () {
          CommonUtils.showRedToastMessage("Unable to get response");
        });
    if (response != null) {
      await appPreferences.setWalletDetails(response);
    } else {
      CommonUtils.showRedToastMessage("Failed to get wallet details");
    }
  }

  /********************** CREATE ,UPDATE, GETSENDWYRE ACCOUNT DETAILS APIS ********************/

  Future<void> createSendwyreAccount(
      {String email,
      String phone,
      String phoneCode,
      String dateOfBirth,
      String name,
      String street1,
      String street2,
      String city,
      String state,
      String postalCode,
      String ssn,
      String userId}) async {
    String response = await apiServices.createAccount(
        onFailed: () {
          CommonUtils.showRedToastMessage("failed to create account");
          CommonUtils.hideProgressDialog(mContext);
        },
        params: createAccountParams(
            email: email,
            phone: phone,
            phoneCode: phoneCode,
            dateOfBirth: dateOfBirth,
            name: name,
            street1: street1,
            street2: street2,
            city: city,
            state: state,
            postalCode: postalCode,
            ssn: ssn));

    if (response != null) {
      printf("Response => " + response);
      await appPreferences.setAccountDetails(response);
      var mapresponse = jsonDecode(response);
      if (mapresponse != null && mapresponse['id'] != null) {
        await firebaseDatabaseHelper.updateSendwyreAccountId(
            userId: userId,
            accountId: mapresponse['id'],
            onSuccess: (value) {
              CommonUtils.hideProgressDialog(mContext);
              CommonUtils.showGreenToastMessage(
                  "Account created successfully!");
              navigateUser();
            },
            onError: (error, stackTrace) {
              // CommonUtils.showRedToastMessage(
              //     "Create Account => " + error.toString());
            });
      } else {
        CommonUtils.showRedToastMessage("Failed to create account");
      }
    } else {
      CommonUtils.showRedToastMessage("failed to create account");
    }
  }

  String createAccountParams(
      {String email,
      String phone,
      String phoneCode,
      String dateOfBirth,
      String name,
      String street1,
      String street2,
      String city,
      String state,
      String postalCode,
      String ssn}) {
    CreateAccountParams createAccountParams = CreateAccountParams();
    createAccountParams.type = "INDIVIDUAL";
    createAccountParams.country = "US";
    createAccountParams.subaccount = true;
    createAccountParams.referrerAccountId = "AC_L2LGFRVWYPZ";
    createAccountParams.disableEmail = false;
    List<ProfileFields> fieldList = [];
    if (email != null && email.isNotEmpty) {
      ProfileFields emailField =
          ProfileFields(fieldId: "individualEmail", value: email);
      fieldList.add(emailField);
    }
    if (dateOfBirth != null && dateOfBirth.isNotEmpty) {
      ProfileFields dobField =
          ProfileFields(fieldId: "individualDateOfBirth", value: dateOfBirth);
      fieldList.add(dobField);
    }
    if (phone != null && phone.isNotEmpty) {
      ProfileFields phoneField = ProfileFields(
          fieldId: "individualCellphoneNumber", value: "+" + phoneCode + phone);
      fieldList.add(phoneField);
    }
    if (name != null && name.isNotEmpty) {
      ProfileFields nameField =
          ProfileFields(fieldId: "individualLegalName", value: name);
      fieldList.add(nameField);
    }
    if (ssn != null && ssn.isNotEmpty) {
      ProfileFields ssnField =
          ProfileFields(fieldId: "individualSsn", value: ssn);
      fieldList.add(ssnField);
    }
    if (street1 != null && street1.isNotEmpty) {
      ProfileFields addressField =
          ProfileFields(fieldId: "individualResidenceAddress", value: {
        "street1": street1,
        "street2": street2,
        "city": city,
        "state": state,
        "postalCode": postalCode,
        "country": "US",
      });
      fieldList.add(addressField);
    }
    createAccountParams.profileFields = fieldList;
    printf(jsonEncode(createAccountParams));
    return jsonEncode(createAccountParams);
  }

  Future<void> getSendwyreAccountDetails({String accountId}) async {
    final response = await apiServices.getSendwyreAccountDetails(
        accountId: accountId,
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("failed to get account details");
        });
    CommonUtils.hideProgressDialog(mContext);
    if (response != null) {
      await appPreferences.setAccountDetails(response);
      navigateUser();
    } else {
      CommonUtils.showRedToastMessage("failed to get account details");
    }
  }

  /* LINK USER SOCIAL ACCOUNT*/

  void addToLinkedAccount({Map<String, dynamic> mapData}) async {
    // CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.addLinkedAccounts(
        data: mapData,
        alreadyExists: () {
          //       CommonUtils.hideProgressDialog(mContext);
        },
        onSuccess: (documentReference) {
          //     CommonUtils.hideProgressDialog(mContext);
        },
        onError: (object, tracktrace) {
          //    CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("Failed to add  linked accounts");
        });
  }

  /******************  NORMAL SIGNUP FUNCTION  ************************/

  void showDatpickerDialog() async {
    await showDatePicker(
      context: mContext,
      initialDate: DateTime.now(),
      firstDate: DateTime(1950),
      lastDate: DateTime.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: CommonColors.gradientColorBottom,
            accentColor: CommonColors.gradientColorBottom,
            colorScheme: ColorScheme.light(primary: CommonColors.primaryColor),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
    ).then((value) {
      final birthday = value;
      final date2 = DateTime.now();
      final difference = date2.difference(birthday).inDays;
      if(difference > (365 * 18)){
      selectedDay = value.day.toString();
      selectedYear = value.year.toString();
      selectedMonth = value.month.toString();
      selectedBirthdate = value;
      notifyListeners();
      }else{
        CommonUtils.showRedToastMessage("Your age must be above 18 to signup.");
      }
    });
  }

  Future<void> showPhoneCodePickerDialog() async {
    await showCountryPicker(
      context: mContext,
      showPhoneCode: true,
      onSelect: (Country value) {
        selectedCountry = value;
        selectedPhoneCountry = value;
        phoneCode = "+" + value.phoneCode;
        notifyListeners();
      },
    );
  }

  Future<void> showCountryPickerDialog() async {
    await showCountryPicker(
      context: mContext,
      showPhoneCode: true,
      onSelect: (Country value) {
        selectedCountry = value;
        notifyListeners();
      },
    );
  }

  Future<void> checkPhoneAlreadyExists({String mobileNumber}) async {
    await firebaseDatabaseHelper.checkMobileNumberExists(
        onError: (error, stacktrace) {
          Fluttertoast.showToast(
              msg: "failed to get userdetails " + error.toString());
        },
        onSuccess: (querysnapshot) async {
          if (querysnapshot.size > 0) {
            isMobileExists = true;
            CommonUtils.showRedToastMessage(
                S.of(mContext).mobileNumberAlreadyExists);
            notifyListeners();
          } else {
            isMobileExists = false;
            notifyListeners();
          }
        },
        mobileNumber: mobileNumber);
  }

  void checkEmailExists({email}) async {
    await firebaseDatabaseHelper.checkEmailExists(
        onError: (error, stacktrace) {
          Fluttertoast.showToast(
              msg: "failed to get userdetails " + error.toString());
        },
        onSuccess: (querysnapshot) async {
          if (querysnapshot.size > 0) {
            isEmailExists = true;
            CommonUtils.showRedToastMessage(S.of(mContext).emailAlreadyExists);
          } else {
            isEmailExists = false;
          }
          notifyListeners();
        },
        emailAddress: email);
  }

  void normalRegisterUserApi({Map<String, dynamic> userData}) async {
    DateTime currentDateTime = DateTime.now();
    DateFormat dateFormat = DateFormat("dd/MM/yyyy HH:mm:ss");
    DateFormat passwordDateFormat = DateFormat("dd/MM/yyyy HH:mm:ss");
    userData[DbKey.CREATED_AT] = dateFormat.format(currentDateTime);
    userData[DbKey.UPDATED_AT] = dateFormat.format(currentDateTime);
    userData[DbKey.CHANGE_PASSWORD_DATE] =
        passwordDateFormat.format(currentDateTime);
    CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.chedkUserExists(
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage(
              "Normal Reister => " + error.toString());
        },
        onSuccess: (querysnapshot) async {
          if (querysnapshot.size > 0) {
            CommonUtils.hideProgressDialog(mContext);
            CommonUtils.showRedToastMessage(isEmailExists
                ? S.of(mContext).userExistsWithEmail
                : S.of(mContext).userExistsWithPhone);
          } else {
            registerSocialUserToFirebase(userData, false);
          }
        },
        userdata: userData);
  }

  /************************************* NORMAL LOGIN APIS *******************************/

  void loginUsingEmailPassword({String email, String password}) async {
    CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.loginUser(
        email: email,
        password: password,
        onSuccess: (QuerySnapshot querySnapshot) async {
          if (querySnapshot.size > 0) {
            await firebaseDatabaseHelper.updateUserIdToDevice(
                userId: querySnapshot.docs[0].id,
                deviceId: deviceId,
                onError: (error, stackTrace) {
                  CommonUtils.showProgressDialog(mContext);
                  CommonUtils.showRedToastMessage(
                      "Failed to login " + error.toString());
                },
                onSuccess: (_) async {
                  await appPreferences
                      .setUserFirebaseId(querySnapshot.docs[0].id);
                  Map<String, dynamic> mapData =
                      querySnapshot.docs[0].data() as Map<String, dynamic>;
                  await appPreferences.setLoginDetails(jsonEncode(mapData));
                  loginDetails = LoginDetails.fromJson(mapData);

                  if (loginDetails.walletId == null ||
                      loginDetails.walletId.isEmpty) {
                    await createWalletApi(userId: querySnapshot.docs[0].id);
                  } else {
                    await getWalletDetails(walletId: loginDetails.walletId);
                  }
                  if (loginDetails.sendwyreAccountId == null ||
                      loginDetails.sendwyreAccountId.isEmpty) {
                    var birthDate = null;
                    if(loginDetails.birthdate != null && loginDetails.birthdate.isNotEmpty){
                      birthDate = loginDetails.birthdate.split("--")[2]+"-"+ loginDetails.birthdate.split("--")[1]+loginDetails.birthdate.split("--")[0];
                    }


                    createSendwyreAccount(
                        userId: querySnapshot.docs[0].id,
                        phoneCode: loginDetails.phoneCode,
                        ssn: loginDetails.ssn,
                        email: loginDetails.email,
                        name: loginDetails.firstName +
                            " " +
                            loginDetails.lastName,
                        street1: loginDetails.address.addressLine1,
                        street2: loginDetails.address.addressLine2,
                        city: loginDetails.address.city,
                        state: loginDetails.address.state,
                        postalCode: loginDetails.address.zipcode,
                        dateOfBirth: birthDate);
                  } else {
                    getSendwyreAccountDetails(
                        accountId: loginDetails.sendwyreAccountId);
                  }
                });
          } else {
            CommonUtils.hideProgressDialog(mContext);
            CommonUtils.showRedToastMessage(
                S.of(mContext).invalidEmailOrPassword);
          }
        },
        onError: (error, stackTrace) {
          CommonUtils.hideProgressDialog(mContext);
        });
  }

  /*---------------- OTP VERIFICATION --------------------*/

  void sendOtp({
    String mobileNumber,
    String phoneCode,
  }) async {
    CommonUtils.showProgressDialog(mContext);
    String code = getRandomCodeFourDigit();
    Map<String, dynamic> sendOtpMap = Map();
    sendOtpMap['phone_number'] = phoneCode.replaceAll("+", "") + mobileNumber;
    sendOtpMap['message'] = "Your code is ${code.toString()}";
    sendOtpMap['message_type'] = "OTP";

    String response = await apiServices.sendOtp(
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("Failed to send otp");
        },
        mapParams: sendOtpMap);
    if (response != null) {
      Map<String, dynamic> mapResponse = Map();
      mapResponse = jsonDecode(response);
      if (mapResponse['reference_id'] != null &&
          mapResponse['status']['code'] == 290) {
        insertOtpToFiresbase(
            mobileNumber: mobileNumber, phoneCode: phoneCode, otp: code);
      } else {
        CommonUtils.hideProgressDialog(mContext);
        CommonUtils.showRedToastMessage("Failed to send otp");
      }
      // insertOtpToFiresbase(
      //     mobileNumber: mobileNumber, phoneCode: phoneCode, otp: code);
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showRedToastMessage("Oops something went wrong");
    }
  }

  Future<void> insertOtpToFiresbase(
      {String mobileNumber, String phoneCode, String otp}) async {
    Map<String, dynamic> mapDynamic = Map();
    mapDynamic[DbKey.MOBILE_NO] = mobileNumber;
    mapDynamic[DbKey.PHONE_CODE] = phoneCode;
    mapDynamic[DbKey.OTP] = otp;
    String userId = await appPreferences.getUserFirebaseId();
    await firebaseDatabaseHelper.insertFirebaseOtp(
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("failed to add otp firebase");
        },
        onSuccess: () {
          CommonUtils.hideProgressDialog(mContext);
          if (mScreenName != null &&
              (mScreenName == AppConstants.SCREEN_WELCOME ||
                  mScreenName == AppConstants.SCREEN_LOGIN)) {
            CommonUtils.showGreenToastMessage(
                "You have not verified your phone number.Please verify it.");
          } else {
            CommonUtils.showGreenToastMessage("Otp send successfully!");
          }
          Navigator.push(mContext, CupertinoPageRoute(builder: (context) {
            return OTPView(
              mobileNumber: mobileNumber,
              phoneCode: phoneCode,
              loginDetails: loginDetails,
              firebaseUserId: userId,
              from: mScreenName,
            );
          }));
        },
        userdata: mapDynamic,
        onDocumentAdded: (documentRefernce) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage("Otp send successfully!");
          Navigator.push(mContext, CupertinoPageRoute(builder: (context) {
            return OTPView(
              mobileNumber: mobileNumber,
              phoneCode: phoneCode,
              loginDetails: loginDetails,
              firebaseUserId: userId,
              from: mScreenName,
            );
          }));
        });
  }
}

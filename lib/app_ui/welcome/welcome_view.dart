import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/login/login_view.dart';
import 'package:sendcash_flutter/app_ui/signup/signup_view.dart';
import 'package:sendcash_flutter/app_ui/welcome/user_view_model.dart';
import 'package:sendcash_flutter/common_widgets/social_row/social_row.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class WelcomeView extends StatefulWidget {
  final String from;

  const WelcomeView({Key key, this.from}) : super(key: key);

  @override
  _WelcomeViewState createState() => _WelcomeViewState();
}

class _WelcomeViewState extends State<WelcomeView> {
  UserViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context,"WELCOME");
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<UserViewModel>(context);
    final logoView = Container(
      alignment: Alignment.center,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 130.0),
            child: Image.asset(
              LocalImages.applogotext,
              width: 240,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            padding: EdgeInsets.only(left: 30, right: 30),
            child: Text(
              S.of(context).appSlogan,
              textAlign: TextAlign.center,
              maxLines: 3,
              style: CommonStyle.getAppFont(
                color: Colors.white,
                fontSize: 22,
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 25),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30.0)),
              color: CommonColors.shadowcolor,
            ),
            height: 3,
            width: 28,
          ),
          SizedBox(
            height: 45,
          ),
          InkWell(
            onTap: () async {
              await Navigator.push(context,
                  CupertinoPageRoute(builder: (context) {
                return LoginView();
              }));
              mViewModel.attachContext(context,"WELCOME");
            },
            child: Container(
              alignment: Alignment.center,
              height: 50,
              width: 345,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(30.0)),
                color: Colors.white,
              ),
              child: Text(S.of(context).signIn,
                  style: CommonStyle.getAppFont(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  )),
            ),
          ),
          Visibility(
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                InkWell(
                  onTap: () {
                    mViewModel.authenticateWithBiometrics();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 48,
                    width: 345,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(30.0)),
                      color: Colors.white,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          LocalImages.fingerprint,
                          height: 25,
                          width: 25,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Text(S.of(context).loginWithFinger,
                            style: CommonStyle.getAppFont(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            )),
                      ],
                    ),
                  ),
                )
              ],
            ),
            visible: mViewModel.canCheckBiometrics,
          ),
        ],
      ),
    );

    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
              CommonColors.gradientColorTop,
              CommonColors.gradientColorBottom
            ])),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).padding.top + 10.0,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Visibility(
                        child: InkWell(
                          child: Container(
                            child: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                              size: 30.0,
                            ),
                          ),
                        ),
                        visible: widget.from == null),
                  ),
                  logoView,
                  SocialRow(
                    onTextHintClicked: () async{
                      await Navigator.push(context,
                          CupertinoPageRoute(builder: (context) {
                        return SignupView();
                      }));
                      mViewModel.attachContext(context,"WELCOME");
                    },
                    onClicked: (String clickType) {
                      if (clickType == AppConstants.CLICK_TYPE_FACEBOOK) {
                        // call facebook login
                        mViewModel.fbLogin();
                      } else if (clickType == AppConstants.CLICK_TYPE_GOOGLE) {
                        // call google login
                        mViewModel.googleLogin();
                      } else {
                        // call twitter login
                        mViewModel.twitterLogin();
                      }
                    },
                  ),
                  SizedBox(
                    height: 60,
                  )
                ],
              ),
            ),
          ),
        ));
  }
}

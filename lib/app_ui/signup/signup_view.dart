import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/login/login_view.dart';
import 'package:sendcash_flutter/app_ui/welcome/user_view_model.dart';
import 'package:sendcash_flutter/common_widgets/appbar/back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/buttons/Buttons.dart';
import 'package:sendcash_flutter/common_widgets/edittext/edittext.dart';
import 'package:sendcash_flutter/common_widgets/social_row/social_row.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/country_uils.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class SignupView extends StatefulWidget {
  const SignupView({Key key}) : super(key: key);

  @override
  _SignupViewState createState() => _SignupViewState();
}

class _SignupViewState extends State<SignupView> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  TextEditingController mUsernameController = TextEditingController();
  final mFirstnameController = TextEditingController();
  final mLastnameController = TextEditingController();
  final mEmailController = TextEditingController();
  final mPhoneController = TextEditingController();
  final mStreet1Controller = TextEditingController();
  final mStreet2Controller = TextEditingController();
  final mCityController = TextEditingController();
  final mPostalController = TextEditingController();
  final mStateProvinceController = TextEditingController();
  final mSSnController = TextEditingController();
  final mPasswordController = TextEditingController();

  // UserViewModel userViewModel;
  bool isAgreed = false;
  final formKey = GlobalKey<FormState>();
  UserViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context,AppConstants.SCREEN_REGISTER);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<UserViewModel>(context);

    final checkbox = Container(
      height: 30.0,
      width: 30.0,
      padding: EdgeInsets.all(5.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: CommonColors.shadowcolor.withOpacity(.5)),
      child: isAgreed ? Image.asset(LocalImages.ic_check) : Container(),
    );

    final termsandConditions = InkWell(
      onTap: () {
        setState(() {
          isAgreed = !isAgreed;
        });
      },
      child: Container(
        height: 35.0,
        margin: EdgeInsets.only(bottom: 15, top: 15.0),
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              checkbox,
              Container(
                margin: EdgeInsets.only(left: 10.0),
                child: Text(S.of(context).termsAndCondition,
                    style: CommonStyle.getAppFont(
                        fontWeight: FontWeight.normal,
                        fontSize: 17,
                        color: Colors.white)),
              ),
            ]),
      ),
    );

    final phonenumberTextfield = Container(
      height: 48,
      margin: EdgeInsets.only(top: 8),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          color: CommonColors.shadowcolor.withOpacity(.5)),
      child: Row(
        children: [
          InkWell(
            onTap: () async {
              await mViewModel.showPhoneCodePickerDialog();
            },
            child: mViewModel.selectedPhoneCountry == null
                ? Container()
                : Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 20),
                        child: Text(
                          Utils.countryCodeToEmoji(
                              mViewModel.selectedPhoneCountry.countryCode),
                          style: TextStyle(fontSize: 22.0),
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        mViewModel.selectedPhoneCountry.phoneCode,
                        style: TextStyle(color: Colors.white),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.white,
                        size: 20,
                      )
                    ],
                  ),
          ),
          Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              height: 28,
              width: 2,
              child: VerticalDivider(color: Colors.white)),
          Flexible(
            child: Container(
              height: 50,
              width: 220,
              margin: EdgeInsets.only(left: 20),
              child: TextField(
                maxLines: 1,
                maxLength: 11,
                controller: mPhoneController,
                cursorColor: Colors.white,
                onSubmitted: (value) {
                  mViewModel.checkPhoneAlreadyExists(mobileNumber: value);
                },
                style: TextStyle(fontSize: 15, color: Colors.white),
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  counterText: "",
                  filled: true,
                  border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                  fillColor: Colors.transparent,
                  contentPadding: EdgeInsets.zero,
                  hintStyle: TextStyle(color: Colors.white, fontSize: 15),
                  hintText: S.of(context).phoneNumber,
                ),
              ),
            ),
          )
        ],
      ),
    );

    final birthdateTextfield = Container(
      child: Column(
        children: [
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsetsDirectional.only(start: 20.0, top: 10.0),
            child: Text(
              S.of(context).birthday,
              style: CommonStyle.getAppFont(fontSize: 14, color: Colors.white),
            ),
          ),
          InkWell(
            onTap: () {
              mViewModel.showDatpickerDialog();
            },
            child: Container(
              height: 48,
              margin: EdgeInsets.only(top: 8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.0),
                  color: CommonColors.shadowcolor.withOpacity(.5)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  getDropdownView(
                      text: mViewModel.selectedDay == null
                          ? "DD"
                          : mViewModel.selectedDay,
                      onClick: () {}),
                  Container(
                      height: 28,
                      width: 2,
                      child: VerticalDivider(color: Colors.white)),
                  getDropdownView(
                      text: mViewModel.selectedMonth == null
                          ? "MM"
                          : mViewModel.selectedMonth,
                      onClick: () {}),
                  Container(
                      height: 28,
                      width: 2,
                      child: VerticalDivider(color: Colors.white)),
                  getDropdownView(
                      text: mViewModel.selectedYear == null
                          ? "YYYY"
                          : mViewModel.selectedYear,
                      onClick: () {}),
                ],
              ),
            ),
          )
        ],
      ),
    );

    final countryDropdown = InkWell(
      onTap: (){
        mViewModel.showCountryPickerDialog();
      },
      child: Container(
        height: 50.0,
        padding: EdgeInsets.only(left: 20, right: 30.0),
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(top: 10.0, bottom: 8.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
            color: CommonColors.shadowcolor.withOpacity(.5)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              mViewModel.selectedCountry == null
                  ? "United States"
                  : mViewModel.selectedCountry.name,
              style: CommonStyle.getAppFont(
                  fontSize: 15,
                  color: Colors.white,
                  fontWeight: FontWeight.normal),
            ),
            Icon(
              Icons.keyboard_arrow_down_outlined,
              color: Colors.white,
            )
          ],
        ),
      ),
    );

    return GestureDetector(
      onTap: (){
      //  FocusManager.instance.primaryFocus.unfocus();
        SystemChannels.textInput.invokeMethod('TextInput.hide');
      },
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
              CommonColors.gradientColorTop,
              CommonColors.gradientColorBottom
            ])),
        child: Scaffold(
          key: scaffoldKey,
          backgroundColor: Colors.transparent,
          appBar: BackAppbar(
            title: S.of(context).signUp,
          ),
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(left: 20.0, right: 20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 40,
                  ),
                  Image.asset(
                    LocalImages.applogotext,
                    width: 240,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  EditText(
                    hint: S.of(context).userName,
                    controller: mUsernameController,
                    inputAction: TextInputAction.next,
                    inputType: TextInputType.text,
                  ),
                  EditText(
                      hint: S.of(context).firstName,
                      controller: mFirstnameController,
                      inputAction: TextInputAction.next,
                      inputType: TextInputType.text),
                  EditText(
                      hint: S.of(context).lastName,
                      controller: mLastnameController,
                      inputAction: TextInputAction.next,
                      inputType: TextInputType.text),
                  EditText(
                      hint: S.of(context).email,
                      controller: mEmailController,
                      onSubmit: (value) {
                        mViewModel.checkEmailExists(email: value);
                      },
                      inputAction: TextInputAction.next,
                      inputType: TextInputType.emailAddress),
                  phonenumberTextfield,
                  birthdateTextfield,
                  Container(
                    alignment: Alignment.topLeft,
                    // padding: EdgeInsets.only(left: 10, top: 10),
                    padding: EdgeInsetsDirectional.only(start: 20.0, top: 10.0),
                    child: Text(
                      S.of(context).address,
                      style: CommonStyle.getAppFont(
                          fontSize: 14, color: Colors.white),
                    ),
                  ),
                  EditText(
                      hint: S.of(context).street1,
                      controller: mStreet1Controller,
                      inputAction: TextInputAction.next,
                      inputType: TextInputType.text),
                  EditText(
                      hint: S.of(context).street2,
                      controller: mStreet2Controller,
                      inputAction: TextInputAction.next,
                      inputType: TextInputType.text),
                  EditText(
                      hint: S.of(context).city,
                      controller: mCityController,
                      inputAction: TextInputAction.next,
                      inputType: TextInputType.text),
                  EditText(
                      hint: S.of(context).postalCode,
                      controller: mPostalController,
                      inputAction: TextInputAction.next,
                      inputType: TextInputType.text),
                  EditText(
                      hint: S.of(context).stateProvince,
                      controller: mStateProvinceController,
                      inputAction: TextInputAction.next,
                      inputType: TextInputType.text),
                  countryDropdown,
                  EditText(
                      hint: S.of(context).ssn,
                      controller: mSSnController,
                      maxLenth: 9,
                      inputAction: TextInputAction.next,
                      inputType: TextInputType.number),
                  EditText(
                      hint: S.of(context).password,
                      controller: mPasswordController,
                      inputAction: TextInputAction.done,
                      inputType: TextInputType.text,
                      isObscure: true),
                  termsandConditions,
                  SizedBox(
                    height: 13.0,
                  ),
                  Buttons(
                    text: S.of(context).signUp,
                    isEnabled:
                        !mViewModel.isEmailExists && !mViewModel.isMobileExists,
                    onClick: () {
                      if (isValidData()) {
                        Map<String, dynamic> userData = Map();
                        userData[DbKey.FIRST_NAME] = mFirstnameController.text;
                        userData[DbKey.LAST_NAME] = mLastnameController.text;
                        userData[DbKey.USERNAME] = mUsernameController.text;
                        userData[DbKey.EMAIL] = mEmailController.text;
                        userData[DbKey.PHONE_CODE] = mViewModel.selectedPhoneCountry.phoneCode;
                        userData[DbKey.COUNTRY_CODE] =
                            mViewModel.selectedCountry.countryCode;
                        userData[DbKey.MOBILE_NO] = mPhoneController.text;
                        userData[DbKey.PASSWORD] = mPasswordController.text;
                        userData[DbKey.BIRTHDATE] = mViewModel.selectedDay +
                            "--" +
                            mViewModel.selectedMonth +
                            "--" +
                            mViewModel.selectedYear;
                        userData[DbKey.IS_EMAIL_VERIFIED] = false;
                        userData[DbKey.IS_PHONE_VERIFIED] = false;
                        userData[DbKey.SSN] = mSSnController.text;
                        userData[DbKey.PROFILE_PIC] = "";
                        userData[DbKey.LOGIN_TYPE] = "Normal";
                        userData[DbKey.EMAIL_PASSWORD] =
                            mEmailController.text + mPasswordController.text;
                        userData[DbKey.ADDRESS] = {
                          DbKey.ADDRESS_LINE_1: mStreet1Controller.text,
                          DbKey.ADDRESS_LINE_2: mStreet2Controller.text,
                          DbKey.CITY: mCityController.text,
                          DbKey.STATE: mStateProvinceController.text,
                          DbKey.ZIPCODE: mPostalController.text,
                          DbKey.COUNTRY: mViewModel.selectedCountry == null
                              ? "US"
                              : mViewModel.selectedCountry.name,
                        };

                        userData[DbKey.USER_APP_SETTINGS] = {
                          DbKey.IS_NOTIFICATION_ON: true,
                          DbKey.IS_DARK_THEME: false,
                          DbKey.DEFAULT_CURRENCY: "USD",
                          DbKey.CURRENCY_SYMBOL: "\$"
                        };
                        mViewModel.normalRegisterUserApi(userData: userData);
                      }
                    },
                  ),
                  SocialRow(
                    textHint: S.of(context).loginHint,
                    onTextHintClicked: () async {
                      await Navigator.push(context,
                          CupertinoPageRoute(builder: (context) {
                        return LoginView();
                      }));
                      mViewModel.attachContext(context,AppConstants.SCREEN_REGISTER);
                    },
                    onClicked: (String clickType) {
                      if (clickType == AppConstants.CLICK_TYPE_FACEBOOK) {
                        // call facebook login
                        mViewModel.fbLogin();
                      } else if (clickType == AppConstants.CLICK_TYPE_GOOGLE) {
                        // call google login
                        mViewModel.googleLogin();
                      } else {
                        // call twitter login
                        mViewModel.twitterLogin();
                      }
                    },
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).viewPadding.bottom + 20.0,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getDropdownView({String text, Function onClick}) {
    return Flexible(
        child: Container(
      alignment: Alignment.center,
      width: 100.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Text(
              text,
              style: CommonStyle.getAppFont(fontSize: 15, color: Colors.white),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Icon(
            Icons.keyboard_arrow_down,
            color: Colors.white,
            size: 20,
          ),
        ],
      ),
    ));
  }

  // just check validation
  bool isValidData() {
    if (mUsernameController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).userNameValiidation);
      return false;
    } else if (mFirstnameController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).firstNameValidation);
      return false;
    } else if (mLastnameController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).lastNameValidation);
      return false;
    } else if (mEmailController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).emailValidation);
      return false;
    } else if (!CommonUtils.isvalidEmail(mEmailController.text)) {
      CommonUtils.showRedToastMessage(S.of(context).validEmailValidation);
      return false;
    } else if (mPhoneController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).phoneValidation);
      return false;
    } else if (mViewModel.selectedDay == null) {
      CommonUtils.showRedToastMessage(S.of(context).birthdateValidation);
      return false;
    } else if (mStreet1Controller.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).street1Validation);
      return false;
    } else if (mStreet2Controller.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).street2Validation);
      return false;
    } else if (mPostalController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).postalCodeValidation);
      return false;
    } else if (mCityController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).cityValidation);
      return false;
    } else if (mStateProvinceController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).stateProvinceValidation);
      return false;
    } else if (mViewModel.selectedCountry == null) {
      CommonUtils.showRedToastMessage(S.of(context).countryValidation);
      return false;
    } else if (!isAgreed) {
      CommonUtils.showRedToastMessage(
          S.of(context).pleaseAcceptTermsAndConditions);
      return false;
    } else if (mViewModel.isMobileExists) {
      CommonUtils.showRedToastMessage(S.of(context).mobileNumberAlreadyExists);
      return false;
    } else if (mViewModel.isEmailExists) {
      CommonUtils.showRedToastMessage(S.of(context).emailAlreadyExists);
      return false;
    } else {
      return true;
    }
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/forgot_password/forgot_password_view.dart';
import 'package:sendcash_flutter/app_ui/signup/signup_view.dart';
import 'package:sendcash_flutter/app_ui/welcome/user_view_model.dart';
import 'package:sendcash_flutter/common_widgets/appbar/back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/buttons/Buttons.dart';
import 'package:sendcash_flutter/common_widgets/edittext/edittext.dart';
import 'package:sendcash_flutter/common_widgets/social_row/social_row.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final mEmailController = TextEditingController();
  final mPasswordController = TextEditingController();
  UserViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context,AppConstants.SCREEN_LOGIN);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<UserViewModel>(context);

    final logoView = Container(
      alignment: Alignment.center,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Image.asset(
            LocalImages.applogotext,
            width: 240,
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            padding: EdgeInsets.only(left: 30, right: 30),
            child: Text(
              S.of(context).appSlogan,
              textAlign: TextAlign.center,
              maxLines: 3,
              style: CommonStyle.getAppFont(
                color: Colors.white,
                fontSize: 22,
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 26),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30.0)),
              color: CommonColors.shadowColor,
            ),
            height: 3,
            width: 28,
          ),
        ],
      ),
    );
    final toggleSwitch = Container(
      // height: 13,
      // width: 35,
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.only(left: 30, top: 5),
      child: Row(
        children: [
          Switch(
            value: mViewModel.isSwitched,
            onChanged: (value) {
              mViewModel.storeIsKeepLogin(value);
            },
            activeTrackColor: CommonColors.shadowColor.withOpacity(.9),
            activeColor: Colors.white,
          ),
          Text(
            S.of(context).keepmeLoggedIn,
            style: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontWeight: FontWeight.normal),
          )
        ],
      ),
    );
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
            CommonColors.gradientColorTop,
            CommonColors.gradientColorBottom
          ])),
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.transparent,
        appBar: BackAppbar(
          title: S.of(context).logIn,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              children: [
                SizedBox(
                  height: 15,
                ),
                logoView,
                SizedBox(
                  height: 15,
                ),
                EditText(
                    hint: S.of(context).email,
                    controller: mEmailController,
                    inputAction: TextInputAction.next,
                    inputType: TextInputType.emailAddress),
                EditText(
                  hint: S.of(context).password,
                  controller: mPasswordController,
                  inputAction: TextInputAction.done,
                  inputType: TextInputType.text,
                  isObscure: true,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Align(
                  alignment: AlignmentDirectional.centerEnd,
                  child: InkWell(
                    onTap: () {
                      Navigator.push(context,
                          CupertinoPageRoute(builder: (context) {
                        return ForgotPasswordView();
                      }));
                    },
                    child: Text(
                      S.of(context).forgotPassword,
                      style: CommonStyle.getAppFont(
                          color: Colors.white,
                          fontSize: 15.0,
                          fontWeight: FontWeight.normal),
                    ),
                  ),
                ),
                toggleSwitch,
                Buttons(
                  text: S.of(context).signIn,
                  onClick: () {
                    if (isValid()) {
                      mViewModel.loginUsingEmailPassword(
                          email: mEmailController.text.trim(),
                          password: mPasswordController.text);
                    }
                  },
                ),
                Visibility(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 13.0,
                      ),
                      Buttons(
                        text: S.of(context).logIn,
                        isImage: true,
                        onClick: () {
                          mViewModel.authenticateWithBiometrics();
                        },
                      )
                    ],
                  ),
                  visible: mViewModel.canCheckBiometrics,
                ),
                SocialRow(

                  onTextHintClicked: () async {
                   await Navigator.push(context,
                        CupertinoPageRoute(builder: (context) {
                      return SignupView();
                    }));
                    mViewModel.attachContext(context,AppConstants.SCREEN_LOGIN);
                  },
                  onClicked: (String clickType) {
                    if (clickType == AppConstants.CLICK_TYPE_FACEBOOK) {
                      // call facebook login
                      mViewModel.fbLogin();
                    } else if (clickType == AppConstants.CLICK_TYPE_GOOGLE) {
                      // call google login
                      mViewModel.googleLogin();
                    } else {
                      // call twitter login
                      mViewModel.twitterLogin();
                    }
                  },
                ),
                SizedBox(
                  height: MediaQuery.of(context).viewPadding.bottom,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool isValid() {
    if (mEmailController.text.isEmpty) {
      CommonUtils.showSnackBar(S.of(context).emailValidation, scaffoldKey);
      return false;
    } else if (!CommonUtils.isvalidEmail(mEmailController.text.trim())) {
      CommonUtils.showSnackBar(S.of(context).validEmailValidation, scaffoldKey);
      return false;
    } else if (mPasswordController.text.isEmpty) {
      CommonUtils.showSnackBar(S.of(context).passwordValidation, scaffoldKey);
      return false;
    } else {
      return true;
    }
  }
}

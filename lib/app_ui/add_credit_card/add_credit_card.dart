import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/success/success_view.dart';
import 'package:sendcash_flutter/common_widgets/add_bank_account_cards/blue_card_view.dart';
import 'package:sendcash_flutter/common_widgets/add_bank_account_cards/darkblue_card_view.dart';
import 'package:sendcash_flutter/common_widgets/add_bank_account_cards/pink_card_view.dart';
import 'package:sendcash_flutter/common_widgets/add_bank_account_cards/purple_card_view.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/gradient_button/gradientbutton.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/models/bank_details.dart';
import 'package:sendcash_flutter/utils/taskinputformat_view.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/creaditcard_utils.dart';
import 'package:sendcash_flutter/utils/text_style.dart';
import 'add_credit_card_view_model.dart';

class AddCreditCardView extends StatefulWidget {
  const AddCreditCardView({Key key}) : super(key: key);

  @override
  _AddCreditCardViewState createState() => _AddCreditCardViewState();
}

class _AddCreditCardViewState extends State<AddCreditCardView> {
  AddCreditCardViewModel mViewModel;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final mHolderNameController = TextEditingController();
  final mCardNumberController = TextEditingController();
  final mExpiryDataController = TextEditingController();
  PageController _pageController = PageController();
  double index = 0;

  @override
  void initState() {
    // TODO: implement initState
    _pageController.addListener(() {
      setState(() {
        index = _pageController.page;
      });
    });
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<AddCreditCardViewModel>(context);

    // final bankDropdown = mViewModel.selectedbankDetails == null  ? Container() :Container(
    //     padding: EdgeInsets.only(top: 30.0),
    //     child: Column(
    //       crossAxisAlignment: CrossAxisAlignment.start,
    //       children: [
    //         Text(
    //           S.of(context).bank,
    //           style: CommonStyle.getAppFont(
    //               color: CommonColors.getWhiteTextColor(context),
    //               fontWeight: FontWeight.w500,
    //               fontSize: 20.0),
    //         ),
    //         DropdownButton<BankDetails>(
    //           isExpanded: true,
    //           dropdownColor: CommonColors.getDropdownColor(context),
    //           value: mViewModel.selectedbankDetails,
    //           hint: Text(
    //             S.of(context).selectBank,
    //             style: CommonStyle.getAppFont(
    //                 color: CommonColors.getWhiteTextColor(context),
    //                 fontWeight: FontWeight.w500,
    //                 fontSize: 18.0),
    //           ),
    //           icon: Icon(
    //             Icons.keyboard_arrow_down,
    //             color: CommonColors.getWhiteTextColor(context),
    //             size: 25,
    //           ),
    //           style: TextStyle(color: CommonColors.getWhiteTextColor(context)),
    //           underline: Container(
    //             height: 2,
    //             color: CommonColors.getFieldBorderColor(context),
    //           ),
    //           onChanged: mViewModel.onBankChange,
    //           items: mViewModel.bankList.map<DropdownMenuItem<BankDetails>>((BankDetails value) {
    //             return DropdownMenuItem<BankDetails>(
    //               value: value,
    //               child: Text(
    //                 value.name,
    //                 style: CommonStyle.getAppFont(
    //                     color: CommonColors.getWhiteTextColor(context),
    //                     fontWeight: FontWeight.w500,
    //                     fontSize: 18.0),
    //               ),
    //             );
    //           }).toList(),
    //         ),
    //       ],
    //     ));

    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      key: scaffoldKey,
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: Stack(
          children: [
            GAppbar(
              title: S.of(context).addCreditCard,
              height: 130.0,
            ),
            Container(
              height: MediaQuery.of(context).size.height,
              margin: EdgeInsets.only(top: 100.0, left: 15.0, right: 15.0),
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
              decoration: BoxDecoration(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  borderRadius: BorderRadius.circular(20.0),
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 25.0,
                        blurRadius: 25.0,
                        color: Theme.of(context).shadowColor,
                        offset: Offset(0.0, 30.0))
                  ]),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    requiredTextfield(
                      title: S.of(context).cardName,
                      hintText: "John Doe",
                      context: context,
                      controller: mHolderNameController,
                      inputType: TextInputType.text,
                      inputAction: TextInputAction.next,
                    ),
                    requiredTextfield(
                      title: S.of(context).cardNumber,
                      hintText: "1234 5678 9012 3456",
                      context: context,
                      controller: mCardNumberController,
                      inputType: TextInputType.number,
                      inputAction: TextInputAction.next,
                      inputFormatter: [
                        MaskedTextInputFormatter(
                          mask: 'xxxx-xxxx-xxxx-xxxx',
                          separator: '-',
                        ),
                      ],
                    ),
                    requiredTextfield(
                      maxlength: 5,
                      title: S.of(context).expired,
                      hintText: "12 / 25",
                      context: context,
                      controller: mExpiryDataController,
                      inputType: TextInputType.number,
                      inputAction: TextInputAction.done,
                      inputFormatter: [CardExpirationFormatter()],
                    ),
                    Container(
                      height: 200,
                      margin: EdgeInsets.only(top: 25.0),
                      child: PageView(
                          controller: _pageController,
                        children: [
                          BlueCardView(),
                          PinkCardView(),
                          DarkblueCardView(),
                          PurpleCardView()
                        ],
                        onPageChanged: (int pageIndex) {
                          setState(() {
                            index = pageIndex.toDouble();
                          });
                          printf('page changed: ' + pageIndex.toString());
                        },
                      ),
                    ),
                    DotsIndicator(
                      dotsCount: 4,
                      position: this.index,
                      decorator: DotsDecorator(
                        color: CommonColors.buttonGradientColorTop,
                        activeColor: CommonColors.primaryColor,
                      ),
                    ),
                    SizedBox(height: 30.0,),
                    GradientButton(
                      height: 40.0,
                      btnText: S.of(context).addCreditCard,
                      onClick: (){
                        if(isValidData()){
                          Map<String,dynamic>  mapCard = Map();
                          mapCard[DbKey.CARD_NO] = mCardNumberController.text;
                          mapCard[DbKey.CARDHOLDER_NAME] = mHolderNameController.text;
                          mapCard[DbKey.EXPIRY_DATE] = mExpiryDataController.text;
                          mapCard[DbKey.CARD_COLOR] = index;
                          mViewModel.addCreditCard(cardData: mapCard);
                        }
                      },
                    ),
                    SizedBox(height: MediaQuery.of(context).viewPadding.bottom + 10.0,),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool isValidData() {
    if (mHolderNameController.text.isEmpty) {
      CommonUtils.showSnackBar(S.of(context).holdernameValidation, scaffoldKey);
      return false;
    } else if (mCardNumberController.text.isEmpty) {
      CommonUtils.showSnackBar(
          S.of(context).enterCardNumber, scaffoldKey);
      return false;
    } else if (mExpiryDataController.text.isEmpty) {
      CommonUtils.showSnackBar(S.of(context).enterCardExpiryData, scaffoldKey);
      return false;
    } else {
      return true;
    }
  }
}

requiredTextfield(
    {maxlength,
    title,
    hintText,
    context,
    inputFormatter,
    controller,
    inputAction,
    inputType}) {
  return Container(
    margin: EdgeInsets.only(top: 30),
    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(
        title,
        style: CommonStyle.getAppFont(
            color: CommonColors.getWhiteTextColor(context),
            fontWeight: FontWeight.w500,
            fontSize: 20.0),
      ),
      TextFormField(
          maxLength: maxlength ?? 200,
          maxLines: 1,
          controller: controller,
          autocorrect: true,
          textInputAction: inputAction,
          keyboardType: inputType,
          inputFormatters: inputFormatter,
          cursorColor: CommonColors.getWhiteTextColor(context),
          style: CommonStyle.getAppFont(
              color: CommonColors.getWhiteTextColor(context), fontWeight: FontWeight.w500, fontSize: 18.0),
          decoration: InputDecoration(
              counterText: "",
              hintText: hintText,
              hintStyle: CommonStyle.getAppFont(
                  color:CommonColors.getGrayThemeColor(context),
                  fontWeight: FontWeight.w500,
                  fontSize: 18.0),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: CommonColors.getFieldBorderColor(context)),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: CommonColors.getFieldBorderColor(context),
                ),
              )))
    ]),
  );
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/bank_details.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';

class AddCreditCardViewModel with ChangeNotifier {
  BuildContext mContext;
  // List<BankDetails> bankList = [];
  ApiServices apiServices = ApiServices();
  BankDetails selectedbankDetails;
  FirebaseDatabaseHelper firebaseDatabaseHelper = new FirebaseDatabaseHelper();
  LoginDetails loginDetails;
  AppPreferences appPreferences = AppPreferences();
  String userFiresbaseId;

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    // bankList.clear();
    loginDetails = await appPreferences.getLoginDetails();
    userFiresbaseId = await appPreferences.getUserFirebaseId();
    //getSupportedBankList();
  }

  // void getSupportedBankList() async {
  //   CommonUtils.showProgressDialog(mContext);
  //   bankList = await apiServices.getSupportedBank(onFailed: () {
  //     CommonUtils.hideProgressDialog(mContext);
  //   });
  //
  //   selectedbankDetails = bankList[0];
  //   notifyListeners();
  //   CommonUtils.hideProgressDialog(mContext);
  // }

  // void onBankChange(BankDetails value) {
  //   selectedbankDetails = value;
  //   notifyListeners();
  // }

  Future<void> addCreditCard({Map<String, dynamic> cardData}) async {
    cardData[DbKey.USER_ID] = userFiresbaseId;
   // CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.addUserCards(
        onError: (erro, stacktrace) {
          CommonUtils.showGreenToastMessage(erro.toString());
        },
        onSuccess: (value) {
          CommonUtils.showGreenToastMessage(
              S.of(mContext).cardAddedSuccessfully);
          Navigator.pop(mContext,true);
        },
        data: cardData);
  //  CommonUtils.hideProgressDialog(mContext);
  }
}

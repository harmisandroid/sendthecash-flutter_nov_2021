import 'dart:convert';

import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/params/create_account.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';

class EditProfileViewModel with ChangeNotifier {
  BuildContext mContext;
  AppPreferences appPreferences = AppPreferences();
  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();
  LoginDetails loginDetails;
  String userId;
  String selectedDay, selectedMonth, selectedYear;
  Country country,phoneCountry;
  String countryCode;
  String countryPhoneCode;
  String countryName = "India";
  String phoneCode = "+91";
  DateTime selectedBirthdate = null;

  bool isEmailExists = false, isMobileExists = false;
  ApiServices apiServices = ApiServices();

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    userId = await appPreferences.getUserFirebaseId();
    loginDetails = await appPreferences.getLoginDetails();
    if (loginDetails.countryCode != null &&
        loginDetails.countryCode.isNotEmpty) {
      countryCode = loginDetails.countryCode;
    } else {
      countryCode = "US";
    }
    selectedBirthdate = null;

    if (loginDetails.phoneCode != null && loginDetails.phoneCode.isNotEmpty) {
      phoneCode = loginDetails.phoneCode;
    } else {
      phoneCode = "+1";
    }

    if (loginDetails.address != null) {
      if (loginDetails.address.country != null &&
          loginDetails.address.country.isNotEmpty) {
        countryName = loginDetails.address.country;
      }
    }

    if (country == null) {
      country = Country(
          phoneCode: "+1",
          countryCode: "US",
          e164Sc: 1223,
          geographic: true,
          level: 1524,
          name: "United States",
          example: "",
          displayName: "USA",
          displayNameNoCountryCode: "No",
          e164Key: "");
      phoneCountry = country;
    }
    if (loginDetails.birthdate != null && loginDetails.birthdate.isNotEmpty) {
      List<String> splitData = loginDetails.birthdate.split("--");

      if (splitData.length > 2) {
        selectedDay = loginDetails.birthdate.split("--")[0];
        selectedMonth = loginDetails.birthdate.split("--")[1];
        selectedYear = loginDetails.birthdate.split("--")[2];
      } else {
        selectedDay = "DD";
        selectedMonth = "MM";
        selectedYear = "YYYY";
      }
    }

    notifyListeners();
  }

  Future<void> getLoginDetails({Function onSuccess}) async {
    loginDetails = await appPreferences.getLoginDetails();
    onSuccess(loginDetails);
  }

  Future<void> updateUser(
      {username,
      firstname,
      lastname,
      email,
      phoneCode,
      phone,
      birthdate,
      addess1,
      address2,
      city,
      zipcode,
      state,
      country,
      ssn}) async {
    CommonUtils.showProgressDialog(mContext);
    DateTime currentDateTime = DateTime.now();
    DateFormat dateFormat = DateFormat("dd/MM/yyyy HH:mm:ss");
   
    await firebaseDatabaseHelper.updateUserProfile(
        userdata: {
          DbKey.USERNAME: username,
          DbKey.FIRST_NAME: firstname,
          DbKey.LAST_NAME: lastname,
          DbKey.EMAIL: email,
          DbKey.PHONE_CODE: phoneCode,
          DbKey.COUNTRY_CODE: countryCode,
          DbKey.MOBILE_NO: phone,
          DbKey.BIRTHDATE: birthdate,
          DbKey.UPDATED_AT: dateFormat.format(currentDateTime),
          DbKey.SSN: ssn,
          DbKey.ADDRESS: {
            DbKey.ADDRESS_LINE_1: addess1,
            DbKey.ADDRESS_LINE_2: address2,
            DbKey.CITY: city,
            DbKey.ZIPCODE: zipcode,
            DbKey.STATE: state,
            DbKey.COUNTRY: country,
            DbKey.SSN: ssn
          }
        },
        userId: userId,
        onError: (error, stackTrace) {
          CommonUtils.showRedToastMessage("failed to update profile pic");
          CommonUtils.hideProgressDialog(mContext);
        },
        onSuccess: (value) {
          getUserDetails();
          CommonUtils.showGreenToastMessage("profile updated successfully!");
        }).onError((error, stackTrace) {
      notifyListeners();
      CommonUtils.showRedToastMessage("failed to update profile");
    });
    notifyListeners();
  }

  void getUserDetails() async {
    await firebaseDatabaseHelper.getUserDetails(
        userId: userId,
        onSuccess: (documentReference) async {
          Map<String, dynamic> data =
              documentReference.data() as Map<String, dynamic>;
          await appPreferences.setLoginDetails(jsonEncode(data));
          loginDetails = LoginDetails.fromJson(data);
          CommonUtils.hideProgressDialog(mContext);
          await updateSendwyreAccount(
              email: loginDetails.email,
              phone: loginDetails.mobileNo,
              name: loginDetails.firstName +
                  " " +
                  loginDetails.lastName,
              postalCode: loginDetails.address.zipcode,
              street1: loginDetails.address.addressLine1,
              street2: loginDetails.address.addressLine2,
              state: loginDetails.address.state,
              city: loginDetails.address.city,
              ssn: loginDetails.ssn,
              phoneCode: loginDetails.phoneCode

              );
          Navigator.pop(mContext);
        },
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage(
              "Failed to get user details " + error.toString());
        });
  }

  void showDatpickerDialog() async {
    await showDatePicker(
      context: mContext,
      initialDate: DateTime.now(),
      firstDate: DateTime(1950),
      lastDate: DateTime.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: CommonColors.gradientColorBottom,
            accentColor: CommonColors.gradientColorBottom,
            colorScheme: ColorScheme.light(primary: CommonColors.primaryColor),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
    ).then((value) {
      final birthday = value;
      final date2 = DateTime.now();
      final difference = date2.difference(birthday).inDays;
      if(difference > (365 * 18)){
        selectedDay = value.day.toString();
        selectedYear = value.year.toString();
        selectedMonth = value.month.toString();
        loginDetails.birthdate =
            selectedDay + "--" + selectedMonth + "--" + selectedYear;
        selectedBirthdate = value;
        notifyListeners();
      }else{
        CommonUtils.showRedToastMessage("Your age must be above 18 to update.");
      }

    });
  }

  void showCountryPickerDialog() {
    showCountryPicker(
      context: mContext,
      showPhoneCode: true,
      onSelect: (Country value) {
        country = value;
        loginDetails.countryCode = value.countryCode;
        countryCode = value.countryCode;
        countryName = value.name;
        notifyListeners();
      },
    );
  }

  void showPhoneCountryPickerDialog() {
    showCountryPicker(
      context: mContext,
      showPhoneCode: true,
      onSelect: (Country value) {
        country = value;
        loginDetails.phoneCode = "+" + value.phoneCode;
        loginDetails.countryCode = value.countryCode;
        countryCode = value.countryCode;
        phoneCode = "+" + value.phoneCode;
        countryName = value.name;
        notifyListeners();
      },
    );
  }

  void checkEmailExists({email}) async {
    await firebaseDatabaseHelper.checkEmailExists(
        onError: (error, stacktrace) {
          Fluttertoast.showToast(
              msg: "failed to get userdetails " + error.toString());
        },
        onSuccess: (querysnapshot) async {
          if (querysnapshot.size > 0) {
            isEmailExists = true;
            CommonUtils.showRedToastMessage(S.of(mContext).emailAlreadyExists);
          } else {
            isEmailExists = false;
          }
          notifyListeners();
        },
        emailAddress: email);
  }

  // void check phone alreaday exists

  Future<void> checkPhoneAlreadyExists({String mobileNumber}) async {
    await firebaseDatabaseHelper.checkMobileNumberExists(
        onError: (error, stacktrace) {
          Fluttertoast.showToast(
              msg: "failed to get userdetails " + error.toString());
        },
        onSuccess: (querysnapshot) async {
          if (querysnapshot.size > 0) {
            isMobileExists = true;
            CommonUtils.showRedToastMessage(
                S.of(mContext).mobileNumberAlreadyExists);
            notifyListeners();
          } else {
            isMobileExists = false;
            notifyListeners();
          }
        },
        mobileNumber: mobileNumber);
  }

  Future<void> updateSendwyreAccount(
      {String email,
      String phone,
      String phoneCode,
        String name,
      String street1,
      String street2,
      String city,
      String state,
      String postalCode,
      String ssn}) async {
    String updateResponse = await apiServices.updateSendwyreAccount(
      accountId: loginDetails.sendwyreAccountId,
      params: updateAccountParams(
          email: email,
          phone: phone,
          phoneCode: phoneCode,
          name: name,
          street1: street1,
          street2: street2,
          city: city,
          state: state,
          postalCode: postalCode,
          ssn: ssn),
    );
    if(updateResponse != null){
      CommonUtils.showGreenToastMessage("Profile updated successfully!");
    }else{
      CommonUtils.showRedToastMessage("Failed to update account details");
    }
  }

  String updateAccountParams(
      {String email,
      String phone,
      String phoneCode,
        String name,
      String street1,
      String street2,
      String city,
      String state,
      String postalCode,
      String ssn}) {
    String bdate = null;
    DateFormat birthDateFormat = DateFormat("yyyy-MM-dd");
    if(selectedBirthdate != null){
      bdate = birthDateFormat.format(selectedBirthdate);
    }else if(loginDetails.birthdate != null && loginDetails.birthdate.isNotEmpty){
      bdate = loginDetails.birthdate.split("--")[2]+"-"+loginDetails.birthdate.split("--")[1]+"-"+loginDetails.birthdate.split("--")[0];
    }
    
    CreateAccountParams createAccountParams = CreateAccountParams();
    List<ProfileFields> fieldList = [];
    if (email != null && email.isNotEmpty) {
      ProfileFields emailField = ProfileFields(fieldId: "individualEmail", value: email);
      fieldList.add(emailField);
    }
    if (bdate != null && bdate.isNotEmpty) {
      ProfileFields dobField =
          ProfileFields(fieldId: "individualDateOfBirth", value: bdate);
      fieldList.add(dobField);
    }
    if (phone != null && phone.isNotEmpty) {
      ProfileFields phoneField = ProfileFields(
          fieldId: "individualCellphoneNumber", value: phoneCode + phone);
      fieldList.add(phoneField);
    }
    if (name != null && name.isNotEmpty) {
      ProfileFields nameField =
          ProfileFields(fieldId: "individualLegalName", value: name);
      fieldList.add(nameField);
    }
    if (ssn != null && ssn.isNotEmpty) {
      ProfileFields ssnField =
          ProfileFields(fieldId: "individualSsn", value: ssn);
      fieldList.add(ssnField);
    }
    if (street1 != null && street1.isNotEmpty) {
      ProfileFields addressField =
          ProfileFields(fieldId: "individualResidenceAddress", value: {
        "street1": street1,
        "street2": street2,
        "city": city,
        "state": state,
        "postalCode": postalCode,
        "country": "US",
      });
      fieldList.add(addressField);
    }
    createAccountParams.profileFields = fieldList;
    printf("Update params: =>"+jsonEncode(createAccountParams));
    return jsonEncode(createAccountParams);
  }
}

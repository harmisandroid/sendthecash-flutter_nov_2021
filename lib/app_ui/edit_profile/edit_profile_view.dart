import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/gradient_button/gradientbutton.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/country_uils.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

import 'edit_profile_view_model.dart';

class EditProfileView extends StatefulWidget {
  const EditProfileView({Key key}) : super(key: key);

  @override
  _EditProfileViewState createState() => _EditProfileViewState();
}

class _EditProfileViewState extends State<EditProfileView> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  TextEditingController mUsernameController = TextEditingController();
  final mFirstnameController = TextEditingController();
  final mLastnameController = TextEditingController();
  final mEmailController = TextEditingController();
  final mPhoneController = TextEditingController();
  final mStreet1Controller = TextEditingController();
  final mStreet2Controller = TextEditingController();
  final mCityController = TextEditingController();
  final mPostalController = TextEditingController();
  final mStateProvinceController = TextEditingController();
  final mSSnController = TextEditingController();
  final mCountryController = TextEditingController();

  // final mPasswordController = TextEditingController();

  bool isAgreed = false;
  final formKey = GlobalKey<FormState>();
  EditProfileViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
      mViewModel.getLoginDetails(onSuccess: (LoginDetails loginDetails) {
        if (loginDetails != null) {
          mUsernameController.text = loginDetails.username;
          mFirstnameController.text = loginDetails.firstName;
          mLastnameController.text = loginDetails.lastName;
          mEmailController.text = loginDetails.email;
          mPhoneController.text = loginDetails.mobileNo;
          mStreet1Controller.text = loginDetails.address.addressLine1;
          mStreet2Controller.text = loginDetails.address.addressLine2;
          mCityController.text = loginDetails.address.city;
          mPostalController.text = loginDetails.address.zipcode;
          mStateProvinceController.text = loginDetails.address.state;
          mSSnController.text = loginDetails.ssn ?? "";
          mPhoneController.text = loginDetails.mobileNo;
          mCountryController.text = loginDetails.address.country;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<EditProfileViewModel>(context);

    if(mViewModel.loginDetails == null){
      return Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        body: Stack(
          children: [
            GAppbar(
              title: S.of(context).profile,
              height: 153.0,
            ),
            Container(
              height: MediaQuery.of(context).size.height - 200,
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 150.0),
              padding: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 25.0,
                        blurRadius: 25.0,
                        color: Colors.white,
                        offset: Offset(10.0, 0.0))
                  ]),
              child: Center(child: CircularProgressIndicator(),),
            )
          ],
        ),
      );
    }

    final phonenumberTextfield = mViewModel.phoneCountry == null
        ? Container()
        : Container(
            height: 48,
            margin: EdgeInsets.only(top: 8),
            decoration: BoxDecoration(
                border: Border.all(color: CommonColors.getWhiteTextColor(context)),
                borderRadius: BorderRadius.circular(30.0),
                color: Theme.of(context).backgroundColor),
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    mViewModel.showPhoneCountryPickerDialog();
                  },
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 20),
                        child: Text(
                          Utils.countryCodeToEmoji(
                              (mViewModel.loginDetails.countryCode == null || mViewModel.loginDetails.countryCode.isEmpty) ? mViewModel.countryCode :mViewModel.loginDetails.countryCode),
                          style: TextStyle(fontSize: 22.0),
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        (mViewModel.loginDetails.phoneCode == null ||
                                mViewModel.loginDetails.phoneCode.isEmpty)
                            ? mViewModel.country.phoneCode
                            : mViewModel.loginDetails.phoneCode,
                        style: TextStyle(color: CommonColors.getWhiteTextColor(context)),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Icon(
                        Icons.keyboard_arrow_down,
                        color: CommonColors.getWhiteTextColor(context),
                        size: 20,
                      )
                    ],
                  ),
                ),
                Container(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    height: 28,
                    width: 2,
                    child: VerticalDivider(color: CommonColors.getWhiteTextColor(context))),
                Flexible(
                  child: Container(
                    height: 50,
                    width: 220,
                    margin: EdgeInsets.only(left: 20),
                    child: TextField(
                      maxLines: 1,
                      maxLength: 11,
                      controller: mPhoneController,
                      cursorColor: CommonColors.getWhiteTextColor(context),
                      onSubmitted: (value) {
                        mViewModel.checkPhoneAlreadyExists(
                            mobileNumber: value);
                      },
                      style: TextStyle(fontSize: 15, color: CommonColors.getWhiteTextColor(context)),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        counterText: "",
                        filled: true,
                        border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius:
                                BorderRadius.all(Radius.circular(30))),
                        fillColor: Theme.of(context).backgroundColor,
                        contentPadding: EdgeInsets.zero,
                        hintStyle: TextStyle(
                            color: CommonColors.getWhiteTextColor(context), fontSize: 15),
                        hintText: S.of(context).phoneNumber,
                      ),
                    ),
                  ),
                )
              ],
            ),
          );

    final birthdateTextfield = Container(
      child: Column(
        children: [
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsetsDirectional.only(start: 20.0, top: 10.0),
            child: Text(
              S.of(context).birthday,
              style: CommonStyle.getAppFont(fontSize: 14, color: Colors.black),
            ),
          ),
          InkWell(
            onTap: () {
              mViewModel.showDatpickerDialog();
            },
            child: Container(
              height: 48,
              margin: EdgeInsets.only(top: 8),
              decoration: BoxDecoration(
                  border: Border.all(color: CommonColors.getWhiteTextColor(context)),
                  borderRadius: BorderRadius.all(Radius.circular(30.0))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  getDropdownView(
                      text: mViewModel.selectedDay != null
                          ? mViewModel.selectedDay
                          : "DD",
                      onClick: () {}),
                  Container(
                      height: 28,
                      width: 2,
                      child:
                          VerticalDivider(color: CommonColors.getWhiteTextColor(context))),
                  getDropdownView(
                      text: mViewModel.selectedMonth != null
                          ? mViewModel.selectedMonth
                          : "MM",
                      onClick: () {}),
                  Container(
                      height: 28,
                      width: 2,
                      child:
                          VerticalDivider(color: CommonColors.getWhiteTextColor(context))),
                  getDropdownView(
                      text: mViewModel.selectedYear != null
                          ? mViewModel.selectedYear
                          : "YYYY",
                      onClick: () {}),
                ],
              ),
            ),
          )
        ],
      ),
    );

    return GestureDetector(
        onTap: (){
     // FocusManager.instance.primaryFocus.unfocus();
      SystemChannels.textInput.invokeMethod('TextInput.hide');
    },child:Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Stack(
        children: [
          GAppbar(
            title: S.of(context).profile,
            height: 153.0,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 150.0),
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(10.0, 0.0))
                ]),
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(left: 15.0, right: 15.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    getEditText(
                      hint: S.of(context).userName,
                      controller: mUsernameController,
                      inputAction: TextInputAction.next,
                      inputType: TextInputType.text,
                    ),
                    getEditText(
                        hint: S.of(context).firstName,
                        controller: mFirstnameController,
                        inputAction: TextInputAction.next,
                        inputType: TextInputType.text),
                    getEditText(
                        hint: S.of(context).lastName,
                        controller: mLastnameController,
                        inputAction: TextInputAction.next,
                        inputType: TextInputType.text),
                    getEditText(
                        hint: S.of(context).email,
                        controller: mEmailController,
                        onSubmit: (value) {
                          mViewModel.checkEmailExists(email: value);
                        },
                        inputAction: TextInputAction.next,
                        inputType: TextInputType.emailAddress),
                    phonenumberTextfield,
                    birthdateTextfield,
                    Container(
                      alignment: Alignment.topLeft,
                      // padding: EdgeInsets.only(left: 10, top: 10),
                      padding:
                          EdgeInsetsDirectional.only(start: 20.0, top: 10.0),
                      child: Text(
                        S.of(context).address,
                        style: CommonStyle.getAppFont(
                            fontSize: 14, color: Colors.black),
                      ),
                    ),
                    getEditText(
                        hint: S.of(context).street1,
                        controller: mStreet1Controller,
                        inputAction: TextInputAction.next,
                        inputType: TextInputType.text),
                    getEditText(
                        hint: S.of(context).street2,
                        controller: mStreet2Controller,
                        inputAction: TextInputAction.next,
                        inputType: TextInputType.text),
                    getEditText(
                        hint: S.of(context).city,
                        controller: mCityController,
                        inputAction: TextInputAction.next,
                        inputType: TextInputType.text),
                    getEditText(
                        hint: S.of(context).postalCode,
                        controller: mPostalController,
                        inputAction: TextInputAction.next,
                        inputType: TextInputType.text),
                    getEditText(
                        hint: S.of(context).stateProvince,
                        controller: mStateProvinceController,
                        inputAction: TextInputAction.next,
                        inputType: TextInputType.text),
                    InkWell(
                      onTap: (){
                        mViewModel.showCountryPickerDialog();
                      },
                      child: Container(
                        height: 50.0,
                        padding: EdgeInsets.only(left: 20, right: 30.0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(30.0)),
                            border: Border.all(width: 1.0,color: CommonColors.getWhiteTextColor(context)),
                            color: Colors.transparent),
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(top: 10.0, bottom: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              mViewModel.countryName ?? "India",
                              style: CommonStyle.getAppFont(
                                  fontSize: 15,
                                  color: CommonColors.getWhiteTextColor(context),
                                  fontWeight: FontWeight.normal),
                            ), Icon(
                              Icons.keyboard_arrow_down_outlined,
                              color: CommonColors.getWhiteTextColor(context),
                            )
                          ],
                        ),
                      ),
                    ),
                    getEditText(
                        hint: S.of(context).ssn,
                        controller: mSSnController,
                        maxLenth: 9,
                        inputAction: TextInputAction.done,
                        inputType: TextInputType.number),
                    // getEditText(
                    //     hint: S.of(context).password,
                    //     controller: mPasswordController,
                    //     inputAction: TextInputAction.next,
                    //     inputType: TextInputType.text,
                    //     isObscure: true),
                    SizedBox(
                      height: 40.0,
                    ),

                    GradientButton(
                      btnText: S.of(context).save,
                      onClick: () {
                       if(isValidData()){
                         mViewModel.updateUser(
                             username: mUsernameController.text,
                             firstname: mFirstnameController.text,
                             lastname: mLastnameController.text,
                             email: mEmailController.text,
                             phoneCode: mViewModel.phoneCode,
                             phone: mPhoneController.text,
                             addess1: mStreet1Controller.text,
                             address2: mStreet2Controller.text,
                             city: mCityController.text,
                             zipcode: mPostalController.text,
                             state: mStateProvinceController.text,
                             country: mViewModel.countryName,
                             ssn: mSSnController.text,
                             birthdate: mViewModel.loginDetails.birthdate ?? "");
                       }
                      },
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).viewPadding.bottom + 20.0,
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    ));
  }

  Widget getEditText({
    String hint,
    TextEditingController controller,
    TextInputAction inputAction,
    TextInputType inputType,
    int maxLine,
    int maxLenth,
    bool isObscure,
    Function(String) onSubmit,
  }) {
    return Container(
      height: 55,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 10),
      child: TextField(
        cursorColor: CommonColors.getWhiteTextColor(context),
        obscureText: isObscure ?? false,
        textInputAction: inputAction,
        keyboardType: inputType,
        controller: controller,
        onSubmitted: (value) {
          if (onSubmit != null) {
            onSubmit(value);
          }
        },
        style: CommonStyle.getAppFont(
            color: CommonColors.getWhiteTextColor(context), fontWeight: FontWeight.normal, fontSize: 16),
        decoration: InputDecoration(
            counterText: "",
            border: OutlineInputBorder(
              borderSide: BorderSide(color: CommonColors.getWhiteTextColor(context)),
              borderRadius: BorderRadius.circular(30.0),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: CommonColors.getWhiteTextColor(context)),
              borderRadius: BorderRadius.circular(30.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: CommonColors.getWhiteTextColor(context)),
              borderRadius: BorderRadius.circular(30.0),
            ),
            contentPadding: EdgeInsets.only(left: 20, right: 10),
            filled: true,
            hintStyle: TextStyle(
                color: CommonColors.getHintTextStyle(context),
                fontSize: 16,
                fontWeight: FontWeight.normal),
            hintText: hint,
            fillColor: Theme.of(context).backgroundColor),
        maxLines: maxLine ?? 1,
        maxLength: maxLenth ?? 200,
      ),
    );
  }



  Widget getDropdownView({String text, Function onClick}) {
    return Flexible(
        child: Container(
      alignment: Alignment.center,
      width: 100.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Text(
              text,
              style: CommonStyle.getAppFont(fontSize: 15, color: CommonColors.getWhiteTextColor(context)),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Icon(
            Icons.keyboard_arrow_down,
            color: CommonColors.getWhiteTextColor(context),
            size: 20,
          ),
        ],
      ),
    ));
  }

  // just check validation
  bool isValidData() {
    if (mUsernameController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).userNameValiidation);
      return false;
    } else if (mFirstnameController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).firstNameValidation);
      return false;
    } else if (mLastnameController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).lastNameValidation);
      return false;
    } else if (mEmailController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).emailValidation);
      return false;
    } else if (!CommonUtils.isvalidEmail(mEmailController.text)) {
      CommonUtils.showRedToastMessage(S.of(context).validEmailValidation);
      return false;
    } else if (mPhoneController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).phoneValidation);
      return false;
    } else if (mViewModel.selectedDay == null) {
      CommonUtils.showRedToastMessage(S.of(context).birthdateValidation);
      return false;
    } else if (mStreet1Controller.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).street1Validation);
      return false;
    } else if (mStreet2Controller.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).street2Validation);
      return false;
    } else if (mPostalController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).postalCodeValidation);
      return false;
    } else if (mCityController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).cityValidation);
      return false;
    } else if (mStateProvinceController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).stateProvinceValidation);
      return false;
    }else if (mViewModel.isMobileExists) {
      CommonUtils.showRedToastMessage(S.of(context).mobileNumberAlreadyExists);
      return false;
    } else if (mViewModel.isEmailExists) {
      CommonUtils.showRedToastMessage(S.of(context).emailAlreadyExists);
      return false;
    } else {
      return true;
    }
  }
}

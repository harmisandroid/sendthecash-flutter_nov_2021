import 'dart:io';

import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class FullScreenImageView extends StatefulWidget {
  String imageName;
  String imageUrl;

  FullScreenImageView({this.imageName, this.imageUrl});

  @override
  _State createState() => _State();
}

class _State extends State<FullScreenImageView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          centerTitle: true,
          title: Text(widget.imageName),
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.close,
              color: Colors.white,
            ),
          ),
        ),
        body: PhotoView(
          imageProvider: widget.imageUrl.startsWith("http")
              ? NetworkImage(widget.imageUrl)
              : FileImage(File(widget.imageUrl)),
        ));
  }
}

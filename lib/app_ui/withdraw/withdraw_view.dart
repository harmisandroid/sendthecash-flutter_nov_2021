import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/add_bank_account/add_bank_account.dart';
import 'package:sendcash_flutter/app_ui/wallet_list_item/wallet_list_item_view.dart';
import 'package:sendcash_flutter/app_ui/withdraw/withdraw_view_model.dart';
import 'package:sendcash_flutter/common_widgets/appbar/back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/appbar/main_appbar.dart';
import 'package:sendcash_flutter/common_widgets/cards/saving_card_view.dart';
import 'package:sendcash_flutter/common_widgets/gradient_button/gradientbutton.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/currency_formatter.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/taskinputformat_view.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class WithdrawView extends StatefulWidget {
  const WithdrawView({Key key}) : super(key: key);

  @override
  _WithdrawViewState createState() => _WithdrawViewState();
}

class _WithdrawViewState extends State<WithdrawView> {
  int selectedIndex = -1;
  WithdrawViewModel mViewModel;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final mHolderNameController = TextEditingController();
  final mBankAccountController = TextEditingController();
  final mAmountController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<WithdrawViewModel>(context);

    final withDrawTo = Container(
        height: 140.0,
        child: ListView.separated(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            physics: ClampingScrollPhysics(),
            separatorBuilder: (context, index) {
              return Container(
                height: 5.0,
                width: 5.0,
              );
            },
            itemCount: mViewModel.dataList.length,
            itemBuilder: (context, index) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          selectedIndex = index;
                        });
                        if(index == 2){
                          Navigator.push(context, CupertinoPageRoute(builder: (context){
                            return AddBankAccountView();
                          }));
                        }
                      },
                      child: Container(
                        height: 110.0,
                        width: 110.0,
                        decoration: BoxDecoration(
                            color: Theme.of(context).backgroundColor,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 1.0,
                                  spreadRadius: 1.0,
                                  offset: Offset.zero)
                            ],
                            border: Border.all(
                                color: selectedIndex == index
                                    ? CommonColors.primaryColor
                                    : Colors.white,
                                width: 2.0),
                            borderRadius: BorderRadius.circular(10.0)),
                        padding: EdgeInsets.only(
                            left: 10.0, right: 10.0, bottom: 10.0),
                        margin: EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            selectedIndex == index
                                ? Container(
                                    padding: EdgeInsets.only(left: 50.0),
                                    alignment: Alignment.centerRight,
                                    child: Image.asset(
                                      LocalImages.pink_check,
                                      height: 30.0,
                                      width: 50.0,
                                    ),
                                  )
                                : Container(),
                            Image.asset(
                              mViewModel.dataList[index].image,
                              height: 30.0,
                              width: 50.0,
                            ),
                            SizedBox(height: 15.0),
                            Flexible(
                                child: Text(
                              mViewModel.dataList[index].text,
                              style: CommonStyle.getAppFont(
                                  color: CommonColors.getWhiteTextColor(
                                      context),
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal),
                            )),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              );
            }));

    final amount = Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 20.0),
      child: Stack(
        children: [
          Container(
            height: 70.0,
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 20.0),
            margin: EdgeInsets.only(right: 20.0),
            decoration: BoxDecoration(
                color: CommonColors.getWithdrawTextFieldColor(context),
                borderRadius: BorderRadius.circular(10.0)),
            child: TextFormField(
              textAlign: TextAlign.start,
              style: CommonStyle.getAppFont(
                  color: CommonColors.getWhiteTextColor(context),
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
              maxLines: 1,
              controller: mAmountController,
              autocorrect: true,
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.number,
              inputFormatters: [
                FilteringTextInputFormatter.digitsOnly,
                // CurrencyInputFormatter()
              ],
              cursorColor: CommonColors.getWhiteTextColor(context),
              decoration: InputDecoration(
                  counterText: "",
                  hintText: S.of(context).amount,
                  hintStyle: CommonStyle.getAppFont(
                      color: CommonColors.getGrayThemeColor(context),
                      fontWeight: FontWeight.w500,
                      fontSize: 20.0),
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none),
            ),
          ),
          SizedBox(
            width: 10.0,
          ),
          Positioned.directional(
            textDirection: TextDirection.ltr,
            child: Container(
              height: 70.0,
              width: 70.0,
              decoration: BoxDecoration(
                  color: CommonColors.getBoxColor(context),
                  borderRadius: BorderRadius.circular(20.0)),
              child: Center(
                child: Text(
                  "\$",
                  style: CommonStyle.getAppFont(
                      color: CommonColors.getDropdownColor(context),
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            end: 0.0,
          )
        ],
      ),
    );

    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            GAppbar(
              title: S.of(context).Withdraw,
              height: 130.0,
              onBackPressed: () {
                Navigator.pop(context);
              },
            ),
            Container(
              margin: EdgeInsetsDirectional.only(top: 125.0),
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
              decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 25.0,
                        blurRadius: 25.0,
                        color: Theme.of(context).shadowColor,
                        offset: Offset(10.0, 0.0))
                  ]),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        height: 70.0,
                        padding: EdgeInsets.all(18.0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: CommonColors.primaryColor),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              S.of(context).balance,
                              style: CommonStyle.getAppFont(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.normal),
                            ),
                            Text(
                              Globals.currencySymbol + "2648.00",
                              style: CommonStyle.getAppFont(
                                  color: Colors.white,
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        )),
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(
                      S.of(context).amount,
                      style: CommonStyle.getAppFont(
                          color:
                              CommonColors.getWhiteTextColor(context),
                          fontSize: 20,
                          fontWeight: FontWeight.normal),
                    ),
                    amount,
                    SizedBox(
                      height: 30.0,
                    ),
                    Text(
                      S.of(context).WithdrawTo,
                      style: CommonStyle.getAppFont(
                          color:
                              CommonColors.getWhiteTextColor(context),
                          fontSize: 20,
                          fontWeight: FontWeight.normal),
                    ),
                    withDrawTo,
                    SizedBox(
                      height: 30.0,
                    ),
                    requiredTextfield(
                      title: S.of(context).cardHolderName,
                      hintText: "John Doe",
                      context: context,
                      controller: mHolderNameController,
                      inputType: TextInputType.text,
                      inputAction: TextInputAction.next,
                    ),
                    requiredTextfield(
                      title: S.of(context).cardNumber,
                      hintText: "1234 5678 9012 3456",
                      context: context,
                      controller: mBankAccountController,
                      inputType: TextInputType.number,
                      inputAction: TextInputAction.next,
                      inputFormatter: [
                        MaskedTextInputFormatter(
                          mask: 'xxxx-xxxx-xxxx-xxxx',
                          separator: '-',
                        ),
                      ],
                    ),
                    GradientButton(
                      btnText: S.of(context).Withdraw,
                      height: 50.0,
                      onClick: () {},
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).padding.bottom,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  bool isValid() {
    if (mHolderNameController.text.isEmpty) {
      CommonUtils.showSnackBar(S.of(context).holdernameValidation, scaffoldKey);
      return false;
    } else if (mBankAccountController.text.isEmpty) {
      CommonUtils.showSnackBar(
          S.of(context).bankaccountValidation, scaffoldKey);
      return false;
    } else if (mAmountController.text.isEmpty) {
      CommonUtils.showSnackBar(S.of(context).amountValidation, scaffoldKey);
      return false;
    } else {
      return true;
    }
  }
}

requiredTextfield(
    {maxlength,
    title,
    hintText,
    context,
    inputFormatter,
    controller,
    inputAction,
    inputType}) {
  return Container(
    margin: EdgeInsets.only(bottom: 30),
    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(
        title,
        style: CommonStyle.getAppFont(
            color: CommonColors.getWhiteTextColor(context),
            fontWeight: FontWeight.w500,
            fontSize: 20.0),
      ),
      SizedBox(
        height: 15,
      ),
      TextFormField(
        maxLength: maxlength ?? 200,
        maxLines: 1,
        controller: controller,
        autocorrect: true,
        textInputAction: inputAction,
        keyboardType: inputType,
        inputFormatters: inputFormatter,
        cursorColor: CommonColors.getWhiteTextColor(context),
        style: CommonStyle.getAppFont(
            color: CommonColors.getWhiteTextColor(context),
            fontWeight: FontWeight.w500,
            fontSize: 18.0),
        decoration: InputDecoration(
            counterText: "",
            hintText: hintText,
            hintStyle: CommonStyle.getAppFont(
                color: CommonColors.getGrayThemeColor(context),
                fontWeight: FontWeight.w500,
                fontSize: 18.0),
            enabledBorder: UnderlineInputBorder(
              borderSide:
                  BorderSide(color: CommonColors.getFieldBorderColor(context)),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: CommonColors.getFieldBorderColor(context),
              ),
            ),
            contentPadding: EdgeInsets.only(left: 20.0)),
      )
    ]),
  );
}

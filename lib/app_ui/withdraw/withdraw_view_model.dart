import 'package:flutter/material.dart';
import 'package:sendcash_flutter/utils/local_images.dart';

class WithdrawViewModel with ChangeNotifier {
  List<DataModel> dataList = [];
  BuildContext mContext;
  int currentValue;



  attachContext(BuildContext context) async {
    mContext = context;
    dataList.clear();
    dataList.add(DataModel(LocalImages.master_card, "Master Card"));
    dataList.add(DataModel(LocalImages.visa, "Visa"));
    dataList.add(DataModel(LocalImages.ic_bank, "Bank Account"));

    notifyListeners();
  }

  void increament({TextEditingController controller}){
   currentValue = int.parse(controller.text);
    currentValue++;
    controller.text = (currentValue).toString();
    notifyListeners();
  }

  void decreament({TextEditingController controller}){
  currentValue = int.parse(controller.text);
    currentValue--;
    controller.text = (currentValue).toString();
    notifyListeners();
  }

}


class DataModel {
  String image;
  String text;

  DataModel(this.image, this.text);
}

import 'package:flutter/material.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class WithdrawSuccessView extends StatefulWidget {
  const WithdrawSuccessView({Key key}) : super(key: key);

  @override
  _WithdrawSuccessViewState createState() => _WithdrawSuccessViewState();
}

class _WithdrawSuccessViewState extends State<WithdrawSuccessView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      bottomNavigationBar: Container(
        height: 68.0,
        color: Theme.of(context).backgroundColor,
        margin: EdgeInsets.only( bottom: 25),
        padding: EdgeInsets.all(10),
        child: Container(
          child: Row(
            children: [
              Flexible(
                  child: Container(
                height: 50,
                padding: EdgeInsetsDirectional.all(10.0),
                width: MediaQuery.of(context).size.width - 170,
                decoration: BoxDecoration(
                    color: Colors.white,
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          CommonColors.buttonGradientColorTop,
                          CommonColors.buttonGradientColorBottom
                        ]),
                    borderRadius: BorderRadius.circular(30.0),
                    boxShadow: [
                      BoxShadow(
                        color:
                            Theme.of(context).shadowColor,
                        blurRadius: 4.0,
                        spreadRadius: 3.0,
                      )
                    ]),
                child: Center(
                  child: Text(
                    S.of(context).goToTransaction,
                    style: CommonStyle.getAppFont(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 17),
                  ),
                ),
              )),
              SizedBox(
                width: 10,
              ),
              Flexible(
                  child: Container(
                height: 50,
                padding: EdgeInsetsDirectional.all(10.0),
                width: MediaQuery.of(context).size.width - 170,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          CommonColors.buttonGradientColorTop,
                          CommonColors.buttonGradientColorBottom
                        ]),
                    borderRadius: BorderRadius.circular(30.0),
                    boxShadow: [
                      BoxShadow(
                        color:
                        Theme.of(context).shadowColor,
                        blurRadius: 4.0,
                        spreadRadius: 3.0,
                      )
                    ]),
                child: Center(
                  child: Text(
                    S.of(context).backToHome,
                    style: CommonStyle.getAppFont(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 17),
                  ),
                ),
              )),
            ],
          ),
        ),
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: Stack(
          children: [
            GAppbar(
              title: S.of(context).withdrawsuccess,
              height: 135.0,
            ),
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 135.0),
              padding: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 25.0,
                        blurRadius: 25.0,
                        color: Theme.of(context).shadowColor,
                        offset: Offset(10.0, 0.0))
                  ]),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      LocalImages.success,
                      height: 252,
                      width: 386,
                    ),
                    Text(
                      S.of(context).success,
                      style: CommonStyle.getAppFont(
                          color: Colors.black,
                          fontSize: 28,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          S.of(context).succesfullySentTo,
                          style: CommonStyle.getAppFont(
                              color: CommonColors.getWhiteTextColor(
                                  context),
                              fontSize: 20,
                              fontWeight: FontWeight.w500),
                        ),
                        Text(
                          S.of(context).lane,
                          style: CommonStyle.getAppFont(
                              color: CommonColors.gradientColorBottom,
                              fontSize: 20,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      Globals.currencySymbol + "650.00",
                      style: CommonStyle.getAppFont(
                          color: CommonColors.green,
                          fontSize: 28,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          S.of(context).transactionId,
                          style: CommonStyle.getAppFont(
                              color: CommonColors.getWhiteTextColor(
                                  context),
                              fontSize: 20,
                              fontWeight: FontWeight.w500),
                        ),
                        Text(
                          S.of(context).tansNo,
                          style: CommonStyle.getAppFont(
                              color: CommonColors.gradientColorBottom,
                              fontSize: 20,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Lorem ipsum dolor sit amet consectetur adipiscing elit est feugiat convallis aliquam, inceptos viverra consequat.",
                      textAlign: TextAlign.center,
                      style: CommonStyle.getAppFont(
                          color: CommonColors.getWhiteTextColor(context),
                          fontSize: 18,
                          fontWeight: FontWeight.normal),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:local_auth/local_auth.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/about_us/about_us_view.dart';
import 'package:sendcash_flutter/app_ui/app/app_model.dart';

import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';

import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/globals.dart';

import '../../models/crypto_list_master.dart';


class SettingViewModel with ChangeNotifier {
  AppPreferences appPreferences = new AppPreferences();
  LoginDetails loginDetails;
  BuildContext mContext;
  String userId;
  List<CryptoListDetails>  currencyList = [];
  CryptoListDetails selectedCurrency;
  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();
  ApiServices apiServices = ApiServices();

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    currencyList.clear();
    loginDetails = await appPreferences.getLoginDetails();
    userId = await appPreferences.getUserFirebaseId();
    notifyListeners();
    getAllCurrencyList();
  }

  void onChangeCurrency(CryptoListDetails value){
    selectedCurrency = value;
    Map<String,dynamic>  mapParams = Map();
    mapParams[DbKey.DEFAULT_CURRENCY] = value.currencyCode;
    mapParams[DbKey.CURRENCY_SYMBOL] = value.symbol;
    mapParams[DbKey.IS_DARK_THEME] = Provider.of<AppModel>(mContext,listen: false).darkTheme;
    Globals.currencyName = value.currencyCode;
    Globals.currencySymbol = value.symbol;
    updateUser(userData:mapParams,updateType: "currency");
    notifyListeners();
  }

  Future<void> updateUser({Map<String,dynamic>  userData,String updateType}) async {
    CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.updateUserAppSettings(
        data : {
          DbKey.USER_APP_SETTINGS:userData
        },
        userId: userId,
        onError: (error, stackTrace) {
          CommonUtils.showRedToastMessage("failed to update profile pic");
          CommonUtils.hideProgressDialog(mContext);
        },
        onSuccess: (value) {
          getUserDetails();
          if(updateType == "theme"){
            CommonUtils.showGreenToastMessage("Theme changed successfully!");
          }else if(updateType == "home_graph"){
            CommonUtils.showGreenToastMessage("Home dashboard updated successfully!");
          }else{
            CommonUtils.showGreenToastMessage("Currency updated successfully!");

          }
          CommonUtils.showGreenToastMessage("Setting updated successfully!");
        }).onError((error, stackTrace) {
      notifyListeners();
      CommonUtils.showRedToastMessage("failed to update profile");
    });
    notifyListeners();
  }

  void getUserDetails() async {
    await firebaseDatabaseHelper.getUserDetails(
        userId: userId,
        onSuccess: (documentReference) async {
          Map<String, dynamic> data =
          documentReference.data() as Map<String, dynamic>;
          await appPreferences.setLoginDetails(jsonEncode(data));
          Provider.of<AppModel>(mContext,listen: false).applyChanges();
          CommonUtils.hideProgressDialog(mContext);
          },
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage(
              "Failed to get user details " + error.toString());
        });
  }




  void getAllCurrencyList() async{
    CommonUtils.showProgressDialog(mContext);
    CryptoListMaster cryptoListMaster = await  apiServices.getAllCurrencies(
        onFailed: (){
          CommonUtils.hideProgressDialog(mContext);
        });
    CommonUtils.hideProgressDialog(mContext);
    if(cryptoListMaster != null){
      currencyList.addAll(cryptoListMaster.result);
      if(loginDetails.userAppSettings != null){
        try {
          if(loginDetails.userAppSettings.currencySymbol != null && loginDetails.userAppSettings.currencySymbol.isNotEmpty){
            selectedCurrency = currencyList.firstWhere((element) {
              return element.currencyCode == loginDetails.userAppSettings.defaultCurrency;
            });
          }
        } on Exception catch (e) {
          // TODO
          selectedCurrency = currencyList[0];
        }
      }
      notifyListeners();
    }else{
      CommonUtils.showRedToastMessage("Failed to get message");
    }
  }


}

class CurrencyModel{
  String currencyName;
  String currencySymbol;

  CurrencyModel(this.currencyName, this.currencySymbol);
}



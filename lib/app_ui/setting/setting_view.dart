import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/app/app_model.dart';
import 'package:sendcash_flutter/app_ui/change_password/change_password_view.dart';
import 'package:sendcash_flutter/app_ui/edit_profile/edit_profile_view.dart';
import 'package:sendcash_flutter/app_ui/linked_accounts/linked_accounts_view.dart';
import 'package:sendcash_flutter/app_ui/notifications/notification_view.dart';
import 'package:sendcash_flutter/app_ui/setting/setting_view_model.dart';
import 'package:sendcash_flutter/common_widgets/appbar/menu_appbar.dart';
import 'package:sendcash_flutter/common_widgets/custom_switch/toggle_switch.dart';
import 'package:sendcash_flutter/common_widgets/custom_switch/toggle_switch_with_text.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/crypto_list_master.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class SettingView extends StatefulWidget {
  final GlobalKey<ScaffoldState> navScaffoldKey;
  SettingView({this.navScaffoldKey});

  @override
  _SettingViewState createState() => _SettingViewState();
}

class _SettingViewState extends State<SettingView> {
  SettingViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<SettingViewModel>(context);

    if (mViewModel.loginDetails == null) {
      return Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    final mainCurrency = mViewModel.selectedCurrency == null
        ? Container()
        : Container(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Container(
                        margin: EdgeInsets.only(top: 10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              S.of(context).maincurrency,
                              textAlign: TextAlign.start,
                              style: CommonStyle.getAppFont(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  color: CommonColors.getWhiteTextColor(
                                      context)),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 8),
                              child: Text(
                                S.of(context).usDollars,
                                textAlign: TextAlign.start,
                                style: CommonStyle.getAppFont(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 11,
                                    color: CommonColors.getWhiteTextColor(
                                        context)),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    mViewModel.selectedCurrency == null
                        ? Container()
                        : Container(
                            height: 35,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: CommonColors.getWhiteTextColor(
                                      context)
                                        .withOpacity(.5)),
                                borderRadius: BorderRadius.circular(40.0)),
                            padding: EdgeInsets.all(8.0),
                            margin: EdgeInsets.only(top: 12.0),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                value: mViewModel.selectedCurrency,
                                icon: Icon(
                                  Icons.keyboard_arrow_down,
                                  color: CommonColors.getWhiteTextColor(
                                          context)
                                      .withOpacity(.5),
                                  size: 20,
                                ),
                                dropdownColor: CommonColors.getDropdownColor(
                                    context),
                                items: mViewModel.currencyList
                                    .map((CryptoListDetails items) {
                                  return DropdownMenuItem(
                                      value: items,
                                      child: Text(
                                        items.currencyCode,
                                        style: TextStyle(
                                            color:
                                                CommonColors.getWhiteTextColor(
                                                     context),
                                            fontSize: 13),
                                      ));
                                }).toList(),
                                onChanged: mViewModel.onChangeCurrency,
                              ),
                            ),
                          ),
                  ],
                ),
                Container(
                  height: 1,
                  margin: EdgeInsets.only(top: 18, bottom: 10),
                  color: CommonColors.getWhiteTextColor(context)
                      .withOpacity(.2),
                ),
              ],
            ),
          );
    final changeTheme = Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Container(
                  margin: EdgeInsets.only(top: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        S.of(context).changeTheme,
                        textAlign: TextAlign.start,
                        style: CommonStyle.getAppFont(
                            fontWeight: FontWeight.w600,
                            fontSize: 16,
                            color: CommonColors.getWhiteTextColor(context)),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 8),
                        child: Text(
                        Provider.of<AppModel>(context,listen: false).darkTheme ? S.of(context).darkMode :   S.of(context).lightMode,
                          textAlign: TextAlign.start,
                          style: CommonStyle.getAppFont(
                              fontWeight: FontWeight.w500,
                              fontSize: 11,
                              color: CommonColors.getWhiteTextColor(
                                 context)),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: SwitchWithText(
                  value:
                      !Provider.of<AppModel>(context, listen: false).darkTheme,
                  onChanged: (value) {
                    setState(() {
                      Provider.of<AppModel>(context, listen: false)
                          .updateTheme(!value);
                      Map<String, dynamic> mapParams = Map();
                      mapParams[DbKey.IS_DARK_THEME] =
                          Provider.of<AppModel>(context, listen: false)
                              .darkTheme;
                      mapParams[DbKey.CURRENCY_SYMBOL] =
                          mViewModel.selectedCurrency.symbol;
                      mapParams[DbKey.DEFAULT_CURRENCY] =
                          mViewModel.selectedCurrency.currencyCode;
                      mViewModel.updateUser(userData: mapParams,updateType: "theme");
                    });
                  },
                ),
              ),
            ],
          ),
          Container(
            height: 1,
            margin: EdgeInsets.only(top: 18, bottom: 10),
            color: CommonColors.getWhiteTextColor(context)
                .withOpacity(.2),
          ),
        ],
      ),
    );


    final mainBody = Container(
      color: Theme.of(context).backgroundColor,
      margin: EdgeInsets.only(top: 150.0),
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: SingleChildScrollView(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            setRow(
                text: S.of(context).editProfile,
                hintText: S.of(context).editYourPersonal,

                onclick: () {
                  Navigator.push(context,
                      CupertinoPageRoute(builder: (context) {
                    return EditProfileView();
                  }));
                }),
            mainCurrency,
            setRow(
                text: S.of(context).changePassword,
                hintText: S.of(context).unchangedSince +
                    " " +
                    ((mViewModel.loginDetails.changePasswordDate == null ||
                            mViewModel.loginDetails.changePasswordDate.isEmpty)
                        ? "NA"
                        : DateFormat("dd/MM/yyyy").format(
                            DateFormat("dd/MM/yyyy HH:mm:ss").parse(
                                mViewModel.loginDetails.changePasswordDate))),
                onclick: () {
                  Navigator.push(context,
                      CupertinoPageRoute(builder: (context) {
                    return ChangePasswordView();
                  }));
                }),
            changeTheme,
            setRow(
                text: S.of(context).privacySettings,
                hintText: S.of(context).controlYourPrivacy),
           /* dashboards,*/
            setRow(
                text: S.of(context).linkedAccounts,
                hintText: S.of(context).editYourAccounts,onclick: (){
                  Navigator.push(context, CupertinoPageRoute(builder:(context){
                    return LinkedAccountsView();
                  }));
            }),
          ],
        ),
      ),
    );
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Stack(
        children: [
          MenuAppbar(
            height: 153.0,
            title: S.of(context).settings,
            onMenuClick: (){
              widget.navScaffoldKey.currentState.openDrawer();
            },
            onNotificationClick: () {
              Navigator.push(context, CupertinoPageRoute(builder: (context) {
                return NotificationsView();
              }));
            },
          ),
          Container(
            margin: EdgeInsets.only(top: 150.0),
            padding: EdgeInsets.only(left: 20.0, right: 20.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(0.0, 0.0))
                ]),
            clipBehavior: Clip.antiAlias,
          ),
          mainBody
        ],
      ),
    );
  }

  Widget setRow({text, hintText, onclick}) {
    return InkWell(
      onTap: onclick,
      child: Container(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Container(
                    margin: EdgeInsets.only(top: 10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          text,
                          textAlign: TextAlign.start,
                          style: CommonStyle.getAppFont(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: CommonColors.getWhiteTextColor(
                                  context)),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 8),
                          child: Text(
                            hintText,
                            textAlign: TextAlign.start,
                            style: CommonStyle.getAppFont(
                                fontWeight: FontWeight.w500,
                                fontSize: 11,
                                color: CommonColors.getWhiteTextColor(context)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Icon(
                    Icons.arrow_forward_ios,
                    color: CommonColors.getWhiteTextColor(context)
                        .withOpacity(.5),
                    size: 16,
                  ),
                ),
              ],
            ),
            Container(
              height: 1,
              margin: EdgeInsets.only(top: 18, bottom: 10),
              color: CommonColors.getWhiteTextColor(context)
                  .withOpacity(.2),
            ),
          ],
        ),
      ),
    );
  }
}

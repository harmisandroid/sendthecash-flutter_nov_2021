import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:sendcash_flutter/app_ui/transfer_funds/transfer_funds_view.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

import '../../utils/common_colors.dart';

class ScanQrCodeView extends StatefulWidget {
  @override
  _ScanQrCodeViewState createState() => _ScanQrCodeViewState();
}

class _ScanQrCodeViewState extends State<ScanQrCodeView> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode result;
  QRViewController controller;

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller.pauseCamera();
    } else if (Platform.isIOS) {
      controller.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.center,
        child: result == null
            ? Stack(
                children: <Widget>[
                  QRView(
                    key: qrKey,
                    overlay: QrScannerOverlayShape(
                        borderColor: Colors.white,
                        borderRadius: 20,
                        borderLength: 47,
                        borderWidth: 4,
                        cutOutSize: MediaQuery.of(context).size.width * 0.8),
                    onQRViewCreated: _onQRViewCreated,
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child:Container(
                      margin: EdgeInsets.only(bottom: MediaQuery.of(context).viewPadding.bottom + 50),
                      child: Text("Scan QR Code",style: CommonStyle.getAppFont(
                          color: Colors.white,
                          fontSize: 20.0
                      ),),
                    ),
                  ),
                  Positioned(
                      top: 25.0,
                      left: 20.0,
                      child: InkWell(
                        child: Icon(
                          Icons.close,
                          color: Colors.white,
                          size: 30.0,
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      )),
                ],
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 40.0,
                  ),
                  Text(
                    result.code,
                    textAlign: TextAlign.center,
                    style: CommonStyle.getAppFont(
                        fontWeight: FontWeight.w500,
                        fontSize: 15.0,
                        color: Colors.black),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        result = null;
                      });
                    },
                    child: Text(
                      "Scan Again",
                      textAlign: TextAlign.center,
                      style: CommonStyle.getAppFont(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                          color: Colors.black),
                    ),
                  )
                ],
              ),
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      Navigator.pushReplacement(context, CupertinoPageRoute(builder:(context){
        return TransferFundsView();
      }));
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}

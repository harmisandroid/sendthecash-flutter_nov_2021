import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/app_ui/add_funds/bottom_sheet_otp_view.dart';
import 'package:sendcash_flutter/app_ui/app_webview/webview.dart';
import 'package:sendcash_flutter/app_ui/success/order_success.dart';
import 'package:sendcash_flutter/app_ui/success/success_view.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/user_card_details.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/params/wallet_order_reserve.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/local_images.dart';

import '../../utils/common_utils.dart';

class AddFundsViewModel with ChangeNotifier {
  BuildContext mContext;
  List<PaymentModel> paymentList = [];
  List<DestCurrencyModel> cryptoList = [];
  DestCurrencyModel selectedCrypto;
  ApiServices apiServices = ApiServices();
  Map<String, dynamic> cryptoMap = Map();
  LoginDetails loginDetails;
  AppPreferences appPreferences = AppPreferences();
  String transferAddress = null;
  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();
  String firebaseUserId;
  bool isEmptyList = false;
  List<CardDetails> userCardList = [];
  int selectedCardIndex = 0;
  String ipAddress;
  String sendwyreAccountId;
  List<NetworkInterface> ipList;

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    paymentList.clear();
    cryptoList.clear();
    userCardList.clear();
    paymentList
        .add(PaymentModel(S.of(context).debitCreditCards, LocalImages.ic_card));
    paymentList
        .add(PaymentModel(S.of(context).applePay, LocalImages.ic_apple_pay));
    paymentList
        .add(PaymentModel(S.of(context).netBanking, LocalImages.ic_bank));
    cryptoList.add(DestCurrencyModel("BTC", "bitcoin"));
    cryptoList.add(DestCurrencyModel("MATIC", "matic"));
    cryptoList.add(DestCurrencyModel("AVAX", "avalanche"));
    cryptoList.add(DestCurrencyModel("ETH", "ethereum"));
    cryptoList.add(DestCurrencyModel("XLM", "stellar"));
    selectedCrypto = cryptoList[0];
    loginDetails = await appPreferences.getLoginDetails();
    firebaseUserId = await appPreferences.getUserFirebaseId();
    ipList = await NetworkInterface.list();
    ipAddress = ipList[0].addresses[0].address;
    getSendwyreAccountDetails(accountId: loginDetails.sendwyreAccountId);
    getUserCardList();
    notifyListeners();
  }

  void onCryptoChange(DestCurrencyModel value) {
    selectedCrypto = value;
    if (cryptoMap != null) {
      transferAddress =
          selectedCrypto.addressCode + ":" + cryptoMap[selectedCrypto.name];
    }

    notifyListeners();
  }

  Future<void> getSendwyreAccountDetails({String accountId}) async {
    final response = await apiServices.getSendwyreAccountDetails(
        accountId: accountId,
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("failed to get account details");
        });
    CommonUtils.hideProgressDialog(mContext);
    if (response != null) {
      var mapResponse = jsonDecode(response);
      cryptoMap = mapResponse['depositAddresses'];
      transferAddress =
          selectedCrypto.addressCode + ":" + cryptoMap[selectedCrypto.name];
      sendwyreAccountId = mapResponse['id'];
    } else {
      CommonUtils.showRedToastMessage("failed to get account details");
    }
  }

  Future<void> createWalletOrderResrve(
      {WalletOrderReserve walletOrderReserve}) async {
    CommonUtils.showProgressDialog(mContext);
    String response = await apiServices.createWalletOrderReserve(
        params: jsonEncode(walletOrderReserve),
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
        });
    if (response != null) {

      Map<String, dynamic> jsonMap = jsonDecode(response);
      if(jsonMap['reservation'] != null){
        walletOrderReserve.reservationId = jsonMap['reservation'];
        walletOrderReserve.ipAddress = ipAddress ?? "1.1.1.1";
        walletOrderReserve.givenName = loginDetails.firstName;
        createOrder(walletOrderReserve: walletOrderReserve);
      }else{
        CommonUtils.showRedToastMessage("Failed to create reserve order");
        CommonUtils.hideProgressDialog(mContext);
      }

    } else {
      CommonUtils.hideProgressDialog(mContext);
    }
  }

  Future<void> createOrder({WalletOrderReserve walletOrderReserve}) async {
    String response = await apiServices.createWalletOrder(
        params: jsonEncode(walletOrderReserve),
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("Failed to create order");
        });

    if (response != null) {
      Map<String, dynamic> orderMap = Map();
      orderMap = jsonDecode(response);
      if (orderMap['id'] != null) {
       Future.delayed(Duration(seconds: 5),(){
         getOrderAuthorization(
             walletOrderReserve: walletOrderReserve, orderId: orderMap['id']);
       });
      } else {
        CommonUtils.hideProgressDialog(mContext);
        CommonUtils.showRedToastMessage("Failed to create order");
      }
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showRedToastMessage("Failed to create order");
    }
  }

  Future<void> getOrderAuthorization(
      {WalletOrderReserve walletOrderReserve, String orderId}) async {
    String response = await apiServices.getOrderAuthorization(
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("Failed to create order");
        },
        orderId: orderId);
    if (response != null) {
      Map<String, dynamic> authMap = Map();
      authMap = jsonDecode(response);
      if (authMap["walletOrderId"] != null) {
        if (authMap['smsNeeded'] != null && authMap['smsNeeded']) {
          CommonUtils.hideProgressDialog(mContext);
          await showOtpView(
              orderId: orderId,
              walletOrderReserve: walletOrderReserve,
              onResponse: (value) {
                Map<String, dynamic> authMap = jsonDecode(value);
                if (authMap['success'] != null && authMap['success']) {
                  Navigator.pop(mContext);
                  Navigator.pushAndRemoveUntil(mContext,
                      CupertinoPageRoute(builder: (context) {
                    return OrderSuccessView(
                      transactionId: orderId,
                      orderId: orderId,
                    );
                  }), (route) => false);
                } else {
                  CommonUtils.showRedToastMessage("failed to place your order");
                }
              });
        } else {
          getOrderAuthorization(
              walletOrderReserve: walletOrderReserve, orderId: orderId);
        }
      } else {
        CommonUtils.hideProgressDialog(mContext);
        CommonUtils.showRedToastMessage("Failed to create order");
      }
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showRedToastMessage("Failed to create order");
    }
  }

  void getUserCardList() async {
    userCardList.clear();
    CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.getUserCards(
        userId: firebaseUserId,
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage(error.toString());
        },
        onSuccess: (QuerySnapshot<Map<String, dynamic>> querySnapshot) {
          CommonUtils.hideProgressDialog(mContext);
          if (querySnapshot.size > 0) {
            isEmptyList = false;
            querySnapshot.docs.forEach((element) {
              userCardList.add(CardDetails.fromJson(element.data()));
            });
          } else {
            isEmptyList = true;
          }
          notifyListeners();
        });
  }

  void changeCardIndex(int index) {
    selectedCardIndex = index;
    notifyListeners();
  }

  Future<void> showOtpView({walletOrderReserve, orderId, Function(String) onResponse}) async {
    await showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Theme.of(mContext).backgroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        context: mContext,
        builder: (context) {
          return BottomSheetOtpView(walletOrderReserve, orderId, onResponse);
        });
  }
}

class PaymentModel {
  String paymentName;
  String payIcon;

  PaymentModel(this.paymentName, this.payIcon);
}

class DestCurrencyModel {
  String name;
  String addressCode;

  DestCurrencyModel(this.name, this.addressCode);
}

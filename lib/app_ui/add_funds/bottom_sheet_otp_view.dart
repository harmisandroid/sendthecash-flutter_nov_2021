import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:sendcash_flutter/params/submit_auth_request.dart';
import 'package:sendcash_flutter/params/wallet_order_reserve.dart';
import 'package:sendcash_flutter/services/api_services.dart';

import '../../common_widgets/gradient_button/gradientbutton.dart';
import '../../generated/i18n.dart';
import '../../utils/common_colors.dart';
import '../../utils/common_utils.dart';
import '../../utils/text_style.dart';

class BottomSheetOtpView extends StatefulWidget {
  WalletOrderReserve walletOrderReserve;
  String orderId;
  Function(String)  onResponse;
  BottomSheetOtpView(this.walletOrderReserve, this.orderId, this.onResponse);

  @override
  _BottomSheetOtpViewState createState() => _BottomSheetOtpViewState();
}

class _BottomSheetOtpViewState extends State<BottomSheetOtpView> {
  // final otpController = TextEditingController();
  // final bankOtpController = TextEditingController();
  ApiServices apiServices = ApiServices();
  StreamController<ErrorAnimationType> errorController;
  bool hasError = false;
  String otpCode = "",bankOtpCode= "";
  bool isApiLoading = false;


  @override
  Widget build(BuildContext context) {
    // if(isApiLoading){
    //   return Container(
    //     padding: EdgeInsets.all(20.0),
    //     height: 200.0,
    //     child: Column(
    //       mainAxisAlignment: MainAxisAlignment.center,
    //       children: [
    //         CircularProgressIndicator(color: CommonColors.primaryColor,),
    //         SizedBox(height: 20.0,),
    //         Text("Placing your order.....",style: CommonStyle.getAppFont(
    //             color: CommonColors.getWhiteTextColor(context),
    //             fontWeight: FontWeight.bold,
    //             fontSize: 18.0
    //         ))
    //       ],
    //     ),
    //   );
    // }

    return Container(
      padding: EdgeInsets.all(20.0),
      height: 340.0 + MediaQuery.of(context).viewInsets.bottom,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(S.of(context).enterSixDigitOtp,style: CommonStyle.getAppFont(
            color: CommonColors.getWhiteTextColor(context),
            fontWeight: FontWeight.w500,
            fontSize: 18.0
          ),),
          SizedBox(height: 20.0,),
          Padding(
              padding: const EdgeInsets.only(
                  left: 20.0, right: 20.0),
              child: PinCodeTextField(
                appContext: context,
                pastedTextStyle: TextStyle(
                  color: CommonColors.getWhiteTextColor(context),
                  fontWeight: FontWeight.bold,
                ),
                length: 6,
                hintCharacter: "0",
                hintStyle:TextStyle(
                  color: CommonColors.getHintTextStyle(context),
                  fontWeight: FontWeight.normal,
                ),
                textStyle: CommonStyle.getAppFont(
                    color: CommonColors.getWhiteTextColor(context),
                    fontSize: 22.0,
                    fontWeight: FontWeight.normal),
                obscureText: false,
                blinkWhenObscuring: true,
                animationType: AnimationType.fade,
                validator: (v) {},
                pinTheme: PinTheme(
                  shape: PinCodeFieldShape.box,
                  borderWidth: 1,
                  fieldHeight: 40,
                  fieldWidth: 40,
                  activeFillColor:
                  CommonColors.getDropdownColor(context),
                  inactiveColor:
                  CommonColors.getWhiteTextColor(context),
                  selectedFillColor:
                  CommonColors.getDropdownColor(context),
                  inactiveFillColor:
                  CommonColors.getDropdownColor(context),
                  activeColor:
                  CommonColors.getWhiteTextColor(context),
                  selectedColor: CommonColors.getWhiteTextColor(context),
                ),
                cursorColor: CommonColors.getWhiteTextColor(context),
                /*animationDuration: Duration(milliseconds: 300),*/
                enableActiveFill: true,
                errorAnimationController: errorController,

                keyboardType: TextInputType.number,
                onCompleted: (v) {
                  otpCode = v;
                },
                onChanged: (value) {
                  printf(value);
                  setState(() {
                    otpCode = value;
                  });
                },
                beforeTextPaste: (text) {
                  printf("Allowing to paste $text");
                  //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                  //but you can show anything you want here, like your pop up saying wrong paste format or etc
                  otpCode = text;
                  return true;
                },
              )),
          SizedBox(height: 20.0,),
          Text(S.of(context).bankAuthenticationCode,style: CommonStyle.getAppFont(
              color: CommonColors.getWhiteTextColor(context),
              fontWeight: FontWeight.w500,
              fontSize: 18.0
          ),),
          SizedBox(height: 20.0,),
          Padding(
              padding: const EdgeInsets.only(
                  left: 20.0, right: 20.0),
              child: PinCodeTextField(
                appContext: context,
                pastedTextStyle: TextStyle(
                  color: CommonColors.getWhiteTextColor(context),
                  fontWeight: FontWeight.bold,
                ),
                length: 6,
                hintCharacter: "0",
                textStyle: CommonStyle.getAppFont(
                    color: CommonColors.getWhiteTextColor(context),
                    fontSize: 22.0,
                    fontWeight: FontWeight.normal),
                obscureText: false,
                blinkWhenObscuring: true,
                animationType: AnimationType.fade,
                validator: (v) {},
                pinTheme: PinTheme(
                  shape: PinCodeFieldShape.box,
                  borderWidth: 1,
                  fieldHeight: 40,
                  fieldWidth: 40,
                  activeFillColor:
                  CommonColors.getDropdownColor(context),
                  inactiveColor:
                  CommonColors.getWhiteTextColor(context),
                  selectedFillColor:
                  CommonColors.getDropdownColor(context),
                  inactiveFillColor:
                  CommonColors.getDropdownColor(context),
                  activeColor:
                  CommonColors.getWhiteTextColor(context),
                  selectedColor: CommonColors.getWhiteTextColor(context),
                ),
                cursorColor: CommonColors.getWhiteTextColor(context),
                /*animationDuration: Duration(milliseconds: 300),*/
                enableActiveFill: true,
                errorAnimationController: errorController,

                keyboardType: TextInputType.number,
                onCompleted: (v) {
                  bankOtpCode = v;
                },
                onChanged: (value) {
                  printf(value);
                  setState(() {
                    bankOtpCode = value;
                  });
                },
                beforeTextPaste: (text) {
                  printf("Allowing to paste $text");
                  //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                  //but you can show anything you want here, like your pop up saying wrong paste format or etc
                  bankOtpCode = text;
                  return true;
                },
              )),
          SizedBox(height: 20.0,),
          GradientButton(
            btnText: S.of(context).submit,
            onClick: () {
              if(bankOtpCode.isNotEmpty && bankOtpCode.length == 6 && otpCode.isNotEmpty && otpCode.length == 6){
                 submitAuth();
               }else{
               CommonUtils.showRedToastMessage(S.of(context).pleaseEnterValidOtp);
              }
            },
          ),
          SizedBox(height: 20.0,),
        ],
      ),
    );
  }

  void submitAuth() async {
    SubmitAuthRequest submitAuthRequest = SubmitAuthRequest();
    submitAuthRequest.type = "ALL";
    submitAuthRequest.card2fa = bankOtpCode;
    submitAuthRequest.smsCode = otpCode;
    submitAuthRequest.reservation = widget.walletOrderReserve.reservationId;
    submitAuthRequest.walletOrderId = widget.orderId;
    String response = await apiServices.submitAuthorization(params:jsonEncode(submitAuthRequest),onFailed:(){
      CommonUtils.showRedToastMessage("Failed to submit authorization");
    });
    if(response != null){
      widget.onResponse(response);
    }else{
      CommonUtils.showRedToastMessage("Invalid otp details");
    }
  }

  Future<void> getOrderAuthorization({WalletOrderReserve walletOrderReserve, String orderId}) async {
    String response = await apiServices.getOrderAuthorization(
        onFailed: () {
          CommonUtils.showRedToastMessage("Failed to create order");
        },
        orderId: orderId);


    if (response != null) {
      Map<String, dynamic> authMap = Map();
      authMap = jsonDecode(response);
      if (authMap["walletOrderId"] != null) {

      } else {
        CommonUtils.showRedToastMessage("Failed to create order");
      }
    } else {
    CommonUtils.showRedToastMessage("Failed to create order");
    }
  }
}

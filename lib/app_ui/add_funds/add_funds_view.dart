import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/add_bank_account/add_bank_account.dart';
import 'package:sendcash_flutter/app_ui/add_credit_card/add_credit_card.dart';
import 'package:sendcash_flutter/app_ui/add_funds/add_funds_view_model.dart';
import 'package:sendcash_flutter/app_ui/add_funds/card_item.dart';
import 'package:sendcash_flutter/app_ui/add_funds/payment_item.dart';
import 'package:sendcash_flutter/app_ui/success/success_view.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/gradient_button/gradientbutton.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/params/wallet_order_reserve.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/currency_formatter.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class AddFundsView extends StatefulWidget {
  const AddFundsView({Key key}) : super(key: key);

  @override
  _AddFundsViewState createState() => _AddFundsViewState();
}

class _AddFundsViewState extends State<AddFundsView> {
  AddFundsViewModel mViewModel;
  final mAmountController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<AddFundsViewModel>(context);

    final bankDropdown = mViewModel.selectedCrypto == null
        ? Container()
        : Container(
            padding: EdgeInsets.only(top: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  S.of(context).transferTo,
                  style: CommonStyle.getAppFont(
                      color: CommonColors.getWhiteTextColor(context),
                      fontWeight: FontWeight.w500,
                      fontSize: 20.0),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  decoration: BoxDecoration(
                      color: Theme.of(context).scaffoldBackgroundColor,
                      borderRadius: BorderRadius.circular(30.0),
                      border: Border.all(
                          color: CommonColors.getFieldBorderColor(context),
                          width: 1.0)),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<DestCurrencyModel>(
                      isExpanded: true,
                      dropdownColor: CommonColors.getDropdownColor(context),
                      value: mViewModel.selectedCrypto,
                      hint: Text(
                        S.of(context).selectBank,
                        style: CommonStyle.getAppFont(
                            color: CommonColors.getWhiteTextColor(context),
                            fontWeight: FontWeight.w500,
                            fontSize: 14.0),
                      ),
                      icon: Icon(
                        Icons.keyboard_arrow_down,
                        color: CommonColors.getWhiteTextColor(context),
                        size: 25,
                      ),
                      style: TextStyle(
                          color: CommonColors.getWhiteTextColor(context)),
                      underline: Container(
                        height: 2,
                        color: CommonColors.getFieldBorderColor(context),
                      ),
                      onChanged: mViewModel.onCryptoChange,
                      items: mViewModel.cryptoList
                          .map<DropdownMenuItem<DestCurrencyModel>>(
                              (DestCurrencyModel value) {
                        return DropdownMenuItem<DestCurrencyModel>(
                          value: value,
                          child: Text(
                            value.name,
                            style: CommonStyle.getAppFont(
                                color: CommonColors.getWhiteTextColor(context),
                                fontWeight: FontWeight.w500,
                                fontSize: 14.0),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                ),
              ],
            ));

    return GestureDetector(
      onTap: () {
        // FocusManager.instance.primaryFocus.unfocus();
        SystemChannels.textInput.invokeMethod('TextInput.hide');
      },
      child: Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        body: Stack(
          children: [
            GAppbar(
              title: S.of(context).addFunds,
              height: 130.0,
            ),
            Container(
              height: MediaQuery.of(context).size.height,
              margin: EdgeInsets.only(top: 100.0, left: 20.0, right: 20.0),
              padding: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  borderRadius: BorderRadius.circular(20.0),
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 25.0,
                        blurRadius: 25.0,
                        color: Theme.of(context).shadowColor,
                        offset: Offset(0.0, 30.0))
                  ]),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Align(
                      alignment: AlignmentDirectional.centerStart,
                      child: Text(
                        S.of(context).addFundsToWallet,
                        textAlign: TextAlign.start,
                        style: CommonStyle.getAppFont(
                            color: CommonColors.getWhiteTextColor(context),
                            fontWeight: FontWeight.w500,
                            fontSize: 20.0),
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Container(
                      height: 50.0,
                      width: MediaQuery.of(context).size.width,
                      child: TextFormField(
                        controller: mAmountController,
                        style: CommonStyle.getAppFont(
                            color: CommonColors.getWhiteTextColor(context),
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500),
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly,
                          CurrencyInputFormatter()
                        ],
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 10.0),
                            hintStyle: CommonStyle.getAppFont(
                                color: CommonColors.getWhiteTextColor(context)),
                            hintText: "0.00",
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                    width: 1,
                                    color: CommonColors.getFieldBorderColor(
                                        context))),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                borderSide: BorderSide(
                                    width: 1,
                                    color: CommonColors.getFieldBorderColor(
                                        context))),
                            prefixIcon: Container(
                              height: 60.0,
                              width: 50.0,
                              alignment: Alignment.center,
                              padding: EdgeInsetsDirectional.only(start: 10.0),
                              child: Text(
                                Globals.currencySymbol,
                                style: CommonStyle.getAppFont(
                                    color: CommonColors.primaryColor,
                                    fontWeight: FontWeight.w900,
                                    fontSize: 20.0),
                              ),
                            )),
                      ),
                    ),
                    bankDropdown,
                    SizedBox(
                      height: 30.0,
                    ),
                    Align(
                      alignment: AlignmentDirectional.centerStart,
                      child: Text(
                        S.of(context).selectDebitCreditcards,
                        textAlign: TextAlign.start,
                        style: CommonStyle.getAppFont(
                            color: CommonColors.getWhiteTextColor(context),
                            fontWeight: FontWeight.w500,
                            fontSize: 20.0),
                      ),
                    ),
                    Container(
                      child: mViewModel.isEmptyList
                          ? Container(
                              width: MediaQuery.of(context).size.width,
                              height: 100.0,
                              child: Center(
                                child: InkWell(
                                  onTap: () async {
                                    bool isAdded = await Navigator.push(context,
                                        CupertinoPageRoute(builder: (context) {
                                      return AddCreditCardView();
                                    }));

                                    if (isAdded != null && isAdded) {
                                      mViewModel.getUserCardList();
                                    }
                                  },
                                  child: Text(
                                    S.of(context).addNewCard,
                                    style: CommonStyle.getAppFont(
                                        color: CommonColors.getWhiteTextColor(
                                            context)),
                                  ),
                                ),
                              ),
                            )
                          : mViewModel.userCardList.length > 0
                              ? ListView.builder(
                                  padding: EdgeInsets.only(
                                      top: 10.0, left: 0.0, bottom: 10.0),
                                  shrinkWrap: true,
                                  itemCount: mViewModel.userCardList.length,
                                  physics: ClampingScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    return CardItem(
                                      index: index,
                                      selectedIndex:
                                          mViewModel.selectedCardIndex,
                                      cardDetails:
                                          mViewModel.userCardList[index],
                                      onChange: (value) {
                                        mViewModel.changeCardIndex(index);
                                      },
                                    );
                                  })
                              : Container(),
                    ),
                    SizedBox(height: 10.0,),
                    GradientButton(
                      btnText: S.of(context).addFunds,
                      onClick: () {
                        if (isValidData()) {
                          WalletOrderReserve walletOrderreserve =
                              new WalletOrderReserve();
                          DebitCard debitCard = new DebitCard();
                          debitCard.number = mViewModel
                              .userCardList[mViewModel.selectedCardIndex].cardNo
                              .replaceAll("-", "");
                          debitCard.year = "20" +
                              mViewModel
                                  .userCardList[mViewModel.selectedCardIndex]
                                  .expiryDate
                                  .split("/")[1];
                          debitCard.month = mViewModel
                              .userCardList[mViewModel.selectedCardIndex]
                              .expiryDate
                              .split("/")[0];
                          debitCard.cvv = mViewModel
                              .userCardList[mViewModel.selectedCardIndex].cvv;
                          walletOrderreserve.debitCard = debitCard;
                          walletOrderreserve.amount = mAmountController.text;
                          walletOrderreserve.sourceCurrency = "USD";
                          walletOrderreserve.destCurrency =
                              mViewModel.selectedCrypto.name;
                          walletOrderreserve.dest = mViewModel.transferAddress;
                          walletOrderreserve.referrerAccountId =
                              AppConstants.SENDWYRE_ACCOUNT_ID;
                          walletOrderreserve.givenName =
                              mViewModel.loginDetails.firstName;
                          walletOrderreserve.familyName =
                              mViewModel.loginDetails.lastName;
                          walletOrderreserve.firstName =
                              mViewModel.loginDetails.firstName;
                          walletOrderreserve.lastName =
                              mViewModel.loginDetails.lastName;
                          walletOrderreserve.email =
                              mViewModel.loginDetails.email;
                          walletOrderreserve.phone =
                              mViewModel.loginDetails.phoneCode +
                                  mViewModel.loginDetails.mobileNo;
                          walletOrderreserve.referenceId =
                              mViewModel.loginDetails.sendwyreAccountId;
                          walletOrderreserve.redirectUrl =
                              "https://www.sendwyre.com/success";
                          walletOrderreserve.failureUrl =
                              "https://www.sendwyre.com/failure";
                          walletOrderreserve.paymentMethod = "debit-card";
                          Address address = new Address();
                          address.street1 =
                              mViewModel.loginDetails.address.addressLine1;
                          address.city = mViewModel.loginDetails.address.city;
                          address.state = mViewModel.loginDetails.address.state;
                          address.postalCode =
                              mViewModel.loginDetails.address.zipcode;
                          address.country = mViewModel.loginDetails.countryCode;
                          walletOrderreserve.address = address;
                          mViewModel.createWalletOrderResrve(
                              walletOrderReserve: walletOrderreserve);
                        }

                      },
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Align(
                      alignment: AlignmentDirectional.centerStart,
                      child: Text(
                        S.of(context).newPaymentOptions,
                        textAlign: TextAlign.start,
                        style: CommonStyle.getAppFont(
                            color: CommonColors.getWhiteTextColor(context),
                            fontWeight: FontWeight.w500,
                            fontSize: 20.0),
                      ),
                    ),
                    Container(
                      height: 380.0,
                      child: ListView.builder(
                        padding: EdgeInsets.all(0.0),
                          shrinkWrap: true,
                          itemCount: mViewModel.paymentList.length,
                          physics: ClampingScrollPhysics(),
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () async {
                                if (index == 0) {
                                  bool isAdded = await Navigator.push(context,
                                      CupertinoPageRoute(builder: (context) {
                                    return AddCreditCardView();
                                  }));

                                  if (isAdded != null && isAdded) {
                                    mViewModel.getUserCardList();
                                  }
                                } else if (index == 1) {
                                  CommonUtils.showToastMessage(
                                      "work in progress");
                                } else {
                                  Navigator.push(context,
                                      CupertinoPageRoute(builder: (context) {
                                    return AddBankAccountView();
                                  }));
                                }
                              },
                              child: PaymentItem(
                                paymentName:
                                    mViewModel.paymentList[index].paymentName,
                                paymentIcon:
                                    mViewModel.paymentList[index].payIcon,
                              ),
                            );
                          }),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  bool isValidData() {
    if (mAmountController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).enterAmount);
      return false;
    } else if (mViewModel.userCardList == null ||
        mViewModel.userCardList.length == 0) {
      CommonUtils.showRedToastMessage(S.of(context).pleaseAddDebitCard);
      return false;
    } else if (mViewModel.userCardList[mViewModel.selectedCardIndex].cvv ==
        null) {
      CommonUtils.showRedToastMessage(S.of(context).pleaseEnterCvvDetails);
      return false;
    } else if (mViewModel.loginDetails.address == null ||
        mViewModel.loginDetails.address.addressLine1.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).weDidNotFoundYourAddress);
      return false;
    } else {
      return true;
    }
  }
}

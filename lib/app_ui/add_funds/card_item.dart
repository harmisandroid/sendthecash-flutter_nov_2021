import 'package:flutter/material.dart';
import 'package:sendcash_flutter/common_widgets/cards/cvv_card_view.dart';
import 'package:sendcash_flutter/models/user_card_details.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';

class CardItem extends StatelessWidget {
  int index;
  int selectedIndex;
  Function(int) onChange;
  CardDetails cardDetails;

  CardItem({this.index, this.selectedIndex, this.onChange, this.cardDetails});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onChange(index);
      },
      child: Container(
        margin: EdgeInsets.only(top: 5.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(top: 12.0, right: 5.0),
              child: Icon(
                index == selectedIndex
                    ? Icons.radio_button_checked_outlined
                    : Icons.radio_button_off,
                color: CommonColors.primaryColor,
                size: 30.0,
              ),
            ),
            Flexible(
                child: CVVCardView(
              isSelected: index == selectedIndex,
              cardDetails: cardDetails,
            )),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class PaymentItem extends StatelessWidget {
  String paymentName;
  String paymentIcon;

  PaymentItem({this.paymentName, this.paymentIcon});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20.0),
      margin: EdgeInsets.only(top: 20.0,left: 2.0,right: 2.0),
      height: 50.0,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
        BoxShadow(
          color: Theme.of(context).shadowColor,
          offset: Offset.zero,
          blurRadius: 1.0,
          spreadRadius: 1.0
        )
      ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            paymentName,
            textAlign: TextAlign.start,
            style: CommonStyle.getAppFont(
                color: /*CommonColors.black2222.withOpacity(0.5)*/CommonColors.getLightWhiteTextColor(context),
                fontWeight: FontWeight.w500,
                fontSize: 18.0),
          ),
          Image.asset(
           paymentIcon,
            height: 30.0,
            width: 30.0,
          ),
        ],
      ),
    );
  }
}

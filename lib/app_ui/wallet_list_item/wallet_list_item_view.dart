import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class WalletListItemView extends StatelessWidget {
  const WalletListItemView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(

      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  CircleAvatar(
                    radius: 38,
                    backgroundColor: CommonColors.gradientColorBottom,
                    child: CircleAvatar(
                      radius: 36,
                      backgroundColor: Colors.white,
                      child: CircleAvatar(
                        radius: 33,
                        backgroundImage: NetworkImage(
                            'https://cdn.pixabay.com/photo/2018/01/15/07/52/woman-3083390_1280.jpg'),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Column(
                      children: [
                        Text(
                          "John doe",
                          textAlign: TextAlign.start,
                          style: CommonStyle.getAppFont(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              color: CommonColors.getWhiteTextColor(context)),
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 36, top: 2),
                          child: Text(
                            "Sent",
                            textAlign: TextAlign.start,
                            style: CommonStyle.getAppFont(
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                                color: CommonColors.walletItemFontColor),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Flexible(
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 40, top: 2),
                      child: Text(
                        Globals.currencySymbol+"310.00",
                        style: CommonStyle.getAppFont(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: CommonColors.walletItemFontColor),
                      ),
                    ),
                    SizedBox(height: 2.0,),
                    Text(
                      "Today - 11:00 AM",
                      style: CommonStyle.getAppFont(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: CommonColors.getWhiteTextColor(context).withOpacity(.9)),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            height: 2,
            margin: EdgeInsets.only(top: 15, bottom: 10),
            color: CommonColors.bottombarBoxShadow.withOpacity(.2),
          )
        ],
      ),
    );
  }
}

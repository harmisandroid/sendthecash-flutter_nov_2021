import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';

import '../transfer_funds/bank_ui_map.dart';

class AddBankAccountViewModel with ChangeNotifier {
  BuildContext mContext;

  // List<BankDetails> bankList = [];
  ApiServices apiServices = ApiServices();
  BankModel selectedMainBankType;
  CountryBankModel selectedCountryBankModel;
  String selectedBankType;
  List<BankModel> mainBankList = [];
  List<CountryBankModel> countryBankList = [];
  List<String> bankTypeList = [];
  Country selectedCountry, selectedPhoneCountry;
  List<dynamic> methodMaps = addBankUi['method_types'];
  List<dynamic> fieldList = [];
  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();
  String userId;
  AppPreferences appPreferences = AppPreferences();
  bool isSwiftBank = false;

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    // bankList.clear();
    //  getSupportedBankList();
    isSwiftBank = false;
    mainBankList.clear();
    List<String> usBankList = [];
    usBankList.add("US INDIVIDUAL BANK ACCOUNT");
    usBankList.add("US CORPORATE BANK ACCOUNT");
    CountryBankModel usCountryBankModel =
        CountryBankModel("US BANK ACCOUNT", usBankList);
    List<String> brBankList = [];
    brBankList.add("BR INDIVIDUAL BANK ACCOUNT");
    brBankList.add("BR CORPORATE BANK ACCOUNT");
    CountryBankModel brCountryBankModel =
        CountryBankModel("BR BANK ACCOUNT", brBankList);
    List<String> crBankList = [];
    crBankList.add("CR INDIVIDUAL BANK ACCOUNT");
    crBankList.add("CR CORPORATE BANK ACCOUNT");
    CountryBankModel crCountryBankModel =
        CountryBankModel("CR BANK ACCOUNT", crBankList);
    List<CountryBankModel> countrybankList = [];
    countrybankList.add(usCountryBankModel);
    countrybankList.add(brCountryBankModel);
    countrybankList.add(crCountryBankModel);
    BankModel bankModel = new BankModel(countrybankList, "Bank Account");
    BankModel swiftModel = new BankModel(null, "Swift Bank Account");
    mainBankList.add(bankModel);
    mainBankList.add(swiftModel);
    selectedMainBankType = mainBankList[0];
    countryBankList = selectedMainBankType.bankList;
    selectedCountryBankModel = countryBankList[0];
    bankTypeList = selectedCountryBankModel.bankTypeList;
    selectedBankType = bankTypeList[0];
    fieldList = methodMaps[1]['data'][0]['sub_list'][0]['fields'];
    userId = await appPreferences.getUserFirebaseId();
    if (selectedCountry == null) {
      selectedCountry = Country(
          phoneCode: "+1",
          countryCode: "US",
          e164Sc: 1223,
          geographic: true,
          level: 1524,
          name: "United States",
          example: "",
          displayName: "USA",
          displayNameNoCountryCode: "No",
          e164Key: "");
      selectedPhoneCountry = selectedCountry;
    }
    notifyListeners();
  }

  // void getSupportedBankList() async{
  //   CommonUtils.showProgressDialog(mContext);
  //   bankList = await apiServices.getSupportedBank(onFailed: (){
  //     CommonUtils.hideProgressDialog(mContext);
  //   });
  //   selectedbankDetails = bankList[0];
  //   notifyListeners();
  //   CommonUtils.hideProgressDialog(mContext);
  // }
  //
  // void onBankChange(BankDetails value) {
  //   selectedbankDetails  = value;
  //   notifyListeners();
  // }

  void onMainBankChange(BankModel value) {
    selectedMainBankType = value;
    if (value.name == "Swift Bank Account") {
      isSwiftBank = true;
      Map<String, dynamic> countryBankMap = methodMaps[2];
      fieldList = countryBankMap['data'];
    } else {
      isSwiftBank = false;
      countryBankList = value.bankList;
      selectedCountryBankModel = value.bankList[0];
    }
    notifyListeners();
  }

  void onCountryBankChange(CountryBankModel value) {
    selectedCountryBankModel = value;
    selectedBankType = value.bankTypeList[0];
    bankTypeList = value.bankTypeList;
    notifyListeners();
  }

  void onCountryBankType(String value) {
    selectedBankType = value;
    Map<String, dynamic> countryBankMap = methodMaps[1];
    List<dynamic> countryBankList = countryBankMap['data'];
    countryBankList.forEach((element) {
      if (element['field_name'].toString().toLowerCase() ==
          selectedCountryBankModel.name.toLowerCase()) {
        List<dynamic> bankTypeList = element['sub_list'];
        bankTypeList.forEach((typeElement) {
          if (value.toLowerCase() == typeElement['field_name']) {
            fieldList = typeElement['fields'];
            notifyListeners();
          }
        });
      }
    });

    notifyListeners();
  }

  Future<void> showCountryPickerDialog() async {
    await showCountryPicker(
      context: mContext,
      showPhoneCode: true,
      onSelect: (Country value) {
        selectedCountry = value;
        notifyListeners();
      },
    );
  }

  void addBankAccountsFirebase({Map<String,dynamic>  bankParams}) async {
    await firebaseDatabaseHelper.addBankAccount(
        onError: (error, stackTrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("Failed to add bank account");
        },
        onSuccess: (document) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage("Bank account successfully");
          Navigator.pop(mContext);
        },
        data: bankParams);
  }

  void checkBankAccountountExists() async{
    Map<String, dynamic> bankParams = Map();
    bankParams[DbKey.USER_ID] = userId;
    fieldList.forEach((element) {
      if (element['field_type'] == "drop") {
        bankParams[element['field_key']] = element['select'];
      } else {
        bankParams[element['field_key']] = element['field_value'];
      }
    });
    if (selectedMainBankType.name.toLowerCase() == "swift bank account") {
      bankParams[DbKey.BANK_TYPE_NAME] = selectedMainBankType.name;
    } else {
      bankParams[DbKey.BANK_TYPE_NAME] = selectedBankType;
    }
    CommonUtils.showProgressDialog(mContext);

    await firebaseDatabaseHelper.checkBankAccountExists(
        accountNumber: bankParams[DbKey.ACCOUNT_NUMBER],
        onSuccess: (querySnapshot){
      if(querySnapshot.size > 0){
        CommonUtils.hideProgressDialog(mContext);
        CommonUtils.showRedToastMessage("Bank accounts already exists");
        print("bank accounts already exists");
      }else{
        addBankAccountsFirebase(bankParams: bankParams);
      }
    },onError: (error,stackTrace){
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showRedToastMessage("Failed to check bank accounts");
    });
  }
}

class BankModel {
  List<CountryBankModel> bankList;
  String name;

  BankModel(this.bankList, this.name);
}

class CountryBankModel {
  String name;
  List<String> bankTypeList = [];

  CountryBankModel(this.name, this.bankTypeList);
}

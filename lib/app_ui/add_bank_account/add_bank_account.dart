import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/gradient_button/gradientbutton.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/taskinputformat_view.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/text_style.dart';
import 'add_bank_account_view_model.dart';

class AddBankAccountView extends StatefulWidget {
  const AddBankAccountView({Key key}) : super(key: key);

  @override
  _AddBankAccountViewState createState() => _AddBankAccountViewState();
}

class _AddBankAccountViewState extends State<AddBankAccountView> {
  AddBankAccountViewModel mViewModel;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final mHolderNameController = TextEditingController();
  final mBankAccountController = TextEditingController();
  final mIBANController = TextEditingController();
  final mSwiftCodeController = TextEditingController();

  List<String> bankArray = [
    'SBININBB -	BM 93 NEHRU NAGAR SQUARE, BHOPAL MADHYA PRADESH -462001',
    'AXISINBBA03 - BM 93 NEHRU NAGAR SQUARE, BHOPAL MADHYA PRADESH -462001',
  ];
  String bankValue;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<AddBankAccountViewModel>(context);

    final mainBankTypeDropdown =
        (mViewModel.mainBankList != null && mViewModel.mainBankList.length == 0)
            ? Container()
            : Container(
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    S.of(context).bank,
                    style: CommonStyle.getAppFont(
                        color: CommonColors.getWhiteTextColor(context),
                        fontWeight: FontWeight.w500,
                        fontSize: 20.0),
                  ),
                  DropdownButton<BankModel>(
                    isExpanded: true,
                    dropdownColor: CommonColors.getDropdownColor(context),
                    value: mViewModel.selectedMainBankType,
                    itemHeight: 50.0,
                    hint: Text(
                      S.of(context).selectBank,
                      style: CommonStyle.getAppFont(
                          color: CommonColors.getWhiteTextColor(context),
                          fontWeight: FontWeight.w500,
                          fontSize: 18.0),
                    ),
                    icon: Icon(
                      Icons.keyboard_arrow_down,
                      color: CommonColors.getWhiteTextColor(context),
                      size: 25,
                    ),
                    style: const TextStyle(color: Colors.grey),
                    underline: Container(
                      margin: EdgeInsets.only(top: 20.0),
                      height: 2,
                      color: Colors.grey,
                    ),
                    onChanged: mViewModel.onMainBankChange,
                    items: mViewModel.mainBankList
                        .map<DropdownMenuItem<BankModel>>((BankModel value) {
                      return DropdownMenuItem<BankModel>(
                        value: value,
                        child: Text(
                          value.name.toUpperCase(),
                          overflow: TextOverflow.clip,
                          style: CommonStyle.getAppFont(
                            color: CommonColors.getWhiteTextColor(context),
                            fontWeight: FontWeight.normal,
                            fontSize: 14.0,
                          ),
                        ),
                      );
                    }).toList(),
                  ),
                ],
              ));

    final countryBankDropdown = mViewModel.isSwiftBank
        ? Container()
        : Container(
            child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Select your bank with country",
                style: CommonStyle.getAppFont(
                    color: CommonColors.getWhiteTextColor(context),
                    fontWeight: FontWeight.w500,
                    fontSize: 20.0),
              ),
              DropdownButton<CountryBankModel>(
                isExpanded: true,
                dropdownColor: CommonColors.getDropdownColor(context),
                value: mViewModel.selectedCountryBankModel,
                itemHeight: 50.0,
                hint: Text(
                  S.of(context).selectBank,
                  style: CommonStyle.getAppFont(
                      color: CommonColors.getWhiteTextColor(context),
                      fontWeight: FontWeight.w500,
                      fontSize: 18.0),
                ),
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: CommonColors.getWhiteTextColor(context),
                  size: 25,
                ),
                style: const TextStyle(color: Colors.grey),
                underline: Container(
                  margin: EdgeInsets.only(top: 20.0),
                  height: 2,
                  color: Colors.grey,
                ),
                onChanged: mViewModel.onCountryBankChange,
                items: mViewModel.countryBankList
                    .map<DropdownMenuItem<CountryBankModel>>(
                        (CountryBankModel value) {
                  return DropdownMenuItem<CountryBankModel>(
                    value: value,
                    child: Text(
                      value.name,
                      overflow: TextOverflow.clip,
                      style: CommonStyle.getAppFont(
                        color: CommonColors.getWhiteTextColor(context),
                        fontWeight: FontWeight.normal,
                        fontSize: 14.0,
                      ),
                    ),
                  );
                }).toList(),
              ),
            ],
          ));

    final countryBankTypeDropdown =
        (mViewModel.isSwiftBank)
            ? Container()
            : Container(
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Select bank type",
                    style: CommonStyle.getAppFont(
                        color: CommonColors.getWhiteTextColor(context),
                        fontWeight: FontWeight.w500,
                        fontSize: 20.0),
                  ),
                  DropdownButton<String>(
                    isExpanded: true,
                    dropdownColor: CommonColors.getDropdownColor(context),
                    value: mViewModel.selectedBankType,
                    itemHeight: 50.0,
                    hint: Text(
                      S.of(context).selectBank,
                      style: CommonStyle.getAppFont(
                          color: CommonColors.getWhiteTextColor(context),
                          fontWeight: FontWeight.w500,
                          fontSize: 18.0),
                    ),
                    icon: Icon(
                      Icons.keyboard_arrow_down,
                      color: CommonColors.getWhiteTextColor(context),
                      size: 25,
                    ),
                    style: const TextStyle(color: Colors.grey),
                    underline: Container(
                      margin: EdgeInsets.only(top: 20.0),
                      height: 2,
                      color: Colors.grey,
                    ),
                    onChanged: mViewModel.onCountryBankType,
                    items: mViewModel.bankTypeList
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(
                          value,
                          overflow: TextOverflow.clip,
                          style: CommonStyle.getAppFont(
                            color: CommonColors.getWhiteTextColor(context),
                            fontWeight: FontWeight.normal,
                            fontSize: 14.0,
                          ),
                        ),
                      );
                    }).toList(),
                  ),
                ],
              ));

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Container(
        color: Theme.of(context).backgroundColor,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            GAppbar(
              title: S.of(context).addBankAccount,
              height: 130.0,
            ),
            Container(
              height: MediaQuery.of(context).size.height,
              margin: EdgeInsets.only(top: 100.0, left: 15.0, right: 15.0),
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
              decoration: BoxDecoration(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  borderRadius: BorderRadius.circular(20.0),
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 25.0,
                        blurRadius: 25.0,
                        color: Theme.of(context).shadowColor,
                        offset: Offset(0.0, 30.0))
                  ]),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    mainBankTypeDropdown,
                    SizedBox(
                      height: 10.0,
                    ),
                    countryBankDropdown,
                    SizedBox(
                      height: 10.0,
                    ),
                    countryBankTypeDropdown,
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(
                      "Account details",
                      style: CommonStyle.getAppFont(
                          color: CommonColors.getLightWhiteTextColor(context),
                          fontSize: 18.0,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    buildBankFieldWidget(),

                    // InkWell(
                    //   onTap: (){
                    //     mViewModel.showCountryPickerDialog();
                    //   },
                    //   child: Container(
                    //     height: 50.0,
                    //     padding: EdgeInsets.only(left: 10, right: 10.0),
                    //     width: MediaQuery.of(context).size.width,
                    //     decoration: BoxDecoration(
                    //       border: Border(bottom: BorderSide(width: 1.0,color: CommonColors.getWhiteTextColor(context))),
                    //         color: Theme.of(context).scaffoldBackgroundColor),
                    //     child: Row(
                    //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //       children: [
                    //         Text(
                    //           mViewModel.selectedCountry == null
                    //               ? "United States"
                    //               : mViewModel.selectedCountry.name.toUpperCase(),
                    //           style: CommonStyle.getAppFont(
                    //               fontSize: 18,
                    //               color: Colors.white,
                    //               fontWeight: FontWeight.normal),
                    //         ),
                    //         Icon(
                    //           Icons.keyboard_arrow_down_outlined,
                    //           color: Colors.white,
                    //         )
                    //       ],
                    //     ),
                    //   ),
                    // ),
                    SizedBox(
                      height: MediaQuery.of(context).viewInsets.bottom + 100.0,
                    )
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: EdgeInsets.only(
                    left: 30.0,
                    right: 30.0,
                    bottom: MediaQuery.of(context).viewPadding.bottom + 10.0),
                child: GradientButton(
                  height: 40.0,
                  btnText: S.of(context).addAccount,
                  onClick: (){
                    if(isValidData()){
                      mViewModel.checkBankAccountountExists();
                    }

                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  bool isValid() {
    if (mHolderNameController.text.isEmpty) {
      CommonUtils.showSnackBar(S.of(context).holdernameValidation, scaffoldKey);
      return false;
    } else if (mBankAccountController.text.isEmpty) {
      CommonUtils.showSnackBar(
          S.of(context).bankaccountValidation, scaffoldKey);
      return false;
    } else if (mIBANController.text.isEmpty) {
      CommonUtils.showSnackBar(S.of(context).IFSCValidation, scaffoldKey);
      return false;
    } else {
      return true;
    }
  }

  Widget buildBankFieldWidget() {
    List<Widget> widgetList = [];
    mViewModel.fieldList.forEach((element) {
      if (element['field_type'] == "textfield") {
        Widget textFieldWidget = getTextRequiredField(
            label: element['field_name'],
            onChange: (value) {
              element['field_value'] = value;
            },
            textInputType: element['input_type'] == "email"
                ? TextInputType.emailAddress
                : (element['input_type'] == "text"
                    ? TextInputType.text
                    : TextInputType.number),
            hintText: "");
        widgetList.add(textFieldWidget);
      } else if (element['field_type'] == "drop") {
        Widget textFieldWidget = getDropdownButton(
            label: element['field_name'],
            dropList: element['field_value'],
            selectedValue: element['select'],
            onChange: (value) {
              element['select'] = value;
              setState(() {});
            });
        widgetList.add(textFieldWidget);
      }
    });

    return Form(
        child: Container(
      decoration: BoxDecoration(
        color: CommonColors.getBankFieldContainer(context),
      ),
      padding: EdgeInsets.only(bottom: 20.0),
      child: Column(
        children: widgetList,
      ),
    ));
  }

  Widget getTextRequiredField(
      {String label,
      TextInputType textInputType,
      String hintText,
      Function(String) onChange}) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10.0,
          ),
          Text(
            label ?? "NA",
            style: CommonStyle.getAppFont(
                color: CommonColors.getLightWhiteTextColor(context),
                fontSize: 16.0,
                fontWeight: FontWeight.w500),
          ),
          SizedBox(
            height: 10.0,
          ),
          TextFormField(
            keyboardType: textInputType ?? TextInputType.text,
            onChanged: onChange,
            onSaved: onChange,
            onFieldSubmitted: onChange,
            style: CommonStyle.getAppFont(
                color: CommonColors.getWhiteTextColor(context),
                fontSize: 14.0,
                fontWeight: FontWeight.normal),
            decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
                hintText: hintText ?? "",
                hintStyle: CommonStyle.getAppFont(
                    color: CommonColors.getHintTextStyle(context)),
                errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: Colors.red, width: 0.5)),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                        color: CommonColors.getWhiteTextColor(context),
                        width: 0.5)),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                        color: Theme.of(context).primaryColor, width: 0.5))),
          ),
          SizedBox(
            height: 15.0,
          ),
        ],
      ),
    );
  }

  Widget getDropdownButton(
      {String label,
      List<String> dropList,
      Function(String) onChange,
      String selectedValue}) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10.0,
          ),
          Text(
            label ?? "NA",
            style: CommonStyle.getAppFont(
                color: CommonColors.getLightWhiteTextColor(context),
                fontSize: 16.0,
                fontWeight: FontWeight.w500),
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            decoration: BoxDecoration(
                color: CommonColors.getBankFieldContainer(context),
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(
                    color: CommonColors.getWhiteTextColor(context),
                    width: 0.5)),
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                iconSize: 22,
                value: selectedValue,
                isExpanded: true,
                dropdownColor: CommonColors.getDropdownColor(context),
                hint: Text(selectedValue,
                    style: CommonStyle.getAppFont(
                        color: Colors.black,
                        fontSize: 15.0,
                        fontWeight: FontWeight.normal)),
                style: CommonStyle.getAppFont(
                    fontSize: 15.0, fontWeight: FontWeight.normal),
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: CommonColors.getWhiteTextColor(context),
                ),
                items: dropList.map((value) {
                  return DropdownMenuItem(
                    value: value,
                    child: Text(
                      value,
                      style: CommonStyle.getAppFont(
                          fontSize: 15,
                          color: CommonColors.getWhiteTextColor(context),
                          fontWeight: FontWeight.normal),
                    ),
                  );
                }).toList(),
                onChanged: onChange,
              ),
            ),
          )
        ],
      ),
    );
  }

  bool isValidData() {
    if (mViewModel.selectedMainBankType.name.toLowerCase() == "bank account") {
      if (mViewModel.selectedCountryBankModel != null) {
        if (mViewModel.selectedBankType != null) {
          for (dynamic element in mViewModel.fieldList) {
            if (element['field_type'] == "textfield" &&
                element['field_value'].toString().isEmpty) {
              CommonUtils.showRedToastMessage(
                  'Please enter ' + element['field_name']);
              print("validation working");
              return false;
            } else if (element['field_type'] == "textfield" &&
                element['input_type'] == "email") {
              if (!CommonUtils.isvalidEmail(element['field_value'])) {
                CommonUtils.showRedToastMessage(S.of(context).emailValidation);
              }
            }
          }
        } else {
          CommonUtils.showRedToastMessage(
              "Please select bank type individual or corporate");
        }
      } else {
        CommonUtils.showRedToastMessage("Please select bank first");
        return false;
      }
    } else if (mViewModel.selectedMainBankType.name.toLowerCase() == "swift bank account") {
      List<dynamic> fieldList = mViewModel.methodMaps[2]['data'];
      fieldList.forEach((element) {
        if (element['field_type'] == "textfield" &&
            element['field_value'].toString().isEmpty) {
          CommonUtils.showRedToastMessage(
              "Please enter " + element['field_name']);
          return false;
        }
      });
    }
    return true;
  }
}

//  Widget requiredTextfield({maxlength,title, hintText, context, inputFormatter, controller,inputAction,inputType}) {
//   return Container(
//     margin: EdgeInsets.only(bottom: 30),
//     child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
//       Text(
//         title,
//         style: CommonStyle.getAppFont(
//             color: CommonColors.getWhiteTextColor(context),
//             fontWeight: FontWeight.w500,
//             fontSize: 20.0),
//       ),
//       TextFormField(
//         maxLength: maxlength ?? 200,
//         maxLines: 1,
//         controller: controller,
//           autocorrect: true,
//           textInputAction: inputAction,
//           keyboardType: inputType,
//           inputFormatters: inputFormatter,
//           cursorColor:CommonColors.getWhiteTextColor(context),
//           style: CommonStyle.getAppFont(
//               color: CommonColors.getWhiteTextColor(context),
//               fontWeight: FontWeight.w500,
//               fontSize: 18.0),
//           decoration: InputDecoration(
//             counterText: "",
//               hintText: hintText,
//               hintStyle: CommonStyle.getAppFont(
//                   color: CommonColors.getGrayThemeColor(context),
//                   fontWeight: FontWeight.w500,
//                   fontSize: 18.0),
//               enabledBorder: UnderlineInputBorder(
//                 borderSide: BorderSide(color:CommonColors.getFieldBorderColor(context)),
//               ),
//               focusedBorder: UnderlineInputBorder(
//                 borderSide: BorderSide(
//                   color:CommonColors.getFieldBorderColor(context),
//                 ),
//               )))
//     ]),
//   );
//
// }

/* requiredTextfield(
                      title: S.of(context).accountHolderName,
                      hintText: "John Doe",
                      context: context,
                      controller: mHolderNameController,
                      inputType: TextInputType.text,
                      inputAction:TextInputAction.next,),
                    requiredTextfield(
                      title: S.of(context).bankAccount,
                      hintText: "1234 5678 9012 3456",
                      context: context,
                      controller: mBankAccountController,
                      inputType: TextInputType.number,
                      inputAction:TextInputAction.next,
                      inputFormatter:[
                        MaskedTextInputFormatter(
                          mask: 'xxxx-xxxx-xxxx-xxxx',
                          separator: '-',
                        ),
                      ],),
                    requiredTextfield(
                        title: S.of(context).IBAN,
                        hintText: "AT61 1904 3002 3457 3201",
                        context: context,
                        inputFormatter:[
                          MaskedTextInputFormatter(
                            mask: 'xxxx xxxx xxxx xxxxxxx',
                            separator: ' ',
                          ),
                        ],
                        controller: mIBANController,
                        inputType: TextInputType.text,
                        inputAction:TextInputAction.done),
                    requiredTextfield(
                        title: S.of(context).swiftCode,
                        hintText: "SBIN IN BB 455",
                        context: context,
                        controller: mSwiftCodeController,
                        inputType: TextInputType.text,
                        inputAction:TextInputAction.done),
                    Text("Address",style: CommonStyle.getAppFont(
                        color: CommonColors.getLightWhiteTextColor(context),
                        fontSize: 18.0,
                        fontWeight: FontWeight.w500
                    ),),
                    SizedBox(height: 20.0,),*/

/* requiredTextfield(
                        title: S.of(context).street1,
                        hintText: S.of(context).street1,
                        context: context,
                        controller: TextEditingController(),
                        inputType: TextInputType.text,
                        inputAction:TextInputAction.done),
                    requiredTextfield(
                        title: S.of(context).street2,
                        hintText: S.of(context).street2,
                        context: context,
                        controller: TextEditingController(),
                        inputType: TextInputType.text,
                        inputAction:TextInputAction.done),
                    requiredTextfield(
                        title: S.of(context).city,
                        hintText: S.of(context).city,
                        context: context,
                        controller: TextEditingController(),
                        inputType: TextInputType.text,
                        inputAction:TextInputAction.done),
                    requiredTextfield(
                        title: S.of(context).stateProvince,
                        hintText: S.of(context).stateProvince,
                        context: context,
                        controller: TextEditingController(),
                        inputType: TextInputType.text,
                        inputAction:TextInputAction.done),
                    requiredTextfield(
                        title: S.of(context).postalCode,
                        hintText: S.of(context).postalCode,
                        context: context,
                        controller: TextEditingController(),
                        inputType: TextInputType.text,
                        inputAction:TextInputAction.done),*/

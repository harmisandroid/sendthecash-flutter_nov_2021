import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/upload_documents/upload_documents_view_model.dart';
import 'package:sendcash_flutter/common_widgets/appbar/back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/bottomnavbar/bottom_navigation_view.dart';
import 'package:sendcash_flutter/common_widgets/buttons/Buttons.dart';
import 'package:sendcash_flutter/common_widgets/gradient_button/gradientbutton.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/generated/assets.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class UploadDocumentsView extends StatefulWidget {
   String from;
   UploadDocumentsView({this.from});

  @override
  _UplloadDocumentsViewState createState() => _UplloadDocumentsViewState();
}

class _UplloadDocumentsViewState extends State<UploadDocumentsView> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  UploadDocumentsViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<UploadDocumentsViewModel>(context);
    final uploadProfilePicture = Container(
        padding: EdgeInsetsDirectional.all(10.0),
        margin: EdgeInsetsDirectional.all(20.0),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: Theme.of(context).backgroundColor,
        ),
        child: Column(
          children: [
            SizedBox(
              height: 12,
            ),
            GradientButton(
              btnText: S.of(context).uploadProfilePicture,
              onClick: () {
                mViewModel.openBottomSheet(pickType: "profile_pic");
              },
            ),
            InkWell(
              onTap: () {
                mViewModel.openBottomSheet(pickType: "profile_pic");
              },
              child: Container(
                margin: EdgeInsetsDirectional.only(top: 20.0),
                decoration: BoxDecoration(
                  border: Border.all(
                      color: CommonColors.gradientColorBottom, width: 2),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Image(
                    height: 117,
                    width: 117,
                    fit: BoxFit.cover,
                    image: (mViewModel.profileImage == null ||
                            mViewModel.profileImage.isEmpty)
                        ? AssetImage(LocalImages.applogotext)
                        : mViewModel.profileImage.startsWith("http")
                            ? NetworkImage(mViewModel.profileImage)
                            : FileImage(File(mViewModel.profileImage)),
                  ),
                ),
              ),
            ),
          ],
        ));

    final uploadPassportPicture = Container(
        padding: EdgeInsetsDirectional.all(20.0),
        margin: EdgeInsetsDirectional.all(20.0),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: Theme.of(context).backgroundColor,
        ),
        child: Column(
          children: [
            GradientButton(
              btnText: S.of(context).uploadPassportAndCopy,
              onClick: () {},
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                      flex: 1,
                      child: InkWell(
                        onTap: () {
                          mViewModel.openBottomSheet(pickType: "id_front");
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: CommonColors.gradientColorBottom,
                                width: 2),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Image(
                                height: 90,
                                width: 150,
                                fit: BoxFit.cover,
                                image: (mViewModel.frontDocImage == null ||
                                        mViewModel.frontDocImage.isEmpty)
                                    ? AssetImage(Assets.imgImgIdFront)
                                    : mViewModel.frontDocImage
                                            .startsWith("http")
                                        ? NetworkImage(mViewModel.frontDocImage)
                                        : FileImage(
                                            File(mViewModel.frontDocImage))),
                          ),
                        ),
                      )),
                  SizedBox(
                    width: 10.0,
                  ),
                  Flexible(
                    flex: 1,
                    child: InkWell(
                      onTap: () {
                        mViewModel.openBottomSheet(pickType: "id_back");
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: CommonColors.gradientColorBottom,
                              width: 2),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10.0),
                          child: Image(
                              height: 90,
                              width: 150,
                              fit: BoxFit.cover,
                              image: (mViewModel.backDocImage == null ||
                                      mViewModel.backDocImage.isEmpty)
                                  ? AssetImage(Assets.imgImgIdBack)
                                  : mViewModel.backDocImage.startsWith("http")
                                      ? NetworkImage(mViewModel.backDocImage)
                                      : FileImage(
                                          File(mViewModel.backDocImage))),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));

    return Scaffold(
      bottomNavigationBar: Container(
        color: CommonColors.gradientColorBottom,
        child: Container(
          color: Colors.transparent,
          height: 45.0,
          margin: EdgeInsets.only(bottom: MediaQuery.of(context).viewPadding.bottom + 10.0),
          child:Buttons(
            text: S.of(context).submit,
            onClick: () {
              mViewModel.submitAllDocs();
            },
          ),
        ),
        padding: EdgeInsets.all(20.0),
      ),
      body: Container(
          height: MediaQuery.of(context).size.height + MediaQuery.of(context).viewPadding.bottom + 100.0,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                CommonColors.gradientColorTop,
                CommonColors.gradientColorBottom
              ])),
          child: Scaffold(
              key: scaffoldKey,
              backgroundColor: Colors.transparent,
              appBar: BackAppbar(
                title: S.of(context).uploadDocuments,
                onBackPressed: (){
                  if(widget.from == "profile"){
                    Navigator.pop(context);
                  }else{
                    SystemNavigator.pop();
                  }
                },
                actions: InkWell(
                  onTap: () async {
                    await  AppPreferences().setIsDocumentSkiped(true);
                   Navigator.pushAndRemoveUntil(context, CupertinoPageRoute(builder:(context){
                     return BottomNavigationView();
                   }), (route) => false);
                  },
                  child: Container(
                    child: Text("Skip",style: CommonStyle.getAppFont(
                      color: Colors.white
                    ),),
                  ),
                ),
              ),
              body: SingleChildScrollView(
                child: Container(
                  child: Column(
                    children: [
                      uploadPassportPicture,
                      uploadProfilePicture,
                      SizedBox(
                        height: MediaQuery.of(context).viewPadding.bottom + 30.0,
                      )
                    ],
                  ),
                ),
              ))),
    );
  }
}

import 'dart:convert';
import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sendcash_flutter/common_widgets/bottomnavbar/bottom_navigation_view.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

import '../../generated/i18n.dart';

class UploadDocumentsViewModel with ChangeNotifier {
  BuildContext mContext;
  AppPreferences appPreferences = AppPreferences();
  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();
  LoginDetails loginDetails;
  String profileImage;
  String frontDocImage, backDocImage;
  final imagePicker = ImagePicker();
  bool isProfileUploading = false;
  String uploadingPercent = "0";
  String userId;
  ApiServices apiServices = ApiServices();

  Future<void> pickDocFront(bool isCamera) async {
    XFile xFile = await imagePicker.pickImage(
        source: isCamera ? ImageSource.camera : ImageSource.gallery);
    if (xFile != null) {
      frontDocImage = xFile.path;
      cropImage(cropImageType: "front_id_image", filePath: frontDocImage);
      notifyListeners();
    }
  }

  Future<void> pickDocBack(bool isCamera) async {
    XFile xFile = await imagePicker.pickImage(
        source: isCamera ? ImageSource.camera : ImageSource.gallery);
    if (xFile != null) {
      backDocImage = xFile.path;
      cropImage(cropImageType: "back_id_image", filePath: backDocImage);
      notifyListeners();
    }
  }

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    loginDetails = await appPreferences.getLoginDetails();
    userId = await appPreferences.getUserFirebaseId();
    if (loginDetails != null) {
      if (loginDetails.profilePic != null && loginDetails.profilePic != null) {
        profileImage = loginDetails.profilePic;
      }
      if (loginDetails.frontIdImage != null &&
          loginDetails.frontIdImage != null) {
        frontDocImage = loginDetails.frontIdImage;
      }
      if (loginDetails.backIdImage != null &&
          loginDetails.backIdImage != null) {
        backDocImage = loginDetails.backIdImage;
      }
      notifyListeners();
    }
  }

  Future<void> pickProfileImage(bool isCamera) async {
    XFile xFile = await imagePicker.pickImage(
        source: isCamera ? ImageSource.camera : ImageSource.gallery);
    if (xFile != null) {
      profileImage = xFile.path;
      cropImage(cropImageType: "profile_pic", filePath: profileImage);
      notifyListeners();
    }
  }

  void openBottomSheet({String pickType}) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        backgroundColor: Colors.white,
        context: mContext,
        builder: (context) {
          return Container(
            height: 180.0,
            child: ListView(
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                    if (pickType == "id_front") {
                      pickDocFront(true);
                    } else if (pickType == "id_back") {
                      pickDocBack(true);
                    } else {
                      pickProfileImage(true);
                    }
                  },
                  child: Container(
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.camera),
                        SizedBox(
                          width: 5.0,
                        ),
                        Text(
                          S.of(context).camera,
                          style: CommonStyle.getAppFont(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 15.0),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey.shade300,
                  height: 0.5,
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                    if (pickType == "id_front") {
                      pickDocFront(false);
                    } else if (pickType == "id_back") {
                      pickDocBack(false);
                    } else {
                      pickProfileImage(false);
                    }
                  },
                  child: Container(
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.photo),
                        SizedBox(
                          width: 5.0,
                        ),
                        Text(
                          S.of(context).gallery,
                          style: CommonStyle.getAppFont(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 15.0),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey.shade300,
                  height: 0.5,
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: 50.0,
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    child: Text(
                      S.of(context).cancel,
                      style: CommonStyle.getAppFont(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 15.0),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  void getUserDetails() async {
    await firebaseDatabaseHelper.getUserDetails(
        userId: userId,
        onSuccess: (documentReference) async {
          Map<String, dynamic> data =
              documentReference.data() as Map<String, dynamic>;
          await appPreferences.setLoginDetails(jsonEncode(data));
          Navigator.pushAndRemoveUntil(mContext,
              CupertinoPageRoute(builder: (context) {
            return BottomNavigationView();
          }), (route) => false);
        },
        onError: (error, stacktrace) {
          CommonUtils.showRedToastMessage(
              "Failed to get user details " + error.toString());
        });
  }

  Future<void> uploadProfileImage() async {
    FirebaseStorage storage = FirebaseStorage.instance;
    Reference reference = storage.ref().child("profile_images/" +
        loginDetails.firstName +
        DateTime.now().microsecondsSinceEpoch.toString());
    UploadTask uploadTask = reference.putFile(File(profileImage));
    await uploadTask.then((takeSnapshot) async {
      String url = await takeSnapshot.ref.getDownloadURL();
      profileImage = url.toString();
      loginDetails.profilePic = profileImage;
    }).onError((error, stackTrace) {
      CommonUtils.showRedToastMessage("failed to update profile pic");
    });
  }

  Future<void> uploadBackImage() async {
    FirebaseStorage storage = FirebaseStorage.instance;
    Reference reference = storage.ref().child("user_ids/" +
        "back_id" +
        userId +
        DateTime.now().microsecondsSinceEpoch.toString());
    UploadTask uploadTask = reference.putFile(File(backDocImage));
    await uploadTask.then((takeSnapshot) async {
      String url = await takeSnapshot.ref.getDownloadURL();
      backDocImage = url.toString();
      loginDetails.backIdImage = backDocImage;
    }).onError((error, stackTrace) {
      CommonUtils.showRedToastMessage("failed to update profile pic");
    });
  }

  Future<void> uploadFrotImage() async {
    FirebaseStorage storage = FirebaseStorage.instance;
    Reference reference = storage.ref().child("user_ids/" +
        "front_Id" +
        userId +
        DateTime.now().microsecondsSinceEpoch.toString());
    UploadTask uploadTask = reference.putFile(File(frontDocImage));
    await uploadTask.then((takeSnapshot) async {
      String url = await takeSnapshot.ref.getDownloadURL();
      frontDocImage = url.toString();
      loginDetails.frontIdImage = frontDocImage;
    }).onError((error, stackTrace) {
      CommonUtils.showRedToastMessage("failed to update profile pic");
    });
  }

  Future<void> updateUserDocs() async {
    await firebaseDatabaseHelper.updateUserProfile(
        userdata: {
          DbKey.FRONT_ID_IMAGE: frontDocImage,
          DbKey.BACK_ID_IMAGE: backDocImage,
          DbKey.PROFILE_PIC: profileImage,
        },
        userId: userId,
        onError: (error, stackTrace) {
          CommonUtils.showRedToastMessage("failed to update front side");
        },
        onSuccess: (value) {});
  }

  Future<void> submitAllDocs() async {
    CommonUtils.showProgressDialog(mContext);
    try {
      if (profileImage != null &&
          profileImage.isNotEmpty &&
          !profileImage.startsWith("http")) {
        await uploadProfileImage();
      }
      if (frontDocImage != null &&
          frontDocImage.isNotEmpty &&
          !frontDocImage.startsWith("http")) {
        await uploadDocument(file: File(frontDocImage), backFront: "FRONT");
        await uploadFrotImage();
      }
      if (backDocImage != null &&
          backDocImage.isNotEmpty &&
          !backDocImage.startsWith("http")) {
        await uploadDocument(file: File(backDocImage), backFront: "BACK");
        await uploadBackImage();
      }
      await updateUserDocs();
      await getUserDetails();
      CommonUtils.hideProgressDialog(mContext);
    } on Exception catch (e) {
      // TODO
      CommonUtils.hideProgressDialog(mContext);
    }
  }

  Future<void> cropImage({String cropImageType, String filePath}) async {
    File croppedFile = await ImageCropper().cropImage(
        sourcePath: filePath,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Send the cash',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));

    if (croppedFile != null) {
      if (cropImageType == "profile_pic") {
        profileImage = croppedFile.path;
      } else if (cropImageType == "back_id_image") {
        backDocImage = croppedFile.path;
      } else if (cropImageType == "front_id_image") {
        frontDocImage = croppedFile.path;
      }
      notifyListeners();
    } else {
      CommonUtils.showRedToastMessage("failed to crop image");
    }
  }

  Future<void> uploadDocument({File file, String backFront}) async {
    String response = await apiServices.uploadAccountDocuments(
        accountId: loginDetails.sendwyreAccountId,
        onFailed: () {
          //   CommonUtils.showRedToastMessage("Failed to upload");
        },
        bakFront: backFront,
        file: file);
    if (response != null) {
      //   CommonUtils.showGreenToastMessage("File uploaded");
    } else {
      //  CommonUtils.showRedToastMessage("Failed to upload");
    }
  }
}

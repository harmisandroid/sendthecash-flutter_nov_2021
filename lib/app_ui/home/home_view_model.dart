import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/globals.dart';

class HomeViewModel with ChangeNotifier {
  BuildContext mContext;
  ApiServices apiServices = ApiServices();
  LoginDetails loginDetails;
  AppPreferences appPreferences = AppPreferences();
  Map<String, dynamic> accountMap = Map();
  double availableBalance = 0.0, totalBalance = 0.0;
  Map<String, dynamic> exchangeMap = Map();

  void attachContext(BuildContext context) async {
    mContext = context;
    availableBalance = 0.0;
    totalBalance = 0.0;
    loginDetails = await appPreferences.getLoginDetails();
    accountMap = await appPreferences.getAccountDetails();
    if (loginDetails.sendwyreAccountId != null &&
        loginDetails.sendwyreAccountId.isNotEmpty) {
      getExchangeRates();
    }

    notifyListeners();
  }



  Future<void> getSendwyreAccountDetails({String accountId}) async {
    totalBalance = 0.0;
    availableBalance = 0.0;
    final response = await apiServices.getSendwyreAccountDetails(
        accountId: accountId,
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("failed to get account details");
        });
    CommonUtils.hideProgressDialog(mContext);
    if (response != null) {
      await appPreferences.setAccountDetails(response);
      Map<String, dynamic> mapResponse = jsonDecode(response);
      if (mapResponse["totalBalances"] != null) {
        Map<String, dynamic> totalBalanceMap = mapResponse["totalBalances"];
        Map<String, dynamic> availablebalances = mapResponse["availableBalances"];
        totalBalanceMap.forEach((key, value) {
          totalBalance = totalBalance + exchangeMap[Globals.currencyName+key];
        });
        availablebalances.forEach((key, value) {
          availableBalance = availableBalance + exchangeMap[Globals.currencyName+key];
        });
      }
      notifyListeners();
    } else {
      CommonUtils.showRedToastMessage("failed to get account details");
    }
  }

  Future<void> getExchangeRates() async {
    CommonUtils.showProgressDialog(mContext);
    final response = await apiServices.getExhangeRates(onFailed: () {
      CommonUtils.showRedToastMessage("Failed to get data");
      CommonUtils.hideProgressDialog(mContext);
    });
    CommonUtils.hideProgressDialog(mContext);
    if (response != null) {
      Map<String, dynamic> mapResponse = jsonDecode(response) as Map;
      exchangeMap = mapResponse;
      getSendwyreAccountDetails(accountId: loginDetails.sendwyreAccountId);
    } else {
      CommonUtils.showRedToastMessage("Failed to get data");
    }
  }
}

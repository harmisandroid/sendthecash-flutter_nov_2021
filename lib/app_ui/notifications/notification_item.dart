import 'package:flutter/material.dart';
import 'package:sendcash_flutter/generated/assets.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class NotificationItem extends StatelessWidget {

  int index;
  bool isLast;
  NotificationItem({this.index, this.isLast});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        children: [
          index < 4 ? Container(
            height: 15.0,
            width: 8.0,
            margin: EdgeInsets.only(bottom: 25.0),
            decoration: BoxDecoration(
              color: CommonColors.primaryColor,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(20.0),
                bottomRight: Radius.circular(20.0),
              )
            ),
          ) : Container(height: 0,width: 0,),
          SizedBox(
            width: 15.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 67.0,
                width: 67.0,
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  border: Border.all(
                    color: CommonColors.getVerticalLine(context),
                    width: 1.0
                  ),
                  shape: BoxShape.circle

                ),
                child: Image.asset(
                  Assets.imgIcNotiImg,
                  width: 67.0,
                  height: 67.0,
                ),
              ),
              Container(
                height: isLast ? 0.0 : 30.0,
                width: 2.0,
                color: CommonColors.getVerticalLine(context),
              )
            ],
          ),
          SizedBox(
            width: 18.0,
          ),
          Flexible(child:Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("THU, DEC 12",style: CommonStyle.getAppFont(
                  fontSize: 12.0,
                  fontWeight: FontWeight.normal,
                  color: CommonColors.getGrayThemeColor(context)
              ),),
              SizedBox(height: 10.0,),
              Text(Globals.currencySymbol+"PAC just went up 0.4% while ACTN stays steady",
                maxLines: 2,
                style: CommonStyle.getAppFont(
                    fontSize: 15.0,
                    fontWeight: FontWeight.normal,
                    color: CommonColors.getWhiteTextColor(context)
                ),),
              SizedBox(height: 15.0,),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 1.0,
                color: CommonColors.getListDeviderColor(context),
              )
            ],
          )),
          SizedBox(
            width: 18.0,
          ),
        ],
      ),
    );
  }
}

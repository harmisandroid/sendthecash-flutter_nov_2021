import 'package:flutter/material.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';

import 'notification_item.dart';

 class NotificationsView extends StatefulWidget {


   @override
   _NotificationsViewState createState() => _NotificationsViewState();
 }

 class _NotificationsViewState extends State<NotificationsView> {
   @override
   Widget build(BuildContext context) {
     return Scaffold(
       body: Stack(
         children: [
           GAppbar(
             title: "Notifications",
             height: 130.0,
           ),
           Container(
             height: MediaQuery.of(context).size.height,
             margin: EdgeInsets.only(top: 120.0),
             decoration: BoxDecoration(
                 color: Theme.of(context).scaffoldBackgroundColor,
                 boxShadow: [
                   BoxShadow(
                       spreadRadius: 25.0,
                       blurRadius: 25.0,
                       color:Theme.of(context).shadowColor,
                       offset: Offset(0.0, 5.0))
                 ]),
             child: ListView.builder(
               padding: EdgeInsets.only(top: 20.0),
                 itemCount: 10,
                 shrinkWrap: true,
                 physics: ClampingScrollPhysics(),
                 itemBuilder: (context,index){
                   return NotificationItem(index: index,isLast: index == 9,);
             }),

           )
         ],
       ),
     );
   }
 }

import 'package:flutter/material.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class FAQView extends StatefulWidget {
  const FAQView({Key key}) : super(key: key);

  @override
  _FAQViewState createState() => _FAQViewState();
}

class _FAQViewState extends State<FAQView> {
  bool isExapanded = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Stack(
        children: [
          GAppbar(
            title: S.of(context).faq,
            height: 153.0,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 150.0),
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(10.0, 0.0))
                ]),
            child: ListView.builder(
                padding: EdgeInsets.only(top: 6, bottom: 6 + MediaQuery.of(context).padding.bottom),
                shrinkWrap: true,
                itemCount: 6,
                physics: ClampingScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemBuilder: (BuildContext ctxt, int index) {
                  return Container(
                    margin: EdgeInsets.only(top: 10.0),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            CommonColors.buttonGradientColorTop,
                            CommonColors.buttonGradientColorBottom
                          ]),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Theme(
                      data: Theme.of(context).copyWith(
                        unselectedWidgetColor: Colors.white,
                        dividerColor: Theme.of(context).backgroundColor
                      ),
                      child: ExpansionTile(
                        iconColor: Colors.white,
                        title: Text(
                          'Lorem ipsum dolor sit amet?',
                          style: CommonStyle.getAppFont(
                              color: Colors.white, fontSize: 18.0),
                        ),
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.all(10.0),
                              color: Theme.of(context).scaffoldBackgroundColor,
                              child: Text(
                                'Lorem ipsum dolor sit amet consectetur adipiscing elit est feugiat convallis aliquam, inceptos viverra consequat Pellentesque laoreet integer sollicitudin vestibulum vitae elementum.',
                                style: CommonStyle.getAppFont(
                                    color: CommonColors.getWhiteTextColor(context),
                                    fontSize: 18.0),
                              ))
                        ],
                      ),
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}

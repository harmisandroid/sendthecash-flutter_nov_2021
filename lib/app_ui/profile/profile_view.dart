import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/edit_profile/edit_profile_view.dart';
import 'package:sendcash_flutter/app_ui/full_image/full_image_view.dart';
import 'package:sendcash_flutter/app_ui/profile/back_passport_view.dart';
import 'package:sendcash_flutter/app_ui/profile/profile_view_model.dart';
import 'package:sendcash_flutter/app_ui/profile/front_passport_view.dart';
import 'package:sendcash_flutter/app_ui/upload_documents/upload_documents_view.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/image_view/circule_border_image.dart';
import 'package:sendcash_flutter/generated/assets.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({Key key}) : super(key: key);

  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  ProfileViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<ProfileViewModel>(context);

    if (mViewModel.loginDetails == null) {
      return Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        body: Stack(
          children: [
            GAppbar(
              title: "@johndoe",
              height: 153.0,
              actions: Image.asset(
                LocalImages.edit_profile,
                height: 23,
                width: 29,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 120, left: 15, right: 15),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0)),
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 25.0,
                        blurRadius: 25.0,
                        color: Theme.of(context).shadowColor,
                        offset: Offset(0.0, 25.0))
                  ],
                  color: Theme.of(context).backgroundColor),
              child: Container(
                height: MediaQuery.of(context).size.height * 0.7,
                padding: EdgeInsets.only(left: 20.0),
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
          ],
        ),
      );
    }

    final items = Container(
      padding: EdgeInsets.only(left: 20.0),
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(
                LocalImages.message,
                height: 18,
                width: 24,
              ),
              Flexible(
                child: Container(
                  margin: EdgeInsets.only(left: 10, top: 10.0, right: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            S.of(context).email,
                            textAlign: TextAlign.start,
                            style: CommonStyle.getAppFont(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                color: CommonColors.getWhiteTextColor(context)),
                          ),
                          !mViewModel.loginDetails.isEmailVerified
                              ? InkWell(
                                  onTap: () {
                                    String code = getRandomCodeFourDigit();
                                    if (mViewModel.loginDetails.email != null &&
                                        mViewModel
                                            .loginDetails.email.isNotEmpty) {
                                      mViewModel.sendOtpToEmail(
                                          email: mViewModel.loginDetails.email,
                                          otp: code);
                                    }
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 5.0, vertical: 2.0),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                        color: Colors.green),
                                    child: Text(
                                      S.of(context).verify,
                                      textAlign: TextAlign.start,
                                      style: CommonStyle.getAppFont(
                                          fontWeight: FontWeight.normal,
                                          fontSize: 12.0,
                                          color: Colors.white),
                                    ),
                                  ),
                                )
                              : Icon(
                                  Icons.check_circle,
                                  color: Colors.green,
                            size: 20.0,
                                )
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 20, top: 2),
                        child: Text(
                          mViewModel.loginDetails.email,
                          maxLines: 1,
                          textAlign: TextAlign.start,
                          style: CommonStyle.getAppFont(
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                              color: CommonColors.getWhiteTextColor(
                                      context)
                                  .withOpacity(0.5)),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            height: 1,
            margin: EdgeInsets.only(top: 8, right: 10.0),
            color: CommonColors.getWhiteTextColor(context)
                .withOpacity(.2),
          ),
          Row(
            children: [
              Image.asset(
                LocalImages.dob,
                height: 28,
                width: 24,
              ),
              Flexible(
                child: Container(
                  margin: EdgeInsets.only(left: 10, top: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        S.of(context).dob,
                        textAlign: TextAlign.start,
                        style: CommonStyle.getAppFont(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            color: CommonColors.getWhiteTextColor(context)),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 20, top: 2),
                        child: Text(
                          (mViewModel.loginDetails.birthdate == null ||
                                  mViewModel.loginDetails.birthdate.isEmpty)
                              ? "DD--MM-YYYY"
                              : mViewModel.loginDetails.birthdate,
                          maxLines: 3,
                          textAlign: TextAlign.start,
                          style: CommonStyle.getAppFont(
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                              color: CommonColors.getWhiteTextColor(
                                      context)
                                  .withOpacity(0.5)),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            height: 1,
            margin: EdgeInsets.only(top: 8, right: 10.0),
            color: CommonColors.getWhiteTextColor(context)
                .withOpacity(.2),
          ),
          Row(
            children: [
              Image.asset(
                LocalImages.location,
                height: 29,
                width: 22,
              ),
              Flexible(
                child: Container(
                  margin: EdgeInsets.only(left: 10, top: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        S.of(context).address,
                        textAlign: TextAlign.start,
                        style: CommonStyle.getAppFont(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            color: CommonColors.getWhiteTextColor(context)),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 20, top: 2),
                        child: Text(
                          getAddress(),
                          textAlign: TextAlign.start,
                          style: CommonStyle.getAppFont(
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                              color: CommonColors.getWhiteTextColor(
                                      context)
                                  .withOpacity(0.5)),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );

    final passportView = Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                S.of(context).passport,
                textAlign: TextAlign.start,
                style: CommonStyle.getAppFont(
                    fontWeight: FontWeight.w600,
                    fontSize: 18,
                    color: CommonColors.getWhiteTextColor(context)),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context,
                      CupertinoPageRoute(builder: (context) {
                    return UploadDocumentsView(
                      from: "profile",
                    );
                  }));
                },
                child: Image.asset(
                  LocalImages.edit_passport,
                  height: 19,
                  width: 25,
                ),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Flexible(
                flex: 1,
                child: InkWell(
                  onTap: () {
                    if (mViewModel.loginDetails.frontIdImage != null &&
                        mViewModel.loginDetails.frontIdImage.isNotEmpty) {
                      Navigator.push(context,
                          CupertinoPageRoute(builder: (context) {
                        return FullScreenImageView(
                          imageUrl: mViewModel.loginDetails.frontIdImage,
                          imageName: "Front Id",
                        );
                      }));
                    }
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: CommonColors.gradientColorBottom, width: 2),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: (mViewModel.loginDetails.frontIdImage == null ||
                              mViewModel.loginDetails.frontIdImage.isEmpty)
                          ? Image.asset(
                              Assets.imgImgIdFront,
                              height: 90,
                              width: 150,
                              fit: BoxFit.cover,
                            )
                          : Image.network(
                              mViewModel.loginDetails.frontIdImage.toString(),
                              height: 90,
                              width: 150,
                              fit: BoxFit.cover,
                            ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 10.0,
              ),
              Flexible(
                flex: 1,
                child: InkWell(
                  onTap: () {
                    if (mViewModel.loginDetails.backIdImage != null && mViewModel.loginDetails.backIdImage.isNotEmpty) {
                      Navigator.push(context,
                          CupertinoPageRoute(builder: (context) {
                        return FullScreenImageView(
                          imageUrl: mViewModel.loginDetails.backIdImage,
                          imageName: "Back Id",
                        );
                      }));
                    }
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: CommonColors.gradientColorBottom, width: 2),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: (mViewModel.loginDetails.backIdImage == null ||
                              mViewModel.loginDetails.backIdImage.isEmpty)
                          ? Image.asset(
                              Assets.imgImgIdBack,
                              height: 90,
                              width: 150,
                              fit: BoxFit.cover,
                            )
                          : Image.network(
                              mViewModel.loginDetails.backIdImage.toString(),
                              height: 90,
                              width: 150,
                              fit: BoxFit.cover,
                            ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );

    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Stack(
        children: [
          GAppbar(
              title: mViewModel.loginDetails.firstName,
              height: 153.0,
              actions: Image.asset(
                LocalImages.edit_profile,
                height: 23,
                width: 29,
              ),
              onIconClick: () async {
                await Navigator.push(context,
                    CupertinoPageRoute(builder: (context) {
                  return EditProfileView();
                }));
                if (mViewModel.loginDetails != null) {
                  mViewModel.getLoginDetails().then((value) {});
                }
              }),
          Container(
            margin: EdgeInsets.only(top: 120, left: 15, right: 15),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0)),
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(0.0, 25.0))
                ],
                color: Theme.of(context).backgroundColor),
            child: Container(
              height: 120.0,
              padding: EdgeInsets.only(left: 20.0),
              child: Stack(
                children: [
                  Row(
                    children: [
                      InkWell(
                        onTap: () {
                          mViewModel.openBottomSheet();
                        },
                        child: Container(
                          child: Stack(
                            children: [
                              CirculeBorderImage(
                                imageUrl: mViewModel.profileImage,
                                height: 60.0,
                                width: 60.0,
                              ),
                              Align(
                                alignment: AlignmentDirectional.bottomEnd,
                                child: Container(
                                    padding: EdgeInsets.all(5.0),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.grey,
                                              offset: Offset.zero,
                                              spreadRadius: 1.0,
                                              blurRadius: 1.0)
                                        ]),
                                    child: Image.asset(
                                      LocalImages.camera,
                                      height: 12,
                                      width: 12,
                                    )),
                              ),
                              Visibility(
                                child: Container(
                                  child: Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                  height: 60.0,
                                  width: 60.0,
                                ),
                                visible: mViewModel.isProfileUploading,
                              )
                            ],
                          ),
                          height: 60.0,
                          width: 60.0,
                        ),
                      ),
                      Flexible(
                          child: Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              children: [
                                Flexible(
                                    child: Text(
                                  mViewModel.loginDetails.firstName +
                                      " " +
                                      mViewModel.loginDetails.lastName,
                                  textAlign: TextAlign.start,
                                  style: CommonStyle.getAppFont(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18,
                                      color: CommonColors.getWhiteTextColor(
                                          context)),
                                )),
                                SizedBox(width: 10.0),
                                Image.asset(
                                  LocalImages.verify,
                                  height: 16,
                                  width: 17,
                                ),
                                SizedBox(width: 10.0),
                              ],
                            ),
                            Container(
                              padding: EdgeInsets.only(right: 36, top: 2),
                              child: Row(
                                children: [
                                  SizedBox(
                                    height: 8,
                                  ),
                                  Image.asset(
                                    Assets.iconsIcCall,
                                    height: 15.0,
                                    width: 15.0,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                 InkWell(
                                   onTap: (){
                                     Navigator.push(context, CupertinoPageRoute(builder:(context){
                                       return EditProfileView();
                                     }));
                                   },
                                   child: Text(
                                     mViewModel.loginDetails.mobileNo.isEmpty
                                         ? S.of(context).addPhoneNumber
                                         : mViewModel.loginDetails.phoneCode +
                                         " / " +
                                         mViewModel.loginDetails.mobileNo,
                                     textAlign: TextAlign.start,
                                     style: CommonStyle.getAppFont(
                                         fontWeight: FontWeight.w500,
                                         fontSize: 18,
                                         color: CommonColors.getWhiteTextColor(
                                             context)
                                             .withOpacity(0.5)),
                                   ),
                                 ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ))
                    ],
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 250.0),
            padding: EdgeInsets.all(10.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(10.0, 0.0))
                ]),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  items,
                  SizedBox(
                    height: 40,
                  ),
                  passportView
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  String getAddress() {
    if (mViewModel.loginDetails != null &&
        mViewModel.loginDetails.address.addressLine1 != null &&
        mViewModel.loginDetails.address.addressLine1.isNotEmpty) {
      return (mViewModel.loginDetails.address.addressLine1 +
          "," +
          mViewModel.loginDetails.address.addressLine2 +
          "," +
          mViewModel.loginDetails.address.city +
          "," +
          mViewModel.loginDetails.address.state +
          ",\n" +
          mViewModel.loginDetails.address.country +
          "-" +
          mViewModel.loginDetails.address.zipcode);
    } else {
      return "NA";
    }
  }
}

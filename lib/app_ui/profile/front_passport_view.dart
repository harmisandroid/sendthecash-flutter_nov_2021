import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/profile/profile_view_model.dart';
import 'package:sendcash_flutter/utils/local_images.dart';

class FrontPassportView extends StatefulWidget {
  const FrontPassportView({Key key}) : super(key: key);

  @override
  _FrontPassportViewState createState() => _FrontPassportViewState();
}

class _FrontPassportViewState extends State<FrontPassportView> {
  ProfileViewModel mViewModel;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }
  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<ProfileViewModel>(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: Image.network(mViewModel.loginDetails.frontIdImage,
      height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,),
    );
  }
}

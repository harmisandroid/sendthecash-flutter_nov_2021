import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

import '../../database/db_key.dart';
import '../../generated/i18n.dart';
import '../otp/otp_view.dart';

class ProfileViewModel with ChangeNotifier {
  BuildContext mContext;
  AppPreferences appPreferences = AppPreferences();
  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();
  LoginDetails loginDetails;
  String profileImage;
  final imagePicker = ImagePicker();
  bool isProfileUploading = false;
  String uploadingPercent = "0";
  String userId;
  ApiServices apiServices = ApiServices();

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    loginDetails = await appPreferences.getLoginDetails();
    userId = await appPreferences.getUserFirebaseId();
    if (loginDetails != null) {
      profileImage = loginDetails.profilePic;
      notifyListeners();
    }
  }

  Future<void> getLoginDetails() async {
    loginDetails = await appPreferences.getLoginDetails();
    notifyListeners();
  }

  Future<void> pickProfileImage(bool isCamera) async {
    XFile xFile = await imagePicker.pickImage(
        source: isCamera ? ImageSource.camera : ImageSource.gallery);

    if (xFile != null) {
      profileImage = xFile.path;

      isProfileUploading = true;
      notifyListeners();
      FirebaseStorage storage = FirebaseStorage.instance;
      Reference reference = storage.ref().child("profile_images/" +
          loginDetails.firstName +
          DateTime.now().microsecondsSinceEpoch.toString());
      UploadTask uploadTask = reference.putFile(File(profileImage));
      await uploadTask.then((takeSnapshot) async {
        String url = await takeSnapshot.ref.getDownloadURL();
        profileImage = url.toString();
        loginDetails.profilePic = profileImage;
        await firebaseDatabaseHelper.updateUserProfile(
            userdata: loginDetails.toJson(),
            userId: userId,
            onError: (error, stackTrace) {
              CommonUtils.showRedToastMessage("failed to update profile pic");
            },
            onSuccess: (value) {
              getUserDetails();
              CommonUtils.showGreenToastMessage(
                  "Profile image uploaded successfully!");
            });
      }).onError((error, stackTrace) {
        isProfileUploading = false;
        notifyListeners();
        CommonUtils.showRedToastMessage("failed to update profile pic");
      });

      // uploadTask.snapshotEvents.listen((event) {
      //   printf(((event.totalBytes/event.totalBytes) * 100).toInt().toString());
      //   uploadingPercent = ((event.totalBytes/event.totalBytes) * 100).toInt().toString();
      //   notifyListeners();
      // });

      // Waits till the file is uploaded then stores the download url

      notifyListeners();
    } else {
      CommonUtils.showRedToastMessage("failed to get image");
      printf("failed to get image");
    }
  }

  void openBottomSheet() {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        backgroundColor: Colors.white,
        context: mContext,
        builder: (context) {
          return Container(
            height: 180.0,
            child: ListView(
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                    pickProfileImage(true);
                  },
                  child: Container(
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.camera),
                        SizedBox(
                          width: 5.0,
                        ),
                        Text(
                          S.of(context).camera,
                          style: CommonStyle.getAppFont(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 15.0),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey.shade300,
                  height: 0.5,
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                    pickProfileImage(false);
                  },
                  child: Container(
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.photo),
                        SizedBox(
                          width: 5.0,
                        ),
                        Text(
                          S.of(context).gallery,
                          style: CommonStyle.getAppFont(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 15.0),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey.shade300,
                  height: 0.5,
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: 50.0,
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    child: Text(
                      S.of(context).cancel,
                      style: CommonStyle.getAppFont(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 15.0),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  void getUserDetails() async {
    await firebaseDatabaseHelper.getUserDetails(
        userId: userId,
        onSuccess: (documentReference) async {
          Map<String, dynamic> data =
              documentReference.data() as Map<String, dynamic>;
          await appPreferences.setLoginDetails(jsonEncode(data));
          isProfileUploading = false;
          notifyListeners();
        },
        onError: (error, stacktrace) {
          isProfileUploading = false;
          notifyListeners();
          CommonUtils.showRedToastMessage(
              "Failed to get user details " + error.toString());
        });
  }

  Future<void> sendOtpToEmail({otp, email}) async {
    CommonUtils.showProgressDialog(mContext);
    String response = await apiServices.sendOtpToEmail(
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
        },
        otp: otp,
        email: email);
    if (response == "Success") {
      CommonUtils.showGreenToastMessage(
          "otp sent to email address successfully");
      insertEmailotpToFirebase(emailAddress: email, otp: otp);
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showRedToastMessage("Failed to send email address");
    }
  }

  Future<void> insertEmailotpToFirebase(
      {String emailAddress, String otp}) async {
    Map<String, dynamic> emailParams = Map();
    emailParams[DbKey.EMAIL] = emailAddress;
    emailParams[DbKey.OTP] = otp;
    await firebaseDatabaseHelper.insertEmailFirebaseOtp(
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("failed to add otp firebase");
        },
        onSuccess: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage("Otp send successfully!");
          Navigator.push(mContext, CupertinoPageRoute(builder: (context) {
            return OTPView(
              mobileNumber: null,
              emailAddress: emailAddress,
              phoneCode: null,
              loginDetails: loginDetails,
              firebaseUserId: userId,
              from: AppConstants.SCREEN_PROFILE,
            );
          }));
        },
        userdata: emailParams,
        onDocumentAdded: (documentRefernce) async {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage("Otp send successfully!");
          bool isVerified = await Navigator.push(mContext,
              CupertinoPageRoute(builder: (context) {
            return OTPView(
              mobileNumber: null,
              emailAddress: emailAddress,
              phoneCode: null,
              loginDetails: loginDetails,
              firebaseUserId: userId,
              from: AppConstants.SCREEN_PROFILE,
            );
          }));

          if (isVerified != null && isVerified) {
            loginDetails = await appPreferences.getLoginDetails();
            notifyListeners();
            updateEmailAddressVerifiedTrue();
          }
        });
  }

  void updateEmailAddressVerifiedTrue() async {
    await firebaseDatabaseHelper.setEmailVerifiedTrue(
        onSuccess: (value) {
          CommonUtils.showRedToastMessage("Email verified successfully!");
        },
        onError: (error, object) {
          CommonUtils.showRedToastMessage("Failed to update email verified");
        },
        userId: userId,
        isEmailVerified: true);
  }
}

import 'package:flutter/material.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class AboutUsView extends StatefulWidget {
  const AboutUsView({Key key}) : super(key: key);

  @override
  _AboutUsViewState createState() => _AboutUsViewState();
}

class _AboutUsViewState extends State<AboutUsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Stack(
        children: [
          GAppbar(
            title: S.of(context).about,
            height: 153.0,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 150.0),
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(10.0, 0.0))
                ]),
            child: SingleChildScrollView(
              child: Text(
                  "Quis curabitur nullam orci eleifend ligula sapien interdum nostra viverra, porta odio pretium patent dictum natoque integer euismod cum, justo nascetur morbi faucibus vitae quisque lobortis congue. Quis curabitur nullam orci eleifend ligula interdum nostra viverra, porta odio pretium patent dictum natoque integer euismod cum, justo nascetur morbi faucibus vitae quisque lobortis congue.",style: CommonStyle.getAppFont(
                color: CommonColors.getWhiteTextColor(
                                      context)
              ),),
            ),
          )
        ],
      ),
    );
  }
}

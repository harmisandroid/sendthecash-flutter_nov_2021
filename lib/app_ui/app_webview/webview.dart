import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:sendcash_flutter/app_ui/error/error_view.dart';
import 'package:sendcash_flutter/app_ui/success/order_success.dart';
import 'package:sendcash_flutter/app_ui/success/success_view.dart';
import 'package:sendcash_flutter/params/wallet_order_reserve.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

 class AppWebView extends StatefulWidget {
   String url;
   WalletOrderReserve walletOrderReserve;
   AppWebView({this.url,this.walletOrderReserve});

  @override
   _AppWebViewState createState() => _AppWebViewState();
 }

 class _AppWebViewState extends State<AppWebView> {

   final flutterWebviewPlugin = new FlutterWebviewPlugin();
   ApiServices apiServices = ApiServices();
   bool isApiLoading = false;
   String transferId,orderId;


   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    flutterWebviewPlugin.onUrlChanged.listen((String url) {
      printf("url changed => "+url);
      // https://www.sendwyre.com/success?accountId=user%3AUS_L6VXRFYREDN&blockchainNetworkTx&dest=bitcoin%3An3kkrs7WRiWHxANJiiqcyqGh2N88yhPxxi&destAmount=0.00025453&fees=%7B%22BTC%22%3A0%2C%22USD%22%3A5%7D&orderId=WO_XY8YTJE8XU&transferId=TF_MHDVN2XH6JT
      if(url.startsWith("https://www.sendwyre.com/success")){
       // createWalletOrder(walletOrderReserve: widget.walletOrderReserve);
        Uri successUri = Uri.parse(url);
        transferId = successUri.queryParameters['transferId'];
        orderId = successUri.queryParameters['orderId'];
        Navigator.pushAndRemoveUntil(context, CupertinoPageRoute(builder: (context){
          return OrderSuccessView(orderId:orderId ,transactionId:transferId,);
        }), (route) => false);

      }else if(url.contains("https://www.sendwyre.com/failure")){
        Navigator.push(context, CupertinoPageRoute(builder:(context){
          return ErrorView();
        }));
      }
    });


  }





   @override
   Widget build(BuildContext context) {
     if(isApiLoading){
       return Scaffold(
         body: Center(
           child: Column(
             mainAxisAlignment: MainAxisAlignment.center,
             crossAxisAlignment: CrossAxisAlignment.center,
             children: [
              CircularProgressIndicator(color: CommonColors.primaryColor,),
              SizedBox(height: 20.0,),
              Text("Please wait... Placing order",style: CommonStyle.getAppFont(
                color: Colors.black,
                fontSize: 20.0,
                fontWeight: FontWeight.bold
              ),)
             ],
           ),
         ),
       );
     }

     return WebviewScaffold(
       url:widget.url,
       withZoom: true,
       withLocalStorage: true,
       hidden: true,
       initialChild: Container(
         color: Colors.white,
         child: const Center(
           child: CircularProgressIndicator(color: CommonColors.primaryColor,),
         ),
       ),
     );

   }

   void createWalletOrder({WalletOrderReserve walletOrderReserve}) async {
     setState(() {
       isApiLoading = true;
     });
     String response = await apiServices.createWalletOrder(
         params: jsonEncode(walletOrderReserve),
         onFailed: () {
           CommonUtils.showRedToastMessage("failed to create order");
         });
     setState(() {
       isApiLoading = true;
     });
     if (response != null) {
       var responseMap = jsonDecode(response);
       if (responseMap['id'] != null) {
         Navigator.pushAndRemoveUntil(context, CupertinoPageRoute(builder: (context) {
           return SuccessView(
             transactionId: responseMap['id'],
           );
         }), (route) => false);
       } else {
         CommonUtils.showRedToastMessage("failed to create order");
       }
     } else {
       CommonUtils.showRedToastMessage("failed to create order");
     }
   }
 }

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sendcash_flutter/app_ui/welcome/welcome_view.dart';
import 'package:sendcash_flutter/common_widgets/bottomnavbar/bottom_navigation_view.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/constant.dart';

class ChangePasswordViewModel with ChangeNotifier {
  BuildContext mContext;
  AppPreferences appPreferences = AppPreferences();
  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();
  LoginDetails loginDetails;
  String userId;
  String selectedDay, selectedMonth, selectedYear;
  String mFrom;

  Future<void> attachContext(BuildContext context, String from,
      String firebaseUserId, LoginDetails mLoginDetails) async {
    mContext = context;
    mFrom = from;
    if (mLoginDetails != null) {
      userId = firebaseUserId;
      loginDetails = mLoginDetails;
    } else {
      userId = await appPreferences.getUserFirebaseId();
      loginDetails = await appPreferences.getLoginDetails();
    }
    notifyListeners();
  }

  Future<void> updateUser({
    confirmPassword,
  }) async {
    DateTime currentDateTime = DateTime.now();
    DateFormat passwordDateFormat = DateFormat("dd/MM/yyyy HH:mm:ss");
    CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.updateUserProfile(
        userdata: {
          DbKey.PASSWORD: confirmPassword,
          DbKey.EMAIL_PASSWORD: loginDetails.email + confirmPassword,
          DbKey.CHANGE_PASSWORD_DATE: passwordDateFormat.format(currentDateTime)
        },
        userId: userId,
        onError: (error, stackTrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("failed to change the password");
        },
        onSuccess: (value) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage("Password Change Successfully!");
          getUserDetails();
        }).onError((error, stackTrace) {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showRedToastMessage("failed to change the password");
      notifyListeners();
    });
    notifyListeners();
  }

  void getUserDetails() async {
    await firebaseDatabaseHelper.getUserDetails(
        userId: userId,
        onSuccess: (documentReference) async {
          Map<String, dynamic> data =
              documentReference.data() as Map<String, dynamic>;
          await appPreferences.setLoginDetails(jsonEncode(data));
          await appPreferences.setUserFirebaseId(documentReference.id);
          if (mFrom != null && mFrom == AppConstants.SCREEN_FORGOT_PASSWORD) {
            Navigator.pushAndRemoveUntil(mContext,
                CupertinoPageRoute(builder: (mContext) {
              return BottomNavigationView();
            }), (route) => false);
          } else {
            Navigator.pop(mContext);
          }
        },
        onError: (error, stacktrace) {
          notifyListeners();
          CommonUtils.showRedToastMessage("Failed to get user details " + error.toString());
        });
  }
}

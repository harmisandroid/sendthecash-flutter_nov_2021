import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/change_password/change_password_view_model.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/gradient_button/gradientbutton.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class ChangePasswordView extends StatefulWidget {
  String from;
  LoginDetails loginDetails;
  String firebaseUserId;
  ChangePasswordView({Key key,this.loginDetails,this.firebaseUserId,this.from}) : super(key: key);

  @override
  _ChangePasswordViewState createState() => _ChangePasswordViewState();
}

class _ChangePasswordViewState extends State<ChangePasswordView> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  TextEditingController mNewPasswordController = TextEditingController();
  TextEditingController mConfirmPasswordController = TextEditingController();
  TextEditingController mOldPasswordController = TextEditingController();
  ChangePasswordViewModel mViewModel;
  bool isObscure;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context,widget.from,widget.firebaseUserId,widget.loginDetails);

    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<ChangePasswordViewModel>(context);

    if (mViewModel.loginDetails == null) {
      return Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        body: Container(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              GAppbar(
                title: S.of(context).changePassword,
                height: 153.0,
                onBackPressed: () {
                  Navigator.pop(context);
                },
              ),
              SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  margin: EdgeInsets.only(top: 150.0),
                  padding: EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      boxShadow: [
                        BoxShadow(
                            spreadRadius: 25.0,
                            blurRadius: 25.0,
                            color: Theme.of(context).shadowColor,
                            offset: Offset(10.0, 0.0))
                      ]),
                  child: Container(child: Text("Loading...."),),
                ),
              ),

            ],
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            GAppbar(
              title: S.of(context).changePassword,
              height: 153.0,
              onBackPressed: () {
                Navigator.pop(context);
              },
            ),
            Container(
              margin: EdgeInsetsDirectional.only(top: 150.0),
              decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 25.0,
                        blurRadius: 25.0,
                        color: Theme.of(context).shadowColor,
                        offset: Offset(10.0, 0.0))
                  ]),
              child: SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.all(20.0),
                  child: Container(
                    padding: EdgeInsets.only(left: 15.0, right: 15.0),
                    child: Form(
                      key: _formKey,
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 100.0,
                            ),
                            ((mViewModel.loginDetails.loginType == AppConstants.LOGIN_TYPE_NORMAL) && widget.firebaseUserId == null)
                                ? getEditText(
                                    hint: S.of(context).oldPassword,
                                    controller: mOldPasswordController,
                                    inputAction: TextInputAction.next,
                                    isObscure: true,
                                    inputType: TextInputType.text,
                                  )
                                : Container(),
                            getEditText(
                                hint: S.of(context).newPassword,
                                controller: mNewPasswordController,
                                isObscure: true,
                                inputAction: TextInputAction.next,
                                inputType: TextInputType.text),
                            getEditText(
                                hint: S.of(context).confirmPassword,
                                isObscure: true,
                                controller: mConfirmPasswordController,
                                inputAction: TextInputAction.done,
                                inputType: TextInputType.text),
                            SizedBox(
                              height: 40.0,
                            ),
                            GradientButton(
                              btnText: S.of(context).save,
                              onClick: () {
                                if (isValidData()) {
                                  mViewModel.updateUser(
                                    confirmPassword:
                                        mConfirmPasswordController.text,
                                  );
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getEditText({
    String hint,
    TextEditingController controller,
    TextInputAction inputAction,
    TextInputType inputType,
    int maxLine,
    int maxLenth,
    bool isObscure,
    Function(String) onSubmit,
  }) {
    return Container(
      height: 55,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 15),
      child: TextField(
        cursorColor: CommonColors.getWhiteTextColor(context),
        obscureText: isObscure ?? false,
        textInputAction: inputAction,
        keyboardType: inputType,
        controller: controller,
        onSubmitted: (value) {
          if (onSubmit != null) {
            onSubmit(value);
          }
        },
        style: CommonStyle.getAppFont(
            color: CommonColors.getWhiteTextColor(context),
            fontWeight: FontWeight.normal,
            fontSize: 16),
        decoration: InputDecoration(
            counterText: "",
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: CommonColors.getWhiteTextColor(context),
              ),
              borderRadius: BorderRadius.circular(30.0),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: CommonColors.getWhiteTextColor(context),
              ),
              borderRadius: BorderRadius.circular(30.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: CommonColors.getWhiteTextColor(context),
                  width: 0.0),
              borderRadius: BorderRadius.circular(30.0),
            ),
            contentPadding: EdgeInsets.only(left: 20, right: 10),
            filled: true,
            hintStyle: TextStyle(
                color: CommonColors.getWhiteTextColor(context),
                fontSize: 16,
                fontWeight: FontWeight.normal),
            hintText: hint,
            fillColor: Theme.of(context).backgroundColor),
        maxLines: maxLine ?? 1,
        maxLength: maxLenth ?? 200,
      ),
    );
  }

  // just check validation
  bool isValidData() {
    if (mOldPasswordController.text.isEmpty &&
        mViewModel.loginDetails.loginType == AppConstants.LOGIN_TYPE_NORMAL && widget.firebaseUserId == null) {
      CommonUtils.showRedToastMessage(S.of(context).oldPasswordValidation);
      return false;
    } else if (mOldPasswordController.text !=
            mViewModel.loginDetails.password &&
        mViewModel.loginDetails.loginType == AppConstants.LOGIN_TYPE_NORMAL && widget.firebaseUserId == null) {
      CommonUtils.showRedToastMessage(S.of(context).wrongOldPassword);
      return false;
    } else if (mNewPasswordController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).newPasswordValidation);
      return false;
    } else if (mConfirmPasswordController.text.isEmpty) {
      CommonUtils.showRedToastMessage(S.of(context).confirmPasswordValidation);
      return false;
    } else if (mConfirmPasswordController.text != mNewPasswordController.text) {
      CommonUtils.showRedToastMessage(S.of(context).passwordNotMatching);
      return false;
    } else {
      return true;
    }
  }
}

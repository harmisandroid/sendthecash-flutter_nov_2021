import 'package:flutter/material.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class CChart extends StatefulWidget {
  @override
  _CChartState createState() => _CChartState();
}

class _CChartState extends State<CChart> {
  List<String> verticalData = ["10", "20", "50", "57"];
  List<String> horizontalData = [
    "08:00",
    "09:00",
    "10:00",
    "11:00",
    "12:00",
    "13:00",
    "15:00",
    "16:00"
  ];

  List<double>  chartValue = [
    10.0,100.0,30.0,80.0,140.0,13.0,40.0,160.0
  ];

  @override
  Widget build(BuildContext context) {
    final verticalListview = Container(
      height: 300.0,
      margin: EdgeInsets.only(bottom: 20.0),
      child: ListView.builder(
          shrinkWrap: true,
          reverse: true,
          itemCount: verticalData.length,
          padding: EdgeInsets.all(0.0),
          itemBuilder: (context, index) {
            return Container(
              height: 53.0,
              width: MediaQuery.of(context).size.width,
              child: Row(
                children: [
                  Text(
                    "\$" + verticalData[index] + "K",
                    style: CommonStyle.getAppFont(
                      color: CommonColors.chartLabelColor,
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Flexible(child: Container(
                    height: 1.0,
                    color: CommonColors.boxColor,
                    width: MediaQuery.of(context).size.width,
                  ))
                ],
              ),
            );
          }),
    );

    final horizontalListview = Container(
      height: 300.0,
      margin: EdgeInsets.only(left: 40.0),
      padding: EdgeInsets.all(0.0),
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.bottomCenter,
      child: ListView.separated(
          shrinkWrap: true,
          itemCount: horizontalData.length,
          scrollDirection: Axis.horizontal,
          separatorBuilder: (context, index) {
            return SizedBox(
              width: 0.0,
            );
          },
          itemBuilder: (context, index) {
            return Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: 40.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Flexible(child:Container(
                      height: chartValue[index],
                      width: 8.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(3.5),
                          gradient: LinearGradient(
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                              colors: [
                                CommonColors.chartGradientTwo,
                                CommonColors.chartGradientOne
                              ])),
                    )),
                    SizedBox(
                      height: 20.0,
                    ),
                    RotationTransition(
                      turns: new AlwaysStoppedAnimation(-45 / 360),
                      child: Text(horizontalData[index],
                          style: CommonStyle.getAppFont(
                            color: CommonColors.chartLabelColor,
                          )),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                ),
              ),
            );
          }),
    );

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Center(
        child: Container(
          height: 300.0,
          margin: EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.height,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              verticalListview,
              Align(
                child: horizontalListview,
                alignment: Alignment.bottomCenter,
              )
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class WithdrawErrorView extends StatefulWidget {
  const WithdrawErrorView({Key key}) : super(key: key);

  @override
  _WithdrawErrorViewState createState() => _WithdrawErrorViewState();
}

class _WithdrawErrorViewState extends State<WithdrawErrorView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      bottomNavigationBar: Container(
        height: 68.0,
        margin: EdgeInsets.all(12),
        padding: EdgeInsets.all(10),
        child: Row(
          children: [
            Flexible(
                child: Container(
              height: 50,
              padding: EdgeInsetsDirectional.all(10.0),
              width: MediaQuery.of(context).size.width - 170,
              decoration: BoxDecoration(
                  color: Colors.white,
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        CommonColors.buttonGradientColorTop,
                        CommonColors.buttonGradientColorBottom
                      ]),
                  borderRadius: BorderRadius.circular(30.0),
                  boxShadow: [
                    BoxShadow(
                      color: CommonColors.gradientButtonShadow.withOpacity(.1),
                      blurRadius: 4.0,
                      spreadRadius: 3.0,
                    )
                  ]),
              child: Center(
                child: Text(
                  S.of(context).support,
                  style: CommonStyle.getAppFont(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 17),
                ),
              ),
            )),
            SizedBox(
              width: 10,
            ),
            Flexible(
                child: Container(
              height: 50,
              padding: EdgeInsetsDirectional.all(10.0),
              width: MediaQuery.of(context).size.width - 170,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        CommonColors.buttonGradientColorTop,
                        CommonColors.buttonGradientColorBottom
                      ]),
                  borderRadius: BorderRadius.circular(30.0),
                  boxShadow: [
                    BoxShadow(
                      color: CommonColors.gradientButtonShadow.withOpacity(.1),
                      blurRadius: 4.0,
                      spreadRadius: 3.0,
                    )
                  ]),
              child: Center(
                child: Text(
                  S.of(context).backToHome,
                  style: CommonStyle.getAppFont(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 17),
                ),
              ),
            ))
          ],
        ),
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: Stack(
          children: [
            GAppbar(
              title: S.of(context).withdrawError,
              height: 153.0,
            ),
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 150.0),
              padding: EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 25.0,
                        blurRadius: 25.0,
                        color: Colors.white,
                        offset: Offset(10.0, 0.0))
                  ]),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      LocalImages.error,
                      height: 252,
                      width: 386,
                    ),
                    Text(
                      S.of(context).error,
                      style: CommonStyle.getAppFont(
                          color: Colors.black,
                          fontSize: 28,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          S.of(context).succesfullySentTo,
                          style: CommonStyle.getAppFont(
                              color: CommonColors.getWhiteTextColor(context),
                              fontSize: 20,
                              fontWeight: FontWeight.w500),
                        ),
                        Text(
                          S.of(context).lane,
                          style: CommonStyle.getAppFont(
                              color: CommonColors.gradientColorBottom,
                              fontSize: 20,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      Globals.currencySymbol+"650.00",
                      style: CommonStyle.getAppFont(
                          color: CommonColors.walletItemFontColor,
                          fontSize: 28,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          S.of(context).transactionId,
                          style: CommonStyle.getAppFont(
                              color: CommonColors.getWhiteTextColor(context),
                              fontSize: 20,
                              fontWeight: FontWeight.w500),
                        ),
                        Text(
                          S.of(context).tansNo,
                          style: CommonStyle.getAppFont(
                              color: CommonColors.gradientColorBottom,
                              fontSize: 20,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Lorem ipsum dolor sit amet consectetur adipiscing elit est feugiat convallis aliquam, inceptos viverra consequat.",
                      textAlign: TextAlign.center,
                      style: CommonStyle.getAppFont(
                          color: CommonColors.getWhiteTextColor(context),
                          fontSize: 18,
                          fontWeight: FontWeight.normal),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

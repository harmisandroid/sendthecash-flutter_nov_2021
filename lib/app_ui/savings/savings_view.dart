import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/send_view/send_view.dart';
import 'package:sendcash_flutter/app_ui/home/home_view_model.dart';
import 'package:sendcash_flutter/app_ui/my_qr/my_qr_view.dart';
import 'package:sendcash_flutter/app_ui/notifications/notification_view.dart';
import 'package:sendcash_flutter/app_ui/scan_code/scan_code_view.dart';
import 'package:sendcash_flutter/app_ui/transfer_funds/transfer_funds_view.dart';
import 'package:sendcash_flutter/common_widgets/appbar/main_appbar.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/event_bus.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class SavingsView extends StatefulWidget {

  final GlobalKey<ScaffoldState> navScaffoldKey;
  SavingsView({this.navScaffoldKey});

  @override
  _SavingsViewState createState() => _SavingsViewState();
}

class _SavingsViewState extends State<SavingsView> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  PageController _pageController;
  HomeViewModel mViewModel;
  int pageChanged = 0;

  @override
  void initState() {
    _pageController = PageController(initialPage: 0);
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
    // TODO: implement initState
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<HomeViewModel>(context);
    final mainBody = Container(
      margin: EdgeInsets.only(top: 130.0,left: 20.0,right: 20.0),
      height: MediaQuery.of(context).size.height,
      child: Stack(
        children: [
          Container(
            height: 90.0,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Theme.of(context).scaffoldBackgroundColor,
                borderRadius: BorderRadius.all(Radius.circular(15)),
                boxShadow: [
                  BoxShadow(
                    color: Theme.of(context).shadowColor,
                    blurRadius: 6.0,
                  ),
                ]),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    /* _pageController.animateToPage(0,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeIn);*/
                    Navigator.push(context, CupertinoPageRoute(builder: (context){
                      return TransferFundsView();
                    }));

                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        LocalImages.send,
                        height: 25,
                        width: 32,
                      ),
                      SizedBox(height: 5.0,),
                      Text(
                        S.of(context).send,
                        textAlign: TextAlign.center,
                        style: CommonStyle.getAppFont(
                            color: CommonColors.getWhiteTextColor(context),
                            fontSize: 14.0,
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    /*_pageController.animateToPage(1,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeIn);*/
                    eventBus.fire(TabClickEvent("wallet"));
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        LocalImages.wallet,
                        height: 25,
                        width: 30,
                      ),
                      SizedBox(height: 5.0,),
                      Text(
                        S.of(context).wallet,
                        textAlign: TextAlign.center,
                        style: CommonStyle.getAppFont(
                            color: CommonColors.getWhiteTextColor(context),
                            fontSize: 14.0,
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    _pageController.animateToPage(2,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeIn);
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        LocalImages.scan,
                        height: 26,
                        width: 31,
                      ),
                      SizedBox(height: 5.0,),
                      Text(
                        S.of(context).scan,
                        textAlign: TextAlign.center,
                        style: CommonStyle.getAppFont(
                            color: CommonColors.getWhiteTextColor(context),
                            fontSize: 14.0,
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    _pageController.animateToPage(3,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeIn);
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        LocalImages.qr,
                        height: 24,
                        width: 24,
                      ),
                      SizedBox(height: 5.0,),
                      Text(
                        S.of(context).myQr,
                        textAlign: TextAlign.center,
                        style: CommonStyle.getAppFont(
                            color: CommonColors.getWhiteTextColor(context),
                            fontSize: 14.0,
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 90.0),
            color: Theme.of(context).backgroundColor,
            child: PageView(
              controller: _pageController,
              physics: ClampingScrollPhysics(),
              children: <Widget>[
                SendView(),
                SendView(),
                ScanQrCodeView(),
                MyQrCodeView()
              ],
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Stack(
        children: [
          MainAppbar(
            isBack: true,
            height: 184.0,
            titleText: S.of(context).savings,
            balanceText: Globals.currencySymbol+" "+Globals.getConvertedCurrency(15723.00),

            onIconClick: () {
              Navigator.pop(context);
            },
            onNotificationClick: () {
              Navigator.push(context,
                  CupertinoPageRoute(builder: (context) {
                    return NotificationsView();
                  }));
            },
          ),
          Container(
            margin: EdgeInsets.only(top: 180.0),
            padding: EdgeInsets.only(left: 20.0,right: 20.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(0.0, 0.0))
                ]),
            clipBehavior: Clip.antiAlias,

          ),
          mainBody

        ],
      ),
    );
  }
}

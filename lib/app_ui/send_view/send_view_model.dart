import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/models/user_master.dart';

import '../../generated/assets.dart';
import '../../utils/local_images.dart';

class SendViewModel with ChangeNotifier {
  BuildContext mContext;
  LoginDetails loginDetails;
  AppPreferences appPreferences = AppPreferences();
  Map<String, dynamic> accountMap = Map();
  List<CurrencyModel> currencyList = [];

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    loginDetails = await appPreferences.getLoginDetails();
    accountMap = await appPreferences.getAccountDetails();
    currencyList.clear();
    if (accountMap != null) {
      if(accountMap['totalBalances']['USD'] != null)
      currencyList.add(CurrencyModel(LocalImages.dollar, "USD", "United State Dollar","\$27000"));
      if(accountMap['totalBalances']['BTC'] != null)
      currencyList.add(CurrencyModel(LocalImages.bitcoin, "BTC", "Bitcoin",accountMap['totalBalances']['BTC'].toString()+"BTC"));
      if(accountMap['totalBalances']['EUR'] != null)
      currencyList.add(CurrencyModel("assets/img/ic_euro.png", "EUR", "Euro","€0.000052"));
      if(accountMap['totalBalances']['ETH'] != null)
      currencyList.add(CurrencyModel("assets/img/ic_ethereum.png", "ETH", "Ethereum",accountMap['totalBalances']['ETH'].toString()+"ETH"));
      if(accountMap['totalBalances']['AVAX'] != null)
        currencyList.add(CurrencyModel(Assets.iconsIcAvx, "AVAX", "Avalanche",accountMap['totalBalances']['AVAX'].toString()+"AVAX"));

    }
    notifyListeners();
  }
}

class CurrencyModel {
  String image;
  String shortName;
  String name;
  String value;

  CurrencyModel(this.image, this.shortName, this.name,this.value);
}

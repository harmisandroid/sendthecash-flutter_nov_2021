import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/home/home_view_model.dart';
import 'package:sendcash_flutter/app_ui/send_view/send_view_model.dart';
import 'package:sendcash_flutter/app_ui/transaction/transaction_view.dart';
import 'package:sendcash_flutter/common_widgets/cards/home_card_view.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class SendView extends StatefulWidget {
  const SendView({Key key}) : super(key: key);

  @override
  _SendViewState createState() => _SendViewState();
}

class _SendViewState extends State<SendView> {
  SendViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<SendViewModel>(context);
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Container(
        color: Theme.of(context).backgroundColor,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              HomeCardView(
                isCvv: false,
                totalBalance: Provider.of<HomeViewModel>(context,listen: false).totalBalance,
              ),
              ListView.builder(
                  padding: EdgeInsets.all(0.0),
                  itemCount: mViewModel.currencyList.length,
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: (){
                        Navigator.push(context, CupertinoPageRoute(builder:(context){
                          return TransactionView(currencyCode: mViewModel.currencyList[index].shortName,from: "Account",);
                        }));
                      },
                      child: getCurrencyItem(
                          currencyModel: mViewModel.currencyList[index]),
                    );
                  }),
              SizedBox(
                height: 150.0 + MediaQuery.of(context).viewPadding.bottom,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget getCurrencyItem({CurrencyModel currencyModel}) {
    return Container(
      margin: EdgeInsets.only(top: 10.0, left: 5.0, right: 5.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
                color: CommonColors.getLightShadowColor(context),
                blurRadius: 2.0,
                spreadRadius: 2.0,
                offset: Offset.zero)
          ],
          color: Theme.of(context).scaffoldBackgroundColor),
      padding: EdgeInsets.all(15.0),
      child: Row(
        children: [
          Flexible(
              flex: 1,
              child:Image.asset(
            currencyModel.image,
            height: 34,
            width: 34,
          )),
          Flexible(
              flex: 3,
              fit: FlexFit.tight,
              child:Container(
            padding: EdgeInsets.only(left: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  currencyModel.name,
                  style: CommonStyle.getAppFont(
                      color: CommonColors.getWhiteTextColor(context),
                      fontWeight: FontWeight.normal,
                      fontSize: 16),
                ),
                Text(currencyModel.shortName,
                    style: CommonStyle.getAppFont(
                        color: CommonColors.getLightWhiteTextColor(
                            context),
                        fontWeight: FontWeight.normal,
                        fontSize: 16)),
              ],
            ),
          )),
          Flexible(
            flex: 4,
              fit: FlexFit.tight,
              child: Align(
                alignment: AlignmentDirectional.centerEnd,
                child:Text(
                  currencyModel.value,
                  style: CommonStyle.getAppFont(
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold,
                      color: CommonColors.getWhiteTextColor(context)),
                ),
              ))
        ],
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/add_bank_account/add_bank_account.dart';
import 'package:sendcash_flutter/app_ui/add_funds/add_funds_view.dart';
import 'package:sendcash_flutter/app_ui/currency_rates/currency_rates_view.dart';
import 'package:sendcash_flutter/app_ui/notifications/notification_view.dart';
import 'package:sendcash_flutter/app_ui/transaction/transaction_view.dart';
import 'package:sendcash_flutter/app_ui/transfer_funds/transfer_funds_view.dart';
import 'package:sendcash_flutter/app_ui/wallet/wallet_view_model.dart';
import 'package:sendcash_flutter/app_ui/wallet_list_item/wallet_list_item_view.dart';
import 'package:sendcash_flutter/app_ui/withdraw/withdraw_view.dart';
import 'package:sendcash_flutter/app_ui/withdraw_error/withdraw_error_view.dart';
import 'package:sendcash_flutter/common_widgets/appbar/main_appbar.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/event_bus.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

import '../../common_widgets/cards/home_card_view.dart';
import '../../utils/common_utils.dart';
import '../empty_view/empty_view.dart';
import '../home/home_view_model.dart';
import '../transaction/transaction_item.dart';

class WalletView extends StatefulWidget {
  final GlobalKey<ScaffoldState> navScaffoldKey;

  WalletView({this.navScaffoldKey});

  @override
  _WalletViewState createState() => _WalletViewState();
}

class _WalletViewState extends State<WalletView> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  // PageController _pageController;
  // var tabsView = GlobalKey();
  // double stackHeight;
  double height, width;
  WalletViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    // _pageController =
    //     PageController(initialPage: 0, keepPage: true, viewportFraction: 1);
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  void dispose() {
    // _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    mViewModel = Provider.of<WalletViewModel>(context);
    final list = NotificationListener(
      child: SingleChildScrollView(
          child: Container(
        color: Theme.of(context).backgroundColor,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20.0),
              child: HomeCardView(
                isCvv: false,
                totalBalance: mViewModel
                    .totalBalance,
              ),
            ),
            Container(
              color: Theme.of(context).backgroundColor,
              margin: EdgeInsetsDirectional.only(start: 20.0, end: 20.0),
              padding: EdgeInsets.only(
                top: 15,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    S.of(context).recentTransaction,
                    style: CommonStyle.getAppFont(
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        color: CommonColors.getWhiteTextColor(context)),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          CupertinoPageRoute(builder: (context) {
                        return TransactionView(
                          from: "Wallet",
                        );
                      }));
                    },
                    child: Text(
                      S.of(context).viewAll,
                      style: CommonStyle.getAppFont(
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          color: CommonColors.gradientColorBottom),
                    ),
                  ),
                ],
              ),
            ),
            mViewModel.isEmptyTransactions
                ? EmptyView(
                    message: S.of(context).noTransactionFound,
                    height: 200.0,
                  )
                : Container(
                    child: ListView.builder(
                        padding: EdgeInsets.only(
                            bottom: 90.0, top: 10.0, left: 20.0, right: 20.0),
                        physics: ClampingScrollPhysics(),
                        itemCount: mViewModel.transferList.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext buildContext, int index) {
                          return mViewModel.transferList[index] == null
                              ? CommonUtils.getListItemProgressBar()
                              : TransactionItem(
                                  transferDetails:
                                      mViewModel.transferList[index],
                                );
                        }))
          ],
        ),
      )),
      onNotification: mViewModel.loadMoreTransactions,
    );

    final mainBody = Container(
      margin: EdgeInsets.only(top: 130.0),
      height: MediaQuery.of(context).size.height,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 50.0),
            padding: EdgeInsets.only(top: width > 360.0 ? 50.0 : 30.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(0.0, 0.0))
                ]),
            child: /*PageView(
                controller: _pageController,
                children: <Widget>[list, list, list, list] //Pages],
                )*/
                list,
          ),
          Container(
            height: (width > 360) ? 100.0 : 80.0,
            margin: EdgeInsets.only(bottom: 10.0),
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                Container(
                  width: 20.0,
                ),
                getItem(
                    context: context,
                    image: LocalImages.send,
                    onClick: () {
                      // _pageController.animateToPage(1, duration: Duration(milliseconds: 250), curve: Curves.easeIn);
                      Navigator.push(context,
                          CupertinoPageRoute(builder: (context) {
                        return TransferFundsView();
                      }));
                    },
                    text: S.of(context).send),
                getItem(
                    context: context,
                    image: LocalImages.exchange,
                    onClick: () {
                      //_pageController.animateToPage(0, duration: Duration(milliseconds: 250), curve: Curves.easeIn);
                      /*Navigator.push(context,
                          CupertinoPageRoute(builder: (context) {
                        return CurrencyRatesView();
                      }));*/
                      eventBus.fire(TabClickEvent("exchange"));
                    },
                    text: S.of(context).exchange),
                getItem(
                    context: context,
                    image: LocalImages.add,
                    onClick: () {
                      Navigator.push(context,
                          CupertinoPageRoute(builder: (context) {
                        return AddFundsView();
                      }));
                    },
                    text: S.of(context).add),
                getItem(
                    context: context,
                    image: LocalImages.withdraw,
                    onClick: () {
                      // _pageController.animateToPage(4,
                      //     duration: Duration(milliseconds: 250),
                      //     curve: Curves.easeIn);
                      Navigator.push(context,
                          CupertinoPageRoute(builder: (context) {
                        return WithdrawView();
                      }));
                    },
                    text: S.of(context).withdraw),
              ],
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      backgroundColor: Colors.transparent,
      key: scaffoldKey,
      body: Stack(
        children: [
          MainAppbar(
            isBack: false,
            height: 184.0,
            titleText: S.of(context).balance,
            balanceText: Globals.currencySymbol +
                " " +
                Globals.getConvertedCurrency(mViewModel.availableBalance),
            onIconClick: () {
              widget.navScaffoldKey.currentState.openDrawer();
            },
            onNotificationClick: () {
              Navigator.push(context, CupertinoPageRoute(builder: (context) {
                return NotificationsView();
              }));
            },
          ),
          Container(
            margin: EdgeInsets.only(top: 180.0),
            padding: EdgeInsets.only(left: 20.0, right: 20.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(0.0, 0.0))
                ]),
            clipBehavior: Clip.antiAlias,
          ),
          mainBody
        ],
      ),
      // bottomNavigationBar: BottomNavBarView(),
    );
  }

  Widget getItem({context, image, text, onClick}) {
    if (width > 360) {
      return InkWell(
        onTap: onClick,
        child: Container(
          height: 60.0,
          width: 95.0,
          margin: EdgeInsets.only(right: 10, top: 10.0, bottom: 10.0),
          decoration: BoxDecoration(
              color: CommonColors.getDropdownColor(context),
              borderRadius: BorderRadius.all(Radius.circular(15)),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.shade100,
                    blurRadius: 1.0,
                    spreadRadius: 1.0,
                    offset: Offset.zero),
              ]),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                image,
                height: 20.0,
                width: 20.0,
              ),
              SizedBox(
                height: 5.0,
              ),
              Text(
                text.toString().toUpperCase(),
                style: CommonStyle.getAppFont(
                    color: CommonColors.getWhiteTextColor(context),
                    fontSize: 14,
                    fontWeight: FontWeight.w500),
              )
            ],
          ),
        ),
      );
    } else {
      return Center(
        child: InkWell(
          onTap: onClick,
          child: Container(
            height: 70.0,
            width: 90.0,
            margin: EdgeInsets.only(right: 10, top: 5.0, bottom: 5.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 1.0,
                      blurRadius: 1.0,
                      color: Colors.grey.shade100,
                      offset: Offset.zero)
                ]),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  image,
                  height: 25.0,
                  width: 25.0,
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  text,
                  style: CommonStyle.getAppFont(
                      color: CommonColors.getWhiteTextColor(context),
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                )
              ],
            ),
          ),
        ),
      );
    }
  }
}

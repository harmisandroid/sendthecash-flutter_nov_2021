import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';

import '../../database/app_preferences.dart';
import '../../models/account_transfer_master.dart';
import '../../models/user_master.dart';
import '../../services/api_services.dart';
import '../../utils/globals.dart';

class WalletViewModel with ChangeNotifier {
  BuildContext mContext;
  LoginDetails loginDetails;
  AppPreferences appPreferences = new AppPreferences();
  ApiServices apiServices = ApiServices();
  Map<String, dynamic> walletMap = Map();
  List<TransferDetails> transferList = [];
  bool isEmptyTransactions = false;
  int offset = 0, limit = 10;
  bool isTransactionApiLoading = false;
  int totalTransactions = 0;
  double availableBalance = 0.0, totalBalance = 0.0;
  Map<String, dynamic> exchangeMap = Map();

  void attachContext(BuildContext context) async {
    mContext = context;
    transferList.clear();
    offset = 0;
    totalTransactions = 0;
    isEmptyTransactions = false;
    loginDetails = await appPreferences.getLoginDetails();
    walletMap = await appPreferences.getWalletDetails();
    if (walletMap != null) {
      getExchangeRates();
      getAllTransactionsByPage(
          accountId: walletMap['id'],
          limit: limit.toString(),
          offset: offset.toString());
    }
  }

  Future<void> getExchangeRates() async {
    CommonUtils.showProgressDialog(mContext);
    final response = await apiServices.getExhangeRates(onFailed: () {
      CommonUtils.showRedToastMessage("Failed to get data");
      CommonUtils.hideProgressDialog(mContext);
    });
    CommonUtils.hideProgressDialog(mContext);
    if (response != null) {
      Map<String, dynamic> mapResponse = jsonDecode(response) as Map;
      exchangeMap = mapResponse;
      lookUpWallet(walletId: walletMap['id']);
    } else {
      CommonUtils.showRedToastMessage("Failed to get data");
    }
  }

  Future<void> getAllTransactionsByPage({accountId, limit, offset}) async {
    isEmptyTransactions = false;
    isTransactionApiLoading = true;
    if (offset == "0") {
      CommonUtils.showProgressDialog(mContext);
    } else {
      transferList.add(null);
    }
    notifyListeners();

    AccountTransferMaster accountTransferMaster =
        await apiServices.getWalletTransfers(
            walletId: accountId,
            offset: offset,
            limit: limit,
            onFailed: () {
              CommonUtils.showRedToastMessage("Faild to get transactions");
              if (offset == "0") {
                CommonUtils.hideProgressDialog(mContext);
              }
            });
    isTransactionApiLoading = false;
    if (offset == "0") {
      CommonUtils.hideProgressDialog(mContext);
    } else {
      if (transferList.length > 0) {
        transferList.removeLast();
      }
    }
    if (accountTransferMaster != null) {
      if (accountTransferMaster.data != null &&
          accountTransferMaster.data.length > 0) {
        totalTransactions = accountTransferMaster.recordsTotal;
        transferList.addAll(accountTransferMaster.data);
      } else {
        isEmptyTransactions = true;
      }
      notifyListeners();
    } else {
      isEmptyTransactions = true;
      notifyListeners();
      CommonUtils.showRedToastMessage("Faild to get transactions");
    }
  }

  bool loadMoreTransactions(ScrollNotification scrollInfo) {
    if(walletMap != null && walletMap['id'] != null){
      if (scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent &&
          !isTransactionApiLoading &&
          transferList.length < totalTransactions) {
        offset = offset + 1;
        getAllTransactionsByPage(
            accountId: walletMap['id'], limit: "10", offset: offset.toString());
      }
    }

    return false;
  }

  Future<void> lookUpWallet({String walletId}) async {
    totalBalance = 0.0;
    availableBalance = 0.0;
    String response = await apiServices.lookUpWallet(
        walletId: walletId,
        onFailed: () {
          CommonUtils.showRedToastMessage("Failed to get wallet details");
        });
    if (response != null) {
      Map<String, dynamic> mapResponse = jsonDecode(response);
      if (mapResponse["totalBalances"] != null) {
        Map<String, dynamic> totalBalanceMap = mapResponse["totalBalances"];
        Map<String, dynamic> availablebalances = mapResponse["availableBalances"];
        totalBalanceMap.forEach((key, value) {
          totalBalance = totalBalance + exchangeMap[Globals.currencyName+key];
        });
        availablebalances.forEach((key, value) {
          availableBalance = availableBalance + exchangeMap[Globals.currencyName+key];
        });
        notifyListeners();
      }
    } else {
      CommonUtils.showRedToastMessage("Failed to get wallet details");
    }
  }
}

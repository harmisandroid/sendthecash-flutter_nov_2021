import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/my_qr/qr_view_model.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/text_style.dart';
import 'package:qr_flutter/qr_flutter.dart';

class MyQrCodeView extends StatefulWidget {
  @override
  _MyQrCodeViewState createState() => _MyQrCodeViewState();
}

class _MyQrCodeViewState extends State<MyQrCodeView> {
  MyQRCodeViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<MyQRCodeViewModel>(context);
    if(mViewModel.loginDetails == null){
      return Scaffold(
        body: Center(
          child: CircularProgressIndicator(color: CommonColors.primaryColor,),
        ),
      );
    }
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
        height: MediaQuery.of(context).size.height * 0.55,
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              color: Colors.white,
              child: QrImage(
                data: mViewModel.loginDetails.sendwyreAccountId,
                version: QrVersions.auto,
                size: 200.0,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              "Please scan the qr code to make a transactions or transfer crypto currency",
              textAlign: TextAlign.center,
              style: CommonStyle.getAppFont(
                  color: CommonColors.getWhiteTextColor(context),
                  fontSize: 15.0,
                  fontWeight: FontWeight.w500),
            )
          ],
        ),
      ),
    );
  }
}

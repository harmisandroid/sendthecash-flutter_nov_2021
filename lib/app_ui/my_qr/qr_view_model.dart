import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/models/user_master.dart';


class MyQRCodeViewModel with ChangeNotifier {
  BuildContext mContext;
  LoginDetails loginDetails;
  AppPreferences appPreferences = AppPreferences();


  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    loginDetails = await appPreferences.getLoginDetails();
    notifyListeners();
  }



}

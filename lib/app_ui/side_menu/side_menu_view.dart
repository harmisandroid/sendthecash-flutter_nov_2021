import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/profile/profile_view.dart';
import 'package:sendcash_flutter/app_ui/side_menu/side_menu_view_model.dart';
import 'package:sendcash_flutter/common_widgets/image_view/circule_border_image.dart';
import 'package:sendcash_flutter/generated/assets.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class SideMenuView extends StatefulWidget {
  Function(MenuModel) onMenuItemClick;
  SideMenuView({this.onMenuItemClick});

  @override
  _SideMenuViewState createState() => _SideMenuViewState();
}

class _SideMenuViewState extends State<SideMenuView> {
  SideMenuViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<SideMenuViewModel>(context);
    final userCard = InkWell(
      onTap: (){
        Navigator.pop(context);
        Navigator.push(context, CupertinoPageRoute(builder: (context){
          return ProfileView();
        }));
      },
      child: Container(
        padding: EdgeInsets.all(20.0),
        margin: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top + 20.0,
            left: 20.0,
            right: 20.0,
            bottom: 20.0),
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                  color: Theme.of(context).shadowColor,
                  blurRadius: 2.0,
                  spreadRadius: 2.0,
                  offset: Offset.zero)
            ]),
        child: Row(
          children: [
            CirculeBorderImage(imageUrl: mViewModel.loginDetails == null ? null : mViewModel.loginDetails.profilePic,),
            SizedBox(
              width: 10.0,
            ),
            Flexible(child:Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                        child:Text(
                      mViewModel.loginDetails == null ? "Guest" : mViewModel.loginDetails.firstName+" "+mViewModel.loginDetails.lastName,
                      maxLines: 1,
                      style: CommonStyle.getAppFont(
                          color: CommonColors.getWhiteTextColor(context),
                          fontSize: 20.0,
                          fontWeight: FontWeight.w500),
                    ),fit: FlexFit.loose,),
                    SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      Assets.iconsIcTick,
                      height: 15.0,
                      width: 15.0,
                    )
                  ],
                ),
                SizedBox(height: 5.0,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      Assets.iconsIcCall,
                      height: 15.0,
                      width: 15.0,
                      color: CommonColors.getLightWhiteTextColor(context),
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Flexible(child:Text(
                      mViewModel.loginDetails == null ? "NA" : (mViewModel.loginDetails.mobileNo.isEmpty ? "NA" : mViewModel.loginDetails.phoneCode+mViewModel.loginDetails.mobileNo),
                      style: CommonStyle.getAppFont(
                          color: CommonColors.getWhiteTextColor(context).withOpacity(0.5),
                          fontSize: 18.0,
                          fontWeight: FontWeight.w500),
                    ))
                  ],
                ),

              ],
            ))
          ],
        ),
      ),
    );
    return Container(
      width: MediaQuery.of(context).size.width * 0.9,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              end: Alignment.bottomRight,
              begin: Alignment.topLeft,
              colors: [
            CommonColors.gradientColorTop,
            CommonColors.gradientColorBottom
          ])),
      child: Stack(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.only(bottom: 50.0),
            child: Column(
              children: List.generate(mViewModel.menuList.length + 1, (index) {
                return index == 0
                    ? userCard
                    : getMenuItem(mViewModel.menuList[index - 1]);
              }),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(

              padding: EdgeInsets.symmetric(vertical: 20.0,horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Image.asset(
                      LocalImages.applogotext,
                      width: 100,
                    ),
                  ),
                  Text(S.of(context).appVersion + " 1.0.0",style: CommonStyle.getAppFont(
                      fontSize: 16.0,
                      fontWeight: FontWeight.normal,
                      color: Colors.white
                  ),)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  getMenuItem(MenuModel menuModel) {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
        if(menuModel.name == S.of(context).home || menuModel.name == S.of(context).wallets || menuModel.name == S.of(context).settings || menuModel.name == S.of(context).currencyRates){
          widget.onMenuItemClick(menuModel);

          return;
        }
        if (menuModel.screen != null) {
          Navigator.push(context, CupertinoPageRoute(builder: (context) {
            return menuModel.screen;
          }));
        }else{
          if(menuModel.name == S.of(context).logout){
           mViewModel.logoutUser();
          }
        }
      },
      child: Container(
        height: 40.0,
        margin: EdgeInsets.only(bottom: 15.0, left: 30.0),
        child: Row(
          children: [
            Image.asset(
              menuModel.icon,
              height: 40.0,
              width: 40.0,
            ),
            SizedBox(
              width: 20.0,
            ),
            Text(
              menuModel.name,
              style: CommonStyle.getAppFont(
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                  fontSize: 18.0),
            ),
          ],
        ),
      ),
    );
  }
}

import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:local_auth/local_auth.dart';
import 'package:sendcash_flutter/app_ui/about_us/about_us_view.dart';
import 'package:sendcash_flutter/app_ui/currency_rates/currency_rates_view.dart';
import 'package:sendcash_flutter/app_ui/faq/faq_view.dart';
import 'package:sendcash_flutter/app_ui/notifications/notification_view.dart';
import 'package:sendcash_flutter/app_ui/savings/savings_view.dart';
import 'package:sendcash_flutter/app_ui/support/support_view.dart';
import 'package:sendcash_flutter/app_ui/transaction/transaction_view.dart';
import 'package:sendcash_flutter/app_ui/transfer_funds/transfer_funds_view.dart';
import 'package:sendcash_flutter/app_ui/wallet/wallet_view.dart';
import 'package:sendcash_flutter/app_ui/welcome/welcome_view.dart';
import 'package:sendcash_flutter/common_widgets/bottomnavbar/bottom_navigation_view.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/generated/assets.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:http/http.dart' as http;
import 'package:sendcash_flutter/utils/constant.dart';

class SideMenuViewModel with ChangeNotifier {
  AppPreferences appPreferences = new AppPreferences();
  LoginDetails loginDetails;
  BuildContext mContext;
  List<MenuModel> menuList = [];

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    menuList.clear();
    menuList.add(MenuModel(S.of(context).home, Assets.iconsHome, null));
    menuList.add(
        MenuModel(S.of(context).wallets, Assets.iconsWallets, WalletView()));
    menuList.add(MenuModel(
        S.of(context).transfers, Assets.iconsTransfers, TransferFundsView()));
    menuList.add(
        MenuModel(S.of(context).savings, Assets.iconsSavings, SavingsView()));
    menuList.add(MenuModel(
        S.of(context).currencyRates, Assets.iconsCurrencyRates, null));
    menuList.add(MenuModel(S.of(context).transactions, Assets.iconsTransactions,
        TransactionView(from: "Account",)));
    menuList.add(MenuModel(
        S.of(context).notifications, Assets.iconsNotifications, NotificationsView()));
    menuList.add(MenuModel(S.of(context).faq, Assets.iconsFaq, FAQView()));
    menuList.add(MenuModel(S.of(context).about, Assets.iconsAbout, AboutUsView()));
    menuList.add(MenuModel(S.of(context).settings, Assets.iconsSettings, null));
    menuList.add(MenuModel(S.of(context).support, Assets.iconsSupport, SupportView()));
    menuList.add(MenuModel(S.of(context).logout, Assets.iconsLogout, null));
    loginDetails = await appPreferences.getLoginDetails();
    notifyListeners();
  }

  void logoutUser() {
    appPreferences.removeLogindetails();
    Navigator.pushAndRemoveUntil(mContext,
        CupertinoPageRoute(builder: (context) {
      return WelcomeView();
    }), (route) => false);
  }
}

class MenuModel {
  String name;
  String icon;
  Widget screen;

  MenuModel(this.name, this.icon, this.screen);
}

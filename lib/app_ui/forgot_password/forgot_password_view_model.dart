import 'dart:convert';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sendcash_flutter/app_ui/otp/otp_view.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import '../../utils/constant.dart';

class ForgotPasswordViewModel with ChangeNotifier {
  BuildContext mContext;
  AppPreferences appPreferences = AppPreferences();
  ApiServices apiServices = ApiServices();
  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();
  LoginDetails loginDetails;
  String userId;
  String selectedDay, selectedMonth, selectedYear;
  Country selectedCountry;
  String phoneCode;

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    if (selectedCountry == null) {
      phoneCode = "+1";
      selectedCountry = Country(
          phoneCode: "+1",
          countryCode: "US",
          e164Sc: 1223,
          geographic: true,
          level: 1524,
          name: "United States",
          example: "",
          displayName: "USA",
          displayNameNoCountryCode: "No",
          e164Key: "");
    }
    notifyListeners();
  }


  Future<void> insertOtpToFiresbase(
      {String mobileNumber, String phoneCode, String otp}) async {
    printf("Mobile => " + phoneCode + mobileNumber);
    Map<String, dynamic> mapDynamic = Map();
    mapDynamic[DbKey.MOBILE_NO] = mobileNumber;
    mapDynamic[DbKey.PHONE_CODE] = phoneCode;
    mapDynamic[DbKey.OTP] = otp;
    await firebaseDatabaseHelper.insertFirebaseOtp(
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("failed to add otp firebase");
        },
        onSuccess: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage("Otp send successfully!");
          Navigator.push(mContext, CupertinoPageRoute(builder: (context) {
            return OTPView(
              mobileNumber: mobileNumber,
              phoneCode: phoneCode,
              loginDetails: loginDetails,
              firebaseUserId: userId,
              from: AppConstants.SCREEN_FORGOT_PASSWORD,
            );
          }));
        },
        userdata: mapDynamic,
        onDocumentAdded: (documentRefernce) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage("Otp send successfully!");
          Navigator.push(mContext, CupertinoPageRoute(builder: (context) {
            return OTPView(
              mobileNumber: mobileNumber,
              phoneCode: phoneCode,
              loginDetails: loginDetails,
              firebaseUserId: userId,
              from: AppConstants.SCREEN_FORGOT_PASSWORD,
            );
          }));
        });
  }

  Future<void> showCountryPickerDialog() async {
    await showCountryPicker(
      context: mContext,
      showPhoneCode: true,
      onSelect: (Country value) {
        selectedCountry = value;
        phoneCode = "+" + value.phoneCode;
        notifyListeners();
      },
    );
  }

  Future<void> checkPhoneAlreadyExists({String mobileNumber, String phoneCode}) async {
    CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.checkMobileNumberExists(
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          Fluttertoast.showToast(
              msg: "failed to get userdetails " + error.toString());
        },
        onSuccess: (querysnapshot) async {
          if (querysnapshot.size > 0) {
            loginDetails = LoginDetails.fromJson(querysnapshot.docs[0].data());

            userId = querysnapshot.docs[0].id;
            String code = getRandomCodeFourDigit();
            Map<String, dynamic> sendOtpMap = Map();
            sendOtpMap['phone_number'] =
                phoneCode.replaceAll("+", "") + mobileNumber;
            sendOtpMap['message'] = "Your code is ${code.toString()}";
            sendOtpMap['message_type'] = "OTP";
            sendOtp(
                otpParams: sendOtpMap,
                mobileNumber: mobileNumber,
                phoneCode: phoneCode,
                otpCode: code);
          } else {
            CommonUtils.hideProgressDialog(mContext);
            CommonUtils.showRedToastMessage(
                S.of(mContext).userNotExistsWithThisMobileNo);
          }
        },
        mobileNumber: mobileNumber);
  }

  void sendOtp({Map<String, dynamic> otpParams,
      String mobileNumber,
      String phoneCode,
      String otpCode}) async {
    String response = await apiServices.sendOtp(
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("Failed to send otp");
        },
        mapParams: otpParams);
    if (response != null) {
      Map<String, dynamic> mapResponse = Map();
      mapResponse = jsonDecode(response);
      if (mapResponse['reference_id'] != null &&
          mapResponse['status']['code'] == 290) {
        insertOtpToFiresbase(
            mobileNumber: mobileNumber, phoneCode: phoneCode, otp: otpCode);
      } else {
        CommonUtils.hideProgressDialog(mContext);
        CommonUtils.showRedToastMessage("Failed to send otp");
      }
      // insertOtpToFiresbase(
      //     mobileNumber: mobileNumber, phoneCode: phoneCode, otp: otpCode);
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showRedToastMessage("Oops something went wrong");
    }
  }

  /*----------Email otp Verification ---------------*/

 void checkEmailExistsWithEmail({String emailAddress}) async{
   CommonUtils.showProgressDialog(mContext);
   await firebaseDatabaseHelper.checkEmailExists(
       onError: (error, stacktrace) {
         CommonUtils.hideProgressDialog(mContext);
         Fluttertoast.showToast(
             msg: "failed to get userdetails " + error.toString());
       },
       onSuccess: (querysnapshot) async {
         if (querysnapshot.size > 0) {
           loginDetails = LoginDetails.fromJson(querysnapshot.docs[0].data());
           userId = querysnapshot.docs[0].id;
           String code = getRandomCodeFourDigit();
           sendOtpToEmail(otp: code,email: emailAddress);
         } else {
           CommonUtils.hideProgressDialog(mContext);
           CommonUtils.showRedToastMessage(
               S.of(mContext).userNotExistsWithThisMobileNo);
         }
       },
       emailAddress: emailAddress);
 }

 Future<void> sendOtpToEmail({otp,email}) async {
   String response = await apiServices.sendOtpToEmail(onFailed: (){
     CommonUtils.hideProgressDialog(mContext);
   },otp: otp,email: email);
   if(response == "Success"){
     CommonUtils.showGreenToastMessage("otp sent to email address successfully");
     insertEmailotpToFirebase(emailAddress: email,otp: otp);
   }else{
     CommonUtils.hideProgressDialog(mContext);
     CommonUtils.showRedToastMessage("Failed to send email address");
   }

 }

  Future<void> insertEmailotpToFirebase(
      {String emailAddress, String otp}) async {
    Map<String,dynamic>  emailParams =  Map();
    emailParams[DbKey.EMAIL] = emailAddress;
    emailParams[DbKey.OTP] = otp;
    await firebaseDatabaseHelper.insertEmailFirebaseOtp(
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage("failed to add otp firebase");
        },
        onSuccess: () {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage("Otp send successfully!");
          Navigator.push(mContext, CupertinoPageRoute(builder: (context) {
            return OTPView(
              mobileNumber: null,
              emailAddress: emailAddress,
              phoneCode: null,
              loginDetails: loginDetails,
              firebaseUserId: userId,
              from: AppConstants.SCREEN_FORGOT_PASSWORD,
            );
          }));
        },
        userdata: emailParams,
        onDocumentAdded: (documentRefernce) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showGreenToastMessage("Otp send successfully!");
          Navigator.push(mContext, CupertinoPageRoute(builder: (context) {
            return OTPView(
              mobileNumber: null,
              emailAddress: emailAddress,
              phoneCode: null,
              loginDetails: loginDetails,
              firebaseUserId: userId,
              from: AppConstants.SCREEN_FORGOT_PASSWORD,
            );
          }));
        });
  }


}

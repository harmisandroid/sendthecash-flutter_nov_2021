import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/forgot_password/forgot_password_view.dart';
import 'package:sendcash_flutter/app_ui/forgot_password/forgot_password_view_model.dart';
import 'package:sendcash_flutter/app_ui/home/home_view.dart';
import 'package:sendcash_flutter/app_ui/welcome/user_view_model.dart';
import 'package:sendcash_flutter/common_widgets/appbar/back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/bottomnavbar/bottom_navigation_view.dart';
import 'package:sendcash_flutter/common_widgets/buttons/Buttons.dart';
import 'package:sendcash_flutter/common_widgets/edittext/edittext.dart';
import 'package:sendcash_flutter/common_widgets/logo/custom_logo.dart';
import 'package:sendcash_flutter/common_widgets/social_row/social_row.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/country_uils.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class ForgotPasswordView extends StatefulWidget {
  const ForgotPasswordView({Key key}) : super(key: key);

  @override
  _ForgotPasswordViewState createState() => _ForgotPasswordViewState();
}

class _ForgotPasswordViewState extends State<ForgotPasswordView> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final mEmailController = TextEditingController();
  final mPhoneController = TextEditingController();
  ForgotPasswordViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<ForgotPasswordViewModel>(context);

    final logoView = Container(
      alignment: Alignment.center,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Image.asset(
            LocalImages.applogotext,
            width: 240,
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            padding: EdgeInsets.only(left: 30, right: 30),
            child: Text(
              S.of(context).enterEmailOrpasswordToGetOtp,
              textAlign: TextAlign.center,
              maxLines: 3,
              style: CommonStyle.getAppFont(
                color: Colors.white,
                fontSize: 22,
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 26),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30.0)),
              color: CommonColors.shadowColor,
            ),
            height: 3,
            width: 28,
          ),
        ],
      ),
    );

    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
            CommonColors.gradientColorTop,
            CommonColors.gradientColorBottom
          ])),
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.transparent,
        appBar: BackAppbar(
          title: S.of(context).forgotPassword,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              children: [
                SizedBox(
                  height: 15,
                ),
                logoView,
                SizedBox(
                  height: 15,
                ),
                EditText(
                    hint: S.of(context).email,
                    controller: mEmailController,
                    inputAction: TextInputAction.done,
                    inputType: TextInputType.emailAddress),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 1,
                        width: 100.0,
                        color: CommonColors.shadowcolor.withOpacity(.7),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Text(
                        S.of(context).or,
                        style: CommonStyle.getAppFont(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Container(
                        height: 1,
                        width: 100.0,
                        color: CommonColors.shadowcolor.withOpacity(.7),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 48,
                  margin: EdgeInsets.only(top: 8),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30.0),
                      color: CommonColors.shadowcolor.withOpacity(.5)),
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () {
                          mViewModel.showCountryPickerDialog();
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                           mViewModel.selectedCountry == null ? Container() :  Container(
                             padding: EdgeInsets.only(left: 20),
                             child: Text(
                               Utils.countryCodeToEmoji(
                                   mViewModel.selectedCountry.countryCode),
                               style: TextStyle(fontSize: 22.0),
                             ),
                           ),
                            SizedBox(
                              width: 5.0,
                            ),
                            Text(
                             mViewModel.phoneCode == null ? "": mViewModel.phoneCode,
                              style: TextStyle(color: Colors.white),
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            Icon(
                              Icons.keyboard_arrow_down,
                              color: Colors.white,
                              size: 20,
                            )
                          ],
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          height: 28,
                          width: 2,
                          child: VerticalDivider(color: Colors.white)),
                      Flexible(
                        child: Container(
                          height: 50,
                          width: 220,
                          margin: EdgeInsets.only(left: 20),
                          child: TextField(
                            maxLines: 1,
                            maxLength: 11,
                            controller: mPhoneController,
                            cursorColor: Colors.white,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            // Only numbers can be entered

                            onSubmitted: (value) {},
                            style: TextStyle(fontSize: 15, color: Colors.white),
                            textInputAction: TextInputAction.next,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              counterText: "",
                              filled: true,
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30))),
                              fillColor: Colors.transparent,
                              contentPadding: EdgeInsets.zero,
                              hintStyle:
                                  TextStyle(color: Colors.white, fontSize: 15),
                              hintText: S.of(context).phoneNumber,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 40.0,
                ),
                Buttons(
                  text: S.of(context).submit,
                  onClick: () {
                    if (isValid()) {
                      if (mEmailController.text.isNotEmpty && CommonUtils.isvalidEmail(mEmailController.text.trim())) {
                       mViewModel.checkEmailExistsWithEmail(emailAddress: mEmailController.text.trim());
                      } else if (mPhoneController.text.isNotEmpty) {
                        // mViewModel.sendOtpToEmail();
                        mViewModel.checkPhoneAlreadyExists(
                            mobileNumber: mPhoneController.text,
                            phoneCode: mViewModel.phoneCode);

                      }
                    }
                  },
                ),
                SizedBox(
                  height: MediaQuery.of(context).viewPadding.bottom,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool isValid() {
    if (mEmailController.text.isEmpty && mPhoneController.text.isEmpty) {
      CommonUtils.showSnackBar(
          S.of(context).pleaseEnterEmailAddressOrPhone, scaffoldKey);
      return false;
    } else if (mEmailController.text.isNotEmpty) {
      if (!CommonUtils.isvalidEmail(mEmailController.text.trim())) {
        CommonUtils.showSnackBar(
            S.of(context).validEmailValidation, scaffoldKey);
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }
}

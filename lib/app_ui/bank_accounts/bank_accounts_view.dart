import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/add_bank_account/add_bank_account.dart';
import 'package:sendcash_flutter/app_ui/bank_accounts/bank_accounts_view_model.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/generated/assets.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/text_style.dart';
import 'package:url_launcher/url_launcher.dart';

class BankAccountsView extends StatefulWidget {
  @override
  _BankAccountsViewState createState() => _BankAccountsViewState();
}

class _BankAccountsViewState extends State<BankAccountsView> {
  BankAccountsViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<BankAccountsViewModel>(context);
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Stack(
        children: [
          GAppbar(
            title: S.of(context).bankAccount,
            height: 153.0,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 150.0),
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(10.0, 0.0))
                ]),
            child: Container(
              child: mViewModel.isEmptyList
                  ? Center(
                      child:InkWell(
                        onTap: (){
                          Navigator.push(context, CupertinoPageRoute(builder: (context){
                            return AddBankAccountView();
                          }));
                        },
                        child: Text(
                          "You have not added any bank accounts \n Add Bank Accounts",
                          style: CommonStyle.getAppFont(
                              fontWeight: FontWeight.w500,
                              fontSize: 16.0,
                              color: CommonColors.getHintTextStyle(context)),
                        ),
                      ),
                    )
                  : ListView.separated(
                      itemCount: mViewModel.accountList.length,
                      shrinkWrap: true,
                      padding: EdgeInsets.all(0.0),
                      separatorBuilder: (context, index) {
                        return Container(
                          margin: EdgeInsets.symmetric(vertical: 20.0),
                          width: MediaQuery.of(context).size.width,
                          height: 1.0,
                          color: CommonColors.getListDeviderColor(context),
                        );
                      },
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () async {
                           Navigator.pop(context,mViewModel.accountList[index]);
                          },
                          child: getLinkedAccountItem(mViewModel.accountList[index]),
                        );
                      }),
            ),
          )
        ],
      ),
    );
  }

  Widget getLinkedAccountItem(dynamic linkedAccountDetails) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            Assets.imgIcBank,
            height: 30.0,
            width: 30.0,
          ),
          SizedBox(
            width: 20.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                linkedAccountDetails[DbKey.BANK_TYPE_NAME],
                style: CommonStyle.getAppFont(
                    fontSize: 20.0,
                    fontWeight: FontWeight.normal,
                    color: CommonColors.getWhiteTextColor(context)),
              ),
              SizedBox(
                height: 5.0,
              ),
              Text(
                "XXXXXXXXXXXX"+linkedAccountDetails[DbKey.ACCOUNT_NUMBER].toString().substring(linkedAccountDetails[DbKey.ACCOUNT_NUMBER].toString().length - 4),
                style: CommonStyle.getAppFont(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    color: CommonColors.getLightWhiteTextColor(context)),
              )
            ],
          ),
          SizedBox(
            width: 10.0,
          ),
        ],
      ),
    );
  }
}

import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:local_auth/local_auth.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/linked_account_details.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:http/http.dart' as http;
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';
import 'package:twitter_login/entity/auth_result.dart';
import 'package:twitter_login/twitter_login.dart';

class BankAccountsViewModel with ChangeNotifier {
  AppPreferences appPreferences = new AppPreferences();
  LoginDetails loginDetails;
  BuildContext mContext;
  final facebookLogin = FacebookLogin();
  final LocalAuthentication localAuth = LocalAuthentication();
  ApiServices apiServices = ApiServices();
  final firebaseAuth = FirebaseAuth.instance;
  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();
  String firebaseUserId;
  List<dynamic> accountList = [];
  bool isEmptyList = false;

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    accountList.clear();
    isEmptyList = false;
    firebaseUserId = await appPreferences.getUserFirebaseId();
    getLinkedAccounts();
  }

  void getLinkedAccounts() async {
    CommonUtils.showProgressDialog(mContext);
    await firebaseDatabaseHelper.getBankAccounts(
        userId: firebaseUserId,
        onError: (error, stacktrace) {
          CommonUtils.hideProgressDialog(mContext);
          CommonUtils.showRedToastMessage(error.toString());
        },
        onSuccess: (QuerySnapshot<Map<String, dynamic>> querySnapshot) {
          CommonUtils.hideProgressDialog(mContext);
          if (querySnapshot.size > 0) {
            isEmptyList = false;
            querySnapshot.docs.forEach((element) {
              accountList.add(element);
            });
          } else {
            isEmptyList = true;
          }
          notifyListeners();
        });
  }
}

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/app/app_model.dart';
import 'package:sendcash_flutter/app_ui/home/home_view.dart';
import 'package:sendcash_flutter/app_ui/login/login_view.dart';
import 'package:sendcash_flutter/app_ui/upload_documents/upload_documents_view.dart';
import 'package:sendcash_flutter/app_ui/welcome/welcome_view.dart';
import 'package:sendcash_flutter/common_widgets/bottomnavbar/bottom_navigation_view.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/globals.dart';

class SplashViewModel with ChangeNotifier {
  AppPreferences appPreferences = new AppPreferences();
  LoginDetails loginDetails;
  bool isNetworkConnected = true;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  StreamSubscription subscription;
  BuildContext mContext;
  final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();
  String deviceToken = "";
  String userId;
  ApiServices apiServices = ApiServices();

  startTimer(BuildContext context) {
    mContext = context;

    return new Timer(Duration(milliseconds: 1000), () async {
      deviceToken = await appPreferences.getDeviceToken();
      loginDetails = await appPreferences.getLoginDetails();
      isNetworkConnected = await CommonUtils.isNetworkConnected();
      if (isNetworkConnected) {
        if (subscription != null) {
          subscription.cancel();
        }
        userId = await appPreferences.getUserFirebaseId();
        await readDeviceInfo();
      } else {
        CommonUtils.showRedSnackBar(
            S.of(mContext).noInternetConnected, scaffoldKey,
            duration: 10);
        registerConnctivityListener();
      }
    });
  }

  void registerConnctivityListener() {
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) async {
      if (result == ConnectivityResult.mobile ||
          result == ConnectivityResult.wifi) {
        subscription.cancel();
        await readDeviceInfo();
      }
    });
  }

  Future<void> navigateUser(String loginType) async {
    // check user verified mobile

    bool status = await appPreferences.getKeepLoggedIn();
    if (status != null && status) {
      Navigator.pushAndRemoveUntil(mContext,
          CupertinoPageRoute(builder: (context) {
        return BottomNavigationView();
      }), (route) => false);
    } else {
      if (loginType == AppConstants.LOGIN_TYPE_NORMAL) {
        Navigator.pushAndRemoveUntil(mContext,
            CupertinoPageRoute(builder: (context) {
          return WelcomeView(from: "splash",);
        }), (route) => false);
      } else {
        Navigator.pushAndRemoveUntil(mContext,
            CupertinoPageRoute(builder: (context) {
          return BottomNavigationView();
        }), (route) => false);
      }
    }
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      DbKey.DEVICE_OS: build.version.codename,
      DbKey.DEVICE_BRAND: build.brand,
      DbKey.DEVICE_ID: build.id,
      DbKey.DEVICE_MODEL: build.model,
      DbKey.DEVICE_TYPE: "ANDROID",
      DbKey.OS_VERSION: build.version.sdkInt,
      DbKey.USER_ID: "",
      DbKey.DEVICE_TOKEN: deviceToken,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      DbKey.DEVICE_OS: data.systemName,
      DbKey.DEVICE_BRAND: "Apple",
      DbKey.DEVICE_ID: data.identifierForVendor,
      DbKey.DEVICE_MODEL: data.localizedModel,
      DbKey.DEVICE_TYPE: "IOS",
      DbKey.OS_VERSION: data.systemVersion,
      DbKey.USER_ID: "",
      DbKey.DEVICE_TOKEN: deviceToken,
    };
  }

  Future<void> readDeviceInfo() async {
    var deviceData = <String, dynamic>{};
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidDeviceInfo =
            await deviceInfoPlugin.androidInfo;
        deviceData = _readAndroidBuildData(androidDeviceInfo);
      } else if (Platform.isIOS) {
        IosDeviceInfo iosDeviceInfo = await deviceInfoPlugin.iosInfo;
        deviceData = _readIosDeviceInfo(iosDeviceInfo);
      }
      await firebaseDatabaseHelper.checkDeviceExists(
          userdata: deviceData,
          onSuccess: (value) {

            if (value.size > 0) {
              updateDevice(deviceData, value.docs[0].id);
            } else {
              addDevice(deviceData);
            }
          },
          onError: (error, stacktrace) {
            addDevice(deviceData);
          });
    } on PlatformException {
      Fluttertoast.showToast(msg: "Failed to get device data");
    }
  }

  Future<void> addDevice(Map<String, dynamic> data) async {
    await firebaseDatabaseHelper.addDevice(
        data: data,
        onError: (error, stacktrace) {
          CommonUtils.showRedToastMessage("failed to add device");
        },
        onSuccess: (value) {
          getUserDetails();
        });
  }

  Future<void> updateDevice(Map<String, dynamic> data, String id) async {
    await firebaseDatabaseHelper.updateDevice(
        data: data,
        id: id,
        onError: (error, stacktrace) {
          CommonUtils.showRedToastMessage("failed to update device");
        },
        onSuccess: (value) {
          getUserDetails();
        });
  }

  Future<void> getUserDetails() async {
    if (loginDetails != null) {
      await firebaseDatabaseHelper.getUserDetails(
          userId: userId,
          onError: (error, stacktrace) {
            CommonUtils.showRedToastMessage("failed to get user details");
          },
          onSuccess: (documentReference) async {
            Map<String, dynamic> data =
                documentReference.data() as Map<String, dynamic>;
            await appPreferences.setLoginDetails(jsonEncode(data));
            if (loginDetails.userAppSettings != null) {
              if (loginDetails.userAppSettings.isDarkTheme != null) {
                Provider.of<AppModel>(mContext, listen: false)
                    .updateTheme(loginDetails.userAppSettings.isDarkTheme);
              }
            }

            if (loginDetails.userAppSettings != null) {
              if (loginDetails.userAppSettings.currencySymbol != null &&
                  loginDetails.userAppSettings.currencySymbol.isNotEmpty) {
                Globals.currencySymbol =
                    loginDetails.userAppSettings.currencySymbol;
                Globals.currencyName =
                    loginDetails.userAppSettings.defaultCurrency;
              }
            }
            if(loginDetails.sendwyreAccountId != null && loginDetails.sendwyreAccountId.isNotEmpty){
              await getSendwyreAccountDetails(sendwyreAccounId:loginDetails.sendwyreAccountId);
            }
            if(loginDetails.walletId != null && loginDetails.walletId.isNotEmpty){
              await getWalletDetails(walletId:loginDetails.walletId);
            }

            if (loginDetails.profilePic == null ||
                loginDetails.profilePic.isEmpty ||
                loginDetails.frontIdImage == null ||
                loginDetails.frontIdImage.isEmpty ||
                loginDetails.backIdImage == null ||
                loginDetails.backIdImage.isEmpty) {
              bool isSkiped = await appPreferences.getIsDocumentSkiped();
              if (isSkiped) {
                navigateUser(loginDetails.loginType);
              } else {
                Navigator.pushAndRemoveUntil(mContext,
                    CupertinoPageRoute(builder: (context) {
                  return UploadDocumentsView(
                    from: "splash",
                  );
                }), (route) => false);
              }
            }else{
              navigateUser(loginDetails.loginType);
            }
          });
    } else {
      Navigator.pushAndRemoveUntil(mContext,
          CupertinoPageRoute(builder: (context) {
        return WelcomeView(from: "splash",);
      }), (route) => false);
    }
  }

  Future<void> getSendwyreAccountDetails({String sendwyreAccounId}) async {
    final response = await apiServices.getSendwyreAccountDetails(
        accountId: sendwyreAccounId,
        onFailed: () {
          // CommonUtils.showRedToastMessage("failed to get account details");
          printf("Failed to get account details");
        });
    if (response != null) {
      await appPreferences.setAccountDetails(response);
    } else {
      printf("Failed to get account details");
    }
  }

  Future<void> getWalletDetails({String walletId}) async {
    String response = await apiServices.getWalletDetails(
        walletId: walletId,
        onFailed: () {
          // CommonUtils.showRedToastMessage("Unable to get response");
          printf("Failed to get wallet details");
        });
    if (response != null) {
      await appPreferences.setWalletDetails(response);
    } else {
      printf("Failed to get wallet details");
    }
  }


}

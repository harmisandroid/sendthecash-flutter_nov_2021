import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/splash/splash_view_model.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/generated/assets.dart';

class SplashView extends StatefulWidget {
  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  SplashViewModel mViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);
    Future.delayed(Duration.zero, () {
      FirebaseMessaging.instance.getToken().then((value) {
        AppPreferences().setDeviceToken(value);
        mViewModel.startTimer(context);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<SplashViewModel>(context);
    return Scaffold(
      key: mViewModel.scaffoldKey,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            Image.asset(
              Assets.imgImgSplash,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.fill,
            ),
            Align(
              child: Container(
                padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom+10.0),
                child: CircularProgressIndicator(
                  color: Colors.white,
                  strokeWidth: 0.5,
                ),
              ),
              alignment: Alignment.bottomCenter,
            )
          ],
        ),
      ),
    );
  }
}

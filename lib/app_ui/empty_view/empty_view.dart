import 'package:flutter/material.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

import '../../utils/common_colors.dart';

class EmptyView extends StatelessWidget {
  double height;
  String message;


  EmptyView({this.height, this.message});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height ?? MediaQuery.of(context).size.height,
      width:MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            message ?? "No date found",
            style: CommonStyle.getAppFont(
                color: CommonColors.getHintTextStyle(context)),

          ),
          SizedBox(height: 20.0,),
        ],
      ),
    );
  }
}

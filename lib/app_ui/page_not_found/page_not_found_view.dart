import 'package:flutter/material.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/gradient_button/gradientbutton.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class PageNotFoundView extends StatefulWidget {
  const PageNotFoundView({Key key}) : super(key: key);

  @override
  _PageNotFoundViewState createState() => _PageNotFoundViewState();
}

class _PageNotFoundViewState extends State<PageNotFoundView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Stack(
        children: [
          GAppbar(
            title: S.of(context).pageNotFound,
            height: 153.0,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 150.0),
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                color: Theme.of(context).scaffoldBackgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Colors.white,
                      offset: Offset(10.0, 0.0))
                ]),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 30,
                ),
                Image.asset(
                  LocalImages.notfound,
                  height: 252,
                  width: 386,
                ),
                Text(
                  S.of(context).oppspage,
                  style: CommonStyle.getAppFont(
                      color: Colors.black,
                      fontSize: 24,
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Lorem ipsum dolor sit amet consectetur adipiscing elit est feugiat convallis aliquam, inceptos viverra consequat.",
                  textAlign: TextAlign.center,
                  style: CommonStyle.getAppFont(
                      color: CommonColors.getWhiteTextColor(context),
                      fontSize: 18,
                      fontWeight: FontWeight.normal),
                ),
                Spacer(),
                GradientButton(
                  btnText: S.of(context).backToHome,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

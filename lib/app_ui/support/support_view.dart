import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/support/support_view_model.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/gradient_button/gradientbutton.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class SupportView extends StatefulWidget {
  const SupportView({Key key}) : super(key: key);

  @override
  _SupportViewState createState() => _SupportViewState();
}

class _SupportViewState extends State<SupportView> {
  final mController = TextEditingController();
  SupportViewModel mViewModel;
  String selectedID = 'Select Transaction ID';
  var countryArray = [
    'Select Transaction ID',
    'D7856',
    'FD258',
    'E7896',
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero,(){
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<SupportViewModel>(context);
    final transactionId = Container(
      padding: EdgeInsets.only(left: 20, right: 30.0),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          border:
              Border.all(color: CommonColors.getWhiteTextColor(context).withOpacity(.5)),
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
          color: Theme.of(context).scaffoldBackgroundColor),
      child: DropdownButtonHideUnderline(
        child: DropdownButton(
          iconSize: 22,
          value: selectedID,
          isExpanded: true,
          dropdownColor: CommonColors.getDropdownColor(context),
          hint: Text(selectedID,
              style: CommonStyle.getAppFont(
                  color: Colors.black,
                  fontSize: 15.0,
                  fontWeight: FontWeight.normal)),
          style: CommonStyle.getAppFont(
              fontSize: 15.0, fontWeight: FontWeight.normal),
          icon: Icon(
            Icons.keyboard_arrow_down_outlined,
            color: CommonColors.getWhiteTextColor(context),
          ),
          items: countryArray.map((value) {
            return DropdownMenuItem(
              value: value,
              child: Text(
                value,
                style: CommonStyle.getAppFont(
                    fontSize: 15,
                    color: CommonColors.getWhiteTextColor(context),
                    fontWeight: FontWeight.normal),
              ),
            );
          }).toList(),
          onChanged: (String newValue) {
            setState(() {
              selectedID = newValue;
            });
          },
        ),
      ),
    );

    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Stack(
        children: [
          GAppbar(
            title: S.of(context).support,
            height: 153.0,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 150.0),
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(10.0, 0.0))
                ]),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  transactionId,
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(top: 20),
                    child: TextField(
                      cursorColor: CommonColors.getWhiteTextColor(context),
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.text,
                      controller: mController,
                      style: CommonStyle.getAppFont(
                          color: CommonColors.getWhiteTextColor(context),
                          fontWeight: FontWeight.normal,
                          fontSize: 16),
                      decoration: InputDecoration(
                          counterText: "",
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                            BorderSide(color: CommonColors.getWhiteTextColor(context)),
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: CommonColors.getWhiteTextColor(context)),
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: CommonColors.getWhiteTextColor(context)),
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          contentPadding:
                              EdgeInsets.only(left: 20, right: 10, top: 30),
                          filled: true,
                          hintStyle: TextStyle(
                              color:
                                  CommonColors.getWhiteTextColor(context).withOpacity(.5),
                              fontSize: 16,
                              fontWeight: FontWeight.normal),
                          hintText: S.of(context).typeText,
                          fillColor: Theme.of(context).scaffoldBackgroundColor),
                      maxLines: 16,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Lorem ipsum dolor sit amet consectetur adipiscing elit est feugiat convallis aliquam, inceptos viverra consequat.",
                    textAlign: TextAlign.center,
                    style: CommonStyle.getAppFont(
                        color: CommonColors.getWhiteTextColor(context),
                        fontSize: 18,
                        fontWeight: FontWeight.normal),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  GradientButton(
                    btnText: S.of(context).submit,
                    onClick: (){
                      if(selectedID != "Select Transaction ID" && mController.text.isNotEmpty){
                        mViewModel.addSupportRequest(supportText: mController.text,transactionId: selectedID);
                      }else{
                        if(selectedID == "Select Transaction ID"){
                          CommonUtils.showRedToastMessage("Please select transaction ID");
                        }else{
                          CommonUtils.showRedToastMessage("Please enter support message");
                        }
                      }
                    },

                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

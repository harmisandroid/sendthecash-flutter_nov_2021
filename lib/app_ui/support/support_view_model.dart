
import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:local_auth/local_auth.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/about_us/about_us_view.dart';
import 'package:sendcash_flutter/app_ui/app/app_model.dart';

import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/database/db_key.dart';
import 'package:sendcash_flutter/database/firebase_database_helper.dart';

import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/globals.dart';


class SupportViewModel with ChangeNotifier {
  AppPreferences appPreferences = new AppPreferences();
  LoginDetails loginDetails;
  BuildContext mContext;
  String userId;

  FirebaseDatabaseHelper firebaseDatabaseHelper = FirebaseDatabaseHelper();

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    loginDetails = await appPreferences.getLoginDetails();
    userId = await appPreferences.getUserFirebaseId();
    notifyListeners();
  }


  void addSupportRequest({transactionId,supportText}) async{
    CommonUtils.showProgressDialog(mContext);
    await  firebaseDatabaseHelper.addSupportRequest(
      data: {
        DbKey.SUPPORT_MESSAGE : supportText,
        DbKey.TRANSACTION_ID:transactionId
      },
      onError: (object,stacktrace){
        CommonUtils.showRedToastMessage("Failed to add support request");
        CommonUtils.hideProgressDialog(mContext);
        },
      onSuccess: (documentRefernce){
        CommonUtils.hideProgressDialog(mContext);
        CommonUtils.showGreenToastMessage("Support request send successfully");
        Navigator.pop(mContext);
      }
    );
  }







}





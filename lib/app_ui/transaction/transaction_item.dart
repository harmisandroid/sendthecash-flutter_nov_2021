import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sendcash_flutter/generated/assets.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/account_transfer_master.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class TransactionItem extends StatelessWidget {
  TransferDetails transferDetails;
  TransactionItem({this.transferDetails});

  @override
  Widget build(BuildContext context) {
    // RUNNING_CHECKS
    // COMPLETED
    //FAILED
    // PROCESSING
    String statusImage = Assets.iconsIcCheck;
    String statusText = S.of(context).statusPending;
    if(transferDetails.status == AppConstants.STATUS_COMPLETED){
      statusImage = Assets.iconsIcCheck;
      statusText = S.of(context).statusSuccessfull;
    }else if(transferDetails.status == AppConstants.STATUS_RUNNING_CHECKS){
      statusImage = Assets.iconsIcInfo;
      statusText = S.of(context).statusPending;
    }else if(transferDetails.status == AppConstants.STATUS_PROCESSING){
      statusImage = Assets.iconsIcInfo;
      statusText = S.of(context).statusPending;
    }else if(transferDetails.status == AppConstants.STATUS_FAILED){
      statusImage = Assets.iconsIcFailed;
      statusText = S.of(context).statusRejected;
    }
    DateTime date = new DateTime.fromMillisecondsSinceEpoch(int.parse(transferDetails.createdAt));
    String transactionDate =  DateFormat("dd MMM yyyy hh:mm a").format(date);


    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(child: Image.asset(statusImage,height: 40.0,width: 40.0,),flex: 1,),
              Flexible(
                  flex: 2,
                  child:Container(
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      statusText,
                      textAlign: TextAlign.start,
                      style: CommonStyle.getAppFont(
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          color: CommonColors.getWhiteTextColor(context)),
                    ),
                    SizedBox(height: 5.0),
                    Container(
                      padding: EdgeInsets.only(right: 36, top: 2),
                      child: Text(
                        transactionDate,
                        textAlign: TextAlign.start,
                        style: CommonStyle.getAppFont(
                            fontWeight: FontWeight.w500,
                            fontSize: 10.0,
                            color: CommonColors.getLightWhiteTextColor(context)),
                      ),
                    ),
                  ],
                ),
              )),
              Flexible(child: Align(
                alignment: AlignmentDirectional.centerEnd,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      child: Text(
                        (transferDetails.type == "INCOMING"? "+" : "-")+transferDetails.destAmount + " "+transferDetails.destCurrency,
                        style: CommonStyle.getAppFont(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: transferDetails.type == "INCOMING" ? CommonColors.green : CommonColors.red),
                      ),
                    ),
                    SizedBox(height: 5.0,),
                    Text(
                      transferDetails.sourceAmount+" "+transferDetails.sourceCurrency,
                      style: CommonStyle.getAppFont(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: CommonColors.getWhiteTextColor(context).withOpacity(.9)),
                    ),
                  ],
                ),
              ),flex: 3,)
            ],
          ),
          Container(
            height: 2,
            margin: EdgeInsets.only(top: 15, bottom: 10),
            color: CommonColors.getListDeviderColor(context),
          )
        ],
      ),
    );
  }
}

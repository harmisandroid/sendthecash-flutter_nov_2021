import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/account_transfer_master.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:intl/intl.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:http/http.dart' as http;

class TransactionViewModel with ChangeNotifier {
  BuildContext mContext;
  LoginDetails loginDetails;
  AppPreferences appPreferences = new AppPreferences();
  var formatter = new DateFormat('dd/MM/yyyy');
  String starDate = null, endDate = null;
  DateTime startDateTime, endDateTime;
  List<ChartModel> chartList = [];
  ApiServices apiServices = ApiServices();
  Map<String, dynamic> accountMap = Map();
  Map<String, dynamic> walletMap = Map();
  List<TransferDetails> transferList = [];
  List<TransferDetails> allTransactionList = [];
  bool isEmptyTransactions = false;
  int offset = 0, limit = 10;
  bool isTransactionApiLoading = false;
  int totalTransactions = 0;
  String mCurrencyCode, mFrom;

  void attachContext(
      BuildContext context, String currencyCode, String from) async {
    mContext = context;
    chartList.clear();
    transferList.clear();
    allTransactionList.clear();
    startDateTime = null;
    endDateTime = null;
    starDate = null;
    endDate = null;
    offset = 0;
    totalTransactions = 0;
    isEmptyTransactions = false;
    mFrom = from;
    mCurrencyCode = currencyCode;
    if (mCurrencyCode != null) {
      printf("CODE => " + mCurrencyCode);
    }
    chartList.add(ChartModel("J", 41));
    chartList.add(ChartModel("k", 31));
    chartList.add(ChartModel("L", 123));
    chartList.add(ChartModel("M", 130));
    chartList.add(ChartModel("N", 100));
    chartList.add(ChartModel("o", 10));
    chartList.add(ChartModel("P", 8));
    chartList.add(ChartModel("Q", 23));
    chartList.add(ChartModel("Q", 21));
    chartList.add(ChartModel("Q", 80));
    chartList.add(ChartModel("Q", 90));
    chartList.add(ChartModel("Q", 15));
    chartList.add(ChartModel("Q", 70));
    chartList.add(ChartModel("Q", 75));
    loginDetails = await appPreferences.getLoginDetails();
    accountMap = await appPreferences.getAccountDetails();
    walletMap = await appPreferences.getWalletDetails();

    if (from != null && from == "Wallet") {
      getWalletTransactionsByPage(
          walletId: walletMap['id'],
          limit: limit.toString(),
          offset: offset.toString());
      getAllWalletTransactions(
        accountId: walletMap['id'],
      );
    } else if (accountMap != null) {
      await getAllTransactionsByPage(
          accountId: accountMap['id'],
          limit: limit.toString(),
          offset: offset.toString());
      getAllTransactions(accountId: accountMap['id']);
    }

    notifyListeners();
  }

  Future<void> selectDate(BuildContext context, bool isStartDate) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2018),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: CommonColors.gradientColorBottom,
            accentColor: CommonColors.gradientColorBottom,
            colorScheme: ColorScheme.light(primary: CommonColors.primaryColor),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
    );

    if (isStartDate) {
      startDateTime = picked;
      if (endDateTime != null && endDateTime.isBefore(startDateTime)) {
        CommonUtils.showRedToastMessage(S.of(mContext).invalidDateSelected);
        return;
      }
      starDate = formatter.format(picked);
    } else {
      endDateTime = picked;
      if (startDateTime != null && startDateTime.isAfter(endDateTime)) {
        CommonUtils.showRedToastMessage(S.of(mContext).invalidDateSelected);
        return;
      }
      endDate = formatter.format(picked);
    }
    notifyListeners();
  }

  Future<void> showRangeDatePicker() async {
    DateTimeRange picked = await showDateRangePicker(
        context: mContext,
        firstDate: DateTime(DateTime.now().year - 20),
        lastDate: DateTime.now(),
        builder: (context, child) {
          return Column(
            children: [
              ConstrainedBox(
                constraints: BoxConstraints(
                  maxWidth: 400.0,
                ),
                child: child,
              )
            ],
          );
        });
  }

  Future<void> getAllTransactionsByPage({accountId, limit, offset}) async {
    isEmptyTransactions = false;
    isTransactionApiLoading = true;
    if (offset == "0") {
      CommonUtils.showProgressDialog(mContext);
    } else {
      transferList.add(null);
    }
    notifyListeners();
    AccountTransferMaster accountTransferMaster =
        await apiServices.getAccountTransfers(
            accountId: accountId,
            offset: offset,
            limit: limit,
            onFailed: () {
              CommonUtils.showRedToastMessage("Faild to get transactions");
              if (offset == "0") {
                CommonUtils.hideProgressDialog(mContext);
              }
            });
    isTransactionApiLoading = false;
    if (offset == "0") {
      CommonUtils.hideProgressDialog(mContext);
    } else {
      if (transferList.length > 0) {
        transferList.removeLast();
      }
    }
    if (accountTransferMaster != null) {
      if (accountTransferMaster.data != null &&
          accountTransferMaster.data.length > 0) {
        totalTransactions = accountTransferMaster.recordsTotal;
        transferList.addAll(accountTransferMaster.data);
      } else {
        isEmptyTransactions = true;
      }
      notifyListeners();
    } else {
      isEmptyTransactions = true;
      notifyListeners();
      CommonUtils.showRedToastMessage("Faild to get transactions");
    }
  }

  Future<void> getAllTransactions({accountId}) async {
    AccountTransferMaster accountTransferMaster =
        await apiServices.getAccountTransfers(
            accountId: accountId,
            onFailed: () {
              CommonUtils.showRedToastMessage("Faild to get transactions");
            });

    if (accountTransferMaster != null) {
      if (accountTransferMaster.data != null &&
          accountTransferMaster.data.length > 0) {
        allTransactionList.addAll(accountTransferMaster.data);
        // here to load list based on currency code from homescreen
        if (mCurrencyCode != null) {
          Future.delayed(Duration.zero, () {
            transferList.clear();
            accountTransferMaster.data.forEach((element) {
              if (element.destCurrency == mCurrencyCode ||
                  element.sourceCurrency == mCurrencyCode) {
                transferList.add(element);
              }
            });
            notifyListeners();
          });
        }
      }
    } else {
      CommonUtils.showRedToastMessage("Faild to get all transactions");
    }
  }

  bool loadMoreTransactions(ScrollNotification scrollInfo) {
    if (startDateTime == null && endDateTime == null && mCurrencyCode == null) {
      if (scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent &&
          !isTransactionApiLoading &&
          transferList.length < totalTransactions) {
        offset = offset + 1;
        if (mFrom != null && mFrom == "Wallet") {
          getWalletTransactionsByPage(
              walletId: accountMap['id'],
              limit: "10",
              offset: offset.toString());
        } else {
          getAllTransactionsByPage(
              accountId: accountMap['id'],
              limit: "10",
              offset: offset.toString());
        }
      }
      return false;
    }
  }

  void getListBetweenDates() {
    if (startDateTime != null && endDateTime != null) {
      transferList.clear();
      Future.delayed(Duration.zero, () {
        allTransactionList.forEach((element) {
          DateTime dateTime =
              DateTime.fromMillisecondsSinceEpoch(int.parse(element.createdAt));
          if (dateTime.isAfter(startDateTime) &&
              dateTime.isBefore(endDateTime)) {
            if (mCurrencyCode != null &&
                (element.destCurrency == mCurrencyCode ||
                    element.sourceCurrency == mCurrencyCode)) {
              transferList.add(element);
            } else {
              transferList.add(element);
            }
          }
        });
        CommonUtils.showGreenToastMessage("Transactions loaded successfully");
        if (transferList.length == 0) {
          isEmptyTransactions = true;
        }
        notifyListeners();
      });
    } else {
      CommonUtils.showRedToastMessage(S.of(mContext).pleaseSelectStartEndDate);
    }
  }

  Future<void> getWalletTransactionsByPage({walletId, limit, offset}) async {
    isEmptyTransactions = false;
    isTransactionApiLoading = true;
    if (offset == "0") {
      CommonUtils.showProgressDialog(mContext);
    } else {
      transferList.add(null);
    }
    notifyListeners();
    AccountTransferMaster accountTransferMaster =
        await apiServices.getWalletTransfers(
            walletId: walletId,
            offset: offset,
            limit: limit,
            onFailed: () {
              CommonUtils.showRedToastMessage("Faild to get transactions");
              if (offset == "0") {
                CommonUtils.hideProgressDialog(mContext);
              }
            });
    isTransactionApiLoading = false;
    if (offset == "0") {
      CommonUtils.hideProgressDialog(mContext);
    } else {
      if (transferList.length > 0) {
        transferList.removeLast();
      }
    }
    if (accountTransferMaster != null) {
      if (accountTransferMaster.data != null &&
          accountTransferMaster.data.length > 0) {
        totalTransactions = accountTransferMaster.recordsTotal;
        transferList.addAll(accountTransferMaster.data);
      } else {
        isEmptyTransactions = true;
      }
      notifyListeners();
    } else {
      isEmptyTransactions = true;
      notifyListeners();
      CommonUtils.showRedToastMessage("Faild to get transactions");
    }
  }

  Future<void> getAllWalletTransactions({accountId}) async {
    AccountTransferMaster accountTransferMaster =
        await apiServices.getWalletTransfers(
            walletId: accountId,
            onFailed: () {
              CommonUtils.showRedToastMessage("Faild to get transactions");
            });

    if (accountTransferMaster != null) {
      if (accountTransferMaster.data != null &&
          accountTransferMaster.data.length > 0) {
        allTransactionList.addAll(accountTransferMaster.data);
      }
    } else {
      CommonUtils.showRedToastMessage("Faild to get all transactions");
    }
  }

  Future<void> checkStoragePermissions() async {
    var status = await Permission.storage.status;
    if (status != null && status.isDenied) {
      await Permission.storage.request().then((value) async {
        if (value.isDenied) {
          CommonUtils.showRedToastMessage(
              "Please allow us to your gallery to download pdf");
        } else {
          downLoadPdf();
        }
      });
    } else {
      downLoadPdf();
    }
  }

  void downLoadPdf() async {
    Map<String, dynamic> mapParams = Map();
    mapParams["isWallet"] = (mFrom != null && mFrom == "Wallet") ? true : false;
    mapParams["id"] = (mFrom != null && mFrom == "Wallet")
        ? walletMap["id"]
        : accountMap["id"];
    if(startDateTime != null && endDateTime != null){
      mapParams['start_date'] = DateFormat("dd-MM-yyyy").format(startDateTime);
      mapParams['end_date'] = DateFormat("dd-MM-yyyy").format(endDateTime);
    }
    print(jsonEncode(mapParams));
    CommonUtils.showProgressDialog(mContext);
    Uint8List uint8list = await apiServices.downloaPdfFile(
        mapParams: mapParams,
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
        });
    CommonUtils.hideProgressDialog(mContext);
    if (uint8list != null) {
      final directory = await getApplicationDocumentsDirectory();
      int timeStamp = DateTime.now().millisecond;
      File fileToWrite = new File(
          "${directory.path}/${timeStamp.toString()}_transcations.pdf");
      await fileToWrite.writeAsBytes(uint8list);
      await OpenFile.open(fileToWrite.path);
    } else {
      CommonUtils.showRedToastMessage("Failed to download file");
    }
  }
}

class ChartModel {
  String character;
  double height;

  ChartModel(this.character, this.height);
}

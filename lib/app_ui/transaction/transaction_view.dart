import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/empty_view/empty_view.dart';
import 'package:sendcash_flutter/app_ui/notifications/notification_view.dart';
import 'package:sendcash_flutter/app_ui/pdf_viewer/pdf_view.dart';
import 'package:sendcash_flutter/app_ui/transaction/transaction_item.dart';
import 'package:sendcash_flutter/app_ui/transaction/transaction_view_model.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class TransactionView extends StatefulWidget {

  String currencyCode;
  String from;
  TransactionView({this.currencyCode,this.from});

  @override
  _TransactionViewState createState() => _TransactionViewState();
}

class _TransactionViewState extends State<TransactionView> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  TransactionViewModel mViewModel;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context,widget.currencyCode,widget.from);
    });
    // TODO: implement initState
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<TransactionViewModel>(context);
    final body = Container(
        margin: EdgeInsets.only(top: 210.0, left: 20, right: 20, bottom: 100.0),
        child: NotificationListener<ScrollNotification>(
          onNotification: mViewModel.loadMoreTransactions,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 260.0,
                  padding: EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      borderRadius: BorderRadius.circular(20.0),
                      boxShadow: [
                        BoxShadow(
                            color:
                                CommonColors.bottomSheetShadow.withOpacity(0.1),
                            blurRadius: 1.0,
                            spreadRadius: 1.0,
                            offset: Offset.zero)
                      ]),
                  // margin: EdgeInsets.only(top: 220.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        S.of(context).received,
                        textAlign: TextAlign.start,
                        style: CommonStyle.getAppFont(
                            color:
                                CommonColors.getWhiteTextColor(context)
                                    .withOpacity(.5),
                            fontWeight: FontWeight.w500,
                            fontSize: 16),
                      ),
                      Text(
                        Globals.currencySymbol + " 1052,00",
                        style: CommonStyle.getAppFont(
                            color: CommonColors.getWhiteTextColor(context),
                            fontWeight: FontWeight.w500,
                            fontSize: 20),
                      ),
                      Container(
                        height: 156.0,
                        margin: EdgeInsets.only(top: 10.0),
                        child: ListView.separated(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            physics: ClampingScrollPhysics(),
                            itemCount: mViewModel.chartList.length,
                            separatorBuilder: (context, index) {
                              return Container(
                                height: 156.0,
                                width: 20.0,
                                color: Theme.of(context).backgroundColor,
                              );
                            },
                            itemBuilder: (context, index) {
                              return Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Container(
                                    height: mViewModel.chartList[index].height,
                                    width: 8.0,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(3.5),
                                        gradient: LinearGradient(
                                            begin: Alignment.bottomCenter,
                                            end: Alignment.topCenter,
                                            colors: [
                                              CommonColors.chartGradientTwo,
                                              CommonColors.chartGradientOne
                                            ])),
                                  ),
                                  Container(
                                    height: 21.0,
                                    width: 15.0,
                                    child: Center(
                                      child: Text(
                                        mViewModel.chartList[index].character,
                                        maxLines: 1,
                                        style: CommonStyle.getAppFont(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.normal,
                                            color: CommonColors
                                                .getLightWhiteTextColor(
                                                   context)),
                                      ),
                                    ),
                                  )
                                ],
                              );
                            }),
                      )
                    ],
                  ),
                ),
                mViewModel.isEmptyTransactions ? EmptyView(message: S.of(context).noTransactionFound,height: 200.0,): Container(
                    child: ListView.builder(
                        padding: EdgeInsets.only(bottom: 90.0, top: 10.0),
                        physics: ClampingScrollPhysics(),
                        itemCount: mViewModel.transferList.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext buildContext, int index) {
                          return mViewModel.transferList[index] == null
                              ? CommonUtils.getListItemProgressBar()
                              : TransactionItem(
                            transferDetails:
                            mViewModel.transferList[index],
                          );
                        }))
              ],
            ),
          ),
        ));
    final bottomView = Container(
      height: 80.0 + MediaQuery.of(context).viewPadding.bottom,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          borderRadius: BorderRadius.circular(18.0),
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).shadowColor,
              blurRadius: 4.0,
              spreadRadius: 3.0,
            )
          ]),
      margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 10.0),
      padding: EdgeInsets.only(
          left: 10,
          right: 10.0,
          top: 10.0,
          bottom: MediaQuery.of(context).viewPadding.bottom),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
              child: InkWell(
            onTap: () {
            /*  Navigator.push(context, CupertinoPageRoute(builder: (context) {
                return PdfView();
              }));*/
              mViewModel.checkStoragePermissions();
            },
            child: Container(
              height: 50,
              padding: EdgeInsetsDirectional.all(10.0),
              width: MediaQuery.of(context).size.width - 170,
              decoration: BoxDecoration(
                  color: Colors.white,
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        CommonColors.buttonGradientColorTop,
                        CommonColors.buttonGradientColorBottom
                      ]),
                  borderRadius: BorderRadius.circular(30.0),
                  boxShadow: [
                    BoxShadow(
                      color: CommonColors.gradientButtonShadow.withOpacity(.1),
                      blurRadius: 4.0,
                      spreadRadius: 3.0,
                    )
                  ]),
              child: Center(
                child: Text(
                  S.of(context).downloadPdf,
                  style: CommonStyle.getAppFont(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 17),
                ),
              ),
            ),
          )),
          /* SizedBox(
            width: 10,
          ),
          Flexible(
              child: Container(
            height: 50,
            padding: EdgeInsetsDirectional.all(10.0),
            width: MediaQuery.of(context).size.width - 170,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      CommonColors.buttonGradientColorTop,
                      CommonColors.buttonGradientColorBottom
                    ]),
                borderRadius: BorderRadius.circular(30.0),
                boxShadow: [
                  BoxShadow(
                    color: CommonColors.gradientButtonShadow.withOpacity(.1),
                    blurRadius: 4.0,
                    spreadRadius: 3.0,
                  )
                ]),
            child: Center(
              child: Text(
                S.of(context).sendEmail,
                style: CommonStyle.getAppFont(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 17),
              ),
            ),
          ))*/
        ],
      ),
    );

    return Scaffold(
        key: scaffoldKey,
        extendBody: true,
        bottomNavigationBar: bottomView,
        body: Container(
          height: MediaQuery.of(context).size.height,
          color: Theme.of(context).backgroundColor,
          child: Stack(
            children: [
              GAppbar(
                title: S.of(context).transaction,
                actions: Image.asset(
                  LocalImages.notification,
                  height: 20,
                  width: 22,
                ),
                height: 153.0,
                onIconClick: () {
                  Navigator.push(context,
                      CupertinoPageRoute(builder: (context) {
                    return NotificationsView();
                  }));
                },
              ),
              Container(
                  height: 85,
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(15.0),
                  margin: EdgeInsets.only(left: 20, right: 20, top: 110.0),
                  decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      boxShadow: [
                        BoxShadow(
                            spreadRadius: 25.0,
                            blurRadius: 25.0,
                            color: Theme.of(context).shadowColor,
                            offset: Offset(0.0, 35.0))
                      ]),
                  child: Container(
                    height: 50,
                    padding: EdgeInsets.only(left: 8.0, right: 8.0),
                    decoration: BoxDecoration(
                        color: Theme.of(context).backgroundColor,
                        borderRadius: BorderRadius.all(Radius.circular(25)),
                        border: Border.all(
                            color: CommonColors.getWhiteTextColor(context))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Flexible(
                          child:InkWell(
                          onTap: () async {
                            mViewModel.selectDate(context, true);
                          },
                          child: Center(
                            child: Text(
                              mViewModel.starDate == null ? 'From Date ' : mViewModel.starDate,
                              style: CommonStyle.getAppFont(
                                  color:
                                  CommonColors.getGrayThemeColor(context),
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16),
                            ),
                          ),
                        ),flex: 3,fit: FlexFit.tight,),
                        Flexible(child:Container(
                          padding: EdgeInsets.all(5.0),
                          child: Text('-',
                              style: CommonStyle.getAppFont(
                                  color: CommonColors.getWhiteTextColor(
                                      context),
                                  fontWeight: FontWeight.w500,
                                  fontSize: 22)),
                        ),flex: 1,),
                        Flexible(child: InkWell(
                          onTap: () async {
                            mViewModel.selectDate(context, false);
                          },
                          child: Center(
                            child: Text(
                              mViewModel.endDate == null ? 'To Date ' : mViewModel.endDate,
                              style: CommonStyle.getAppFont(
                                  color:
                                  CommonColors.getGrayThemeColor(context),
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16),
                            ),
                          ),
                        ),flex: 3,fit: FlexFit.tight,),

                        Flexible(child:InkWell(
                          onTap: () {
                            mViewModel.getListBetweenDates();
                          },
                          child: Image.asset(
                            LocalImages.calender,
                            height: 25,
                            width: 22,
                            color: CommonColors.gradientColorBottom,
                          ),
                        ),flex: 1,)
                      ],
                    ),
                  )),
              body,
            ],
          ),
        ));
  }
}

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/services/wyre_params.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import '../../models/crypto_list_master.dart';
import '../../services/api_services.dart';
import '../../utils/globals.dart';
import 'currency_bottom_sheet.dart';

class TransferFundsViewModel with ChangeNotifier {
  BuildContext mContext;

  bool gettingContacts = false;
  String selectedPaymentMethod = null;
  String selectedCountryBank = null;
  String selectedBankType = null;
  List<dynamic> bankFieldList = [];
  Map<String, dynamic> transferMap = Map();
  bool isUseWalletBalance = true;
  LoginDetails loginDetails;
  AppPreferences appPreferences = AppPreferences();
  ApiServices apiServices = ApiServices();
  String bitcoinUSD = "";
  String currencyPair = "BTCUSD";
  List<CryptoListDetails> currencyList = [];
  List<CryptoListDetails> filteredCurrencyList = [];
  CryptoListDetails selectedSourceCurrency = null;
  CryptoListDetails selectedDestCurrency = null;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  double availableBalance = 0.0, totalBalance = 0.0;
  Map<String, dynamic> exchangeMap = Map();
  dynamic selectedBankAccount;

  Future<void> attachContext(BuildContext context) async {
    mContext = context;
    currencyList.clear();
    filteredCurrencyList.clear();
    loginDetails = await appPreferences.getLoginDetails();
    selectedBankAccount = null;
    await getCurrencyList();
    await getExchangeRates();
    notifyListeners();
  }

  void clearAllData() {
    selectedPaymentMethod = null;
    selectedCountryBank = null;
    selectedBankType = null;
    //  bankFieldList.clear();
  }

  void onSelectSource(CryptoListDetails currencyName) {
    selectedSourceCurrency = currencyName;
    notifyListeners();
  }

  void searchContacts(String query) {
    notifyListeners();
  }

  void onWalletChange(bool value) {
    isUseWalletBalance = value;
    notifyListeners();
  }

  void getCurrencyList() async {
    CommonUtils.showProgressDialog(mContext);
    CryptoListMaster cryptoListMaster =
        await apiServices.getAllCurrencies(onFailed: () {
      CommonUtils.hideProgressDialog(mContext);
    });

    if (cryptoListMaster != null) {
      currencyList.addAll(cryptoListMaster.result);
      getCryptoList();
    } else {
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showRedToastMessage("Failed to get message");
    }
  }

  void getCryptoList() async {
    CryptoListMaster cryptoListMaster =
        await apiServices.getCryptoList(onFailed: () {
      CommonUtils.hideProgressDialog(mContext);
    });
    CommonUtils.hideProgressDialog(mContext);
    if (cryptoListMaster != null) {
      currencyList.addAll(cryptoListMaster.result);
      filteredCurrencyList.addAll(cryptoListMaster.result);
      selectedSourceCurrency = currencyList[0];
      selectedDestCurrency = currencyList[29];
      notifyListeners();
    } else {
      CommonUtils.showRedToastMessage("Failed to get message");
    }
  }

  void showCurrencyBottomSheet(bool isDest) {
    showModalBottomSheet(
        context: mContext,
        clipBehavior: Clip.antiAlias,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        builder: (context) {
          return CurrencyBottomSheetView(
            currencyList: currencyList,
            onSelectList: (currencyListDetails) {
              if (isDest) {
                selectedDestCurrency = currencyListDetails;
                notifyListeners();
              } else {
                selectedSourceCurrency = currencyListDetails;
                notifyListeners();
              }

              Navigator.pop(context);
            },
          );
        });
  }

  Future<void> createTransferApi(
      {String sourceAmount,
      String depositAddress,
      Map<String, dynamic> bankParams}) async {
    Map<String, dynamic> mapParams = Map();
    mapParams[WyreParams.AUTO_CONFIRM] = true;
    mapParams[WyreParams.CHARGEABLE_PM] = false;
    if (isUseWalletBalance) {
      mapParams[WyreParams.SOURCE] = "wallet:${loginDetails.walletId}";
    } else {
      mapParams[WyreParams.SOURCE] =
          "account:${loginDetails.sendwyreAccountId}";
    }

    mapParams[WyreParams.SOURCE_CURRENCY] = selectedSourceCurrency.currencyCode;
    mapParams[WyreParams.SOURCE_AMOUNT] = sourceAmount;

    if (depositAddress != null && depositAddress.isNotEmpty) {
      mapParams[WyreParams.DEST] = depositAddress;
    } else {
      Map<String, dynamic> bankParams = Map();
      bankFieldList.forEach((element) {
        if (element['field_type'] == "drop") {
          bankParams[element['field_key']] = element['select'];
        } else {
          bankParams[element['field_key']] = element['field_value'];
        }
      });

      mapParams[WyreParams.DEST] = bankParams;
    }

    print("Craete Params => " + jsonEncode(mapParams));
    CommonUtils.showProgressDialog(mContext);
    String response = await apiServices.createTransfer(
        onFailed: () {
          CommonUtils.hideProgressDialog(mContext);
        },
        params: jsonEncode(mapParams),
        accountId: isUseWalletBalance
            ? loginDetails.walletId
            : loginDetails.sendwyreAccountId);
    CommonUtils.hideProgressDialog(mContext);
    if (response != null) {
      Map<String, dynamic> mapResponse = jsonDecode(response);
      if (mapResponse['exceptionId'] != null) {
        CommonUtils.showRedToastMessage(mapResponse['message']);
      } else {

      }
    }
  }

  /*------------ for displaying balance -----------------*/

  Future<void> getExchangeRates() async {
    CommonUtils.showProgressDialog(mContext);
    final response = await apiServices.getExhangeRates(onFailed: () {
      CommonUtils.showRedToastMessage("Failed to get data");
      CommonUtils.hideProgressDialog(mContext);
    });
    CommonUtils.hideProgressDialog(mContext);
    if (response != null) {
      Map<String, dynamic> mapResponse = jsonDecode(response) as Map;
      exchangeMap = mapResponse;
      lookUpWallet(walletId: loginDetails.walletId);
    } else {
      CommonUtils.showRedToastMessage("Failed to get data");
    }
  }

  Future<void> lookUpWallet({String walletId}) async {
    totalBalance = 0.0;
    availableBalance = 0.0;
    String response = await apiServices.lookUpWallet(
        walletId: walletId,
        onFailed: () {
          CommonUtils.showRedToastMessage("Failed to get wallet details");
        });
    if (response != null) {
      Map<String, dynamic> mapResponse = jsonDecode(response);
      if (mapResponse["totalBalances"] != null) {
        Map<String, dynamic> totalBalanceMap = mapResponse["totalBalances"];
        Map<String, dynamic> availablebalances = mapResponse["availableBalances"];
        totalBalanceMap.forEach((key, value) {
          totalBalance = totalBalance + exchangeMap[Globals.currencyName+key];
        });
        availablebalances.forEach((key, value) {
          availableBalance = availableBalance + exchangeMap[Globals.currencyName+key];
        });
        notifyListeners();
      }
    } else {
      CommonUtils.showRedToastMessage("Failed to get wallet details");
    }
  }
}

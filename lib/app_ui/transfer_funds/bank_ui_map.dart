Map<String, dynamic> uiMap = {
  "method_types": [
    {
      "method_name": "plaid processor token",
      "is_sub_list": false,
      "is_visible": false,
      "is_open": false,
      "data": [
        {
          "field_name": "Country",
          "field_key": "country",
          "select": "US",
          "field_value": ["US"],
          "is_visible": false,
          "field_type": "drop"
        },
        {
          "field_name": "Plaid processor token",
          "field_key": "plaidProcessorToken",
          "is_visible": false,
          "field_type": "textfield",
          "input_type": "text",
          "field_value": ""
        },
        {
          "field_name": "Payment method type",
          "field_key": "paymentMethodType",
          "is_visible": false,
          "field_type": "drop",
          "select": "LOCAL_TRANSFER",
          "field_value": ["LOCAL_TRANSFER"]
        }
      ]
    },
    {
      "method_name": "bank account",
      "is_sub_list": true,
      "is_visible": false,
      "is_open": false,
      "data": [
        {
          "field_name": "us bank account",
          "field_key": "us_bank_account",
          "field_type": "main_sub_item",
          "is_visible": false,
          "is_sub_list": true,
          "sub_list": [
            {
              "field_name": "us individual bank account",
              "field_key": "us_individual_bank_account",
              "field_value": "us individual bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "US",
                  "is_visible": false,
                  "field_value": ["US"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "USD",
                  "is_visible": false,
                  "field_value": ["USD"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "INDIVIDUAL",
                  "field_value": ["INDIVIDUAL"]
                },
                {
                  "field_name": "First name",
                  "field_key": "firstNameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Last name",
                  "field_key": "lastNameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Account Number",
                  "field_key": "accountNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Routing Number",
                  "field_key": "routingNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Account Type",
                  "field_key": "accountType",
                  "field_type": "drop",
                  "select": "CHECKING",
                  "is_visible": true,
                  "field_value": ["CHECKING", "SAVINGS"]
                }
              ]
            },
            {
              "field_name": "us corporate bank account",
              "field_key": "us_corporate_bank_account",
              "field_value": "us corporate bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "US",
                  "is_visible": false,
                  "field_value": ["US"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "USD",
                  "is_visible": false,
                  "field_value": ["USD"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "CORPORATE",
                  "field_value": ["CORPORATE"]
                },
                {
                  "field_name": "Name on the account",
                  "field_key": "beneficiaryCompanyName",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Email Address",
                  "field_key": "beneficiaryEmailAddress",
                  "field_type": "textfield",
                  "input_type": "email",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "EIN or TIN",
                  "field_key": "beneficiaryEinTin",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Bank account number",
                  "field_key": "accountNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Routing Number",
                  "field_key": "routingNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Account Type",
                  "field_key": "accountType",
                  "field_type": "drop",
                  "select": "CHECKING",
                  "is_visible": true,
                  "field_value": ["CHECKING", "SAVINGS"]
                }
              ]
            }
          ]
        },
        {
          "field_name": "br bank account",
          "field_key": "br_bank_account",
          "field_type": "main_sub_item",
          "is_visible": false,
          "is_sub_list": true,
          "sub_list": [
            {
              "field_name": "br individual bank account",
              "field_key": "br_individual_bank_account",
              "field_value": "br individual bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "BR",
                  "is_visible": false,
                  "field_value": ["BR"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "BRL",
                  "is_visible": false,
                  "field_value": ["BRL"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "INDIVIDUAL",
                  "field_value": ["INDIVIDUAL"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "Name on Account",
                  "field_key": "nameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Phone number",
                  "field_key": "accountHolderPhoneNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account Type",
                  "field_key": "accountType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "poupanca",
                  "field_value": ["poupanca", "corrente"]
                },
                {
                  "field_name": "CPF or CNPJ",
                  "field_key": "cpfCnpj",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account Type",
                  "field_key": "accountType",
                  "field_type": "drop",
                  "select": "CHECKING",
                  "is_visible": false,
                  "field_value": ["CHECKING", "SAVINGS"]
                },
                {
                  "field_name": "Bank Code",
                  "field_key": "bankCode",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Branch Code",
                  "field_key": "bankCode",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
              ]
            },
            {
              "field_name": "br corporate bank account",
              "field_key": "br_corporate_bank_account",
              "field_value": "br corporate bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "BR",
                  "is_visible": false,
                  "field_value": ["BR"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "BRL",
                  "is_visible": false,
                  "field_value": ["BRL"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "CORPORATE",
                  "field_value": ["CORPORATE"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "Name on Account",
                  "field_key": "nameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Phone number",
                  "field_key": "accountHolderPhoneNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account Type",
                  "field_key": "accountType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "poupanca",
                  "field_value": ["poupanca", "corrente"]
                },
                {
                  "field_name": "CPF or CNPJ",
                  "field_key": "cpfCnpj",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account Type",
                  "field_key": "accountType",
                  "field_type": "drop",
                  "select": "CHECKING",
                  "is_visible": false,
                  "field_value": ["CHECKING", "SAVINGS"]
                },
                {
                  "field_name": "Bank Code",
                  "field_key": "bankCode",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Branch Code",
                  "field_key": "bankCode",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
              ]
            }
          ]
        },
        {
          "field_name": "cn bank account",
          "field_key": "cn_bank_account",
          "field_type": "main_sub_item",
          "is_visible": false,
          "is_sub_list": true,
          "sub_list": [
            {
              "field_name": "cn individual bank account",
              "field_key": "cn_individual_bank_account",
              "field_value": "cn individual bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "CN",
                  "is_visible": false,
                  "field_value": ["CN"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "CNY",
                  "is_visible": false,
                  "field_value": ["CNY"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "INDIVIDUAL",
                  "field_value": ["INDIVIDUAL"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "Name on Account",
                  "field_key": "nameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Phone number",
                  "field_key": "accountHolderPhoneNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account number",
                  "field_key": "accountNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank Code",
                  "field_key": "bankCode",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank Name",
                  "field_key": "bankName",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Branch Name",
                  "field_key": "branchName",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank City",
                  "field_key": "bankCity",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank Province",
                  "field_key": "bankProvince",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
              ]
            },
            {
              "field_name": "cn corporate bank account",
              "field_key": "cn_corporate_bank_account",
              "field_value": "cn corporate bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "CN",
                  "is_visible": false,
                  "field_value": ["CN"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "CNY",
                  "is_visible": false,
                  "field_value": ["CNY"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "CORPORATE",
                  "field_value": ["CORPORATE"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "Name on Account",
                  "field_key": "nameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Phone number",
                  "field_key": "accountHolderPhoneNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account number",
                  "field_key": "accountNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank Code",
                  "field_key": "bankCode",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank Name",
                  "field_key": "bankName",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Branch Name",
                  "field_key": "branchName",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank City",
                  "field_key": "bankCity",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank Province",
                  "field_key": "bankProvince",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
              ]
            }
          ]
        },
        {
          "field_name": "mx bank account",
          "field_key": "mx_bank_account",
          "field_type": "main_sub_item",
          "is_visible": false,
          "is_sub_list": true,
          "sub_list": [
            {
              "field_name": "mx individual bank account",
              "field_key": "mx_individual_bank_account",
              "field_value": "mx individual bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "MX",
                  "is_visible": false,
                  "field_value": ["MX"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "MXN",
                  "is_visible": false,
                  "field_value": ["MXN"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "INDIVIDUAL",
                  "field_value": ["INDIVIDUAL"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "First name",
                  "field_key": "firstNameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Last name",
                  "field_key": "lastNameOnAccount",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Clabe",
                  "field_key": "clabe",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                }
              ]
            },
            {
              "field_name": "mx corporate bank account",
              "field_key": "mx_corporate_bank_account",
              "field_value": "mx corporate bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "MX",
                  "is_visible": false,
                  "field_value": ["MX"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "MXN",
                  "is_visible": false,
                  "field_value": ["MXN"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "CORPORATE",
                  "field_value": ["CORPORATE"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "First name",
                  "field_key": "firstNameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Last name",
                  "field_key": "lastNameOnAccount",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Clabe",
                  "field_key": "clabe",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Beneficiary name",
                  "field_key": "beneficiaryName",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                }
              ]
            }
          ]
        },
        {
          "field_name": "au bank account",
          "field_key": "au_bank_account",
          "field_type": "main_sub_item",
          "is_visible": false,
          "is_sub_list": true,
          "sub_list": [
            {
              "field_name": "au individual bank account",
              "field_key": "au_individual_bank_account",
              "field_value": "au individual bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "AU",
                  "is_visible": false,
                  "field_value": ["AU"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "AUD",
                  "is_visible": false,
                  "field_value": ["AUD"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "INDIVIDUAL",
                  "field_value": ["INDIVIDUAL"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "Name on account",
                  "field_key": "nameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account number",
                  "field_key": "accountNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "BSB Number",
                  "field_key": "bsbNumber",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                }
              ]
            }
          ]
        },
      ]
    },
    {
      "method_name": "swift account",
      "is_sub_list": false,
      "is_visible": false,
      "is_open": false,
      "data": [
        {
          "field_name": "Payment method type",
          "field_key": "paymentMethodType",
          "is_visible": false,
          "field_type": "drop",
          "select": "INTERNATIONAL_TRANSFER",
          "field_value": ["INTERNATIONAL_TRANSFER"]
        },
        {
          "field_name": "Country",
          "field_key": "country",
          "select": "US",
          "field_value": [
            "DZ",
            "AR",
            "AU",
            "AT",
            "BE",
            "BO",
            "BR",
            "CA",
            "CL",
            "CO",
            "CR",
            "CY",
            "CZ",
            "DK",
            "DO",
            "EE",
            "FI",
            "FR",
            "DE",
            "GR",
            "HK",
            "IS",
            "IN",
            "ID",
            "IE",
            "IL",
            "IT",
            "JP",
            "LV",
            "LT",
            "LU",
            "MY",
            "MX",
            "NP",
            "NL",
            "NZ",
            "NO",
            "PY",
            "PE",
            "PH",
            "PL",
            "PT",
            "SG",
            "SK",
            "SI",
            "ZA",
            "KR",
            "ES",
            "SE",
            "CH",
            "TZ",
            "TH",
            "TR",
            "GB",
            "US",
            "VN"
          ],
          "is_visible": false,
          "field_type": "drop"
        },
        {
          "field_name": "Currency",
          "field_key": "currency",
          "select": "USD",
          "field_value": ["EUR", "GBP", "USD", "HKT"],
          "is_visible": false,
          "field_type": "drop"
        },
        {
          "field_name": "BeneficiaryType",
          "field_key": "beneficiaryType",
          "select": "INDIVIDUAL",
          "field_value": ["INDIVIDUAL"],
          "is_visible": false,
          "field_type": "drop"
        },
        {
          "field_name": "paymentType",
          "field_key": "paymentType",
          "select": "LOCAL_BANK_TRANSFER",
          "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"],
          "is_visible": false,
          "field_type": "drop"
        },
        {
          "field_name": "Beneficiary Name",
          "field_key": "beneficiaryName",
          "field_type": "textfield",
          "input_type": "text",
          "is_visible": true,
          "field_value": ""
        },
        {
          "field_name": "IBAN/Account Number",
          "field_key": "accountNumber",
          "field_type": "textfield",
          "input_type": "number",
          "is_visible": true,
          "field_value": ""
        },
        {
          "field_name": "Swift or BIC",
          "field_key": "swiftBic",
          "field_type": "textfield",
          "input_type": "text",
          "is_visible": true,
          "field_value": ""
        },
      ]
    },
  ]
};


Map<String, dynamic> addBankUi = {
  "method_types": [
    {
      "method_name": "plaid processor token",
      "is_sub_list": false,
      "is_visible": false,
      "is_open": false,
      "data": [
        {
          "field_name": "Country",
          "field_key": "country",
          "select": "US",
          "field_value": ["US"],
          "is_visible": false,
          "field_type": "drop"
        },
        {
          "field_name": "Plaid processor token",
          "field_key": "plaidProcessorToken",
          "is_visible": false,
          "field_type": "textfield",
          "input_type": "text",
          "field_value": ""
        },
        {
          "field_name": "Payment method type",
          "field_key": "paymentMethodType",
          "is_visible": false,
          "field_type": "drop",
          "select": "LOCAL_TRANSFER",
          "field_value": ["LOCAL_TRANSFER"]
        }
      ]
    },
    {
      "method_name": "bank account",
      "is_sub_list": true,
      "is_visible": false,
      "is_open": false,
      "data": [
        {
          "field_name": "us bank account",
          "field_key": "us_bank_account",
          "field_type": "main_sub_item",
          "is_visible": false,
          "is_sub_list": true,
          "sub_list": [
            {
              "field_name": "us individual bank account",
              "field_key": "us_individual_bank_account",
              "field_value": "us individual bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "US",
                  "is_visible": false,
                  "field_value": ["US"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "USD",
                  "is_visible": false,
                  "field_value": ["USD"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "INDIVIDUAL",
                  "field_value": ["INDIVIDUAL"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "select": "INDIVIDUAL",
                  "is_visible": false,
                  "field_value": ["INDIVIDUAL"]
                },
                {
                  "field_name": "First name",
                  "field_key": "firstNameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Last name",
                  "field_key": "lastNameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Account Number",
                  "field_key": "accountNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Routing Number",
                  "field_key": "routingNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Account Type",
                  "field_key": "accountType",
                  "field_type": "drop",
                  "select": "CHECKING",
                  "is_visible": true,
                  "field_value": ["CHECKING", "SAVINGS"]
                }
              ]
            },
            {
              "field_name": "us corporate bank account",
              "field_key": "us_corporate_bank_account",
              "field_value": "us corporate bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "US",
                  "is_visible": false,
                  "field_value": ["US"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "USD",
                  "is_visible": false,
                  "field_value": ["USD"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "CORPORATE",
                  "field_value": ["CORPORATE"]
                },
                {
                  "field_name": "Name on the account",
                  "field_key": "beneficiaryCompanyName",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Email Address",
                  "field_key": "beneficiaryEmailAddress",
                  "field_type": "textfield",
                  "input_type": "email",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "EIN or TIN",
                  "field_key": "beneficiaryEinTin",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Bank account number",
                  "field_key": "accountNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Routing Number",
                  "field_key": "routingNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": true,
                  "field_value": ""
                },
                {
                  "field_name": "Account Type",
                  "field_key": "accountType",
                  "field_type": "drop",
                  "select": "CHECKING",
                  "is_visible": true,
                  "field_value": ["CHECKING", "SAVINGS"]
                }
              ]
            }
          ]
        },
        {
          "field_name": "br bank account",
          "field_key": "br_bank_account",
          "field_type": "main_sub_item",
          "is_visible": false,
          "is_sub_list": true,
          "sub_list": [
            {
              "field_name": "br individual bank account",
              "field_key": "br_individual_bank_account",
              "field_value": "br individual bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "BR",
                  "is_visible": false,
                  "field_value": ["BR"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "BRL",
                  "is_visible": false,
                  "field_value": ["BRL"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "INDIVIDUAL",
                  "field_value": ["INDIVIDUAL"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "Name on Account",
                  "field_key": "nameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Phone number",
                  "field_key": "accountHolderPhoneNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account Type",
                  "field_key": "accountType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "poupanca",
                  "field_value": ["poupanca", "corrente"]
                },
                {
                  "field_name": "CPF or CNPJ",
                  "field_key": "cpfCnpj",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account Type",
                  "field_key": "accountType",
                  "field_type": "drop",
                  "select": "CHECKING",
                  "is_visible": false,
                  "field_value": ["CHECKING", "SAVINGS"]
                },
                {
                  "field_name": "Bank Code",
                  "field_key": "bankCode",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Branch Code",
                  "field_key": "bankCode",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
              ]
            },
            {
              "field_name": "br corporate bank account",
              "field_key": "br_corporate_bank_account",
              "field_value": "br corporate bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "BR",
                  "is_visible": false,
                  "field_value": ["BR"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "BRL",
                  "is_visible": false,
                  "field_value": ["BRL"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "CORPORATE",
                  "field_value": ["CORPORATE"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "Name on Account",
                  "field_key": "nameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Phone number",
                  "field_key": "accountHolderPhoneNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account Type",
                  "field_key": "accountType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "poupanca",
                  "field_value": ["poupanca", "corrente"]
                },
                {
                  "field_name": "CPF or CNPJ",
                  "field_key": "cpfCnpj",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account Type",
                  "field_key": "accountType",
                  "field_type": "drop",
                  "select": "CHECKING",
                  "is_visible": false,
                  "field_value": ["CHECKING", "SAVINGS"]
                },
                {
                  "field_name": "Bank Code",
                  "field_key": "bankCode",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Branch Code",
                  "field_key": "bankCode",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
              ]
            }
          ]
        },
        {
          "field_name": "cn bank account",
          "field_key": "cn_bank_account",
          "field_type": "main_sub_item",
          "is_visible": false,
          "is_sub_list": true,
          "sub_list": [
            {
              "field_name": "cn individual bank account",
              "field_key": "cn_individual_bank_account",
              "field_value": "cn individual bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "CN",
                  "is_visible": false,
                  "field_value": ["CN"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "CNY",
                  "is_visible": false,
                  "field_value": ["CNY"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "INDIVIDUAL",
                  "field_value": ["INDIVIDUAL"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "Name on Account",
                  "field_key": "nameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Phone number",
                  "field_key": "accountHolderPhoneNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account number",
                  "field_key": "accountNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank Code",
                  "field_key": "bankCode",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank Name",
                  "field_key": "bankName",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Branch Name",
                  "field_key": "branchName",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank City",
                  "field_key": "bankCity",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank Province",
                  "field_key": "bankProvince",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
              ]
            },
            {
              "field_name": "cn corporate bank account",
              "field_key": "cn_corporate_bank_account",
              "field_value": "cn corporate bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "CN",
                  "is_visible": false,
                  "field_value": ["CN"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "CNY",
                  "is_visible": false,
                  "field_value": ["CNY"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "CORPORATE",
                  "field_value": ["CORPORATE"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "Name on Account",
                  "field_key": "nameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Phone number",
                  "field_key": "accountHolderPhoneNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account number",
                  "field_key": "accountNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank Code",
                  "field_key": "bankCode",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank Name",
                  "field_key": "bankName",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Branch Name",
                  "field_key": "branchName",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank City",
                  "field_key": "bankCity",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Bank Province",
                  "field_key": "bankProvince",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
              ]
            }
          ]
        },
        {
          "field_name": "mx bank account",
          "field_key": "mx_bank_account",
          "field_type": "main_sub_item",
          "is_visible": false,
          "is_sub_list": true,
          "sub_list": [
            {
              "field_name": "mx individual bank account",
              "field_key": "mx_individual_bank_account",
              "field_value": "mx individual bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "MX",
                  "is_visible": false,
                  "field_value": ["MX"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "MXN",
                  "is_visible": false,
                  "field_value": ["MXN"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "INDIVIDUAL",
                  "field_value": ["INDIVIDUAL"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "First name",
                  "field_key": "firstNameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Last name",
                  "field_key": "lastNameOnAccount",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Clabe",
                  "field_key": "clabe",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                }
              ]
            },
            {
              "field_name": "mx corporate bank account",
              "field_key": "mx_corporate_bank_account",
              "field_value": "mx corporate bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "MX",
                  "is_visible": false,
                  "field_value": ["MX"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "MXN",
                  "is_visible": false,
                  "field_value": ["MXN"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "CORPORATE",
                  "field_value": ["CORPORATE"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "First name",
                  "field_key": "firstNameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Last name",
                  "field_key": "lastNameOnAccount",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Clabe",
                  "field_key": "clabe",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Beneficiary name",
                  "field_key": "beneficiaryName",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                }
              ]
            }
          ]
        },
        {
          "field_name": "au bank account",
          "field_key": "au_bank_account",
          "field_type": "main_sub_item",
          "is_visible": false,
          "is_sub_list": true,
          "sub_list": [
            {
              "field_name": "au individual bank account",
              "field_key": "au_individual_bank_account",
              "field_value": "au individual bank account",
              "is_visible": false,
              "field_type": "sub_sub_item",
              "fields": [
                {
                  "field_name": "Payment Method Type",
                  "field_key": "paymentMethodType",
                  "field_type": "drop",
                  "select": "INTERNATIONAL_TRANSFER",
                  "is_visible": false,
                  "field_value": ["INTERNATIONAL_TRANSFER"]
                },
                {
                  "field_name": "Country",
                  "field_key": "country",
                  "field_type": "drop",
                  "select": "AU",
                  "is_visible": false,
                  "field_value": ["AU"]
                },
                {
                  "field_name": "Currency",
                  "field_key": "currency",
                  "field_type": "drop",
                  "select": "AUD",
                  "is_visible": false,
                  "field_value": ["AUD"]
                },
                {
                  "field_name": "Beneficiary Type",
                  "field_key": "beneficiaryType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "INDIVIDUAL",
                  "field_value": ["INDIVIDUAL"]
                },
                {
                  "field_name": "Payment Type",
                  "field_key": "paymentType",
                  "field_type": "drop",
                  "is_visible": false,
                  "select": "LOCAL_BANK_TRANSFER",
                  "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"]
                },
                {
                  "field_name": "Name on account",
                  "field_key": "nameOnAccount",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "Account number",
                  "field_key": "accountNumber",
                  "field_type": "textfield",
                  "input_type": "number",
                  "is_visible": false,
                  "field_value": ""
                },
                {
                  "field_name": "BSB Number",
                  "field_key": "bsbNumber",
                  "field_type": "textfield",
                  "input_type": "text",
                  "is_visible": false,
                  "field_value": ""
                }
              ]
            }
          ]
        },
      ]
    },
    {
      "method_name": "swift account",
      "is_sub_list": false,
      "is_visible": false,
      "is_open": false,
      "data": [
        {
          "field_name": "Payment method type",
          "field_key": "paymentMethodType",
          "is_visible": false,
          "field_type": "drop",
          "select": "INTERNATIONAL_TRANSFER",
          "field_value": ["INTERNATIONAL_TRANSFER"]
        },
        {
          "field_name": "Country",
          "field_key": "country",
          "select": "US",
          "field_value": [
            "DZ",
            "AR",
            "AU",
            "AT",
            "BE",
            "BO",
            "BR",
            "CA",
            "CL",
            "CO",
            "CR",
            "CY",
            "CZ",
            "DK",
            "DO",
            "EE",
            "FI",
            "FR",
            "DE",
            "GR",
            "HK",
            "IS",
            "IN",
            "ID",
            "IE",
            "IL",
            "IT",
            "JP",
            "LV",
            "LT",
            "LU",
            "MY",
            "MX",
            "NP",
            "NL",
            "NZ",
            "NO",
            "PY",
            "PE",
            "PH",
            "PL",
            "PT",
            "SG",
            "SK",
            "SI",
            "ZA",
            "KR",
            "ES",
            "SE",
            "CH",
            "TZ",
            "TH",
            "TR",
            "GB",
            "US",
            "VN"
          ],
          "is_visible": false,
          "field_type": "drop"
        },
        {
          "field_name": "Currency",
          "field_key": "currency",
          "select": "USD",
          "field_value": ["EUR", "GBP", "USD", "HKT"],
          "is_visible": false,
          "field_type": "drop"
        },
        {
          "field_name": "BeneficiaryType",
          "field_key": "beneficiaryType",
          "select": "INDIVIDUAL",
          "field_value": ["INDIVIDUAL"],
          "is_visible": false,
          "field_type": "drop"
        },
        {
          "field_name": "paymentType",
          "field_key": "paymentType",
          "select": "LOCAL_BANK_TRANSFER",
          "field_value": ["LOCAL_BANK_TRANSFER", "LOCAL_BANK_WIRE"],
          "is_visible": false,
          "field_type": "drop"
        },
        {
          "field_name": "Beneficiary Name",
          "field_key": "beneficiaryName",
          "field_type": "textfield",
          "input_type": "text",
          "is_visible": true,
          "field_value": ""
        },
        {
          "field_name": "IBAN/Account Number",
          "field_key": "accountNumber",
          "field_type": "textfield",
          "input_type": "number",
          "is_visible": true,
          "field_value": ""
        },
        {
          "field_name": "Swift or BIC",
          "field_key": "swiftBic",
          "field_type": "textfield",
          "input_type": "text",
          "is_visible": true,
          "field_value": ""
        },
      ]
    },
  ]
};

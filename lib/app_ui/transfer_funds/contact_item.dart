import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class ContactItem extends StatelessWidget {

  Contact contact;

  ContactItem({this.contact});

  @override
  Widget build(BuildContext context) {



    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 10.0),
      decoration: BoxDecoration(
          color: Theme
              .of(context)
              .scaffoldBackgroundColor, boxShadow: [
        BoxShadow(
            color: CommonColors.bottomSheetShadow.withOpacity(0.20),
            offset: Offset.zero,
            spreadRadius: 1.0,
            blurRadius: 1.0)
      ]),
      margin: EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Container(
            height: 66.0,
            width: 66.0,
            padding: EdgeInsets.all(2.0),
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                border: Border.all(
                    color: CommonColors.gradientColorBottom, width: 2.0)),
            child: CircleAvatar(
              backgroundImage: AssetImage("assets/img/ic_person_contact.png"),
            ),
          ),
          Flexible(child:Container(
            margin: EdgeInsets.only(left: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  contact.displayName,
                  textAlign: TextAlign.start,
                  style: CommonStyle.getAppFont(
                      fontWeight: FontWeight.w600,
                      fontSize: 18,
                      color: CommonColors.getWhiteTextColor(context)),
                ),
                Container(
                  padding: EdgeInsets.only(right: 36, top: 2),
                  child: Text(
                    (contact.phones != null && contact.phones.length > 0)
                        ? contact.phones[0].value
                        : "",
                    textAlign: TextAlign.start,
                    style: CommonStyle.getAppFont(
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                        color: CommonColors.getWhiteTextColor(context)
                            .withOpacity(0.5)),
                  ),
                ),
              ],
            ),
          ))
        ],
      ),
    );
  }
}

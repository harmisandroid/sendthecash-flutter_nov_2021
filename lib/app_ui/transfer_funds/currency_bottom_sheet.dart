import 'package:flutter/material.dart';
import 'package:sendcash_flutter/models/crypto_list_master.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

import '../../utils/common_colors.dart';
import '../../utils/local_images.dart';

class CurrencyBottomSheetView extends StatefulWidget {
  List<CryptoListDetails> currencyList;
  Function(CryptoListDetails) onSelectList;

  CurrencyBottomSheetView({this.currencyList, this.onSelectList});

  @override
  _CurrencyBottomSheetViewState createState() =>
      _CurrencyBottomSheetViewState();
}

class _CurrencyBottomSheetViewState extends State<CurrencyBottomSheetView> {
  List<CryptoListDetails> filteredCryptoList = [];
  int cryptoIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    filteredCryptoList.addAll(widget.currencyList);
    cryptoIndex = widget.currencyList
        .indexWhere((element) => element.name.toLowerCase() == "bitcoin");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        height: MediaQuery.of(context).size.height * 0.7,
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            TextFormField(
              style: CommonStyle.getAppFont(
                color: CommonColors.getWhiteTextColor(context),
              ),
              onChanged: (value) {
                filteredCryptoList.clear();
                if (value.isEmpty) {
                  filteredCryptoList.addAll(widget.currencyList);
                  setState(() {});
                } else {
                  widget.currencyList.forEach((element) {
                    if (element.name.toLowerCase().contains(value) ||
                        element.currencyCode.toLowerCase().contains(value)) {
                      filteredCryptoList.add(element);
                    }
                  });
                  setState(() {});
                }
              },
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
                  hintText: "search currency.....",
                  suffixIcon: Icon(
                    Icons.search,
                    color: CommonColors.getHintTextStyle(context),
                  ),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30.0),
                      borderSide: BorderSide(
                          color: CommonColors.getWhiteTextColor(context),
                          width: 1)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30.0),
                      borderSide: BorderSide(
                          color: CommonColors.getWhiteTextColor(context),
                          width: 1)),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30.0),
                      borderSide: BorderSide(
                          color: CommonColors.getWhiteTextColor(context),
                          width: 1))),
            ),
            SizedBox(
              height: 10.0,
            ),
            Expanded(
                child: ListView.separated(
                    shrinkWrap: true,
                    padding:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          widget.onSelectList(filteredCryptoList[index]);
                        },
                        child: Container(
                          child: Row(
                            children: [
                              index < cryptoIndex
                                  ? Image.network(
                                      filteredCryptoList[index].image,
                                      height: 23.0,
                                      width: 23.0,
                                      color: CommonColors.getWhiteTextColor(context),
                                      errorBuilder:
                                          (context, object, stacktrace) {
                                        return Image.asset(
                                          LocalImages.dollar,
                                          height: 23.0,
                                          width: 23.0,
                                        );
                                      },
                                    )
                                  : Image.network(
                                      filteredCryptoList[index].image,
                                      height: 23.0,
                                      width: 23.0,
                                      errorBuilder:
                                          (context, object, stacktrace) {
                                        return Image.asset(
                                          LocalImages.dollar,
                                          height: 23.0,
                                          width: 23.0,
                                        );
                                      },
                                    ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Text(
                                filteredCryptoList[index].name,
                                style: TextStyle(
                                    color:
                                        CommonColors.getWhiteTextColor(context),
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.symmetric(vertical: 20.0),
                        width: MediaQuery.of(context).size.width,
                        height: 1.0,
                        color: CommonColors.getListDeviderColor(context),
                      );
                    },
                    itemCount: filteredCryptoList.length))
          ],
        ),
      ),
    );
  }
}

import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/bank_accounts/bank_accounts_view.dart';

import 'package:sendcash_flutter/app_ui/transfer_funds/bank_ui_map.dart';
import 'package:sendcash_flutter/app_ui/transfer_funds/transfer_funds_view_model.dart';

import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/gradient_button/gradientbutton.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/currency_formatter.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

import '../../database/db_key.dart';
import '../../generated/assets.dart';
import '../../utils/globals.dart';

class TransferFundsView extends StatefulWidget {
  const TransferFundsView({Key key}) : super(key: key);

  @override
  _TransferFundsViewState createState() => _TransferFundsViewState();
}

class _TransferFundsViewState extends State<TransferFundsView> {
  int selectedCardIndex = 0;
  TransferFundsViewModel mViewModel;
  final amountController = TextEditingController();
  final crytoAddressController = TextEditingController();
  List<dynamic> methodTypeList = uiMap["method_types"];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<TransferFundsViewModel>(context);
    return Scaffold(
      key: mViewModel.scaffoldKey,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Stack(
        children: [
          GAppbar(
            title: S.of(context).transfer,
            height: 130.0,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            margin: EdgeInsets.only(top: 100.0, left: 20.0, right: 20.0),
            padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
            decoration: BoxDecoration(
                color: Theme.of(context).scaffoldBackgroundColor,
                borderRadius: BorderRadius.circular(20.0),
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(0.0, 30.0))
                ]),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            mViewModel
                                .onWalletChange(!mViewModel.isUseWalletBalance);
                          },
                          child: Icon(
                            mViewModel.isUseWalletBalance
                                ? Icons.check_box
                                : Icons.check_box_outline_blank,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                        SizedBox(
                          width: 5.0,
                        ),
                        new RichText(
                          text: new TextSpan(
                            // Note: Styles for TextSpans must be explicitly defined.
                            // Child text spans will inherit styles from parent

                            style: CommonStyle.getAppFont(
                                color: CommonColors.getWhiteTextColor(context),
                                fontWeight: FontWeight.w500,
                                fontSize: 18.0),
                            children: <TextSpan>[
                              new TextSpan(text: 'Use Wallet Balance '),
                              new TextSpan(
                                  text: Globals.currencySymbol +
                                      Globals.getConvertedCurrency(
                                          mViewModel.availableBalance),
                                  style: CommonStyle.getAppFont(
                                      color: Theme.of(context).primaryColor,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18.0)),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 30),
                    alignment: AlignmentDirectional.centerStart,
                    child: Text(
                      S.of(context).enterAmount,
                      textAlign: TextAlign.start,
                      style: CommonStyle.getAppFont(
                          color: CommonColors.getWhiteTextColor(context),
                          fontWeight: FontWeight.w500,
                          fontSize: 20.0),
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Container(
                    height: 50.0,
                    margin: EdgeInsets.only(left: 20.0, right: 20.0),
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: [
                        Flexible(
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            controller: amountController,
                            style: CommonStyle.getAppFont(
                                color: CommonColors.getWhiteTextColor(context),
                                fontSize: 14.0,
                                fontWeight: FontWeight.w500),
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                              CurrencyInputFormatter()
                            ],
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 0.0),
                              hintText: S.of(context).enterAmount,
                              hintStyle: CommonStyle.getAppFont(
                                  color: CommonColors.getHintTextStyle(context),
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w500),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                  borderSide: BorderSide(
                                      width: 1,
                                      color: Theme.of(context)
                                          .inputDecorationTheme
                                          .border
                                          .borderSide
                                          .color)),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                  borderSide: BorderSide(
                                      width: 1,
                                      color: Theme.of(context)
                                          .inputDecorationTheme
                                          .border
                                          .borderSide
                                          .color)),
                              /* prefixIcon: Container(
                                height: 0.0,
                                width: 50.0,
                                alignment: Alignment.center,
                                padding: EdgeInsetsDirectional.only(start: 10.0),
                                child: Text(
                                  Globals.currencySymbol,
                                  style: CommonStyle.getAppFont(
                                      color: CommonColors.primaryColor,
                                      fontWeight: FontWeight.w900,
                                      fontSize: 20.0),
                                ),
                              )*/
                            ),
                          ),
                          flex: 3,
                        ),
                        SizedBox(
                          width: 5.0,
                        ),
                        mViewModel.selectedSourceCurrency == null
                            ? Center(
                                child: Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: CircularProgressIndicator(),
                                ),
                              )
                            : Flexible(
                                child: InkWell(
                                  onTap: () {
                                    mViewModel.showCurrencyBottomSheet(false);
                                  },
                                  child: Container(
                                    height: 50.0,
                                    width: 80.0,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color:
                                                CommonColors.getWhiteTextColor(
                                                        context)
                                                    .withOpacity(.5)),
                                        borderRadius:
                                            BorderRadius.circular(40.0)),
                                    padding: EdgeInsets.all(8.0),
                                    child: Row(
                                      children: [
                                        SizedBox(
                                          width: 5.0,
                                        ),
                                        mViewModel.selectedSourceCurrency
                                                    .symbol !=
                                                null
                                            ? Image.network(
                                                mViewModel
                                                    .selectedSourceCurrency
                                                    .image,
                                                height: 23.0,
                                                width: 23.0,
                                                color: CommonColors
                                                    .getWhiteTextColor(context),
                                                errorBuilder: (context, object,
                                                    stacktrace) {
                                                  return Image.asset(
                                                    LocalImages.dollar,
                                                    height: 23.0,
                                                    width: 23.0,
                                                  );
                                                },
                                              )
                                            : Image.network(
                                                mViewModel
                                                    .selectedSourceCurrency
                                                    .image,
                                                height: 23.0,
                                                width: 23.0,
                                                errorBuilder: (context, object,
                                                    stacktrace) {
                                                  return Image.asset(
                                                    LocalImages.dollar,
                                                    height: 23.0,
                                                    width: 23.0,
                                                  );
                                                },
                                              ),
                                        SizedBox(
                                          width: 5.0,
                                        ),
                                        Text(
                                          mViewModel.selectedSourceCurrency
                                              .currencyCode,
                                          style: TextStyle(
                                              color: CommonColors
                                                  .getWhiteTextColor(context),
                                              fontSize: 15),
                                        ),
                                        Spacer(),
                                        Icon(
                                          Icons.keyboard_arrow_down,
                                          color: CommonColors.getWhiteTextColor(
                                              context),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                fit: FlexFit.tight,
                                flex: 2,
                              ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 30),
                    alignment: AlignmentDirectional.centerStart,
                    child: Text(
                      S.of(context).transferTo,
                      textAlign: TextAlign.start,
                      style: CommonStyle.getAppFont(
                          color: CommonColors.getWhiteTextColor(context),
                          fontWeight: FontWeight.w500,
                          fontSize: 20.0),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  mViewModel.selectedDestCurrency == null
                      ? Text("Loading...")
                      : Container(
                          height: 50.0,
                          margin: EdgeInsets.only(left: 20.0, right: 20.0),
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            children: [
                              mViewModel.selectedDestCurrency.symbol != null
                                  ? Container(
                                      height: 0,
                                      width: 0,
                                    )
                                  : Flexible(
                                      child: TextFormField(
                                        controller: crytoAddressController,
                                        style: CommonStyle.getAppFont(
                                            color: CommonColors
                                                .getLightWhiteTextColor(
                                                    context),
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.w500),
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.symmetric(
                                              horizontal: 20.0),
                                          hintText:
                                              S.of(context).depositAddress,
                                          hintStyle: CommonStyle.getAppFont(
                                              color:
                                                  CommonColors.getHintTextStyle(
                                                      context),
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.normal),
                                          enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(30.0),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: CommonColors
                                                      .getWhiteTextColor(
                                                          context))),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(30.0),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Theme.of(context)
                                                      .inputDecorationTheme
                                                      .border
                                                      .borderSide
                                                      .color)),
                                        ),
                                      ),
                                      flex: 3,
                                    ),
                              SizedBox(
                                width: 10.0,
                              ),
                              mViewModel.selectedSourceCurrency == null
                                  ? Center(
                                      child: Padding(
                                        padding: EdgeInsets.all(10.0),
                                        child: CircularProgressIndicator(),
                                      ),
                                    )
                                  : Flexible(
                                      child: InkWell(
                                        onTap: () {
                                          mViewModel
                                              .showCurrencyBottomSheet(true);
                                        },
                                        child: Container(
                                          height: 50.0,
                                          width: 80.0,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: CommonColors
                                                          .getWhiteTextColor(
                                                              context)
                                                      .withOpacity(.5)),
                                              borderRadius:
                                                  BorderRadius.circular(40.0)),
                                          padding: EdgeInsets.all(8.0),
                                          child: Row(
                                            children: [
                                              SizedBox(
                                                width: 5.0,
                                              ),
                                              mViewModel.selectedDestCurrency
                                                          .symbol !=
                                                      null
                                                  ? Image.network(
                                                      mViewModel
                                                          .selectedDestCurrency
                                                          .image,
                                                      height: 23.0,
                                                      width: 23.0,
                                                      color: CommonColors
                                                          .getWhiteTextColor(
                                                              context),
                                                      errorBuilder: (context,
                                                          object, stacktrace) {
                                                        return Image.asset(
                                                          LocalImages.dollar,
                                                          height: 23.0,
                                                          width: 23.0,
                                                        );
                                                      },
                                                    )
                                                  : Image.network(
                                                      mViewModel
                                                          .selectedDestCurrency
                                                          .image,
                                                      height: 23.0,
                                                      width: 23.0,
                                                      errorBuilder: (context,
                                                          object, stacktrace) {
                                                        return Image.asset(
                                                          LocalImages.dollar,
                                                          height: 23.0,
                                                          width: 23.0,
                                                        );
                                                      },
                                                    ),
                                              SizedBox(
                                                width: 10.0,
                                              ),
                                              Text(
                                                mViewModel.selectedDestCurrency
                                                    .currencyCode,
                                                style: TextStyle(
                                                    color: CommonColors
                                                        .getWhiteTextColor(
                                                            context),
                                                    fontSize: 15),
                                              ),
                                              Spacer(),
                                              Icon(
                                                Icons.keyboard_arrow_down,
                                                color: CommonColors
                                                    .getWhiteTextColor(context),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      fit: FlexFit.tight,
                                      flex: 2,
                                    ),
                            ],
                          ),
                        ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Align(
                    alignment: AlignmentDirectional.center,
                    child: Text(
                      S.of(context).or.toUpperCase(),
                      textAlign: TextAlign.start,
                      style: CommonStyle.getAppFont(
                          color: CommonColors.getWhiteTextColor(context),
                          fontWeight: FontWeight.w500,
                          fontSize: 20.0),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  InkWell(
                    onTap: () async {
                      dynamic accountDetails = await Navigator.push(context,
                          CupertinoPageRoute(builder: (context) {
                        return BankAccountsView();
                      }));
                      if (accountDetails != null) {
                        mViewModel.selectedBankAccount = accountDetails;
                        setState(() {});
                      }
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      margin:
                          EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                          color: Theme.of(context).backgroundColor,
                          borderRadius: BorderRadius.circular(10.0),
                          border: Border.all(
                              width: 0.5,
                              color: CommonColors.getWhiteTextColor(context))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            S.of(context).savedAccounts,
                            style: CommonStyle.getAppFont(
                                color: CommonColors.getWhiteTextColor(context),
                                fontSize: 16.0,
                                fontWeight: FontWeight.normal),
                          ),
                          Icon(
                            Icons.keyboard_arrow_right,
                            color: CommonColors.getWhiteTextColor(context),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  mViewModel.selectedBankAccount == null
                      ? Container(
                          height: 0,
                          width: 0,
                        )
                      : getLinkedAccountItem(mViewModel.selectedBankAccount),
                  SizedBox(
                    height: 10.0,
                  ),
                  buildUIPaymentMethods(),
                  SizedBox(
                    height: MediaQuery.of(context).viewPadding.bottom + 100.0,
                  )
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
              decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 3.0,
                        spreadRadius: 3.0,
                        color: CommonColors.bottomSheetShadow.withOpacity(0.20),
                        offset: Offset.zero)
                  ]),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 15.0,
                  ),
                  GradientButton(
                    btnText: S.of(context).addFunds,
                    onClick: () {
                      if (isValidData()) {
                        mViewModel.createTransferApi(
                            sourceAmount:
                                amountController.text.replaceAll(",", ""),
                            depositAddress: crytoAddressController.text.isEmpty
                                ? null
                                : crytoAddressController.text);
                      }
                    },
                  ),
                  SizedBox(height: MediaQuery.of(context).padding.bottom),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildUIPaymentMethods() {
    // create main list

    List<Widget> mainItemList = [];
    methodTypeList.forEach((element) {
      // main for loop start
      final mainItemContainer =
          getWhiteTextContainer(element, element['method_name']);
      final mainSubItem = getMainSubItem(element);

      final mainColumn = Column(
        children: [
          mainItemContainer,
          !element['is_visible'] ? Container(height: 0, width: 0) : mainSubItem
        ],
      );
      mainItemList.add(mainColumn);

      // here main for loop end
    });

    return Column(
      children: mainItemList,
    );
  }

  // here get ui for different banks and method types

  Widget getMainSubItem(dynamic aElement) {
    var mainSubItem = null;
    if (aElement['is_sub_list']) {
      if (aElement['data'] != null) {
        List<Map<dynamic, dynamic>> mainSubItemList = aElement['data'];
        // sub item for loop
        List<Widget> subItemList = [];
        mainSubItemList.forEach((element) {
          final mainSubContainer =
              getLightWhiteTextContainer(element, element['field_name']);
          final subSubContainer = getMainSubSubItem(element);
          final mainSubColumn = Column(
            children: [
              mainSubContainer,
              !element['is_visible']
                  ? Container(
                      height: 0,
                      width: 0,
                    )
                  : subSubContainer
            ],
          );
          subItemList.add(mainSubColumn);
        });
        mainSubItem = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: subItemList,
        );
      } else {
        return Container(
          height: 50,
          width: MediaQuery.of(context).size.width,
          color: Theme.of(context).primaryColor,
        );
      }
    } else {
      if (aElement['data'] != null) {
        mainSubItem = buildInputItems(aElement['data']);
      } else {
        mainSubItem = Container(
          height: 0,
          width: 0,
        );
      }
    }
    return mainSubItem;
  }

  Widget getMainSubSubItem(dynamic aElement) {
    var subSubItem = null;
    if (aElement['is_sub_list']) {
      List<Map<dynamic, dynamic>> subSubItemList = aElement['sub_list'];
      // sub item for loop
      List<Widget> subItemList = [];
      subSubItemList.forEach((element) {
        final subSubContainer =
            getWhiteTextContainer(element, element['field_name']);
        var fieldList = null;
        if (element['fields'] != null) {
          fieldList = !element['is_visible']
              ? Container(
                  height: 0,
                  width: 0,
                )
              : buildInputItems(element['fields']);
        } else {
          fieldList = Container(
            child: Text(
              "Empty field",
              style: CommonStyle.getAppFont(
                  color: CommonColors.getLightWhiteTextColor(context)),
            ),
          );
        }
        final subSubColumn = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [subSubContainer, fieldList],
        );
        subItemList.add(subSubColumn);
      });
      subSubItem = Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: subItemList,
        ),
      );
    } else {
      subSubItem = buildInputItems(aElement['data']);
    }
    return subSubItem;
  }

  /*------------ all method to build inputs ------------------*/

  Widget getWhiteTextContainer(dynamic element, String label) {
    return InkWell(
      onTap: () {
        element["is_visible"] = !element["is_visible"];
        if (element['method_name'] != null) {
          closeMainList(element['method_name']);
          mViewModel.clearAllData();
          mViewModel.selectedPaymentMethod = element['method_name'];
        }
        if (element['field_type'] != null &&
            element['field_type'] == "sub_sub_item") {
          closeSubSubList(element['field_name']);
          mViewModel.bankFieldList = element['fields'];
          mViewModel.selectedBankType = element['field_name'];
        }
        setState(() {});
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(
                width: 0.5, color: CommonColors.getWhiteTextColor(context))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              label.toString().toUpperCase(),
              style: CommonStyle.getAppFont(
                  color: CommonColors.getWhiteTextColor(context),
                  fontSize: 16.0,
                  fontWeight: FontWeight.normal),
            ),
            Icon(
              element['is_visible'] ? Icons.remove : Icons.add,
              color: CommonColors.getWhiteTextColor(context),
            )
          ],
        ),
      ),
    );
  }

  Widget getLightWhiteTextContainer(dynamic element, String label) {
    return InkWell(
      onTap: () {
        // element["is_visible"] = !element["is_visible"];
        if (element['field_type'] != null &&
            element['field_type'] == "main_sub_item") {
          closeSubList(element['field_name']);
          mViewModel.selectedCountryBank = element['field_name'];
        }
        setState(() {});
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(
                width: 0.5,
                color: CommonColors.getLightWhiteTextColor(context))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              element['field_name'].toString().toUpperCase(),
              style: CommonStyle.getAppFont(
                  color: CommonColors.getLightWhiteTextColor(context),
                  fontSize: 16.0,
                  fontWeight: FontWeight.normal),
            ),
            Icon(
              element['is_visible'] ? Icons.remove : Icons.add,
              color: CommonColors.getWhiteTextColor(context),
            )
          ],
        ),
      ),
    );
  }

  Widget buildInputItems(List<Map<dynamic, dynamic>> fieldList) {
    List<Widget> widgetList = [];
    fieldList.forEach((element) {
      if (element['field_type'] == "textfield") {
        Widget textFieldWidget = getTextRequiredField(
            label: element['field_name'],
            onChange: (value) {
              element['field_value'] = value;
            },
            textInputType: element['input_type'] == "email"
                ? TextInputType.emailAddress
                : (element['input_type'] == "text"
                    ? TextInputType.text
                    : TextInputType.number),
            hintText: "");
        widgetList.add(textFieldWidget);
      } else if (element['field_type'] == "drop") {
        Widget textFieldWidget = getDropdownButton(
            label: element['field_name'],
            dropList: element['field_value'],
            selectedValue: element['select'],
            onChange: (value) {
              element['select'] = value;
              setState(() {});
            });
        widgetList.add(textFieldWidget);
      }
    });

    return Form(
        child: Container(
      decoration: BoxDecoration(
          color: CommonColors.getBankFieldContainer(context),
          borderRadius: BorderRadius.circular(10.0),
          border: Border.all(color: Colors.grey, width: 0.5)),
      margin: EdgeInsets.only(top: 5.0, left: 10.0, right: 10.0, bottom: 10.0),
      padding:
          EdgeInsets.only(top: 20.0, bottom: 20.0, left: 10.0, right: 10.0),
      child: Column(
        children: widgetList,
      ),
    ));
  }

  Widget getTextRequiredField(
      {String label,
      TextInputType textInputType,
      String hintText,
      Function(String) onChange}) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10.0,
          ),
          Text(
            label ?? "NA",
            style: CommonStyle.getAppFont(
                color: CommonColors.getLightWhiteTextColor(context),
                fontSize: 16.0,
                fontWeight: FontWeight.w500),
          ),
          SizedBox(
            height: 10.0,
          ),
          TextFormField(
            keyboardType: textInputType ?? TextInputType.text,
            onChanged: onChange,
            onSaved: onChange,
            onFieldSubmitted: onChange,
            style: CommonStyle.getAppFont(
                color: CommonColors.getWhiteTextColor(context),
                fontSize: 14.0,
                fontWeight: FontWeight.normal),
            decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
                hintText: hintText ?? "",
                hintStyle: CommonStyle.getAppFont(
                    color: CommonColors.getHintTextStyle(context)),
                errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: Colors.red, width: 0.5)),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                        color: CommonColors.getWhiteTextColor(context),
                        width: 0.5)),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                        color: Theme.of(context).primaryColor, width: 0.5))),
          ),
          SizedBox(
            height: 15.0,
          ),
        ],
      ),
    );
  }

  Widget getDropdownButton(
      {String label,
      List<String> dropList,
      Function(String) onChange,
      String selectedValue}) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10.0,
          ),
          Text(
            label ?? "NA",
            style: CommonStyle.getAppFont(
                color: CommonColors.getLightWhiteTextColor(context),
                fontSize: 16.0,
                fontWeight: FontWeight.w500),
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            decoration: BoxDecoration(
                color: CommonColors.getBankFieldContainer(context),
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(
                    color: CommonColors.getWhiteTextColor(context),
                    width: 0.5)),
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                iconSize: 22,
                value: selectedValue,
                isExpanded: true,
                dropdownColor: CommonColors.getDropdownColor(context),
                hint: Text(selectedValue,
                    style: CommonStyle.getAppFont(
                        color: Colors.black,
                        fontSize: 15.0,
                        fontWeight: FontWeight.normal)),
                style: CommonStyle.getAppFont(
                    fontSize: 15.0, fontWeight: FontWeight.normal),
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: CommonColors.getWhiteTextColor(context),
                ),
                items: dropList.map((value) {
                  return DropdownMenuItem(
                    value: value,
                    child: Text(
                      value,
                      style: CommonStyle.getAppFont(
                          fontSize: 15,
                          color: CommonColors.getWhiteTextColor(context),
                          fontWeight: FontWeight.normal),
                    ),
                  );
                }).toList(),
                onChanged: onChange,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget getLinkedAccountItem(dynamic linkedAccountDetails) {
    return Container(
      decoration: BoxDecoration(
          color: CommonColors.getDropdownColor(context),
          borderRadius: BorderRadius.circular(10.0)),
      margin: EdgeInsets.symmetric(horizontal: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            Assets.imgIcBank,
            height: 30.0,
            width: 30.0,
          ),
          SizedBox(
            width: 20.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                linkedAccountDetails[DbKey.BANK_TYPE_NAME],
                style: CommonStyle.getAppFont(
                    fontSize: 20.0,
                    fontWeight: FontWeight.normal,
                    color: CommonColors.getWhiteTextColor(context)),
              ),
              SizedBox(
                height: 5.0,
              ),
              Text(
                "XXXXXXXXXXXX" +
                    linkedAccountDetails[DbKey.ACCOUNT_NUMBER]
                        .toString()
                        .substring(linkedAccountDetails[DbKey.ACCOUNT_NUMBER]
                                .toString()
                                .length -
                            4),
                style: CommonStyle.getAppFont(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    color: CommonColors.getLightWhiteTextColor(context)),
              ),


            ],
          )
        ],
      ),
    );
  }

  /*----------------check validation is user entered all data-----------------*/

  bool isValidData() {
    if (amountController.text.isEmpty || amountController.text == "0.00") {
      CommonUtils.showRedToastMessage(S.of(context).enterAmount);
      return false;
    } else if (crytoAddressController.text.isEmpty) {
      if (mViewModel.selectedPaymentMethod != null) {
        if (mViewModel.selectedPaymentMethod == "plaid processor token") {
          List<dynamic> fieldList = methodTypeList[0]['data'];

          for (dynamic element in fieldList) {
            if (element['field_type'] == "textfield" &&
                element['field_value'].toString().isEmpty) {
              CommonUtils.showRedToastMessage(
                  "Please enter " + element['field_name']);
              printf("Empty....................");
              // break;
              return false;
            } else {
              printf("Not Empty....................");
            }
          }
        } else if (mViewModel.selectedPaymentMethod == "bank account") {
          if (mViewModel.selectedCountryBank != null) {
            if (mViewModel.selectedBankType != null) {
              for (dynamic element in mViewModel.bankFieldList) {
                if (element['field_type'] == "textfield" &&
                    element['field_value'].toString().isEmpty) {
                  CommonUtils.showRedToastMessage(
                      'Please enter ' + element['field_name']);
                  return false;
                } else if (element['field_type'] == "textfield" &&
                    element['input_type'] == "email") {
                  if (!CommonUtils.isvalidEmail(element['field_value'])) {
                    CommonUtils.showRedToastMessage(
                        S.of(context).emailValidation);
                  }
                }
              }
            } else {
              CommonUtils.showRedToastMessage(
                  "Please select bank type individual or corporate");
            }
          } else {
            CommonUtils.showRedToastMessage("Please select bank first");
            return false;
          }
        } else if (mViewModel.selectedPaymentMethod == "swift bank account") {
          List<dynamic> fieldList = methodTypeList[2]['data'];
          fieldList.forEach((element) {
            if (element['field_type'] == "textfield" &&
                element['field_value'].toString().isEmpty) {
              CommonUtils.showRedToastMessage(
                  "Please enter " + element['field_name']);
              return false;
            }
          });
        }
      } else {
        CommonUtils.showRedToastMessage(S.of(context).pleaseEnterCrptoAddress);
        return false;
      }
    }
    return true;
  }

  void closeMainList(String name) {
    methodTypeList.forEach((element) {
      if (element['method_name'] == name) {
        if (!element['is_visible']) {
          element['is_visible'] = false;
        } else {
          element['is_visible'] = true;
        }
      } else {
        element['is_visible'] = false;
      }
    });
    setState(() {});
  }

  void closeSubList(String name) {
    List<Map<dynamic, dynamic>> list = methodTypeList[1]['data'];
    list.forEach((element) {
      if (element['field_name'] == name) {
        if (element['is_visible']) {
          element['is_visible'] = false;
        } else {
          element['is_visible'] = true;
        }
      } else {
        element['is_visible'] = false;
      }
    });
  }

  void closeSubSubList(String name) {
    List<Map<dynamic, dynamic>> list = methodTypeList[1]['data'];
    list.forEach((element) {
      List<Map<dynamic, dynamic>> innerList = element['sub_list'];
      innerList.forEach((innerElement) {
        if (innerElement['field_name'] == name) {
          if (!innerElement['is_visible']) {
            innerElement['is_visible'] = false;
          } else {
            innerElement['is_visible'] = true;
          }
        } else {
          innerElement['is_visible'] = false;
        }
      });
    });
  }
}

import 'dart:convert';
import 'dart:developer';
import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/database/app_preferences.dart';
import 'package:sendcash_flutter/models/crypto_list_master.dart';
import 'package:sendcash_flutter/models/user_master.dart';
import 'package:sendcash_flutter/services/api_services.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/local_images.dart';

class CurrencyRatesViewModel with ChangeNotifier {
  BuildContext mContext;
  List<ChartModel> chartList = [];
  final sourceController = TextEditingController();
  final destController = TextEditingController();
  LoginDetails loginDetails;
  AppPreferences appPreferences = AppPreferences();
  Map<String, dynamic> exchangeRates = Map();
  // DropdownModel selectedSourceCurrency = null;
  // DropdownModel selectedDestCurrency = null;
  // List<DropdownModel> sourceArray = [];
  // List<DropdownModel> destArray = [];
  ApiServices apiServices = ApiServices();
  String bitcoinUSD = "";
  String currencyPair = "BTCUSD";
  Map<String,dynamic>   cryptoExchangeRates = Map();
  List<CryptoListDetails>  cryptoList = [];
  CryptoListDetails selectedSourceCurrency = null;
  List<CryptoListDetails>  currencyList = [];
  CryptoListDetails selectedDestCurrency = null;

  final List<SalesData> chartData = [
    SalesData(2010, 35, 23),
    SalesData(2011, 38, 49),
    SalesData(2012, 34, 12),
    SalesData(2013, 52, 33),
    SalesData(2014, 40, 30)
  ];

  void attachContext(BuildContext context) async {
    mContext = context;
    chartList.clear();
    cryptoList.clear();
    currencyList.clear();
    chartList.add(ChartModel("J", 41));
    chartList.add(ChartModel("k", 31));
    chartList.add(ChartModel("L", 123));
    chartList.add(ChartModel("M", 130));
    chartList.add(ChartModel("N", 100));
    chartList.add(ChartModel("o", 10));
    chartList.add(ChartModel("P", 8));
    chartList.add(ChartModel("Q", 23));
    chartList.add(ChartModel("Q", 21));
    chartList.add(ChartModel("Q", 80));
    chartList.add(ChartModel("Q", 90));
    chartList.add(ChartModel("Q", 15));
    chartList.add(ChartModel("Q", 70));
    chartList.add(ChartModel("Q", 75));
    destController.text = "";
    sourceController.text = "";
    loginDetails = await appPreferences.getLoginDetails();
    notifyListeners();
    await getPreviousDay();

  }

  Future<void> getExchangeRates() async {
    CommonUtils.showProgressDialog(mContext);
    final response = await apiServices.getExhangeRates(onFailed: () {
      CommonUtils.showRedToastMessage("Failed to get data");
      CommonUtils.hideProgressDialog(mContext);
    });
      CommonUtils.hideProgressDialog(mContext);
    if (response != null) {
      Map<String, dynamic> mapResponse = jsonDecode(response) as Map;
      exchangeRates = mapResponse;
      bitcoinUSD = (1 / mapResponse['BTCUSD']).toStringAsFixed(2);
      await getCryptoList();

    } else {
      CommonUtils.showRedToastMessage("Failed to get data");
    }
  }

  void onChangeSource(String amount) {
    if (amount.isEmpty) {
      destController.text = "";
      return;
    }else if(selectedSourceCurrency.currencyCode == selectedDestCurrency.currencyCode){
       destController.text =  sourceController.text;
    }else{
      try {
       // printf("EEEEE=> ${currencyPair} "+exchangeRates[currencyPair].toString());
        currencyPair =   selectedDestCurrency.currencyCode+selectedSourceCurrency.currencyCode;
        destController.text =
            (double.parse(amount) * exchangeRates[currencyPair]).toString();
      } on Exception catch (e) {
        // TODO
      }

    }

    notifyListeners();
  }

  void onSelectSource(CryptoListDetails currencyName) {
    selectedSourceCurrency = currencyName;
    if (selectedSourceCurrency.currencyCode == selectedDestCurrency.currencyCode) {
      sourceController.text = destController.text;
    } else {
      currencyPair = selectedSourceCurrency.currencyCode + selectedDestCurrency.currencyCode;
      try {
        sourceController.text =
            (double.parse(destController.text) * exchangeRates[currencyPair])
                .toString();
      } on Exception catch (e) {
        // TODO
      }
    }
    notifyListeners();
  }

  void onChangeDestination(String amount) {
    if (amount.isEmpty) {
      sourceController.text = "";
    }else if(selectedDestCurrency.currencyCode == selectedSourceCurrency.currencyCode){
      sourceController.text = destController.text;
    }else{
      try {
        sourceController.text =
            (double.parse(amount) * exchangeRates[currencyPair]).toString();
      } on Exception catch (e) {
        // TODO
      }
    }

    notifyListeners();
  }

  void onSelectDest(CryptoListDetails currencyName) {
    selectedDestCurrency = currencyName;
    if (selectedDestCurrency.currencyCode == selectedSourceCurrency.currencyCode) {
      destController.text = sourceController.text;
    } else {
      currencyPair = selectedSourceCurrency.currencyCode + selectedDestCurrency.currencyCode;
      try {
        destController.text =
            (double.parse(sourceController.text) / exchangeRates[currencyPair])
                .toString();
      } on Exception catch (e) {
        // TODO
      }
    }
    notifyListeners();
  }

  void getPreviousDay() async{
    CommonUtils.showProgressDialog(mContext);
    String response =  await apiServices.getPreDayExhangeRates(onFailed: (){
      CommonUtils.hideProgressDialog(mContext);
      CommonUtils.showRedToastMessage("failed to get exchange rates");
    });
    CommonUtils.hideProgressDialog(mContext);
    if(response != null){
      cryptoExchangeRates = jsonDecode(response);
      getExchangeRates();
    }else{
      CommonUtils.showRedToastMessage("failed to get exchange rates");
    }
  }


  void getCryptoList() async{
    CommonUtils.showProgressDialog(mContext);
    CryptoListMaster cryptoListMaster = await  apiServices.getCryptoList(
        onFailed: (){
        CommonUtils.hideProgressDialog(mContext);
    });
    CommonUtils.hideProgressDialog(mContext);
    if(cryptoListMaster != null){
      cryptoList.addAll(cryptoListMaster.result);
      selectedDestCurrency = cryptoList[0];
      getCurrencyList();
      notifyListeners();
    }else{
      CommonUtils.showRedToastMessage("Failed to get message");
    }
  }

  void getCurrencyList() async{
    CommonUtils.showProgressDialog(mContext);
    CryptoListMaster cryptoListMaster = await  apiServices.getAllCurrencies(
        onFailed: (){
          CommonUtils.hideProgressDialog(mContext);
        });
    CommonUtils.hideProgressDialog(mContext);
    if(cryptoListMaster != null){
      currencyList.addAll(cryptoListMaster.result);
      selectedSourceCurrency = currencyList[0];
      notifyListeners();
    }else{
      CommonUtils.showRedToastMessage("Failed to get message");
    }
  }
}

class ChartModel {
  String character;
  double height;

  ChartModel(this.character, this.height);
}

class SalesData {
  final double year;
  final int sales;
  final double time;
  SalesData(this.year, this.sales, this.time);
}



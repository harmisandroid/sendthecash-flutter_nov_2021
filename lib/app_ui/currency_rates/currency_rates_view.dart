import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/c_chart.dart';
import 'package:sendcash_flutter/app_ui/notifications/notification_view.dart';
import 'package:sendcash_flutter/common_widgets/appbar/gradient_back_appbar.dart';
import 'package:sendcash_flutter/common_widgets/appbar/menu_appbar.dart';
import 'package:sendcash_flutter/common_widgets/edittext/c_textfield.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/crypto_list_master.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/common_utils.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';
import 'currency_rates_view_model.dart';

class CurrencyRatesView extends StatefulWidget {
  final GlobalKey<ScaffoldState> navScaffoldKey;

  CurrencyRatesView({this.navScaffoldKey});

  @override
  _CurrencyRatesViewState createState() => _CurrencyRatesViewState();
}

class _CurrencyRatesViewState extends State<CurrencyRatesView> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  CurrencyRatesViewModel mViewModel;
  double height, width;

  @override
  void initState() {
    // TODO: implement initState
    Future.delayed(Duration.zero, () {
      mViewModel.attachContext(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<CurrencyRatesViewModel>(context);

    if (mViewModel.loginDetails == null) {
      return Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        body: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(top: 120, left: 15, right: 15),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0)),
                  boxShadow: [
                    BoxShadow(
                        spreadRadius: 25.0,
                        blurRadius: 25.0,
                        color: Theme.of(context).shadowColor,
                        offset: Offset(0.0, 25.0))
                  ],
                  color: Theme.of(context).scaffoldBackgroundColor),
              child: Container(
                height: MediaQuery.of(context).size.height * 0.7,
                padding: EdgeInsets.only(left: 20.0),
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
          ],
        ),
      );
    }

    width = MediaQuery.of(context).size.width;

    /* final chartView = Container(
      height: 260.0,
      padding: EdgeInsets.all(20.0),
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          borderRadius: BorderRadius.circular(20.0),
          boxShadow: [
            BoxShadow(
                color: CommonColors.bottomSheetShadow.withOpacity(0.1),
                blurRadius: 1.0,
                spreadRadius: 1.0,
                offset: Offset.zero)
          ]),
      margin: EdgeInsets.only(top: 220.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            S.of(context).received,
            maxLines: 1,
            style: CommonStyle.getAppFont(
                fontSize: 18.0,
                fontWeight: FontWeight.normal,
                color: CommonColors.getWhiteTextColor(context)),
          ),
          Text(
            Globals.currencySymbol + "1052.00",
            maxLines: 1,
            style: CommonStyle.getAppFont(
                fontSize: 22.0,
                fontWeight: FontWeight.w500,
                color: CommonColors.getLightWhiteTextColor(context)),
          ),
          Container(
            height: 156.0,
            child: ListView.separated(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                physics: ClampingScrollPhysics(),
                itemCount: mViewModel.chartList.length,
                separatorBuilder: (context, index) {
                  return Container(
                    height: 156.0,
                    width: 20.0,
                  );
                },
                itemBuilder: (context, index) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        height: mViewModel.chartList[index].height,
                        width: 8.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(3.5),
                            gradient: LinearGradient(
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter,
                                colors: [
                                  CommonColors.chartGradientTwo,
                                  CommonColors.chartGradientOne
                                ])),
                      ),
                      Container(
                        height: 21.0,
                        width: 8.0,
                        child: Center(
                          child: Text(
                            mViewModel.chartList[index].character,
                            maxLines: 1,
                            style: CommonStyle.getAppFont(
                                fontSize: 16.0,
                                fontWeight: FontWeight.normal,
                                color: CommonColors.getLightWhiteTextColor(
                                    context: context)),
                          ),
                        ),
                      )
                    ],
                  );
                }),
          )
        ],
      ),
    );
*/

    final customChart = Container(
      margin: EdgeInsets.only(top: 10.0),
      height: 250.0,
      child: CChart(),
    );
    final topView = Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(10.0),
        margin: EdgeInsets.symmetric(horizontal: 10.0),
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.all(Radius.circular(15)),
            boxShadow: [
              BoxShadow(
                  offset: Offset.zero,
                  spreadRadius: 1.0,
                  blurRadius: 1.0,
                  color: CommonColors.getLightShadowColor(context))
            ]),
        child: Column(
          children: [
            Text(
              S.of(context).bitcoinEquals,
              style: CommonStyle.getAppFont(
                  color: CommonColors.getWhiteTextColor(context),
                  fontSize: 19,
                  fontWeight: FontWeight.w500),
            ),
            Text(
              (mViewModel.bitcoinUSD ?? "") + S.of(context).usDollarDigits,
              style: CommonStyle.getAppFont(
                  color: CommonColors.getWhiteTextColor(context),
                  fontSize: 18,
                  fontWeight: FontWeight.w500),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Container(
                    margin: EdgeInsets.only(top: 10.0),
                    child: CRoundTextField(
                      hintText: "0",
                      textInputFormatter: [
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      controller: mViewModel.sourceController,
                      textInputType: TextInputType.number,
                      onChange: mViewModel.onChangeSource,
                    ),
                  ),
                  flex: 3,
                  fit: FlexFit.tight,
                ),
                SizedBox(
                  width: 10.0,
                ),
                Flexible(
                  child: Container(
                    height: 45,
                    width: 100.0,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: CommonColors.getWhiteTextColor(context)
                                .withOpacity(.5)),
                        borderRadius: BorderRadius.circular(40.0)),
                    padding: EdgeInsets.all(8.0),
                    margin: EdgeInsets.only(top: 12.0),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                        value: mViewModel.selectedSourceCurrency,
                        icon: Icon(
                          Icons.keyboard_arrow_down,
                          color: CommonColors.getWhiteTextColor(context)
                              .withOpacity(.5),
                          size: 20,
                        ),
                        dropdownColor: CommonColors.getDropdownColor(context),
                        items:
                            mViewModel.currencyList.map((CryptoListDetails items) {
                          return DropdownMenuItem(
                              value: items,
                              child: Row(
                                children: [
                                  Image.network(
                                    items.image,
                                    height: 23.0,
                                    width: 23.0,
                                    color: CommonColors.getWhiteTextColor(context),
                                    errorBuilder: (context,object,stackTrace){
                                      return Image.asset(
                                        LocalImages.dollar,
                                        height: 23.0,
                                        width: 23.0,);
                                    },
                                  ),
                                  SizedBox(
                                    width: 5.0,
                                  ),
                                  Text(
                                    items.currencyCode,
                                    style: TextStyle(
                                        color: CommonColors.getWhiteTextColor(
                                            context),
                                        fontSize: 15),
                                  ),
                                ],
                              ));
                        }).toList(),
                        onChanged: mViewModel.onSelectSource,
                      ),
                    ),
                  ),
                  fit: FlexFit.tight,
                  flex: 3,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Container(
                    margin: EdgeInsets.only(top: 10.0),
                    child: CRoundTextField(
                      hintText: "0",
                      textInputType: TextInputType.number,
                      textInputFormatter: [
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      controller: mViewModel.destController,
                      onChange: mViewModel.onChangeDestination,
                    ),
                  ),
                  flex: 5,
                  fit: FlexFit.tight,
                ),
                SizedBox(
                  width: 10.0,
                ),
                Flexible(
                  child: Container(
                    height: 45,
                    width: 100,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: CommonColors.getWhiteTextColor(context)
                                .withOpacity(.5)),
                        borderRadius: BorderRadius.circular(40.0)),
                    padding: EdgeInsets.all(8.0),
                    margin: EdgeInsets.only(top: 12.0),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                          value: mViewModel.selectedDestCurrency,
                          dropdownColor: CommonColors.getDropdownColor(context),
                          icon: const Icon(Icons.keyboard_arrow_down),
                          items:
                              mViewModel.cryptoList.map((CryptoListDetails items) {
                            return DropdownMenuItem(
                              value: items,
                              child: Row(
                                children: [
                                  Container(
                                      padding: EdgeInsets.all(4.0),
                                      child: Image.network(
                                        items.image,
                                        height: 23,
                                        width: 23,
                                        errorBuilder: (context,object,stackTrace){
                                          return Image.asset(
                                            LocalImages.bitcoin,
                                            height: 23.0,
                                            width: 23.0,);
                                        },
                                      )),
                                  SizedBox(
                                    width: 5.0,
                                  ),
                                  Text(
                                    items.currencyCode,
                                    style: TextStyle(
                                        color: CommonColors.getWhiteTextColor(
                                            context),
                                        fontSize: 15),
                                  ),
                                ],
                              ),
                            );
                          }).toList(),
                          onChanged: mViewModel.onSelectDest),
                    ),
                  ),
                  fit: FlexFit.tight,
                  flex: 5,
                ),
              ],
            )
          ],
        ));

    // final chart = Container(
    // height: 280.0,
    // decoration: BoxDecoration(
    //     color: Theme.of(context).backgroundColor,
    //     borderRadius: BorderRadius.circular(20.0),
    //     boxShadow: [
    //       BoxShadow(
    //           color: CommonColors.bottomSheetShadow.withOpacity(0.1),
    //           blurRadius: 1.0,
    //           spreadRadius: 1.0,
    //           offset: Offset.zero)
    //     ]),
    // margin: EdgeInsets.only(top: 20.0),
    //     child: SingleChildScrollView(
    //   scrollDirection: Axis.horizontal,
    //   child: SfCartesianChart(
    //       series: <ChartSeries>[
    //         ColumnSeries<SalesData, double>(
    //           width: 0.2,
    //             gradient: LinearGradient(
    //                 begin: Alignment.bottomCenter,
    //                 end: Alignment.topCenter,
    //                 colors: [
    //                   CommonColors.chartGradientTwo,
    //                   CommonColors.chartGradientOne
    //                 ]),
    //             dataSource: mViewModel.chartData,
    //             xValueMapper: (SalesData sales, _) => sales.year,
    //             yValueMapper: (SalesData sales, _) => sales.sales,
    //             borderRadius: BorderRadius.all(Radius.circular(8.0))
    //         )
    //       ]
    //   ),
    // )
    // );

    final body = Container(
      margin: EdgeInsets.only(
          bottom: MediaQuery.of(context).viewPadding.bottom + 150.0,
          left: 20.0,
          right: 20.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                S.of(context).cryptoCurrency,
                style: CommonStyle.getAppFont(
                    color: CommonColors.getWhiteTextColor(context),
                    fontSize: 18,
                    fontWeight: FontWeight.w500),
              ),
              Text(
                S.of(context).viewAll,
                style: CommonStyle.getAppFont(
                    color: CommonColors.gradientColorBottom,
                    fontSize: 18,
                    fontWeight: FontWeight.w500),
              ),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          ListView.builder(
              physics: ClampingScrollPhysics(),
              padding: EdgeInsets.all(0.0),
              itemCount: mViewModel.cryptoList.length,
              shrinkWrap: true,
              itemBuilder: (BuildContext buildContext, int index) {
                return getListItem(cryptoListDetails: mViewModel.cryptoList[index]);
              }),
        ],
      ),
    );

    final mainBody = Container(
      color: Colors.transparent,
      margin: EdgeInsets.only(top: 110.0, left: 20.0, right: 20.0),
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        child: Column(
          children: [
            topView,
            customChart,
            body,
          ],
        ),
      ),
    );

    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Stack(
        children: [
          MenuAppbar(
            height: 153.0,
            title: S.of(context).currencyRates,
            onMenuClick: () {
              widget.navScaffoldKey.currentState.openDrawer();
            },
            onNotificationClick: () {
              Navigator.push(context, CupertinoPageRoute(builder: (context) {
                return NotificationsView();
              }));
            },
          ),
          Container(
            margin: EdgeInsets.only(top: 150.0),
            padding: EdgeInsets.only(left: 20.0, right: 20.0),
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 25.0,
                      blurRadius: 25.0,
                      color: Theme.of(context).shadowColor,
                      offset: Offset(0.0, 0.0))
                ]),
            clipBehavior: Clip.antiAlias,
          ),
          mainBody
        ],
      ),
    );
  }

  Widget getListItem({CryptoListDetails cryptoListDetails}) {
    double  changeInsum = 0.0;
    double priceInDollars = 0.0;

     try {
       changeInsum = (double.parse(mViewModel.cryptoExchangeRates[cryptoListDetails.currencyCode+Globals.currencyName].toString()) - double.parse(mViewModel.exchangeRates[cryptoListDetails.currencyCode+Globals.currencyName].toString()))  * 100 / double.parse(mViewModel.cryptoExchangeRates[cryptoListDetails.currencyCode+Globals.currencyName].toString());
       priceInDollars = 1 / double.parse(mViewModel.exchangeRates[cryptoListDetails.currencyCode+Globals.currencyName].toString());
     } on Exception catch (e) {
       changeInsum = 0.00;
     }


    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(child:Image.network(
            cryptoListDetails.image,
            height: 31,
            width: 31,
            errorBuilder: (context, object, stackTrace) {
              return Image.asset(
                LocalImages.bitcoin,
                height: 31,
                width: 31,
              );
            },
          ),flex: 1,),
          SizedBox(
            width: 10.0,
          ),
          Flexible(child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                cryptoListDetails.name,
                textAlign: TextAlign.start,
                maxLines: 1,
                style: CommonStyle.getAppFont(
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                    color: CommonColors.getWhiteTextColor(context)),
              ),
              Container(
                padding: EdgeInsets.only(top: 2),
                child: Text(
                  cryptoListDetails.currencyCode,
                  textAlign: TextAlign.start,
                  style: CommonStyle.getAppFont(
                      fontWeight: FontWeight.normal,
                      fontSize: 12,
                      color: CommonColors.getWhiteTextColor(context)),
                ),
              ),
            ],
          ),flex: 2,fit: FlexFit.tight,),
          SizedBox(
            width: 10.0,
          ),
          Flexible(child:Image.asset(
            LocalImages.linegraph,
            color: changeInsum.toString().contains("-") ? Colors.red : CommonColors.green,
            height: 27,
            width: 70,
          ),flex: 1,),
          Flexible(child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                padding: EdgeInsets.only(left: 40, top: 2),
                child: Text(
                mViewModel.cryptoExchangeRates[cryptoListDetails.currencyCode+Globals.currencyName].toString() == "null" ? "0.0": Globals.currencySymbol + CommonUtils.getCurrencyFormat(priceInDollars),
                  style: CommonStyle.getAppFont(
                      fontWeight: FontWeight.normal,
                      fontSize: 16,
                      color: CommonColors.getWhiteTextColor(context)),
                ),
              ),
              SizedBox(
                height: 2.0,
              ),
              Container(
                padding: EdgeInsets.only(left: 30.0),
                child: Text(
                 (changeInsum.toString().contains("-") ? "":"+")+changeInsum.toStringAsFixed(2)+"%",
                  style: CommonStyle.getAppFont(
                      fontWeight: FontWeight.w500,
                      fontSize: 14,
                      color: changeInsum.toString().contains("-") ? Colors.red :CommonColors.green),
                ),
              ),
            ],
          ),flex: 4,fit: FlexFit.tight,),
        ],
      ),
    );
  }
}

class CryptoListMaster {
  int _success;
  String _message;
  List<CryptoListDetails> _result;

  CryptoListMaster({int success, String message, List<CryptoListDetails> result}) {
    if (success != null) {
      this._success = success;
    }
    if (message != null) {
      this._message = message;
    }
    if (result != null) {
      this._result = result;
    }
  }

  int get success => _success;
  set success(int success) => _success = success;
  String get message => _message;
  set message(String message) => _message = message;
  List<CryptoListDetails> get result => _result;
  set result(List<CryptoListDetails> result) => _result = result;

  CryptoListMaster.fromJson(Map<String, dynamic> json) {
    _success = json['success'];
    _message = json['message'];
    if (json['result'] != null) {
      _result = <CryptoListDetails>[];
      json['result'].forEach((v) {
        _result.add(new CryptoListDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['message'] = this._message;
    if (this._result != null) {
      data['result'] = this._result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CryptoListDetails {
  String _currencyCode;
  String _name;
  String _image;
  String _symbol;

  CryptoListDetails({String currencyCode, String name, String image}) {
    if (currencyCode != null) {
      this._currencyCode = currencyCode;
    }
    if (name != null) {
      this._name = name;
    }
    if (image != null) {
      this._image = image;
    }
  }


  String get symbol => _symbol;

  set symbol(String value) {
    _symbol = value;
  }

  String get currencyCode => _currencyCode;
  set currencyCode(String currencyCode) => _currencyCode = currencyCode;
  String get name => _name;
  set name(String name) => _name = name;
  String get image => _image;
  set image(String image) => _image = image;

  CryptoListDetails.fromJson(Map<String, dynamic> json) {
    _currencyCode = json['currency_code'];
    _name = json['name'];
    _image = json['image'];
    _symbol = json['symbol'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['currency_code'] = this._currencyCode;
    data['name'] = this._name;
    data['image'] = this._image;
    data['symbol'] = this._symbol;
    return data;
  }
}
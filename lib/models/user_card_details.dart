class CardDetails {
  String _cardNo;
  String _userId;
  String _expiryDate;
  String _cardColor;
  String _cardholderName;
  String _cvv;

  CardDetails(
      {String cardNo,
        String userId,
        String expiryDate,
        String cardColor,
        String cardholderName}) {
    if (cardNo != null) {
      this._cardNo = cardNo;
    }
    if (userId != null) {
      this._userId = userId;
    }
    if (expiryDate != null) {
      this._expiryDate = expiryDate;
    }
    if (cardColor != null) {
      this._cardColor = cardColor;
    }
    if (cardholderName != null) {
      this._cardholderName = cardholderName;
    }
  }


  String get cvv => _cvv;

  set cvv(String value) {
    _cvv = value;
  }

  String get cardNo => _cardNo;
  set cardNo(String cardNo) => _cardNo = cardNo;
  String get userId => _userId;
  set userId(String userId) => _userId = userId;
  String get expiryDate => _expiryDate;
  set expiryDate(String expiryDate) => _expiryDate = expiryDate;

  String get cardholderName => _cardholderName;
  set cardholderName(String cardholderName) =>
      _cardholderName = cardholderName;


  String get cardColor => _cardColor;

  set cardColor(String value) {
    _cardColor = value;
  }

  CardDetails.fromJson(Map<String, dynamic> json) {
    _cardNo = json['card_no'];
    _userId = json['user_id'];
    _expiryDate = json['expiry_date'];
    _cardColor = json['card_color'].toString();
    _cardholderName = json['cardholder_name'];
    _cvv = json['cvv'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['card_no'] = this._cardNo;
    data['user_id'] = this._userId;
    data['expiry_date'] = this._expiryDate;
    data['card_color'] = this._cardColor;
    data['cardholder_name'] = this._cardholderName;
    data['cvv'] = this._cvv;
    return data;
  }
}
class LoginDetails {
  bool _isPhoneVerified;
  Address _address;
  String _birthdate;
  String _loginType;
  bool _isEmailVerified;
  String _profilePic;
  String _mobileNo;
  String _lastName;
  String _emailPassword;
  String _ssn;
  String _password;
  String _firstName;
  String _email;
  String _phoneCode;
  String _username;
  String _frontIdImage;
  String _backIdImage;
  String _countryCode;
  String _changePasswordDate;
  String _socialId;
  String _walletId;
  UserAppSettings _userAppSettings;
  String _sendwyreAccountId;


  LoginDetails(
      {bool isPhoneVerified,
        Address address,
        String birthdate,
        String loginType,
        bool isEmailVerified,
        String profilePic,
        String mobileNo,
        String lastName,
        String emailPassword,
        String ssn,
        String password,
        String firstName,
        String email,
        String phoneCode,
        String username}) {
    this._isPhoneVerified = isPhoneVerified;
    this._address = address;
    this._birthdate = birthdate;
    this._loginType = loginType;
    this._isEmailVerified = isEmailVerified;
    this._profilePic = profilePic;
    this._mobileNo = mobileNo;
    this._lastName = lastName;
    this._emailPassword = emailPassword;
    this._ssn = ssn;
    this._password = password;
    this._firstName = firstName;
    this._email = email;
    this._phoneCode = phoneCode;
    this._username = username;
  }


  String get sendwyreAccountId => _sendwyreAccountId;

  set sendwyreAccountId(String value) {
    _sendwyreAccountId = value;
  }

  String get socialId => _socialId;

  set socialId(String value) {
    _socialId = value;
  }

  UserAppSettings get userAppSettings => _userAppSettings;

  set userAppSettings(UserAppSettings value) {
    _userAppSettings = value;
  }

  String get changePasswordDate => _changePasswordDate;

  set changePasswordDate(String value) {
    _changePasswordDate = value;
  }

  bool get isPhoneVerified => _isPhoneVerified;
  set isPhoneVerified(bool isPhoneVerified) =>
      _isPhoneVerified = isPhoneVerified;
  Address get address => _address;
  set address(Address address) => _address = address;
  String get birthdate => _birthdate;
  set birthdate(String birthdate) => _birthdate = birthdate;
  String get loginType => _loginType;
  set loginType(String loginType) => _loginType = loginType;
  bool get isEmailVerified => _isEmailVerified;
  set isEmailVerified(bool isEmailVerified) =>
      _isEmailVerified = isEmailVerified;
  String get profilePic => _profilePic;
  set profilePic(String profilePic) => _profilePic = profilePic;
  String get mobileNo => _mobileNo;
  set mobileNo(String mobileNo) => _mobileNo = mobileNo;
  String get lastName => _lastName;
  set lastName(String lastName) => _lastName = lastName;
  String get emailPassword => _emailPassword;
  set emailPassword(String emailPassword) => _emailPassword = emailPassword;
  String get ssn => _ssn;
  set ssn(String ssn) => _ssn = ssn;
  String get password => _password;
  set password(String password) => _password = password;
  String get firstName => _firstName;
  set firstName(String firstName) => _firstName = firstName;
  String get email => _email;
  set email(String email) => _email = email;
  String get phoneCode => _phoneCode;
  set phoneCode(String phoneCode) => _phoneCode = phoneCode;
  String get username => _username;
  set username(String username) => _username = username;


  String get frontIdImage => _frontIdImage;

  set frontIdImage(String value) {
    _frontIdImage = value;
  }


  String get countryCode => _countryCode;

  set countryCode(String value) {
    _countryCode = value;
  }

  LoginDetails.fromJson(Map<String, dynamic> json) {
    _isPhoneVerified = json['is_phone_verified'];
    _address =
    json['address'] != null ? new Address.fromJson(json['address']) : null;
    _birthdate = json['birthdate'];
    _loginType = json['login_type'];
    _isEmailVerified = json['is_email_verified'];
    _profilePic = json['profile_pic'] ?? "";
    _mobileNo = json['mobile_no'];
    _lastName = json['last_name'];
    _emailPassword = json['email_password'];
    _ssn = json['ssn'];
    _password = json['password'];
    _firstName = json['first_name'];
    _email = json['email'];
    _phoneCode = json['phone_code'];
    _username = json['username'];
    _frontIdImage = json['front_id_image'] ?? "";
    _backIdImage = json['back_id_image'] ?? "";
    _countryCode = json['country_code'];
    _changePasswordDate = json['change_password_date'];
    _socialId = json['social_id'] ?? "";
    _walletId = json['wallet_id'] ?? "";
    _sendwyreAccountId = json['sedwyre_account_id'] ?? "";
    _userAppSettings =  json['user_app_settings'] != null ? new UserAppSettings.fromJson(json['user_app_settings']) : null;

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['is_phone_verified'] = this._isPhoneVerified;
    if (this._address != null) {
      data['address'] = this._address.toJson();
    }
    data['birthdate'] = this._birthdate;
    data['login_type'] = this._loginType;
    data['is_email_verified'] = this._isEmailVerified;
    data['profile_pic'] = this._profilePic ?? "";
    data['mobile_no'] = this._mobileNo;
    data['last_name'] = this._lastName;
    data['email_password'] = this._emailPassword;
    data['ssn'] = this._ssn;
    data['password'] = this._password;
    data['first_name'] = this._firstName;
    data['email'] = this._email;
    data['phone_code'] = this._phoneCode;
    data['username'] = this._username;
    data['front_id_image'] = this._frontIdImage ?? "";
    data['back_id_image'] = this._backIdImage ?? "";
    data['country_code'] = this._countryCode;
    data['change_password_date'] = this._changePasswordDate;
    data['social_id'] = this._socialId;
    data['wallet_id'] = this._socialId;
    data['sedwyre_account_id'] = this._sendwyreAccountId;
    if (this._userAppSettings != null) {
      data['user_app_settings'] = this._userAppSettings.toJson();
    }
    return data;
  }

  String get backIdImage => _backIdImage;

  set backIdImage(String value) {
    _backIdImage = value;
  }

  String get walletId => _walletId;

  set walletId(String value) {
    _walletId = value;
  }
}

class Address {
  String _zipcode;
  String _country;
  String _city;
  String _addressLine1;
  String _addressLine2;
  String _state;

  Address(
      {String zipcode,
        String country,
        String city,
        String addressLine1,
        String addressLine2,
        String state}) {
    this._zipcode = zipcode;
    this._country = country;
    this._city = city;
    this._addressLine1 = addressLine1;
    this._addressLine2 = addressLine2;
    this._state = state;
  }

  String get zipcode => _zipcode;
  set zipcode(String zipcode) => _zipcode = zipcode;
  String get country => _country;
  set country(String country) => _country = country;
  String get city => _city;
  set city(String city) => _city = city;
  String get addressLine1 => _addressLine1;
  set addressLine1(String addressLine1) => _addressLine1 = addressLine1;
  String get addressLine2 => _addressLine2;
  set addressLine2(String addressLine2) => _addressLine2 = addressLine2;
  String get state => _state;
  set state(String state) => _state = state;

  Address.fromJson(Map<String, dynamic> json) {
    _zipcode = json['zipcode'];
    _country = json['country'];
    _city = json['city'];
    _addressLine1 = json['address_line_1'];
    _addressLine2 = json['address_line_2'];
    _state = json['state'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['zipcode'] = this._zipcode;
    data['country'] = this._country;
    data['city'] = this._city;
    data['address_line_1'] = this._addressLine1;
    data['address_line_2'] = this._addressLine2;
    data['state'] = this._state;
    return data;
  }
}


class UserAppSettings{
  bool _isDarkTheme;
  bool _isNotificationOn;
  String _defaultCurrency;
  String _currencySymbol;
  bool _controlHomeGraph;


  bool get controlHomeGraph => _controlHomeGraph;

  set controlHomeGraph(bool value) {
    _controlHomeGraph = value;
  }

  String get currencySymbol => _currencySymbol;

  set currencySymbol(String value) {
    _currencySymbol = value;
  }

  String get defaultCurrency => _defaultCurrency;

  set defaultCurrency(String value) {
    _defaultCurrency = value;
  }

  bool get isDarkTheme => _isDarkTheme;

  set isDarkTheme(bool value) {
    _isDarkTheme = value;
  }

  bool get isNotificationOn => _isNotificationOn;

  set isNotificationOn(bool value) {
    _isNotificationOn = value;
  }
  UserAppSettings.fromJson(Map<String, dynamic> json) {
    _isDarkTheme = json['is_dark_theme'] ??  false;
    _isNotificationOn = json['is_notification_on'] ?? true;
    _defaultCurrency = json['default_currency'] ?? "USD";
    _currencySymbol = json['currency_symbol'] ?? "\$";
    _controlHomeGraph = json['home_graph'] ?? false;

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['is_dark_theme'] = this._isDarkTheme;
    data['is_notification_on'] = this._isNotificationOn;
    data['default_currency'] = this._defaultCurrency;
    data['currency_symbol'] = this._currencySymbol;
    data['home_graph'] = this._controlHomeGraph;
    return data;
  }


}
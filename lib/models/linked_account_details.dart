class LinkedAccountDetails {
  String _socialId;
  String _userId;
  String _loginType;
  String _profilePic;
  String _email;
  String _username;
  String _userProfileLink;
  String _twitterUserName;

  LinkedAccountDetails(
      {String socialId,
      String userId,
      String loginType,
      String profilePic,
      String email,
      String username}) {
    if (socialId != null) {
      this._socialId = socialId;
    }
    if (userId != null) {
      this._userId = userId;
    }
    if (loginType != null) {
      this._loginType = loginType;
    }
    if (profilePic != null) {
      this._profilePic = profilePic;
    }
    if (email != null) {
      this._email = email;
    }
    if (username != null) {
      this._username = username;
    }
  }

  String get socialId => _socialId;

  set socialId(String socialId) => _socialId = socialId;

  String get userId => _userId;

  set userId(String userId) => _userId = userId;

  String get loginType => _loginType;

  set loginType(String loginType) => _loginType = loginType;

  String get profilePic => _profilePic;

  set profilePic(String profilePic) => _profilePic = profilePic;

  String get email => _email;

  set email(String email) => _email = email;

  String get username => _username;

  set username(String username) => _username = username;

  String get twitterUserName => _twitterUserName;

  set twitterUserName(String value) {
    _twitterUserName = value;
  }

  String get userProfileLink => _userProfileLink;

  set userProfileLink(String value) {
    _userProfileLink = value;
  }

  LinkedAccountDetails.fromJson(Map<String, dynamic> json) {
    _socialId = json['social_id'].toString();
    _userId = json['user_id'];
    _loginType = json['login_type'];
    _profilePic = json['profile_pic'];
    _email = json['email'];
    _username = json['username'];
    _userProfileLink = json['user_profile_link'];
    _twitterUserName = json['user_social_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['social_id'] = this._socialId;
    data['user_id'] = this._userId;
    data['login_type'] = this._loginType;
    data['profile_pic'] = this._profilePic;
    data['email'] = this._email;
    data['username'] = this._username;
    data['user_profile_link'] = this._userProfileLink;
    data['user_social_name'] = this._twitterUserName;
    return data;
  }
}

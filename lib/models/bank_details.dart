class BankDetails {
  String _name;
  String _bankCode;

  BankDetails({String name, String bankCode}) {
    if (name != null) {
      this._name = name;
    }
    if (bankCode != null) {
      this._bankCode = bankCode;
    }
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get bankCode => _bankCode;
  set bankCode(String bankCode) => _bankCode = bankCode;

  BankDetails.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _bankCode = json['bankCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this._name;
    data['bankCode'] = this._bankCode;
    return data;
  }
}
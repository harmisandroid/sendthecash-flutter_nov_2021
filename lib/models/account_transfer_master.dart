class AccountTransferMaster {
  List<TransferDetails> _data;
  int _position;
  int _recordsTotal;
  int _recordsFiltered;

  AccountTransferMaster(
      {List<TransferDetails> data,
        int position,
        int recordsTotal,
        int recordsFiltered}) {
    if (data != null) {
      this._data = data;
    }
    if (position != null) {
      this._position = position;
    }
    if (recordsTotal != null) {
      this._recordsTotal = recordsTotal;
    }
    if (recordsFiltered != null) {
      this._recordsFiltered = recordsFiltered;
    }
  }

  List<TransferDetails> get data => _data;
  set data(List<TransferDetails> data) => _data = data;
  int get position => _position;
  set position(int position) => _position = position;
  int get recordsTotal => _recordsTotal;
  set recordsTotal(int recordsTotal) => _recordsTotal = recordsTotal;
  int get recordsFiltered => _recordsFiltered;
  set recordsFiltered(int recordsFiltered) =>
      _recordsFiltered = recordsFiltered;

  AccountTransferMaster.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      _data = <TransferDetails>[];
      json['data'].forEach((v) {
        _data.add(new TransferDetails.fromJson(v));
      });
    }
    _position = json['position'];
    _recordsTotal = json['recordsTotal'];
    _recordsFiltered = json['recordsFiltered'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._data != null) {
      data['data'] = this._data.map((v) => v.toJson()).toList();
    }
    data['position'] = this._position;
    data['recordsTotal'] = this._recordsTotal;
    data['recordsFiltered'] = this._recordsFiltered;
    return data;
  }
}

class TransferDetails {
  String _closedAt;
  String _createdAt;
  String _id;
 // Null _customId;
  String _source;
  String _dest;
  String _sourceCurrency;
  String _destCurrency;
  String _sourceAmount;
  String _destAmount;
  Fees _fees;
  String _type;
  String _sourceName;
  String _destName;
  String _status;
  // Null _message;
  String _exchangeRate;
  // Null _blockchainTxId;
  // Null _destNickname;
  String _eTHCredit;
  String _aVAXCredit;
  String _bTCCredit;

  TransferDetails(
      {String closedAt,
        String createdAt,
        String id,
       /* Null customId,*/
        String source,
        String dest,
        String sourceCurrency,
        String destCurrency,
        String sourceAmount,
        String destAmount,
        Fees fees,
        String type,
        String sourceName,
        String destName,
        String status,
       /* Null message,*/
        String exchangeRate,
        /*Null blockchainTxId,
        Null destNickname,*/
        String eTHCredit,
        String aVAXCredit,
        String bTCCredit}) {
    if (closedAt != null) {
      this._closedAt = closedAt;
    }
    if (createdAt != null) {
      this._createdAt = createdAt;
    }
    if (id != null) {
      this._id = id;
    }
    /*if (customId != null) {
      this._customId = customId;
    }*/
    if (source != null) {
      this._source = source;
    }
    if (dest != null) {
      this._dest = dest;
    }
    if (sourceCurrency != null) {
      this._sourceCurrency = sourceCurrency;
    }
    if (destCurrency != null) {
      this._destCurrency = destCurrency;
    }
    if (sourceAmount != null) {
      this._sourceAmount = sourceAmount;
    }
    if (destAmount != null) {
      this._destAmount = destAmount;
    }
    if (fees != null) {
      this._fees = fees;
    }
    if (type != null) {
      this._type = type;
    }
    if (sourceName != null) {
      this._sourceName = sourceName;
    }
    if (destName != null) {
      this._destName = destName;
    }
    if (status != null) {
      this._status = status;
    }
    /*if (message != null) {
      this._message = message;
    }*/
    if (exchangeRate != null) {
      this._exchangeRate = exchangeRate;
    }
   /* if (blockchainTxId != null) {
      this._blockchainTxId = blockchainTxId;
    }
    if (destNickname != null) {
      this._destNickname = destNickname;
    }*/
    if (eTHCredit != null) {
      this._eTHCredit = eTHCredit;
    }
    if (aVAXCredit != null) {
      this._aVAXCredit = aVAXCredit;
    }
    if (bTCCredit != null) {
      this._bTCCredit = bTCCredit;
    }
  }


  String get id => _id;
  set id(String id) => _id = id;
  /*Null get customId => _customId;
  set customId(Null customId) => _customId = customId;*/
  String get source => _source;
  set source(String source) => _source = source;
  String get dest => _dest;
  set dest(String dest) => _dest = dest;
  String get sourceCurrency => _sourceCurrency;
  set sourceCurrency(String sourceCurrency) =>
      _sourceCurrency = sourceCurrency;
  String get destCurrency => _destCurrency;
  set destCurrency(String destCurrency) => _destCurrency = destCurrency;

  Fees get fees => _fees;
  set fees(Fees fees) => _fees = fees;
  String get type => _type;
  set type(String type) => _type = type;
  String get sourceName => _sourceName;
  set sourceName(String sourceName) => _sourceName = sourceName;
  String get destName => _destName;
  set destName(String destName) => _destName = destName;
  String get status => _status;
  set status(String status) => _status = status;

  String get closedAt => _closedAt;

  set closedAt(String value) {
    _closedAt = value;
  }

  /* Null get blockchainTxId => _blockchainTxId;
  set blockchainTxId(Null blockchainTxId) => _blockchainTxId = blockchainTxId;
  Null get destNickname => _destNickname;
  set destNickname(Null destNickname) => _destNickname = destNickname;*/




  TransferDetails.fromJson(Map<String, dynamic> json) {
    _closedAt = json['closedAt'].toString();
    _createdAt = json['createdAt'].toString();
    _id = json['id'];
    // _customId = json['customId'];
    _source = json['source'].toString();
    _dest = json['dest'].toString();
    _sourceCurrency = json['sourceCurrency'];
    _destCurrency = json['destCurrency'];
    _sourceAmount = json['sourceAmount'].toString();
    _destAmount = json['destAmount'].toString();
    _fees = json['fees'] != null ? new Fees.fromJson(json['fees']) : null;
    _type = json['type'];
    _sourceName = json['sourceName'];
    _destName = json['destName'];
    _status = json['status'];
    // _message = json['message'];
    _exchangeRate = json['exchangeRate'].toString();
   /* _blockchainTxId = json['blockchainTxId'];
    _destNickname = json['destNickname'];*/
    _eTHCredit = json['ETHCredit'].toString();
    _aVAXCredit = json['AVAXCredit'].toString();
    _bTCCredit = json['BTCCredit'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['closedAt'] = this._closedAt;
    data['createdAt'] = this._createdAt;
    data['id'] = this._id;
    // data['customId'] = this._customId;
    data['source'] = this._source;
    data['dest'] = this._dest;
    data['sourceCurrency'] = this._sourceCurrency;
    data['destCurrency'] = this._destCurrency;
    data['sourceAmount'] = this._sourceAmount;
    data['destAmount'] = this._destAmount;
    if (this._fees != null) {
      data['fees'] = this._fees.toJson();
    }
    data['type'] = this._type;
    data['sourceName'] = this._sourceName;
    data['destName'] = this._destName;
    data['status'] = this._status;
    // data['message'] = this._message;
    data['exchangeRate'] = this._exchangeRate;
    // data['blockchainTxId'] = this._blockchainTxId;
    // data['destNickname'] = this._destNickname;
    data['ETHCredit'] = this._eTHCredit;
    data['AVAXCredit'] = this._aVAXCredit;
    data['BTCCredit'] = this._bTCCredit;
    return data;
  }

  String get createdAt => _createdAt;

  set createdAt(String value) {
    _createdAt = value;
  }

  String get sourceAmount => _sourceAmount;

  set sourceAmount(String value) {
    _sourceAmount = value;
  }

  String get destAmount => _destAmount;

  set destAmount(String value) {
    _destAmount = value;
  }

  String get exchangeRate => _exchangeRate;

  set exchangeRate(String value) {
    _exchangeRate = value;
  }

  String get eTHCredit => _eTHCredit;

  set eTHCredit(String value) {
    _eTHCredit = value;
  }

  String get aVAXCredit => _aVAXCredit;

  set aVAXCredit(String value) {
    _aVAXCredit = value;
  }

  String get bTCCredit => _bTCCredit;

  set bTCCredit(String value) {
    _bTCCredit = value;
  }
}

class Fees {
  String _uSD;

  Fees({String uSD}) {
    if (uSD != null) {
      this._uSD = uSD;
    }
  }


  String get uSD => _uSD;

  set uSD(String value) {
    _uSD = value;
  }

  Fees.fromJson(Map<String, dynamic> json) {
    _uSD = json['USD'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['USD'] = this._uSD;
    return data;
  }
}
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerChipsList extends StatelessWidget {
  const ShimmerChipsList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40.0,
      child: ListView.builder(
          itemCount: 10,
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext buildContext, int index) {
            return Center(
              child: Container(
                margin: EdgeInsets.only(left: 5.0),
                child: Shimmer.fromColors(
                  baseColor: Colors.black38,
                  highlightColor: Colors.white,
                  child: Container(
                    height: 30.0,
                    width: 80.0,
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ),
            );
          }),
    );
  }
}

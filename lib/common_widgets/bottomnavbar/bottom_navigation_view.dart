import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/currency_rates/currency_rates_view.dart';
import 'package:sendcash_flutter/app_ui/home/home_view.dart';
import 'package:sendcash_flutter/app_ui/notifications/notification_view.dart';
import 'package:sendcash_flutter/app_ui/setting/setting_view.dart';
import 'package:sendcash_flutter/app_ui/side_menu/side_menu_view.dart';
import 'package:sendcash_flutter/app_ui/wallet/wallet_view.dart';
import 'package:sendcash_flutter/common_widgets/appbar/main_appbar.dart';
import 'package:sendcash_flutter/common_widgets/appbar/menu_appbar.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/event_bus.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'bottom_nav_view_model.dart';
import 'foating_bottom_nav_bar.dart';
import 'foating_nav_item.dart';

class BottomNavigationView extends StatefulWidget {
  @override
  _BottomNavigationViewState createState() => _BottomNavigationViewState();
}

class _BottomNavigationViewState extends State<BottomNavigationView> {
  BottomNavBarViewModel mViewModel;
  ScrollController controller = new ScrollController();
  int _index = 0;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      mViewModel = Provider.of<BottomNavBarViewModel>(context, listen: false);
      eventBus.on<TabClickEvent>().listen((event) {
        if(event.tabText == "exchange"){
          setState(() {
            _index = 2;
          });
        }else if(event.tabText == "wallet"){
          setState(() {
            _index = 1;
          });
        }
      });

    });
  }



  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    mViewModel = Provider.of<BottomNavBarViewModel>(context);

    callPage(int current) {
      switch (current) {
        case 0:
          return HomeView(navScaffoldKey: scaffoldKey,);
        case 1:
          return WalletView(navScaffoldKey: scaffoldKey,);
        case 2:
          return CurrencyRatesView(navScaffoldKey: scaffoldKey,);
        case 3:
          return SettingView(navScaffoldKey: scaffoldKey,);
        default:
          return HomeView(navScaffoldKey: scaffoldKey,);
      }
    }

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      extendBody: true,
      drawer: SideMenuView(
        onMenuItemClick: (menuModel) {
          if (menuModel.name == S.of(context).home) {
            _index = 0;
          } else if (menuModel.name == S.of(context).wallets) {
            _index = 1;
          } else if (menuModel.name == S.of(context).settings) {
            _index = 3;
          }else if(menuModel.name == S.of(context).currencyRates){
            _index = 2;
          }
          setState(() {

          });
        },
      ),
      key: scaffoldKey,
      body: callPage(_index),
      bottomNavigationBar: Container(
        height: 90.0 + (Platform.isAndroid ? MediaQuery.of(context).viewPadding.bottom : 14.0),
        child: FloatingNavbar(
          padding: EdgeInsets.all(15),
          margin: EdgeInsets.only(left: 20, right: 20),
          borderRadius: 20.0,
          backgroundColor:
              Theme.of(context).bottomNavigationBarTheme.backgroundColor,
          onTap: (int val) {
            setState(() {
              _index = val;
            });
          },
          currentIndex: _index,
          selectedBackgroundColor: CommonColors.bottomIconBorderColor,
          items: [
            FloatingNavbarItem(
              customWidget: Image.asset(LocalImages.home,
                  height: 20,
                  width: 22,
                  color: _index == 0
                      ? Colors.white
                      : CommonColors.gradientColorBottom),
            ),
            FloatingNavbarItem(
                customWidget: Image.asset(LocalImages.wallet,
                    height: 20,
                    width: 22,
                    color: _index == 1
                        ? Colors.white
                        : CommonColors.gradientColorBottom)),
            FloatingNavbarItem(
                customWidget: Image.asset(LocalImages.graph,
                    height: 20,
                    width: 22,
                    color: _index == 2
                        ? Colors.white
                        : CommonColors.gradientColorBottom)),
            FloatingNavbarItem(
                customWidget: Image.asset(LocalImages.setting,
                    height: 20,
                    width: 22,
                    color: _index == 3
                        ? Colors.white
                        : CommonColors.gradientColorBottom)),
          ],
        ),
      ),
    );
  }
  
  /*Stack(
        children: [
          (_index == 3)
              ? MenuAppbar(
                  title: getTitle(_index),
                  height: 153.0,
                  onMenuClick: () {
                    scaffoldKey.currentState.openDrawer();
                  },
                  onNotificationClick: () {
                    Navigator.push(context,
                        CupertinoPageRoute(builder: (context) {
                      return NotificationsView();
                    }));
                  })
              : /*MainAppbar(
                  isBack: false,
                  titleText: getTitle(_index),
                  balanceText: Globals.currencySymbol+" "+Globals.getConvertedCurrency(15723.00),
                  onIconClick: () {
                    scaffoldKey.currentState.openDrawer();
                  },
                  onNotificationClick: () {
                    Navigator.push(context,
                        CupertinoPageRoute(builder: (context) {
                      return NotificationsView();
                    }));
                  },
                )*/Container(),
          _index == 0 || _index == 2
              ? /*Container(
                  margin: EdgeInsets.only(top: 130.0, left: 20.0, right: 20.0),
                  decoration: BoxDecoration(
                      color: _index == 0
                          ? Theme.of(context).backgroundColor
                          : Colors.transparent,
                      borderRadius: BorderRadius.circular(20.0),
                      boxShadow: [
                        BoxShadow(
                            spreadRadius: 20.0,
                            blurRadius: 20.0,
                            color: Theme.of(context).shadowColor,
                            offset: Offset(0.0, 30.0))
                      ]),
                  clipBehavior: Clip.antiAlias,
                  child: callPage(_index),
                )*/Container(height: 0,width: 0,)
              : (_index == 3)
                  ? Container(
                      margin: EdgeInsets.only(top: 150.0),
                      decoration: BoxDecoration(
                          color: Theme.of(context).backgroundColor,
                          boxShadow: [
                            BoxShadow(
                                spreadRadius: 20.0,
                                blurRadius: 20.0,
                                color: Theme.of(context).backgroundColor,
                                offset: Offset(0.0, 0.0))
                          ]),
                      clipBehavior: Clip.antiAlias,
                      child: callPage(_index),
                    )
                  : Container(
                      margin: EdgeInsets.only(top: 130.0),
                      decoration: BoxDecoration(color: Colors.transparent,
                      //     boxShadow: [
                      //   BoxShadow(
                      //       spreadRadius: 20.0,
                      //       blurRadius: 20.0,
                      //       color: Theme.of(context).shadowColor,
                      //       offset: Offset(0.0, 40.0))
                      // ]

                      ),
                      clipBehavior: Clip.antiAlias,
                      child: callPage(_index),
                    ),
        ],
      )*/

  String getTitle(int index) {
    if (index == 0) {
      return S.of(context).balance;
    } else if (index == 1) {
      return S.of(context).wallet;
    } else if (index == 2) {
      return S.of(context).currencyRates;
    } else if (index == 3) {
      return S.of(context).settings;
    }
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class BottomNavBarViewModel with ChangeNotifier {
  BuildContext mContext;
  int currentIndex;
  bool isShowModelSheet = false;
  // final ValueNotifier<bool> visible = ValueNotifier<bool>(true);

  attachContext(BuildContext context) {
    mContext = context;
  }

  selectedTab(int current) {
    currentIndex = current;
    notifyListeners();
  }



  double bottom = 0;
  // double _last = 0;

  // void initializeScrollController(controller) {
  //   visible.value = true;
  //   controller.addListener(() {
  //     if (controller.position.userScrollDirection == ScrollDirection.reverse) {
  //       // isVisible = false;
  //       visible.value = false;
  //       notifyListeners();
  //     }
  //     if (controller.position.userScrollDirection == ScrollDirection.forward) {
  //       // isVisible = true;
  //       visible.value = true;
  //       notifyListeners();
  //     }
  //   });
  // }
}

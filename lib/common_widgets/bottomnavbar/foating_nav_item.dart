import 'package:flutter/material.dart';

class FloatingNavbarItem {
  final String title;
  final IconData icon;
  final Widget customWidget;
  final Widget customBackground;

  FloatingNavbarItem({
    this.icon,
    this.title,
    this.customWidget,
    this.customBackground
  }) : assert(icon != null || customWidget != null || customBackground != null);
}
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class EditText extends StatelessWidget {
   String hint;
   TextEditingController controller;
   TextInputAction inputAction;
   TextInputType inputType;
   int maxLine;
   int maxLenth;
   bool isObscure;
   Function(String)  onSubmit;

  EditText(
      {this.hint,
      this.controller,
      this.inputAction,
      this.inputType,
      this.maxLine,
      this.maxLenth,
      this.isObscure,this.onSubmit});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 10),
      child: TextField(
        cursorColor: Colors.white,
        obscureText: isObscure ?? false,
        textInputAction: inputAction,
        keyboardType: inputType,
        controller: controller,
        onSubmitted: (value){
          if(onSubmit != null){
            onSubmit(value);
          }
        },
        style: CommonStyle.getAppFont(
            color: Colors.white, fontWeight: FontWeight.normal, fontSize: 16),
        decoration: InputDecoration(
            counterText: "",
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(30.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color:
                  Colors.transparent,
                  width: 0.0),
              borderRadius: BorderRadius.circular(30.0),
            ),
            contentPadding: EdgeInsets.only(left: 20, right: 10),
            filled: true,
            hintStyle: TextStyle(color: Colors.white,fontSize: 16,fontWeight: FontWeight.normal),
            hintText: hint,
            fillColor: CommonColors.shadowColor.withOpacity(.5)),
        maxLines: maxLine ?? 1,
        maxLength: maxLenth ?? 200,
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class CRoundTextField extends StatelessWidget {

  final double width,height;
  final TextAlign textAlign;
  final TextStyle textStyle,hintTextStyle;
  final Color borderColor;
  final String hintText;
  final TextEditingController controller;
  final Function(String)  onChange,onSubmit;
  final TextInputType textInputType;
  final List<TextInputFormatter> textInputFormatter;


  CRoundTextField(
      {this.width,
      this.height,
      this.textAlign,
      this.textStyle,
      this.hintTextStyle,
      this.borderColor,
      this.hintText,this.controller,this.onChange,this.onSubmit,this.textInputType,this.textInputFormatter});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? 180.0,
      height: height ?? 45.0,
      child: TextFormField(
        textAlign: textAlign ?? TextAlign.start,
        keyboardType: textInputType ?? TextInputType.text,
        inputFormatters: textInputFormatter ?? [],
        maxLines: 1,
        style: textStyle ?? CommonStyle.getAppFont(
          color: CommonColors.getWhiteTextColor(context),
          fontWeight: FontWeight.bold,
          fontSize: 16.0,
        ),
        controller: controller ?? TextEditingController(),
        onChanged: onChange ?? (String){},
        onFieldSubmitted: onSubmit ?? (String){},
        decoration: InputDecoration(
            contentPadding: EdgeInsets.only(left: 15.0,right: 15.0),
            hintText: hintText ?? "",
            hintStyle: hintTextStyle ?? CommonStyle.getAppFont(
              color: CommonColors.getHintTextStyle(context),
              fontWeight: FontWeight.bold,
              fontSize: 16.0,
            ),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30.0),
                borderSide: BorderSide(
                    color: CommonColors.getWhiteTextColor(context).withOpacity(.5),
                    width: 1.0)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30.0),
                borderSide: BorderSide(
                    color: CommonColors.getWhiteTextColor(context).withOpacity(.5),
                    width: 1.0))),
      ),
    );
  }
}

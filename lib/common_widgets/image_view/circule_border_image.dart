import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sendcash_flutter/generated/assets.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';

class CirculeBorderImage extends StatelessWidget {
  double height, width;
  String imageUrl;

  CirculeBorderImage({this.height, this.width, this.imageUrl});

  @override
  Widget build(BuildContext context) {
    var backgroundImage;
    if (imageUrl == null || imageUrl.startsWith("assets") || imageUrl.isEmpty) {
      backgroundImage = AssetImage((imageUrl == null || imageUrl.isEmpty)
          ? Assets.imgIcProfile
          : imageUrl);
    } else if (imageUrl.startsWith("http")) {
      backgroundImage = NetworkImage(imageUrl ??
          'https://cdn.pixabay.com/photo/2018/01/15/07/52/woman-3083390_1280.jpg');
    } else if (imageUrl.startsWith("assets")) {} else {
      backgroundImage = FileImage(File(imageUrl));
    }

    return Container(
      height: height ?? 66.0,
      width: width ?? 66.0,
      padding: EdgeInsets.all(2.0),
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          border:
          Border.all(color: CommonColors.gradientColorBottom, width: 2.0)),
      child: CircleAvatar(
        backgroundImage: backgroundImage,
      ),
    );
  }
}

import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/utils/local_images.dart';

class RoundImageView extends StatelessWidget {
  final double height, width, radius;
  final BoxFit fit;
  final String imagePath;

  RoundImageView(
      {this.height, this.width, this.imagePath, this.radius, this.fit});

  @override
  Widget build(BuildContext context) {
    return (imagePath == null || imagePath.isEmpty)
        ? ClipRRect(
            clipBehavior: Clip.antiAlias,
            borderRadius: BorderRadius.circular(radius ?? 10),
            child: FadeInImage(
                height: height ?? 50,
                width: width ?? 50,
                fit: fit ?? BoxFit.cover,
                placeholder: AssetImage(LocalImages.logo),
                image: AssetImage(LocalImages.logo)),
          )
        : imagePath.startsWith("http")
            ? ClipRRect(
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(radius ?? 10),
                child: FadeInImage(
                    height: height ?? 50,
                    width: width ?? 50,
                    fit: fit ?? BoxFit.cover,
                    placeholder: AssetImage(LocalImages.logo),
                    imageErrorBuilder: (context, object, stackTrace) {
                      return Image.asset(LocalImages.logo);
                    },
                    image: NetworkImage(imagePath)),
              )
            : (imagePath.startsWith("assets")
                ? ClipRRect(
                    clipBehavior: Clip.antiAlias,
                    borderRadius: BorderRadius.circular(radius ?? 10),
                    child: FadeInImage(
                        height: height ?? 50,
                        width: width ?? 50,
                        fit: fit ?? BoxFit.cover,
                        placeholder: AssetImage(LocalImages.logo),
                        image: AssetImage(imagePath)),
                  )
                : ClipRRect(
                    clipBehavior: Clip.antiAlias,
                    borderRadius: BorderRadius.circular(radius ?? 10),
                    child: FadeInImage(
                        height: height ?? 50,
                        width: width ?? 50,
                        fit: fit ?? BoxFit.cover,
                        placeholder: AssetImage(LocalImages.logo),
                        image: FileImage(new File(imagePath))),
                  ));
  }
}

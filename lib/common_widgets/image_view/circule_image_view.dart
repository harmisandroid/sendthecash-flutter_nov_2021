import 'dart:io';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/utils/local_images.dart';

class CirculeImageView extends StatelessWidget {
  final double height, width;
  final String imageUrl;

  CirculeImageView({this.height, this.width, this.imageUrl});

  @override
  Widget build(BuildContext context) {
    if (imageUrl == null || imageUrl.isEmpty) {
      return ClipOval(
        clipBehavior: Clip.antiAlias,
        child: Image.asset(
          LocalImages.logo,
          height: height ?? 50,
          width: width ?? 50,
          fit: BoxFit.cover,
        ),
      );
    } else if (imageUrl.startsWith("assets")) {
      return ClipOval(
        clipBehavior: Clip.antiAlias,
        child: Image.asset(
          LocalImages.logo,
          height: height ?? 50,
          width: width ?? 50,
          fit: BoxFit.cover,
        ),
      );
    } else if (imageUrl.startsWith("http")) {
      return ClipOval(
        clipBehavior: Clip.antiAlias,
        child: Image.network(imageUrl ?? LocalImages.logo,
            height: height ?? 50,
            width: width ?? 50,
            fit: BoxFit.cover, errorBuilder: (context, obj, stackstrace) {
          return Image.asset(
            LocalImages.logo,
            height: height ?? 50,
            width: width ?? 50,
          );
        }),
      );
    } else {
      return ClipOval(
        clipBehavior: Clip.antiAlias,
        child: Image.file(
          new File(imageUrl),
          height: height ?? 50,
          width: width ?? 50,
          fit: BoxFit.cover,
        ),
      );
    }
  }
}

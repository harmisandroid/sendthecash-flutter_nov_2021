import 'package:flutter/material.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class HomeCardView extends StatelessWidget {
  final mCvvController = TextEditingController();
  bool isCvv;
  double totalBalance;
  HomeCardView({this.isCvv,this.totalBalance});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Container(
            height: 225.0,
            width: 388.0,
            child: Image.asset(LocalImages.blue_card),
          ),
          Container(
            padding: EdgeInsets.only(top: 25.0,left: 12.0),
            child: Text("1234 5678 9012 3456",
            style: CommonStyle.getAppFont(
              color: Colors.black,
              fontSize: 17,
              fontWeight: FontWeight.w600
            ),),
          ),
          Container(
            padding: EdgeInsets.only(top: 142.0,left: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(S.of(context).totalBalance,
                  style: CommonStyle.getAppFont(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.normal
                  ),),
                Text(Globals.currencySymbol+Globals.getConvertedCurrency(totalBalance),
                  style: CommonStyle.getAppFont(
                      color: Colors.black,
                      fontSize: 22,
                      fontWeight: FontWeight.w500
                  ),),
              ],
            ),
          ),
      isCvv == true?
      Container(
        height: 37.0,
        width:88.0,
        margin: EdgeInsets.only(top: 100.0,left: 225.0),
        child: TextField(
          cursorColor: Colors.black,
          obscureText: true,
          textInputAction: TextInputAction.done,
          keyboardType: TextInputType.number,
          controller: mCvvController,
          onSubmitted: (value){
           /* if(onSubmit != null){
              onSubmit(value);
            }*/
          },
          style: CommonStyle.getAppFont(
              color: Colors.black, fontWeight: FontWeight.normal, fontSize: 16),
          decoration: InputDecoration(
              counterText: "",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
                borderRadius: BorderRadius.circular(30.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    color:
                    Colors.transparent,
                    width: 0.0),
                borderRadius: BorderRadius.circular(30.0),
              ),
              contentPadding: EdgeInsets.only(left: 20, right: 10),
              filled: true,
              hintStyle: TextStyle(color: Colors.grey,fontSize: 16,fontWeight: FontWeight.normal),
              hintText: "CVV",
              fillColor: Colors.white),
          maxLines: 1,
          maxLength: 3,
        ),

      ):Container()],
      ),
    );
  }
}

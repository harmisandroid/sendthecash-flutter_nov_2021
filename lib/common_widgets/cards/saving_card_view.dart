import 'package:flutter/material.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class SavingCardView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Container(
            height: 225.0,
            width: 388.0,
            child: Image.asset(LocalImages.pink_card),
          ),
          Container(
            padding: EdgeInsets.only(top: 30.0,left: 12.0),
            child: Text("1234 5678 9012 3456",
            style: CommonStyle.getAppFont(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.w600
            ),),
          ),
          Container(
            padding: EdgeInsets.only(top: 145.0,left: 10.0),
            child: Column(
              children: [
                Text("Total Balance",
                  style: CommonStyle.getAppFont(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.normal
                  ),),
                Text(Globals.currencySymbol+"15,723.00",
                  style: CommonStyle.getAppFont(
                      color: Colors.white,
                      fontSize: 22,
                      fontWeight: FontWeight.w500
                  ),),
              ],
            ),
          )

        ],
      ),
    );
  }
}

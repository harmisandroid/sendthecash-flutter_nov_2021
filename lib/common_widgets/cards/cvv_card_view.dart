import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/models/user_card_details.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class CVVCardView extends StatelessWidget {
  TextEditingController mCvvController = TextEditingController();
  bool isSelected;
  CardDetails cardDetails;

  CVVCardView({this.mCvvController,this.isSelected,this.cardDetails});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 160.0,
      width: MediaQuery.of(context).size.width * 0.8,
      child: Stack(
        children: [
          Container(
            height: 160.0,
            width: MediaQuery.of(context).size.width * 0.8,
            child: Image.asset(LocalImages.blue_card,fit: BoxFit.fill,),
          ),
          Container(
            padding: EdgeInsets.only(top: 15.0, left: 10.0),
            child: Text(
              cardDetails.cardNo,
              style: CommonStyle.getAppFont(
                  color: Colors.black,
                  fontSize: 12,
                  fontWeight: FontWeight.w600),
            ),
          ),
          Align(
            alignment: AlignmentDirectional.bottomStart,
            child: Container(
              margin: EdgeInsets.only(left: 10.0,bottom: 20.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    S.of(context).cardHolderName,
                    style: CommonStyle.getAppFont(
                        color: Colors.black,
                        fontSize: 10,
                        fontWeight: FontWeight.normal),
                  ),
                  Text(
                    cardDetails.cardholderName,
                    style: CommonStyle.getAppFont(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.w500),
                  ),

                ],
              ),
            ),
          ),
          isSelected ? Align(
            alignment: AlignmentDirectional.bottomEnd,
            child:Container(
              margin: EdgeInsetsDirectional.only(bottom: 45.0,end: 10.0),
              height: 30.0,
              width: 60.0,
              alignment: Alignment.center,
              child: TextField(
                cursorColor: Colors.black,
                obscureText: true,
                textAlign: TextAlign.center,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.number,
                controller: mCvvController,
                maxLines: 1,
                onChanged: (value){
                  if(cardDetails != null){
                    cardDetails.cvv = value;
                  }
                },
                onSubmitted: (value) {
                  if(cardDetails != null){
                    cardDetails.cvv = value;
                  }
                },
                style: CommonStyle.getAppFont(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 12),
                decoration: InputDecoration(
                    counterText: "",
                    contentPadding: EdgeInsets.only(top: 5.0),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide:
                      BorderSide(color: Colors.transparent, width: 0.0),
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    filled: true,
                    hintStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: 12,
                        fontWeight: FontWeight.normal),
                    hintText: "CVV",
                    fillColor: Colors.white),

              ),
            ),
          ) : Container(height: 0,width: 0,)
        ],
      ),
    );
  }
}

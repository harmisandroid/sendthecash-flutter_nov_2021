import 'package:flutter/material.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class DarkblueCardView extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 202.0,
      width: MediaQuery.of(context).size.width * 0.8,
      child: Stack(
        children: [
          Container(
            height: 202.0,
            width: MediaQuery.of(context).size.width * 0.8,
            child: Image.asset(LocalImages.dark_blue_card),
          ),
          Container(
            padding: EdgeInsets.only(top: 20.0, left: 10.0),
            child: Text(
              "1234 5678 9012 3456",
              style: CommonStyle.getAppFont(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.w600),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 10.0,bottom: 20.0),
            alignment: Alignment.bottomLeft,
            child: Row(
              children: [
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      S.of(context).cardName,
                      style: CommonStyle.getAppFont(
                          color: Colors.white,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                    ),
                    Text(
                     "John Doe",
                      style: CommonStyle.getAppFont(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w500),
                    ),

                  ],
                ),
                SizedBox(width: 20.0,),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      S.of(context).expired,
                      style: CommonStyle.getAppFont(
                          color: Colors.white,
                          fontSize: 14,
                          fontWeight: FontWeight.normal),
                    ),
                    Text(
                      "12 / 25",
                      style: CommonStyle.getAppFont(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w500),
                    ),

                  ],
                ),
              ],
            ),
          ),

        ],
      ),
    );
  }
}

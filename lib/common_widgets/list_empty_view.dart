import 'package:flutter/material.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class EmptyListView extends StatelessWidget {
  String image, message, btnText;
  Function onButtonPressed;
  double height, topMargin;

  EmptyListView(
      {this.image,
      this.message,
      this.onButtonPressed,
      this.height,
      this.topMargin,
      this.btnText});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 30.0, right: 30.0),
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            image,
            height: 100.0,
            width: 100.0,
            fit: BoxFit.fill,
            color: Colors.grey,
          ),
          SizedBox(
            height: 40.0,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20.0),
            child: Text(
              message ?? "No data found",
              textAlign: TextAlign.center,
              style: CommonStyle.getAppFont(
                  color: Colors.grey,
                  fontWeight: FontWeight.normal,
                  fontSize: 17.0),
            ),
          ),
          SizedBox(
            height: 40.0,
          ),
          /*InkWell(
            onTap: onButtonPressed,
            child: Container(
              alignment: Alignment.center,
              height: 50.0,
              width: 250,
              decoration: BoxDecoration(
                color: CommonColors.primaryColor,
                borderRadius: BorderRadius.circular(11.0),
              ),
              child: Text(
                btnText ?? S.of(context).backToHome,
                style: CommonStyle.getAppFont(
                    fontWeight: FontWeight.w500,
                    fontSize: 17.0,
                    color: Colors.black),
              ),
            ),
          )*/
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class HorizontalEmptyView extends StatelessWidget {
  double height;
  String message;

  HorizontalEmptyView({this.height, this.message});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: height ?? 240.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            LocalImages.logo,
            color: Colors.grey,
            height: 50.0,
            width: 50.0,
          ),
          SizedBox(
            height: 10.0,
          ),
          Text(
            message ?? "No data found",
            style: CommonStyle.getAppFont(
                color: Colors.grey,
                fontSize: 15.0,
                fontWeight: FontWeight.normal),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';

class Background extends StatelessWidget {
  const Background({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                CommonColors.gradientColorTop,
                CommonColors.gradientColorBottom
              ])),
    );
  }
}

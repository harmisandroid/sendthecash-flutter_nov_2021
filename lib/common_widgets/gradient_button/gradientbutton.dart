import 'package:flutter/material.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class GradientButton extends StatelessWidget {
 String btnText;
 Function onClick;
 double height;

 GradientButton({this.btnText,this.onClick,this.height});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        if(onClick != null){
          onClick();
        }
      },
      child: Container(
        height: height,
        padding: EdgeInsetsDirectional.all(10.0),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                CommonColors.buttonGradientColorTop,
                CommonColors.buttonGradientColorBottom
              ]),
          borderRadius: BorderRadius.circular(20.0),),
        child: Center(
          child: Text(btnText,
            style: CommonStyle.getAppFont(
                color: Colors.white,
                fontWeight: FontWeight.w600,
                fontSize: 17
            ),),
        ),
      ),
    );
  }
}

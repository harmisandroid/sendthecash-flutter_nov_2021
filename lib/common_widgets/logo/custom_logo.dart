import 'package:flutter/material.dart';
import 'package:sendcash_flutter/generated/i18n.dart';

class CustomLogo extends StatelessWidget {
  double size;

  CustomLogo({this.size});

  @override
  Widget build(BuildContext context) {
    return Text(
      S.of(context).appName,
      textAlign: TextAlign.center,
      style: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: size ?? 25,
          color: Colors.white),
    );
  }
}

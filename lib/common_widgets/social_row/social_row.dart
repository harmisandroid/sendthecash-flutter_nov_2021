import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/app_ui/login/login_view.dart';
import 'package:sendcash_flutter/app_ui/signup/signup_view.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/constant.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class SocialRow extends StatelessWidget {
  String textHint;

  Function(String) onClicked;
  Function() onTextHintClicked;

  SocialRow({this.textHint, this.onClicked,this.onTextHintClicked});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 25),
      child: Column(
        children: [
          Row(children: <Widget>[
            Expanded(
              child: new Container(
                height: 2,
                color: CommonColors.shadowcolor,
                margin: EdgeInsets.only(left: 10.0, right: 25.0),
              ),
            ),
            Container(
                child: Text(
              S.of(context).or,
              style: CommonStyle.getAppFont(color: Colors.white, fontSize: 14),
            )),
            Expanded(
              child: Container(
                height: 2,
                color: CommonColors.shadowcolor,
                margin: EdgeInsets.only(left: 25.0, right: 10.0),
              ),
            ),
          ]),
          SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
             /* getSocialButton(
                  logo: LocalImages.facebook,
                  onClick: () {
                    if (onClicked != null) {
                      onClicked(AppConstants.CLICK_TYPE_FACEBOOK);
                    }
                  }),*/
              getSocialButton(
                  logo: LocalImages.twitter,
                  onClick: () {
                    if (onClicked != null) {
                      onClicked(AppConstants.CLICK_TYPE_TWITTER);
                    }
                  }),
              getSocialButton(
                  logo: LocalImages.google,
                  onClick: () {
                    if (onClicked != null) {
                      onClicked(AppConstants.CLICK_TYPE_GOOGLE);
                    }
                  }),
            ],
          ),
          InkWell(
            onTap: () {

              if(onTextHintClicked != null){
                onTextHintClicked();
              }
              },
            child: Container(
              padding: EdgeInsets.only(top: 25),
              alignment: Alignment.center,
              child: Text(
                textHint ?? S.of(context).registerHint,
                style: CommonStyle.getAppFont(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.normal),
              ),
            ),
          ),
          SizedBox(height: 20,),
        ],
      ),
    );
  }

  Widget getSocialButton({String logo, Function onClick}) {
    return InkWell(
      onTap: onClick,
      child: Container(
        height: 50,
        width: 95,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(30)),
          color: CommonColors.shadowcolor.withOpacity(.5),
        ),
        child: Center(
          child: Image.asset(
            logo,
            height: 24,
            width: 24,
          ),
        ),
      ),
    );
  }
}

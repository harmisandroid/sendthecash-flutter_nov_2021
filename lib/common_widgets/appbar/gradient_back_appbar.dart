import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/common_widgets/bottomnavbar/bottom_navigation_view.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class GAppbar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Function onBackPressed;
  final Widget actions;
  final double height;
  final Function onIconClick;
  bool isBackVisible;

  GAppbar(
      {this.title,
      this.onBackPressed,
      this.height,
      this.actions,
      this.onIconClick,
      this.isBackVisible});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Container(
          height: height ?? 80.0,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  color: Colors.grey,
                  blurRadius: 2.0,
                  offset: Offset.zero,
                  spreadRadius: 2.0)
            ],
            gradient: LinearGradient(colors: [
              CommonColors.gradientColorTop,
              CommonColors.gradientColorBottom
            ], begin: Alignment.centerLeft, end: Alignment.centerRight),
          ),
          padding: EdgeInsets.only(left: 20.0, right: 20.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              isBackVisible != null && !isBackVisible
                  ? Container(
                      height: 20.0,
                      width: 20.0,
                    )
                  : InkWell(
                      onTap: () {
                        if (onBackPressed == null) {
                          Navigator.pop(context);
                        } else {
                          onBackPressed();
                        }
                      },
                      child: Container(
                        height: 30.0,
                        width: 30.0,
                        alignment: AlignmentDirectional.centerStart,
                        child: Image.asset(
                          LocalImages.back,
                          height: 25.0,
                          width: 25.0,
                        ),
                      ),
                    ),
              Text(
                title ?? "",
                textAlign: TextAlign.start,
                style: CommonStyle.getAppFont(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 22.0),
              ),
              InkWell(
                onTap: () {
                  if (onIconClick != null) {
                    onIconClick();
                  }
                },
                child: actions ??
                    Container(
                      height: 30.0,
                      width: 30.0,
                    ),
              ),
            ],
          ),
        ),
        onWillPop: () {
          Navigator.pushAndRemoveUntil(context,
              CupertinoPageRoute(builder: (context) {
            return BottomNavigationView();
          }), (route) => false);
        });
  }

  @override
  Size get preferredSize => Size.fromHeight(height ?? 80.0);
}

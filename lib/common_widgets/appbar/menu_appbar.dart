import 'package:flutter/material.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class MenuAppbar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Function onBackPressed;
  final  double height;
  final Function onMenuClick;
  final Function onNotificationClick;
  MenuAppbar({this.title, this.onBackPressed, this.height,this.onMenuClick,this.onNotificationClick});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height ?? 153.0,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.grey,
              blurRadius: 2.0,
              offset: Offset.zero,
              spreadRadius: 2.0)
        ],
        gradient: LinearGradient(colors: [
          CommonColors.gradientColorTop,
          CommonColors.gradientColorBottom
        ], begin: Alignment.centerLeft, end: Alignment.centerRight),
      ),
      padding: EdgeInsets.only(left: 20.0,right: 20.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: (){
              onMenuClick();
            },
            child: Container(
              height: 30.0,
              width: 30.0,
              alignment: AlignmentDirectional.centerStart,
              child: Image.asset(
                LocalImages.sidelist,
                height: 23.0,
                width: 23.0,
              ),
            ),
          ),
          Text(
            title ?? "",
            textAlign: TextAlign.start,
            style: CommonStyle.getAppFont(
                color: Colors.white, fontWeight: FontWeight.w500, fontSize: 24.0),
          ),
          InkWell(
            onTap: (){
              onNotificationClick();
            },
            child:Image.asset(LocalImages.notification,height: 21,width:22,),),

        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height ?? 80.0);
}

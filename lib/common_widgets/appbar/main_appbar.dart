import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';
import 'package:sendcash_flutter/utils/globals.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class MainAppbar extends StatelessWidget implements PreferredSizeWidget {
  final String leftIcon;
  final String titleText;
  final String balanceText;
  final bool isBack;
  final double height;
  final Function onIconClick;
  final Function onNotificationClick;

  MainAppbar(
      {this.titleText,
      this.isBack,
      this.balanceText,
      this.height,
      this.leftIcon,this.onIconClick,this.onNotificationClick});

  @override
  Widget build(BuildContext context) {
    String leftIcon = LocalImages.menu;
    if (isBack != null && isBack) {
      leftIcon = LocalImages.back;
    } else {
      leftIcon = LocalImages.menu;
    }

    return Container(
      height: 175,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.grey,
              blurRadius: 2.0,
              offset: Offset.zero,
              spreadRadius: 2.0)
        ],
        gradient: LinearGradient(colors: [
          CommonColors.gradientColorTop,
          CommonColors.gradientColorBottom
        ], begin: Alignment.centerLeft, end: Alignment.centerRight),
      ),
      padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 50.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: (){
              if(onIconClick != null){
                onIconClick();
              }

            },
            child: Image.asset(
              leftIcon,
              height: 21.0,
              width: 21.0,
              fit: BoxFit.contain,
              color: Colors.white,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                titleText ?? S.of(context).balance,
                textAlign: TextAlign.start,
                style: CommonStyle.getAppFont(
                    color: Colors.white,
                    fontSize: 22.0,
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 5.0,
              ),
              Text(
                balanceText ?? "",
                textAlign: TextAlign.start,
                style: CommonStyle.getAppFont(
                    color: Colors.white,
                    fontSize: 28.0,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 20.0,)
            ],
          ),
          InkWell(
            onTap: onNotificationClick,
            child:Image.asset(
              LocalImages.notification,
              height: 21.0,
              width: 21.0,
            ),
          ),
        ],
      ),
    );

    // return Container(
    //   height: 180.0,
    //   width: MediaQuery.of(context).size.height,
    //   ,
    //   child: Container(
    //     child: Stack(
    //       children: [
    //         ,
    //         Positioned(
    //             bottom: 0.0,
    //             child:Blur(
    //               blur: 24.0,
    //                 borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0),topRight: Radius.circular(10.0)),
    //                 child: Container(
    //           width: MediaQuery.of(context).size.width,
    //           height: 30.0,
    //         )))
    //       ],
    //     ),
    //   ),
    // );
  }

  @override
  Size get preferredSize => Size.fromHeight(height ?? 160);

  List<Widget> buildBlurredImage(List<Widget> l) {
    List<Widget> list = [];
    list.addAll(l);
    double sigmaX = 0;
    double sigmaY = 0.1;
    for (int i = 0; i < 160; i += 5) {
      // 100 is the starting height of blur
      // 350 is the ending height of blur
      list.add(Positioned(
        bottom: i.toDouble(),
        left: 0,
        right: 0,
        child: ClipRect(
          child: BackdropFilter(
            filter: ImageFilter.blur(
              sigmaX: sigmaX,
              sigmaY: sigmaY,
            ),
            child: Container(
              height: 1.0,
              color: Colors.transparent,
            ),
          ),
        ),
      ));
      sigmaX += 0.1;
      sigmaY += 0.1;
    }
    return list;
  }
}

import 'package:flutter/material.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class BackAppbar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Function onBackPressed;
  final Widget actions;
  final  double height;
  BackAppbar({this.title, this.onBackPressed, this.height, this.actions});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100.0,
      padding: EdgeInsets.only(left: 20.0,right: 20.0,top: MediaQuery.of(context).viewPadding.top),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: (){
              if(onBackPressed == null){
                Navigator.pop(context);
              }else{
                onBackPressed();
              }
            },
            child: Container(
              height: 30.0,
              width: 30.0,
              alignment: AlignmentDirectional.centerStart,
              child: Image.asset(
                LocalImages.back,
                height: 25.0,
                width: 25.0,
              ),
            ),
          ),
          Text(
            title ?? "",
            textAlign: TextAlign.start,
            style: CommonStyle.getAppFont(
                color: Colors.white, fontWeight: FontWeight.w500, fontSize: 20.0),
          ),
          actions ?? Container( height: 30.0,
             width: 30.0,)
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height ?? 100.0);
}

import 'package:flutter/material.dart';
import 'package:sendcash_flutter/generated/i18n.dart';
import 'package:sendcash_flutter/utils/common_colors.dart';

class SwitchWithText extends StatefulWidget {
  final bool value;
  final ValueChanged<bool> onChanged;
  final Color activeColor;

  const SwitchWithText({Key key, this.value, this.onChanged, this.activeColor})
      : super(key: key);

  @override
  _SwitchWithTextState createState() => _SwitchWithTextState();
}

class _SwitchWithTextState extends State<SwitchWithText>
    with SingleTickerProviderStateMixin {
  Animation _circleAnimation;
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 60));
    _circleAnimation = AlignmentTween(
            begin: widget.value ? Alignment.centerRight : Alignment.centerLeft,
            end: widget.value ? Alignment.centerLeft : Alignment.centerRight)
        .animate(CurvedAnimation(
            parent: _animationController, curve: Curves.linear));
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return GestureDetector(
          onTap: () {
            if (_animationController.isCompleted) {
              _animationController.reverse();
            } else {
              _animationController.forward();
            }
            widget.value == false
                ? widget.onChanged(true)
                : widget.onChanged(false);
          },
          child: Container(
            height: 33.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: _circleAnimation.value == Alignment.centerLeft
                    ? CommonColors.getWhiteTextColor(context).withOpacity(.1)
                    : CommonColors.getWhiteTextColor(context).withOpacity(.1)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _circleAnimation.value == Alignment.centerRight
                    ? Padding(
                        padding: const EdgeInsets.only(left: 18.0,right: 10.0),
                        child: Text(
                          S.of(context).dark,
                          style: TextStyle(
                              color: CommonColors.getWhiteTextColor(context),
                              fontWeight: FontWeight.normal,
                              fontSize: 10.0),
                        ),
                      )
                    : Container(),
                Align(
                  alignment: _circleAnimation.value,
                  child: Container(
                    width: 65.0,
                    height: 35.0,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              CommonColors.buttonGradientColorTop,
                              CommonColors.buttonGradientColorBottom
                            ]),
                        borderRadius: BorderRadius.circular(20.0)),
                    child:  _circleAnimation.value == Alignment.centerRight
                        ?Text(
                      S.of(context).light,
                      style: TextStyle(
                          color: CommonColors.scaffoldWhiteBack,
                          fontWeight: FontWeight.normal,
                          fontSize: 12.0),
                    ):Text(
                      S.of(context).dark,
                      style: TextStyle(
                          color: CommonColors.scaffoldWhiteBack,
                          fontWeight: FontWeight.normal,
                          fontSize: 12.0),
                    )
                  ),
                ),
                _circleAnimation.value == Alignment.centerLeft
                    ? Padding(
                        padding: const EdgeInsets.only(right: 18.0,left: 10.0),
                        child: Text(
                          S.of(context).light,
                          style: TextStyle(
                              color: CommonColors.getWhiteTextColor(context),
                              fontWeight: FontWeight.normal,
                              fontSize: 12.0),
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
        );
      },
    );
  }
}

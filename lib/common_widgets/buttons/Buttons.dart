import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sendcash_flutter/app_ui/login/login_view.dart';
import 'package:sendcash_flutter/utils/local_images.dart';
import 'package:sendcash_flutter/utils/text_style.dart';

class Buttons extends StatelessWidget {
  final String text;
  final bool isImage;
  Function onClick;
  bool isEnabled;
  Buttons({this.text, this.isImage,this.onClick,this.isEnabled});



  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick,
      child: Container(
        alignment: Alignment.center,
        height: 48,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
          color: (isEnabled == null || isEnabled) ?  Colors.white : Colors.grey,),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            isImage == true
                ? Image.asset(
              LocalImages.fingerprint,
              height: 25.0,
              width: 25.0,
            )
                : Container(),
            SizedBox(width: 8,),
            Text(text,
                style: CommonStyle.getAppFont(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,

                )),
          ],
        ),
      ),
    );
  }
}

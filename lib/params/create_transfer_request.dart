class CreateTransfer {
  bool _autoConfirm;
  String _source;
  String _sourceCurrency;
  String _sourceAmount;
  BankAccountDetails _dest;
  String _destCurrency;

  CreateTransfer(
      {bool autoConfirm,
        String source,
        String sourceCurrency,
        String sourceAmount,
        BankAccountDetails dest,
        String destCurrency}) {
    if (autoConfirm != null) {
      this._autoConfirm = autoConfirm;
    }
    if (source != null) {
      this._source = source;
    }
    if (sourceCurrency != null) {
      this._sourceCurrency = sourceCurrency;
    }
    if (sourceAmount != null) {
      this._sourceAmount = sourceAmount;
    }
    if (dest != null) {
      this._dest = dest;
    }
    if (destCurrency != null) {
      this._destCurrency = destCurrency;
    }
  }

  bool get autoConfirm => _autoConfirm;
  set autoConfirm(bool autoConfirm) => _autoConfirm = autoConfirm;
  String get source => _source;
  set source(String source) => _source = source;
  String get sourceCurrency => _sourceCurrency;
  set sourceCurrency(String sourceCurrency) =>
      _sourceCurrency = sourceCurrency;
  String get sourceAmount => _sourceAmount;
  set sourceAmount(String sourceAmount) => _sourceAmount = sourceAmount;
  BankAccountDetails get dest => _dest;
  set dest(BankAccountDetails dest) => _dest = dest;
  String get destCurrency => _destCurrency;
  set destCurrency(String destCurrency) => _destCurrency = destCurrency;

  CreateTransfer.fromJson(Map<String, dynamic> json) {
    _autoConfirm = json['autoConfirm'];
    _source = json['source'];
    _sourceCurrency = json['sourceCurrency'];
    _sourceAmount = json['sourceAmount'];
    _dest = json['dest'] != null ? new BankAccountDetails.fromJson(json['dest']) : null;
    _destCurrency = json['destCurrency'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['autoConfirm'] = this._autoConfirm;
    data['source'] = this._source;
    data['sourceCurrency'] = this._sourceCurrency;
    data['sourceAmount'] = this._sourceAmount;
    if (this._dest != null) {
      data['dest'] = this._dest.toJson();
    }
    data['destCurrency'] = this._destCurrency;
    return data;
  }
}

class BankAccountDetails {
  bool _chargeablePM;
  String _beneficiaryType;
  String _paymentMethodType;
  String _firstNameOnAccount;
  String _lastNameOnAccount;
  String _accountNumber;
  String _routingNumber;
  String _accountType;

  BankAccountDetails(
      {bool chargeablePM,
        String beneficiaryType,
        String paymentMethodType,
        String firstNameOnAccount,
        String lastNameOnAccount,
        String accountNumber,
        String routingNumber,
        String accountType}) {
    if (chargeablePM != null) {
      this._chargeablePM = chargeablePM;
    }
    if (beneficiaryType != null) {
      this._beneficiaryType = beneficiaryType;
    }
    if (paymentMethodType != null) {
      this._paymentMethodType = paymentMethodType;
    }
    if (firstNameOnAccount != null) {
      this._firstNameOnAccount = firstNameOnAccount;
    }
    if (lastNameOnAccount != null) {
      this._lastNameOnAccount = lastNameOnAccount;
    }
    if (accountNumber != null) {
      this._accountNumber = accountNumber;
    }
    if (routingNumber != null) {
      this._routingNumber = routingNumber;
    }
    if (accountType != null) {
      this._accountType = accountType;
    }
  }

  bool get chargeablePM => _chargeablePM;
  set chargeablePM(bool chargeablePM) => _chargeablePM = chargeablePM;
  String get beneficiaryType => _beneficiaryType;
  set beneficiaryType(String beneficiaryType) =>
      _beneficiaryType = beneficiaryType;
  String get paymentMethodType => _paymentMethodType;
  set paymentMethodType(String paymentMethodType) =>
      _paymentMethodType = paymentMethodType;
  String get firstNameOnAccount => _firstNameOnAccount;
  set firstNameOnAccount(String firstNameOnAccount) =>
      _firstNameOnAccount = firstNameOnAccount;
  String get lastNameOnAccount => _lastNameOnAccount;
  set lastNameOnAccount(String lastNameOnAccount) =>
      _lastNameOnAccount = lastNameOnAccount;
  String get accountNumber => _accountNumber;
  set accountNumber(String accountNumber) => _accountNumber = accountNumber;
  String get routingNumber => _routingNumber;
  set routingNumber(String routingNumber) => _routingNumber = routingNumber;
  String get accountType => _accountType;
  set accountType(String accountType) => _accountType = accountType;

  BankAccountDetails.fromJson(Map<String, dynamic> json) {
    _chargeablePM = json['chargeablePM'];
    _beneficiaryType = json['beneficiaryType'];
    _paymentMethodType = json['paymentMethodType'];
    _firstNameOnAccount = json['firstNameOnAccount'];
    _lastNameOnAccount = json['lastNameOnAccount'];
    _accountNumber = json['accountNumber'];
    _routingNumber = json['routingNumber'];
    _accountType = json['accountType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['chargeablePM'] = this._chargeablePM;
    data['beneficiaryType'] = this._beneficiaryType;
    data['paymentMethodType'] = this._paymentMethodType;
    data['firstNameOnAccount'] = this._firstNameOnAccount;
    data['lastNameOnAccount'] = this._lastNameOnAccount;
    data['accountNumber'] = this._accountNumber;
    data['routingNumber'] = this._routingNumber;
    data['accountType'] = this._accountType;
    return data;
  }
}
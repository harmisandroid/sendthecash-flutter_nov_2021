class CreateAccountParams {
  List<ProfileFields> _profileFields;
  String _type;
  String _country;
  bool _subaccount;
  String _referrerAccountId;
  bool _disableEmail;

  CreateAccountParams(
      {List<ProfileFields> profileFields,
      String type,
      String country,
      bool subaccount,
      String referrerAccountId,
      bool disableEmail}) {
    if (profileFields != null) {
      this._profileFields = profileFields;
    }
    if (type != null) {
      this._type = type;
    }
    if (country != null) {
      this._country = country;
    }
    if (subaccount != null) {
      this._subaccount = subaccount;
    }
    if (referrerAccountId != null) {
      this._referrerAccountId = referrerAccountId;
    }
    if (disableEmail != null) {
      this._disableEmail = disableEmail;
    }
  }

  List<ProfileFields> get profileFields => _profileFields;

  set profileFields(List<ProfileFields> profileFields) =>
      _profileFields = profileFields;

  String get type => _type;

  set type(String type) => _type = type;

  String get country => _country;

  set country(String country) => _country = country;

  bool get subaccount => _subaccount;

  set subaccount(bool subaccount) => _subaccount = subaccount;

  String get referrerAccountId => _referrerAccountId;

  set referrerAccountId(String referrerAccountId) =>
      _referrerAccountId = referrerAccountId;

  bool get disableEmail => _disableEmail;

  set disableEmail(bool disableEmail) => _disableEmail = disableEmail;

  CreateAccountParams.fromJson(Map<String, dynamic> json) {
    if (json['profileFields'] != null) {
      _profileFields = <ProfileFields>[];
      json['profileFields'].forEach((v) {
        _profileFields.add(new ProfileFields.fromJson(v));
      });
    }
    _type = json['type'];
    _country = json['country'];
    _subaccount = json['subaccount'];
    _referrerAccountId = json['referrerAccountId'];
    _disableEmail = json['disableEmail'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._profileFields != null) {
      data['profileFields'] =
          this._profileFields.map((v) => v.toJson()).toList();
    }
    if (_type != null) {
      data['type'] = this._type;
      data['country'] = this._country;
      data['subaccount'] = this._subaccount;
      data['referrerAccountId'] = this._referrerAccountId;
      data['disableEmail'] = this._disableEmail;
    }
    return data;
  }
}

class ProfileFields {
  String _fieldId;
  dynamic _value;

  ProfileFields({String fieldId, dynamic value}) {
    if (fieldId != null) {
      this._fieldId = fieldId;
    }
    if (value != null) {
      this._value = value;
    }
  }

  String get fieldId => _fieldId;

  set fieldId(String fieldId) => _fieldId = fieldId;

  dynamic get value => _value;

  set value(dynamic value) => _value = value;

  ProfileFields.fromJson(Map<String, dynamic> json) {
    _fieldId = json['fieldId'];
    _value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fieldId'] = this._fieldId;
    data['value'] = this._value;
    return data;
  }
}

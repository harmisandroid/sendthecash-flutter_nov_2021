class WalletOrderReserve {
  DebitCard _debitCard;
  Address _address;
  String _reservationId;
  String _amount;
  String _sourceCurrency;
  String _destCurrency;
  String _dest;
  String _referrerAccountId;
  String _givenName;
  String _familyName;
  String _email;
  String _phone;
  String _referenceId;
  String _ipAddress;
  String _firstName;
  String _lastName;
  String _redirectUrl;
  String _failureUrl;
  String _paymentMethod;

  WalletOrderReserve(
      {DebitCard debitCard,
        Address address,
        String reservationId,
        String amount,
        String sourceCurrency,
        String destCurrency,
        String dest,
        String referrerAccountId,
        String givenName,
        String familyName,
        String email,
        String phone,
        String referenceId,
        String ipAddress}) {
    if (debitCard != null) {
      this._debitCard = debitCard;
    }
    if (address != null) {
      this._address = address;
    }
    if (reservationId != null) {
      this._reservationId = reservationId;
    }
    if (amount != null) {
      this._amount = amount;
    }
    if (sourceCurrency != null) {
      this._sourceCurrency = sourceCurrency;
    }
    if (destCurrency != null) {
      this._destCurrency = destCurrency;
    }
    if (dest != null) {
      this._dest = dest;
    }
    if (referrerAccountId != null) {
      this._referrerAccountId = referrerAccountId;
    }
    if (givenName != null) {
      this._givenName = givenName;
    }
    if (familyName != null) {
      this._familyName = familyName;
    }
    if (email != null) {
      this._email = email;
    }
    if (phone != null) {
      this._phone = phone;
    }
    if (referenceId != null) {
      this._referenceId = referenceId;
    }
    if (ipAddress != null) {
      this._ipAddress = ipAddress;
    }
  }


  String get redirectUrl => _redirectUrl;

  set redirectUrl(String value) {
    _redirectUrl = value;
  }

  String get firstName => _firstName;

  set firstName(String value) {
    _firstName = value;
  }

  DebitCard get debitCard => _debitCard;
  set debitCard(DebitCard debitCard) => _debitCard = debitCard;
  Address get address => _address;
  set address(Address address) => _address = address;
  String get reservationId => _reservationId;
  set reservationId(String reservationId) => _reservationId = reservationId;
  String get amount => _amount;
  set amount(String amount) => _amount = amount;
  String get sourceCurrency => _sourceCurrency;
  set sourceCurrency(String sourceCurrency) =>
      _sourceCurrency = sourceCurrency;
  String get destCurrency => _destCurrency;
  set destCurrency(String destCurrency) => _destCurrency = destCurrency;
  String get dest => _dest;
  set dest(String dest) => _dest = dest;
  String get referrerAccountId => _referrerAccountId;
  set referrerAccountId(String referrerAccountId) =>
      _referrerAccountId = referrerAccountId;
  String get givenName => _givenName;
  set givenName(String givenName) => _givenName = givenName;
  String get familyName => _familyName;
  set familyName(String familyName) => _familyName = familyName;
  String get email => _email;
  set email(String email) => _email = email;
  String get phone => _phone;
  set phone(String phone) => _phone = phone;
  String get referenceId => _referenceId;
  set referenceId(String referenceId) => _referenceId = referenceId;
  String get ipAddress => _ipAddress;
  set ipAddress(String ipAddress) => _ipAddress = ipAddress;


  String get paymentMethod => _paymentMethod;

  set paymentMethod(String value) {
    _paymentMethod = value;
  }

  WalletOrderReserve.fromJson(Map<String, dynamic> json) {
    _debitCard = json['debitCard'] != null
        ? new DebitCard.fromJson(json['debitCard'])
        : null;
    _address =
    json['address'] != null ? new Address.fromJson(json['address']) : null;
    _reservationId = json['reservationId'];
    _amount = json['amount'];
    _sourceCurrency = json['sourceCurrency'];
    _destCurrency = json['destCurrency'];
    _dest = json['dest'];
    _referrerAccountId = json['referrerAccountId'];
    _givenName = json['givenName'];
    _familyName = json['familyName'];
    _firstName = json['firstName'];
    _lastName = json['lastName'];
    _redirectUrl = json['redirectUrl'];
    _failureUrl = json['failureRedirectUrl'];
    _email = json['email'];
    _phone = json['phone'];
    _referenceId = json['referenceId'];
    _ipAddress = json['ipAddress'];
    _paymentMethod = json['paymentMethod'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._debitCard != null) {
      data['debitCard'] = this._debitCard.toJson();
    }
    if (this._address != null) {
      data['address'] = this._address.toJson();
    }
    data['reservationId'] = this._reservationId;
    data['amount'] = this._amount;
    data['sourceCurrency'] = this._sourceCurrency;
    data['destCurrency'] = this._destCurrency;
    data['dest'] = this._dest;
    data['referrerAccountId'] = this._referrerAccountId;
    data['givenName'] = this._givenName;
    data['familyName'] = this._familyName;
    data['firstName'] = this._firstName;
    data['lastName'] = this._lastName;
    data['redirectUrl'] = this._redirectUrl;
    data['failureRedirectUrl'] = this._failureUrl;
    data['email'] = this._email;
    data['phone'] = this._phone;
    data['referenceId'] = this._referenceId;
    data['ipAddress'] = this._ipAddress;
    data['paymentMethod'] = this._paymentMethod;
    return data;
  }

  String get lastName => _lastName;

  set lastName(String value) {
    _lastName = value;
  }

  String get failureUrl => _failureUrl;

  set failureUrl(String value) {
    _failureUrl = value;
  }
}

class DebitCard {
  String _number;
  String _year;
  String _month;
  String _cvv;

  DebitCard({String number, String year, String month, String cvv}) {
    if (number != null) {
      this._number = number;
    }
    if (year != null) {
      this._year = year;
    }
    if (month != null) {
      this._month = month;
    }
    if (cvv != null) {
      this._cvv = cvv;
    }
  }

  String get number => _number;
  set number(String number) => _number = number;
  String get year => _year;
  set year(String year) => _year = year;
  String get month => _month;
  set month(String month) => _month = month;
  String get cvv => _cvv;
  set cvv(String cvv) => _cvv = cvv;

  DebitCard.fromJson(Map<String, dynamic> json) {
    _number = json['number'];
    _year = json['year'];
    _month = json['month'];
    _cvv = json['cvv'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number'] = this._number;
    data['year'] = this._year;
    data['month'] = this._month;
    data['cvv'] = this._cvv;
    return data;
  }
}

class Address {
  String _street1;
  String _city;
  String _state;
  String _postalCode;
  String _country;

  Address(
      {String street1,
        String city,
        String state,
        String postalCode,
        String country}) {
    if (street1 != null) {
      this._street1 = street1;
    }
    if (city != null) {
      this._city = city;
    }
    if (state != null) {
      this._state = state;
    }
    if (postalCode != null) {
      this._postalCode = postalCode;
    }
    if (country != null) {
      this._country = country;
    }
  }

  String get street1 => _street1;
  set street1(String street1) => _street1 = street1;
  String get city => _city;
  set city(String city) => _city = city;
  String get state => _state;
  set state(String state) => _state = state;
  String get postalCode => _postalCode;
  set postalCode(String postalCode) => _postalCode = postalCode;
  String get country => _country;
  set country(String country) => _country = country;

  Address.fromJson(Map<String, dynamic> json) {
    _street1 = json['street1'];
    _city = json['city'];
    _state = json['state'];
    _postalCode = json['postalCode'];
    _country = json['country'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['street1'] = this._street1;
    data['city'] = this._city;
    data['state'] = this._state;
    data['postalCode'] = this._postalCode;
    data['country'] = this._country;
    return data;
  }
}
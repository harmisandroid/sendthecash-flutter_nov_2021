class SubmitAuthRequest {
  String _type;
  String _walletOrderId;
  String _reservation;
  String _card2fa;
  String _smsCode;

  SubmitAuthRequest(
      {String type,
        String walletOrderId,
        String reservation,
        String card2fa}) {
    if (type != null) {
      this._type = type;
    }
    if (walletOrderId != null) {
      this._walletOrderId = walletOrderId;
    }
    if (reservation != null) {
      this._reservation = reservation;
    }
    if (card2fa != null) {
      this._card2fa = card2fa;
    }
  }


  String get smsCode => _smsCode;

  set smsCode(String value) {
    _smsCode = value;
  }

  String get type => _type;
  set type(String type) => _type = type;
  String get walletOrderId => _walletOrderId;
  set walletOrderId(String walletOrderId) => _walletOrderId = walletOrderId;
  String get reservation => _reservation;
  set reservation(String reservation) => _reservation = reservation;
  String get card2fa => _card2fa;
  set card2fa(String card2fa) => _card2fa = card2fa;

  SubmitAuthRequest.fromJson(Map<String, dynamic> json) {
    _type = json['type'];
    _walletOrderId = json['walletOrderId'];
    _reservation = json['reservation'];
    _card2fa = json['card2fa'];
    _smsCode = json['sms'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this._type;
    data['walletOrderId'] = this._walletOrderId;
    data['reservation'] = this._reservation;
    data['card2fa'] = this._card2fa;
    data['sms'] = this._smsCode;
    return data;
  }
}

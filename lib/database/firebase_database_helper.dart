import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sendcash_flutter/database/db_key.dart';

class FirebaseDatabaseHelper {
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  final String tblUser = "users";
  final String tblDevices = "devices";
  final String tblSupportRequests = "support_requests";
  final String tblLinkedAccounts = "linked_accounts";
  final String tblUserCards = "user_cards";
  final String tblOtp = "tbl_otps";
  final String tblEmailOtp = "tbl_email_otps";
  final String tblUserBanks = "tbl_user_bank";

  // create user
  Future<void> createUser(
      {Map<String, dynamic> userdata,
      Function(DocumentReference) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblUser)
        .add(userdata)
        .onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    }).then((value) {
      onSuccess(value);
    });
  }

  // update user
  Future<void> updateUserProfile(
      {Map<String, dynamic> userdata,
      Function(void) onSuccess,
      Function(dynamic, StackTrace) onError,
      String userId}) async {
    await firestore
        .collection(tblUser)
        .doc(userId)
        .update(userdata)
        .then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
    });
  }

  // check user

  Future<void> chedkUserExists(
      {Map<String, dynamic> userdata,
      Function(QuerySnapshot<Map<String, dynamic>>) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    Query query = null;
    if (userdata[DbKey.EMAIL] != null &&
        userdata[DbKey.EMAIL].toString().isNotEmpty) {
      query = await firestore
          .collection(tblUser)
          .where(DbKey.EMAIL, isEqualTo: userdata[DbKey.EMAIL]);
    } else {
      query = await firestore.collection(tblUser).where(DbKey.USER_SOCIAL_ID,
          isEqualTo: userdata[DbKey.USER_SOCIAL_ID]);
    }
    query.get().then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    });
  }

  // check email alreadt exist
  Future<void> checkEmailExists(
      {String emailAddress,
      Function(QuerySnapshot<Map<String, dynamic>>) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblUser)
        .where(DbKey.EMAIL, isEqualTo: emailAddress.trim())
        .get()
        .then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    });
  }

  // check mobile number exists

  Future<void> checkMobileNumberExists(
      {String mobileNumber,
      Function(QuerySnapshot<Map<String, dynamic>>) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblUser)
        .where(DbKey.MOBILE_NO, isEqualTo: mobileNumber)
        .get()
        .then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    });
  }

  // register device

  Future<void> loginUser(
      {String email,
      String password,
      Function(QuerySnapshot<Map<String, dynamic>>) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblUser)
        .where(DbKey.EMAIL_PASSWORD, isEqualTo: email + password)
        .get()
        .then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    });
  }

  // register device

  Future<void> getUserDetails(
      {String userId,
      Map<String, dynamic> userdata,
      Function(DocumentSnapshot) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore.collection(tblUser).doc(userId).get().then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    });
  }

  Future<void> addDevice(
      {Map<String, dynamic> data,
      Function(DocumentReference) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblDevices)
        .add(data)
        .onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    }).then((value) {
      //Fluttertoast.showToast(msg: "Devi");
      onSuccess(value);
    });
  }

  // register device

  Future<void> checkDeviceExists(
      {Map<String, dynamic> userdata,
      Function(QuerySnapshot<Map<String, dynamic>>) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblDevices)
        .where(DbKey.DEVICE_ID, isEqualTo: userdata[DbKey.DEVICE_ID])
        .get()
        .then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    });
  }

  // update device
  Future<void> updateDevice(
      {Map<String, dynamic> data,
      Function(void) onSuccess,
      Function(dynamic, StackTrace) onError,
      String id}) async {
    await firestore
        .collection(tblDevices)
        .doc(id.toString())
        .update(data)
        .then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
    });
  }

  Future<void> updateUserIdToDevice(
      {String userId,
      Function(void) onSuccess,
      Function(dynamic, StackTrace) onError,
      String deviceId}) async {
    await firestore
        .collection(tblDevices)
        .where(DbKey.DEVICE_ID, isEqualTo: deviceId)
        .get()
        .then((value) async {
      if (value.size > 0) {
        await firestore
            .collection(tblDevices)
            .doc(value.docs[0].id)
            .update({DbKey.USER_ID: userId}).then((value) {
          onSuccess(value);
        }).onError((error, stackTrace) {
          onError(error, stackTrace);
        });
      } else {}
    }).onError((error, stackTrace) {});
  }

  Future<void> updateUserAppSettings(
      {Map<String, dynamic> data,
      Function(void) onSuccess,
      Function(dynamic, StackTrace) onError,
      String userId}) async {
    await firestore.collection(tblUser).doc(userId).update(data).then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
    });
  }

  Future<void> addSupportRequest(
      {Map<String, dynamic> data,
      Function(DocumentReference) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblSupportRequests)
        .add(data)
        .onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    }).then((value) {
      onSuccess(value);
    });
  }

  Future<void> addLinkedAccounts(
      {Map<String, dynamic> data,
      Function(DocumentReference) onSuccess,
      Function() alreadyExists,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblLinkedAccounts)
        .where(DbKey.USER_SOCIAL_ID, isEqualTo: data[DbKey.USER_SOCIAL_ID])
        .get()
        .then((value) async {
      if (value.size > 0) {
        alreadyExists();
      } else {
        await firestore
            .collection(tblLinkedAccounts)
            .add(data)
            .onError((error, stackTrace) {
          onError(error, stackTrace);
          Fluttertoast.showToast(msg: error.toString());
        }).then((value) {
          onSuccess(value);
        });
      }
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
    });
  }

  Future<void> getLinkedAccounts({
    String userId,
    Function(QuerySnapshot<Map<String, dynamic>> querySnapshots) onSuccess,
    Function(Object, StackTrace) onError,
  }) async {
    await firestore
        .collection(tblLinkedAccounts)
        .where(DbKey.USER_ID, isEqualTo: userId)
        .get()
        .then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
    });
  }

  Future<void> updateSendwyreAccountId(
      {String accountId,
      Function(void) onSuccess,
      Function(dynamic, StackTrace) onError,
      String userId}) async {
    await firestore
        .collection(tblUser)
        .doc(userId)
        .update({DbKey.SENDWYRE_ACCOUNT_ID: accountId}).then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
    });
  }

  Future<void> addUserCards(
      {Map<String, dynamic> data,
      Function(DocumentReference) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblUserCards)
        .add(data)
        .onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    }).then((value) {
      onSuccess(value);
    });
  }

  Future<void> getUserCards({
    String userId,
    Function(QuerySnapshot<Map<String, dynamic>> querySnapshots) onSuccess,
    Function(Object, StackTrace) onError,
  }) async {
    await firestore
        .collection(tblUserCards)
        .where(DbKey.USER_ID, isEqualTo: userId)
        .get()
        .then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
    });
  }

  Future<void> insertFirebaseOtp(
      {Map<String, dynamic> userdata,
      Function() onSuccess,
      Function(dynamic, StackTrace) onError,
      Function(DocumentReference) onDocumentAdded}) async {
    await firestore
        .collection(tblOtp)
        .where(DbKey.MOBILE_NO, isEqualTo: userdata[DbKey.MOBILE_NO])
        .get()
        .then((value) {
      if (value.size > 0) {
        // update otp
        updateOtp(
            data: userdata,
            onSuccess: onSuccess,
            onError: onError,
            id: value.docs[0].id);
      } else {
        // insert otp
        addOtp(data: userdata, onSuccess: onDocumentAdded, onError: onError);
      }
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    });
  }

  Future<void> updateOtp(
      {Map<String, dynamic> data,
      Function() onSuccess,
      Function(dynamic, StackTrace) onError,
      String id}) async {
    await firestore
        .collection(tblOtp)
        .doc(id.toString())
        .update(data)
        .then((value) {
      onSuccess();
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
    });
  }

  Future<void> addOtp(
      {Map<String, dynamic> data,
      Function(DocumentReference) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore.collection(tblOtp).add(data).onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    }).then((value) {
      onSuccess(value);
    });
  }

  Future<void> verifyOtp(
      {Map<String, dynamic> userdata,
      Function(QuerySnapshot<Map<String, dynamic>> querySnapshot) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblOtp)
        .where(DbKey.MOBILE_NO, isEqualTo: userdata[DbKey.MOBILE_NO])
        .where(DbKey.OTP, isEqualTo: userdata[DbKey.OTP])
        .get()
        .then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    });
  }

  Future<void> insertEmailFirebaseOtp(
      {Map<String, dynamic> userdata,
      Function() onSuccess,
      Function(dynamic, StackTrace) onError,
      Function(DocumentReference) onDocumentAdded}) async {
    await firestore
        .collection(tblEmailOtp)
        .where(DbKey.EMAIL, isEqualTo: userdata[DbKey.EMAIL])
        .get()
        .then((value) {
      if (value.size > 0) {
        // update otp
        updateEmailOtp(
            data: userdata,
            onSuccess: onSuccess,
            onError: onError,
            id: value.docs[0].id);
      } else {
        // insert otp
        addEmailOtp(
            data: userdata, onSuccess: onDocumentAdded, onError: onError);
      }
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    });
  }

  Future<void> updateEmailOtp(
      {Map<String, dynamic> data,
      Function() onSuccess,
      Function(dynamic, StackTrace) onError,
      String id}) async {
    await firestore
        .collection(tblEmailOtp)
        .doc(id.toString())
        .update(data)
        .then((value) {
      onSuccess();
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
    });
  }

  Future<void> addEmailOtp(
      {Map<String, dynamic> data,
      Function(DocumentReference) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblEmailOtp)
        .add(data)
        .onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    }).then((value) {
      onSuccess(value);
    });
  }

  Future<void> verifyEmailOtp(
      {Map<String, dynamic> userdata,
      Function(QuerySnapshot<Map<String, dynamic>> querySnapshot) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblEmailOtp)
        .where(DbKey.EMAIL, isEqualTo: userdata[DbKey.EMAIL])
        .where(DbKey.OTP, isEqualTo: userdata[DbKey.OTP])
        .get()
        .then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    });
  }

  Future<void> setPhoneVerifiedTrue(
      {bool isPhoneVerified,
      Function(void) onSuccess,
      Function(dynamic, StackTrace) onError,
      String userId}) async {
    await firestore
        .collection(tblUser)
        .doc(userId)
        .update({DbKey.IS_PHONE_VERIFIED: isPhoneVerified}).then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
    });
  }

  Future<void> setEmailVerifiedTrue(
      {bool isEmailVerified,
      Function(void) onSuccess,
      Function(dynamic, StackTrace) onError,
      String userId}) async {
    await firestore
        .collection(tblUser)
        .doc(userId)
        .update({DbKey.IS_EMAIL_VERIFIED: isEmailVerified}).then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
    });
  }

  Future<void> addBankAccount(
      {Map<String, dynamic> data,
      Function(DocumentReference) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblUserBanks)
        .add(data)
        .onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    }).then((value) {
      //Fluttertoast.showToast(msg: "Devi");
      onSuccess(value);
    });
  }

  Future<void> checkBankAccountExists(
      {String accountNumber,
      Function(QuerySnapshot<Map<String, dynamic>>) onSuccess,
      Function(dynamic, StackTrace) onError}) async {
    await firestore
        .collection(tblUserBanks)
        .where(DbKey.ACCOUNT_NUMBER, isEqualTo: accountNumber)
        .get()
        .then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
      Fluttertoast.showToast(msg: error.toString());
    });
  }

  Future<void> getBankAccounts({
    String userId,
    Function(QuerySnapshot<Map<String, dynamic>> querySnapshots) onSuccess,
    Function(Object, StackTrace) onError,
  }) async {
    await firestore
        .collection(tblUserBanks)
        .where(DbKey.USER_ID, isEqualTo: userId)
        .get()
        .then((value) {
      onSuccess(value);
    }).onError((error, stackTrace) {
      onError(error, stackTrace);
    });
  }
  
  void deleteBankAccount() async{
   //await firestore.collection(tblUserBanks).where(field)
  }

  


}

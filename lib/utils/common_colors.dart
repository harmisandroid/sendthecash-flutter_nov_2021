import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendcash_flutter/app_ui/app/app_model.dart';

class CommonColors {
  //  light theme colors

  static const primaryColor = Color(0xFFD41171);
  static const gradientcolortop = Color(0xFF614AD3);
  static const bottomIconBorderColor = Color(0xFFE42C64);
  static const shadowcolor = Color(0xFFCEA0AF);
  static const gradientColorTop = Color(0xFFD41171);
  static const gradientColorBottom = Color(0xFFB71069);
  static const shadowColor = Color(0xFFCEA0AF);
  static const buttonGradientColorTop = Color(0xFF52FFFF);
  static const buttonGradientColorBottom = Color(0xFF6B8CFF);
  static const backgroundShadowColor = Color(0xFFEEF0F3);
  static const bottombarBoxShadow = Color(0xFF00000029);
  static const fontBlackColor = Color(0xFF2E3040);
  static const fontLightBlackColor = Color(0xFF2E3040);
  static const walletItemFontColor = Color(0xFFEB5A5A);
  static const blurShadow = Color(0xFF00000029);
  static const black2222 = Color(0xFF222222);
  static const fieldBorder = Color(0x2E304026);
  static const scaffoldWhiteBack = Color(0xFFFBFBFB);
  static const bottomSheetShadow = Color(0xFF614AD3);
  static const gradientButtonShadow = Color(0xFF00000033);
  static const green = Color(0xFF3DAB25);
  static const red = Color(0xFFFF0000);
  static const bottomNavColor = Color(0xFFF1F3F6);
  static const notificationDate = Color(0xFF9F9F9F);
  static const notificationDevider = Color(0xFF2222221A);
  static const chartGradientOne = Color(0xFF837DFF);
  static const chartGradientTwo = Color(0xFF09F9BF);
  static const backgroundColor = Color(0xFFFFFFFF);
  static const notificationVerticalLine = Color(0x22222226);
  static const hintcolor = Color(0xFF788194);
  static const boxColor = Color(0xFFE4E4E6);
  static const F5F4F7 = Color(0xFFF5F4F7);
  static const chartLabelColor = Color(0xFFBBBBBB);

  static Color getWhiteTextColor(BuildContext context) {
    return Provider.of<AppModel>(context).darkTheme
        ? Colors.white
        : CommonColors.fontBlackColor;
  }

  static Color getDropdownColor(BuildContext context) {
    return Provider.of<AppModel>(context).darkTheme
        ? CommonColors.fontBlackColor
        : Colors.white;
  }

  static getLightWhiteTextColor(BuildContext context) {
    return Provider.of<AppModel>(context).darkTheme
        ? Colors.white60
        : CommonColors.fontLightBlackColor;
  }

  static getFieldBorderColor(BuildContext context) {
    return Provider.of<AppModel>(context).darkTheme
        ? Colors.white
        : CommonColors.fieldBorder;
  }

  static getGrayThemeColor(BuildContext context) {
    return Provider.of<AppModel>(context).darkTheme
        ? Colors.white.withOpacity(0.5)
        : CommonColors.notificationDate;
  }

  static getListDeviderColor(BuildContext context) {
    return Provider.of<AppModel>(context).darkTheme
        ? Colors.white.withOpacity(0.3)
        : CommonColors.notificationDevider;
  }

  static getVerticalLine(BuildContext context) {
    return Provider.of<AppModel>(context).darkTheme
        ? Colors.white.withOpacity(0.7)
        : CommonColors.notificationVerticalLine;
  }

  static getWithdrawTextFieldColor(BuildContext context) {
    return Provider.of<AppModel>(context).darkTheme
        ? Colors.blueGrey
        : F5F4F7;
  }

  static getBoxColor(BuildContext context) {
    return Provider.of<AppModel>(context).darkTheme
        ? Colors.grey.shade200
        : boxColor;
  }

  static getHintTextStyle(BuildContext context) {
    return Provider.of<AppModel>(context).darkTheme
        ? Colors.white70
        : Colors.grey;
  }

  static getLightShadowColor(BuildContext context) {
    return Provider.of<AppModel>(context).darkTheme
        ? Colors.grey
        : boxColor;
  }

  static getOtpBoxColor(BuildContext context) {
    return Provider.of<AppModel>(context).darkTheme
        ? Colors.white
        : boxColor;
  }

  static getBankFieldContainer(BuildContext context) {
    return Provider.of<AppModel>(context).darkTheme
        ? Colors.black12
        : Colors.white;
  }
}

class DarkThemeColors {
  static const scaffoldWhiteBack = Color(0xFF1F1F1F);
  static const backgroundColor =  Color(0xFF000000);
  static const bottomNavColor = Color(0xFF1F1F1F);
  static const shadowColor = /*Color(0xFF646464)*/Colors.black;
}

class ApiUrl {

  /*-------------server apis---------*/

  static final String BASE_URL = "https://harmistechnology.com/yakfeek/api/";
  static final String getPreDayExchangeRates = BASE_URL+"exchange_rate/";
  static final String getCryptoList = BASE_URL+"getcrypto_currenct/";
  static final String getAllCurrencies = BASE_URL+"getcurrencybitcoin/";
  static final String downloadPDF = BASE_URL+"crypto_transactionpdfgenerate";

  /*------ version 2----------*/

  static String baseUrlV2 = "https://api.testwyre.com/v2/";
  static String createWallet = baseUrlV2+"wallets";
  static String getWallet = baseUrlV2+"wallet/";
  static String getAccountTranfers = baseUrlV2+"wallet/";

  /* ------------ version3-------- */

  static String baseUrlV3 = "https://api.testwyre.com/v3/";
  static String createAccount = baseUrlV3+"accounts";
  static String getAccountDetails = baseUrlV3+"accounts/";
  static String updateAccount = baseUrlV3+"accounts/";
  static String uploadDoc = baseUrlV3+"accounts/";
  static String getExchangeRates = baseUrlV3+"rates";
  static String createWalletReserveOrder = baseUrlV3+"orders/reserve";
  static String createOrder = baseUrlV3+"debitcard/process/partner";
  static String createTranfer = baseUrlV3+"transfers";
  static String getOrderAuthorization = baseUrlV3+"debitcard/authorization/";
  static String submitAuthorization = baseUrlV3+"debitcard/authorize/partner";

  /*-------otp for email and phone --------------*/
  static String sendPhoneOtp = "https://rest-api.telesign.com/v1/messaging";
  static String sendEmailOtp = "https://api.mailersend.com/v1/email";
}

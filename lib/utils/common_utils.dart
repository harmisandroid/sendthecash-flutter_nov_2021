import 'dart:math';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:sendcash_flutter/common_widgets/custom_dialog/MyCustomDialog.dart';
import 'package:sendcash_flutter/utils/constant.dart';

import 'common_colors.dart';

class CommonUtils {
  static bool isShowing = false;
  static String langugeCode = AppConstants.LANGUAGE_ENGLISH;
  static bool isArabic = false;

  static void printr(dynamic value) {
    printf(value.toString());
  }

  static Widget getListItemProgressBar() {
    return new Center(
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.all(20.0),
        child: new Center(
          child: new CircularProgressIndicator(
            strokeWidth: 2,
            backgroundColor: CommonColors.primaryColor,
            valueColor:
                new AlwaysStoppedAnimation<Color>(CommonColors.primaryColor),
          ),
        ),
      ),
    );
  }

  static String validateEmpty(value, String enterYourNickName) {
    if (value.isEmpty) {
      return enterYourNickName;
    }
    return null;
  }

  static bool isEmpty(String string) {
    return string == null || string.length == 0;
  }

  static bool isvalidEmail(String email) {
    Pattern pattern = AppConstants.EMAIL_PATTERN;
    RegExp regex = new RegExp(pattern);
    // printf("MATCHHH ${regex.hasMatch(email)}");
    return (!regex.hasMatch(email)) ? false : true;
  }

  static void showSnackBar(String message, globalKey) {
    final snackBar = SnackBar(
      content: Text(message),
      duration: Duration(seconds: 1),
      backgroundColor: Colors.green,
    );
    globalKey.currentState.showSnackBar(snackBar);
  }

  static void showRedSnackBar(String message, globalKey, {int duration}) {
    final snackBar = SnackBar(
      content: Container(
        height: 30.0,
        alignment: Alignment.center,
        child: Text(message),
      ),
      duration: Duration(seconds: duration ?? 1),
      backgroundColor: Colors.red,
    );
    globalKey.currentState.showSnackBar(snackBar);
  }

  static void showSnackBarForValidation(BuildContext context, String message) {
    final snackBar = SnackBar(
      content: Text(message),
    );
    // Find the ScaffoldMessenger in the widget tree
    // and use it to show a SnackBar.
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  static printValue(String value) {
    printf(value);
  }

  RegExp getRegExp(Pattern pattern) => new RegExp(pattern);

  static String validateEmail(String value, String error, String invalid) {
    if ((value.trim().length <= 0)) {
      return error;
    } else {
      RegExp regex = new RegExp(AppConstants.EMAIL_PATTERN);
      if (!regex.hasMatch(value))
        return invalid;
      else
        return null;
    }
  }

  static String validatePassword(String val, String empty, String invalid) {
    if (val.isEmpty) {
      return empty;
    }
    if (val.characters.length < 8 || val.characters.length > 16) {
      return invalid;
    }
    RegExp regexAll = new RegExp(AppConstants.PASSWORD_PATTERN_ALL);
    RegExp regexAB = new RegExp(AppConstants.PASSWORD_PATTERN_AB);
    RegExp regexBC = new RegExp(AppConstants.PASSWORD_PATTERN_BC);
    RegExp regexAC = new RegExp(AppConstants.PASSWORD_PATTERN_AC);

    if (!(regexAll.hasMatch(val) ||
        regexAB.hasMatch(val) ||
        regexBC.hasMatch(val) ||
        regexAC.hasMatch(val))) return invalid;
    return null;
  }

  static String validateConfirmPassword(String val, String empty,
      String invalid, bool match, String matchString) {
    // printf(" MATCH $match");
    if (!match) {
      return matchString;
    }
    return null;
  }

  static void showProgressDialog(BuildContext context) {
    MyCustomDialog changePasswordDialog = MyCustomDialog(
      backgroundColor: Colors.transparent,
      child: SizedBox.expand(
        child: Container(
          color: Colors.transparent,
          padding: EdgeInsets.all(10.0),
          alignment: Alignment.center,
          width: double.infinity,
          height: double.infinity,
          child: new Center(
            child: SpinKitSpinningLines(
              color: Colors.white,
              size: 40.0,
            ),
          ),
        ),
      ),
    );

    if (!isShowing) {
      showMyDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            return changePasswordDialog;
          });
      isShowing = true;
    }
  }

  static void hideProgressDialog(BuildContext context) {
    if (isShowing) {
      Navigator.of(context, rootNavigator: true).pop('dialog');
      isShowing = false;
    }
  }

  static void showToastMessage(String message, {Color color}) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: color ?? Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static void showToastBottomMessage(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static void showGreenToastMessage(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static void showRedToastMessage(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static Future<bool> isNetworkConnected() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  static String getCurrencyFormat(double value) {
    final oCcy = new NumberFormat("#,##0.00", "en_US");
    return oCcy.format(value);
  }
}

String getRandomCodeFourDigit() {
  var rng = new Random();
  String code = rng.nextInt(9999).toString();
  if (code.length == 3) {
    code = code + "0";
  } else if (code.length == 2) {
    code = code + "00";
  } else if (code.length == 1) {
    code = code + "259";
  }
  return code;
}

pushScreen(BuildContext context, Widget screen) {
  Navigator.push(context, CupertinoPageRoute(builder: (context) {
    return screen;
  }));
}

void printf(dynamic data) {
  if (kReleaseMode) {
    // dont print in release
  } else {
    print(data.toString());
  }
}

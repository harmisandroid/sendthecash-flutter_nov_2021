class AppConstants {
  static final String DEVICE_ACCESS_ANDROID = "1";
  static final String DEVICE_ACCESS_IOS = "2";
  static final String DEVICE_ACCESS_WEB = "3";
  static final String FONT_FAMILY = "Sarabun";
  static final String LOGIN_DETAILS = "LOGIN_DETAILS";
  static final String METHOD_POST = 'POST';
  static final String FONT_FAMILY_SARABUN = 'Sarabun';
  static final String CLICK_TYPE_FACEBOOK = 'CLICK_TYPE_FACEBOOK';
  static final String CLICK_TYPE_TWITTER = 'CLICK_TYPE_TWITTER';
  static final String CLICK_TYPE_GOOGLE = 'CLICK_TYPE_GOOGLE';
  static final String LOGIN_TYPE_FACEBOOK = 'FACEBOOK';
  static final String LOGIN_TYPE_GOOGLE = 'GOOGLE';
  static final String LOGIN_TYPE_TWITTER = 'TWITTER';
  static final String LOGIN_TYPE_NORMAL = 'Normal';
  static final String SCREEN_REGISTER = 'REGISTER';
  static final String SCREEN_WELCOME = 'WELCOME';
  static final String SCREEN_LOGIN = 'LOGIN';
  static final String SCREEN_PROFILE = 'PROFILE';
  static final String SCREEN_FORGOT_PASSWORD = 'FORGOT_PASSWORD';

  /* twitter api keys*/

  static final String TWITTER_API_KEY = 'cv225onvLjOxJDxLM9haN9nSI';
  static final String TWITTER_API_SECRET = 'bALhrOkHaca6vqT7Sq2Djm3VsWOpVKnFJxgZ8mv8K02kcCbLLa';
  static final String TWITTER_CALLBACK_URL = 'twittersdk://';


  /* SENDWYRE CRDENTIALS*/

  static final String SENDWYRE_API_KEY = "AK-4W9924YH-6YC6BF7M-93VATWA7-PNT3WTBL";
  static final String SENDWYRE_API_SECRET = "SK-32N27QYH-6LJUUP6E-VNBRRJBA-X74QH7T6";
  static final String SENDWYRE_ACCOUNT_ID= "AC_URQ6TDUMMPG";

  /*TELESIGN CREDENTIAL*/

  static final String TELESIGN_CUSTOMER_ID = "CEB04BD3-72A2-4540-910A-C8E0C42D425F";
  static final String TELESIGN_KEY = "zoAZkrcqSzwd0FFpO9YSeHwtjcUx0hf+faxiCqwCjBpttU5SSNqOJSOrR03Oqi/G5SrvoxOxj+fmERNQjgXE+g==";


  /*--------------- Order status --------------*/


  static final String STATUS_COMPLETED = "COMPLETED";
  static final String STATUS_RUNNING_CHECKS = "RUNNING_CHECKS";
  static final String STATUS_PROCESSING= "PROCESSING";
  static final String STATUS_FAILED= "FAILED";


  /*--------Language Codes-------------------*/
  static const String LANGUAGE_ENGLISH = 'en';
  static const String LANGUAGE_ARABIC = 'ar';




  static var phoneLength = 10;
  static var selectedIndex = 0;

  static const Pattern EMAIL_PATTERN =
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$";
  /*r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'*/

  static const Pattern PASSWORD_PATTERN =
      r'^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$';

  static const Pattern PASSWORD_PATTERN_ALL =
      r'^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-zA-Z]){1}).*$';
  static const Pattern PASSWORD_PATTERN_AB =
      r'^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})((?=.*[a-zA-Z]){1}).*$';
  static const Pattern PASSWORD_PATTERN_BC =
      r'^.(?=.*\d)((?=.*[a-zA-Z]){1}).*$';
  static const Pattern PASSWORD_PATTERN_AC =
      r'^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d).*$';

  // static const Pattern PASSWORD_PATTERN =
  //     r'^(?!.*[\s])'; //^((?!.*[\s])(?=.*[A-Z])(?=.*\d).{8,15})
}

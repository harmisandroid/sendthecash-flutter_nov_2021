import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sendcash_flutter/utils/constant.dart';

class CommonStyle {
  /*static TextStyle getAppFontCairo(
      {Color color,
      double fontSize,
      FontWeight fontWeight,
      FontStyle fontStyle,
      TextDecoration decoration,
      double letterSpacing}) {
    return TextStyle(
      color: color,
      letterSpacing: letterSpacing == null ? 0.3 : letterSpacing,
      decoration: decoration != null ? decoration : TextDecoration.none,
      fontSize: fontSize,
      fontWeight: fontWeight,
      height: 1.5,
      fontFamily: AppConstants.FONT_FAMILY_SARABUN,
      fontStyle: fontStyle == null ? FontStyle.normal : fontStyle,
    );
  }*/

  //default app textstyle Sarabun
  static TextStyle getAppFont(
      {Color color,
        double fontSize,
        FontWeight fontWeight,
        FontStyle fontStyle,
        TextDecoration decoration,
        double height,
        double letterSpacing}) {
    return  GoogleFonts.sarabun(
      textStyle: TextStyle(
          color: color,
          letterSpacing: letterSpacing == null ? 0.3 : letterSpacing,
          decoration: decoration != null ? decoration : TextDecoration.none),
      fontSize: fontSize,
      fontWeight: fontWeight,
      fontStyle: fontStyle == null ? FontStyle.normal : fontStyle,
      height: height,
    );
  }
}

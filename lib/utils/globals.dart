import 'package:sendcash_flutter/utils/common_utils.dart';

class Globals {
  static String currencySymbol = "\$";
  static String currencyName = "USD";
  static Map<String, dynamic> rateMap = Map();

  static String getConvertedCurrency(double dollarValue) {
    if(dollarValue == null){
      return "0.0";
    }

    if (currencyName == "USD") {
      return CommonUtils.getCurrencyFormat(
          double.parse(dollarValue.toStringAsFixed(2)));
    } else {
      if (rateMap[currencyName + "USD"] == null) {
        return "0";
      } else {
        return CommonUtils.getCurrencyFormat(double.parse(double.parse(
                (rateMap[currencyName + "USD"] * dollarValue).toString())
            .toStringAsFixed(2)));
      }
    }
  }
}

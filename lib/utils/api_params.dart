

 class ApiParams{
   static final String content_type = "Content-Type";
   static final String application_json = "application/json";
   static final String x_api_version = "X-Api-Version";
   static final String authorization = "Authorization";
   static final String api_version_value = "2";
   static final String bearer = "Bearer";
   static final String accept = "Accept";
 }
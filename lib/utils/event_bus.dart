import 'package:event_bus/event_bus.dart';
import 'package:sendcash_flutter/app_ui/side_menu/side_menu_view_model.dart';

EventBus eventBus = EventBus();

class MenuItemClickEvent {
  MenuModel menuModel;

  MenuItemClickEvent(this.menuModel);
}

class TabClickEvent {
  String tabText;

  TabClickEvent(this.tabText);
}

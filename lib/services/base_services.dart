import 'package:sendcash_flutter/models/bank_details.dart';

abstract class BaseServices {
  Future<String> createWallet(
          {String params, Function() onFailed}) =>
      null;


  // Future<String>  createAccount({String params, Function() onFailed}) => null;
  Future<String>  lookUpWallet({String walletId, Function() onFailed}) => null;


  Future<String>  createAccount({String params, Function() onFailed}) => null;
  Future<String>  getExhangeRates({Function() onFailed}) => null;
  Future<List<BankDetails>>  getSupportedBank({Function() onFailed}) => null;
}

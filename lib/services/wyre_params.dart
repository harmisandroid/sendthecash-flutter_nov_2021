
 class WyreParams{
  // CREATE ACCOUNT PARAMS

  static final String COUNTRY = "country";
  static final String SUB_ACCOUNT = "subaccount";
  static final String PROFILE_FIELD = "profileFields";
  static final String REFFERAL_ACCOUNT_ID = "referrerAccountId";
  static final String DISABLE_EMAIL = "disableEmail";
  static final String TYPE = "type";
  // CREATE WALLET PARAMS
  static final String WALLET_TYPE = "type";
  static final String NAME = "name";
  static final String CALLBACK_URL = "callbackUrl";
  static final String NOTES = "notes ";

  // create tranfer params

  static final String AUTO_CONFIRM = "autoConfirm";
  static final String SOURCE = "source";
  static final String SOURCE_CURRENCY = "sourceCurrency";
  static final String SOURCE_AMOUNT = "sourceAmount";
  static final String DEST = "dest";
  static final String DEST_CURRENCY = "destCurrency";
  static final String CHARGEABLE_PM = "chargeablePM";


 }
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'package:sendcash_flutter/models/account_transfer_master.dart';
import 'package:sendcash_flutter/models/bank_details.dart';
import 'package:sendcash_flutter/models/crypto_list_master.dart';
import 'package:sendcash_flutter/services/base_services.dart';
import 'package:sendcash_flutter/utils/api_params.dart';
import 'package:sendcash_flutter/utils/constant.dart';

import '../utils/api_url.dart';
import '../utils/common_utils.dart';

class ApiServices extends BaseServices {
  /************ THIS IS FOR SENDBOX ************/

  @override
  Future<String> createWallet({String params, Function() onFailed}) async {
    try {
      int timestamp = DateTime.now().millisecondsSinceEpoch;
      String finalUrl =
          ApiUrl.createWallet + "?timestamp=" + timestamp.toString();
      http.Response response =
          await http.post(Uri.parse(finalUrl), body: params, headers: {
        ApiParams.content_type: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      printf("Response ${response.body}");
      return response.body;
    } on Exception catch (e) {
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> createAccount({String params, Function() onFailed}) async {
    try {
      int timestamp = DateTime.now().millisecondsSinceEpoch;
      String finalUrl =
          ApiUrl.createAccount + "?timestamp=" + timestamp.toString();
      printf("Url => " + finalUrl);
      http.Response response =
          await http.post(Uri.parse(finalUrl), body: params, headers: {
        ApiParams.content_type: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      return response.body;
    } on Exception catch (e) {
      // TODO
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> lookUpWallet({String walletId, Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.getWallet + "${walletId}";
      http.Response response = await http.get(Uri.parse(finalUrl), headers: {
        ApiParams.content_type: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      printf("Lookup Wallet Response ${response.body}");
      return response.body;
    } on Exception catch (e) {
      // TODO
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> getExhangeRates({Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.getExchangeRates;
      http.Response response = await http.get(Uri.parse(finalUrl));
      return response.body;
    } on Exception catch (e) {
      // TODO
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<List<BankDetails>> getSupportedBank({Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.baseUrlV2 + "bankinfo/BR";
      http.Response response = await http.get(Uri.parse(finalUrl));
      List<BankDetails> list = [];
      List<dynamic> dynamicList = json.decode(response.body);
      dynamicList.forEach((element) {
        list.add(BankDetails.fromJson(element));
      });
      return list;
    } on Exception catch (e) {
      // TODO
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> getSendwyreAccountDetails(
      {String accountId, Function() onFailed}) async {
    try {
      //https://api.sendwyre.com/v3/accounts/AC_XXXXXX1?masqueradeAs=AC_XXXXXX1
      String finalUrl =
          ApiUrl.getAccountDetails + "${accountId}?masqueradeAs=${accountId}";
      printf("Url => " + finalUrl);
      http.Response response = await http.get(Uri.parse(finalUrl), headers: {
        ApiParams.content_type: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      printf("Account details => " + response.body);
      return response.body;
    } on Exception catch (e) {
      // TODO
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> updateSendwyreAccount(
      {String accountId, String params, Function() onFailed}) async {
    try {
      String finalUrl =
          ApiUrl.updateAccount + "${accountId}?masqueradeAs=${accountId}";
      printf("Url => " + finalUrl);
      http.Response response =
          await http.post(Uri.parse(finalUrl), body: params, headers: {
        ApiParams.content_type: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      printf("Update details => " + response.body);
      return response.body;
    } on Exception catch (e) {
      // TODO
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> uploadAccountDocuments(
      {String accountId,
      String bakFront,
      File file,
      Function() onFailed}) async {
    try {
      int timestamp = DateTime.now().millisecondsSinceEpoch;
      String finalUrl = ApiUrl.uploadDoc +
          "${accountId}/individualGovernmentId?documentType=PASSPORT&documentSubType=${bakFront}&timestamp=${timestamp}&masqueradeAs=${accountId}";
      printf("Url => " + finalUrl);
      http.Response response = await http
          .post(Uri.parse(finalUrl), body: file.readAsBytesSync(), headers: {
        ApiParams.content_type: "image/png",
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      printf("Upload details => " + response.body);
      return response.body;
    } on Exception catch (e) {
      printf("exception => " + e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> getWalletDetails(
      {String walletId, Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.getWallet + "${walletId}";
      printf("Url => " + finalUrl);
      http.Response response = await http.get(Uri.parse(finalUrl), headers: {
        ApiParams.content_type: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      printf("Wallet Response ${response.body}");
      return response.body;
    } on Exception catch (e) {
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> createWalletOrderReserve(
      {String params, Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.createWalletReserveOrder;
      printf("Url => " + finalUrl);
      printf("Params => " + params);
      http.Response response =
          await http.post(Uri.parse(finalUrl), body: params, headers: {
        ApiParams.content_type: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      printf("Reserve Order Response ${response.body}");
      return response.body;
    } on Exception catch (e) {
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> createWalletOrder({String params, Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.createOrder;
      printf("Url => " + finalUrl);
      printf("Create Order Params " + params);
      http.Response response =
          await http.post(Uri.parse(finalUrl), body: params, headers: {
        ApiParams.content_type: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      printf("Create Order Response ${response.body}");
      return response.body;
    } on Exception catch (e) {
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> createTransfer({String params,String accountId,Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.createTranfer+"?masqueradeAs=${accountId}";
      printf("Url => " + finalUrl);
      http.Response response =
          await http.post(Uri.parse(finalUrl), body: params, headers: {
        ApiParams.content_type: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      printf("Create Transfer Response ${response.body}");
      return response.body;
    } on Exception catch (e) {
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<AccountTransferMaster> getAccountTransfers(
      {String accountId,
      String offset,
      String limit,
      Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.baseUrlV2 +
          "transfers/account:${accountId}?offset=${offset}&limit=${limit}&masqueradeAs=${accountId}";
      if (limit == null) {
        finalUrl = ApiUrl.baseUrlV2 +
            "transfers/account:${accountId}?masqueradeAs=${accountId}";
      }
      printf("Url => " + finalUrl);
      http.Response response = await http.get(Uri.parse(finalUrl), headers: {
        ApiParams.content_type: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      printf("Create Order Response ${response.body}");
      return AccountTransferMaster.fromJson(jsonDecode(response.body));
    } on Exception catch (e) {
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<AccountTransferMaster> getWalletTransfers(
      {String walletId,
      String offset,
      String limit,
      Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.baseUrlV2 +
          "transfers/wallet:${walletId}?offset=${offset}&limit=${limit}";
      if (offset == null) {
        finalUrl = ApiUrl.baseUrlV2 + "transfers/wallet:${walletId}";
      }
      printf("Url => " + finalUrl);
      printf("Params => " + walletId);
      http.Response response = await http.get(Uri.parse(finalUrl), headers: {
        ApiParams.content_type: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      printf("wallet transfer ${response.body}");
      return AccountTransferMaster.fromJson(jsonDecode(response.body));
    } on Exception catch (e) {
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> getOrderAuthorization(
      {String orderId, Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.getOrderAuthorization + "${orderId}";
      printf("Url => " + finalUrl);
      http.Response response = await http.get(Uri.parse(finalUrl), headers: {
        ApiParams.accept: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      printf("Authorization Order Response ${response.body}");
      return response.body;
    } on Exception catch (e) {
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> submitAuthorization(
      {String params, Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.submitAuthorization;
      printf("Url => " + finalUrl);
      printf("Params => " + params);
      http.Response response =
          await http.post(Uri.parse(finalUrl), body: params, headers: {
        ApiParams.content_type: ApiParams.application_json,
        ApiParams.accept: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} ${AppConstants.SENDWYRE_API_SECRET}",
        ApiParams.x_api_version: ApiParams.api_version_value,
      });
      printf("Authorization Order Response ${response.body}");
      return response.body;
    } on Exception catch (e) {
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> sendOtp(
      {Map<String, dynamic> mapParams, Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.sendPhoneOtp;
      printf("Url => " + finalUrl);
      printf("SendOtpParams => " + jsonEncode(mapParams));
      String username = AppConstants.TELESIGN_CUSTOMER_ID;
      String password = AppConstants.TELESIGN_KEY;
      String basicAuth =
          'Basic ' + base64Encode(utf8.encode('$username:$password'));
      printf(basicAuth);
      http.Response response =
          await http.post(Uri.parse(finalUrl), body: mapParams, headers: {
        ApiParams.content_type: "application/x-www-form-urlencoded",
        ApiParams.accept: "application/x-www-form-urlencoded",
        ApiParams.authorization: basicAuth
      });
      printf("SendOtpResponse ${response.body}");
      return response.body;
    } on Exception catch (e) {
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> sendOtpToEmail(
      {String email, String otp, Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.sendEmailOtp;
      var jsonParams = {
        "from": {"email": "noreply@sendthe.cash", "name": "SendTheCash"},
        "to": [
          {"email": email, "name": "SendCashUser"}
        ],
        "subject": "Otp for email verification",
        "text": "This is just a friendly hello from your friends at Email",
        "html": "Your code to verify email is <b>${otp}</b>",
        "variables": [
          {
            "email": email,
            "substitutions": [
              {"var": "company", "value": "SendTheCash"}
            ]
          }
        ]
      };
      printf("Params => " + jsonEncode(jsonParams));

      http.Response response = await http
          .post(Uri.parse(finalUrl), body: jsonEncode(jsonParams), headers: {
        ApiParams.content_type: ApiParams.application_json,
        ApiParams.authorization:
            "${ApiParams.bearer} eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZDZkNjFmOWM4NzJhMGZiMTlmOGRjZTJjM2VmNGY1ODFiNDg3MTg5MDhjYWZiNTEwN2NhYWRjNjRhOTgzYjIzZWMzYzIzN2ZmNGY2YWY5M2UiLCJpYXQiOjE2NDQzOTkzODQuOTczODUzLCJuYmYiOjE2NDQzOTkzODQuOTczODU4LCJleHAiOjQ4MDAwNzI5ODQuOTY5Mjg3LCJzdWIiOiIxNzMxMiIsInNjb3BlcyI6WyJlbWFpbF9mdWxsIiwiZG9tYWluc19mdWxsIiwiYWN0aXZpdHlfZnVsbCIsImFuYWx5dGljc19mdWxsIiwidG9rZW5zX2Z1bGwiLCJ3ZWJob29rc19mdWxsIiwidGVtcGxhdGVzX2Z1bGwiLCJzdXBwcmVzc2lvbnNfZnVsbCJdfQ.hetn-GjGAkm0Ni75wvVpMBCV0oYWg-fQjuhFriVD4a_qrj6JC4XjAs8fQQ3rojk0i86qoxbt53A_mGsEORoNCibjv4SnxnzMix3db-ahrUDj3vwa5n9kdAAfuFNdSm1bsdkf5Cj4gzbn-5ZBEieb_wdmpPJbKwDbJ-5aRjJETdeD4V198RXMZfZhq8Z_yOY3RuOVZ8TIe3C0_mRwPHseIGTRcJO7KgJYjih_Wb7m7juFHkjc8IUP0Asmkd6dLi4QXAG1-zvJdS-ExcDsdGJJ5EzcIx7ZqFhxFkyogkCn8CH8INrm9wrShGT35aD-sS7QJv_XHtmA5XkYxALbtbvj2tS9ADrQ8NK7Sh_3SgDBigb5B9uZXIT2jCShCDcpZRdNaJQbKsprg9G614mYvBOkiv8JANt86ZJSqQLRRfyRamLUgx13lPnMgBEx-6XfDyNRFkMbUCQoY2QUSrqr-7f4q9hk9ZTHH_HMhtZ2uIvxft_LvMlGhTY9z6mEMbJCSvITPQQAvMjiXTfANUe5G41EUZxyunb9cZJWjHkks8sq8UNJUUpnQqP1avq-18qHzmXpWxeuM10deEgfun5tMuYf-Rso_CXPqBS6w-rv6XJfc8UxJrRqUrUZufNqw79v3C62WLr1Vtpji7qdG8hhZKVmesdZyE9yk5z1K8N-49Z7SU8"
      });
      printf("CODE " + response.statusCode.toString());
      if (response.body != null) {
        printf("Email otp response " + response.body);
      }

      if (response.statusCode == 202) {
        return "Success";
      } else {
        return null;
      }
    } on Exception catch (e) {
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<String> getPreDayExhangeRates({Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.getPreDayExchangeRates;
      http.Response response = await http.get(Uri.parse(finalUrl));
      printf("Exchange Response => " + response.body);
      return response.body;
    } on Exception catch (e) {
      // TODO
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<CryptoListMaster> getCryptoList({Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.getCryptoList;
      http.Response response = await http.get(Uri.parse(finalUrl));
      printf("Exchange Response => " + response.body);
      return CryptoListMaster.fromJson(jsonDecode(response.body));
    } on Exception catch (e) {
      // TODO
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  @override
  Future<CryptoListMaster> getAllCurrencies({Function() onFailed}) async {
    try {
      String finalUrl = ApiUrl.getAllCurrencies;
      http.Response response = await http.get(Uri.parse(finalUrl));
      return CryptoListMaster.fromJson(jsonDecode(response.body));
    } on Exception catch (e) {
      // TODO
      printf(e.toString());
      if (onFailed != null) {
        onFailed();
      }
    }
    return null;
  }

  Future<Uint8List> downloaPdfFile(
      {Function onFailed, Map<String, dynamic> mapParams}) async {
    try {
      http.Response request = await http.post(
        Uri.parse(ApiUrl.downloadPDF),
        body: jsonEncode(mapParams),
        headers: {
          ApiParams.content_type: ApiParams.application_json,
          ApiParams.accept: ApiParams.application_json
        },
      );
      if (request.statusCode == 200) {
        return request.bodyBytes;
      } else {
        return null;
      }
    } on Exception catch (e) {
      // TODO
      onFailed();
    }
  }
}

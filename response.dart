// create account response

var createAccountResponse = {
  "id": "AC_VZ6Y9QC7UTA",
  "status": "OPEN",
  "type": "INDIVIDUAL",
  "country": "US",
  "createdAt": 1640346822000,
  "updatedAt": 1640346822000,
  "depositAddresses": {
    "BTC": "mwhGRGtfGhSnzHNRRaRR6i4dDBA7VyBgEY",
    "MATIC": "0x26984542554779d8f7c4b4687cf01e272584da2c",
    "AVAX": "X-fuji1jqv87axtxvcgps558dzdpq2wk3p9pugvfmks33",
    "ETH": "0x8e39466fd7a19c7261037b848b1d656e53b32afe",
    "XLM": "GD7WXI7AOAK2CIPZVBEFYLS2NQZI2J4WN4HFYQQ4A2OMFVWGWAL3IW7K:4H692C2F7A7"
  },
  "totalBalances": {},
  "availableBalances": {},
  "kycHash": null,
  "profileFields": [
    {
      "fieldType": "EMAIL",
      "value": "amit@gmail.com",
      "note": null,
      "status": "APPROVED",
      "updatedAt": 1640346823000,
      "fieldId": "individualEmail"
    },
    {
      "fieldType": "CELLPHONE",
      "value": null,
      "note": null,
      "status": "OPEN",
      "updatedAt": null,
      "fieldId": "individualCellphoneNumber"
    },
    {
      "fieldType": "STRING",
      "value": null,
      "note": null,
      "status": "OPEN",
      "updatedAt": null,
      "fieldId": "individualLegalName"
    },
    {
      "fieldType": "STRING",
      "value": null,
      "note": null,
      "status": "OPEN",
      "updatedAt": null,
      "fieldId": "individualSsn"
    },
    {
      "fieldType": "DATE",
      "value": null,
      "note": null,
      "status": "OPEN",
      "updatedAt": null,
      "fieldId": "individualDateOfBirth"
    },
    {
      "fieldType": "ADDRESS",
      "value": null,
      "note": null,
      "status": "OPEN",
      "updatedAt": null,
      "fieldId": "individualResidenceAddress"
    },
    {
      "fieldType": "DOCUMENT",
      "value": [],
      "note": null,
      "status": "OPEN",
      "updatedAt": 1640346823518,
      "fieldId": "individualGovernmentId"
    },
    {
      "fieldType": "PAYMENT_METHOD",
      "value": null,
      "note": "Payment method not yet submitted",
      "status": "OPEN",
      "updatedAt": null,
      "fieldId": "individualSourceOfFunds"
    },
    {
      "fieldType": "DOCUMENT",
      "value": [],
      "note": null,
      "status": "PENDING",
      "updatedAt": null,
      "fieldId": "individualProofOfAddress"
    }
  ]
};



var createWalletResponse = "";

// create order response

var createorder = {
  "id": "WO_DJ87ETXQXB",
  "createdAt": 1643882549982,
  "owner": "account:AC_J8RQAYT2L72",
  "status": "RUNNING_CHECKS",
  "orderType": "DOMESTIC",
  "sourceAmount": 15,
  "purchaseAmount": 10,
  "sourceCurrency": "USD",
  "destCurrency": "BTC",
  "transferId": null,
  "dest": "bitcoin:mwhGRGtfGhSnzHNRRaRR6i4dDBA7VyBgEY",
  "authCodesRequested": false,
  "blockchainNetworkTx": null,
  "accountId": "AC_J8RQAYT2L72",
  "paymentMethodName": null
};

var createPaymentMethod = {
  "id": "PA_YHVNZLU266G",
  "owner": "account:AC_URQ6TDUMMPG",
  "createdAt": 1645596609694,
  "name": "USD Bank account ending in 0123",
  "defaultCurrency": "USD",
  "fingerprint": "BANK-txgxEt1GSn1xC2mLSXpmJDaxE9xLjIeLGTbwKsRjNBI=",
  "status": "AWAITING_FOLLOWUP",
  "statusMessage": null,
  "waitingPrompts": [
    {
      "id": "2NCGJ49WZWJ",
      "prompt": "Upload a bank statement",
      "responses": [],
      "type": "DOCUMENT"
    }
  ],
  "linkType": "INTERNATIONAL_TRANSFER",
  "beneficiaryType": "INDIVIDUAL",
  "supportsDeposit": true,
  "nameOnMethod": "Sally Smith",
  "last4Digits": "0123",
  "brand": null,
  "expirationDisplay": null,
  "countryCode": "US",
  "nickname": null,
  "rejectionMessage": null,
  "disabled": false,
  "supportsPayment": false,
  "chargeableCurrencies": [],
  "depositableCurrencies": [
    "USD"
  ],
  "chargeFeeSchedule": null,
  "depositFeeSchedule": null,
  "minCharge": null,
  "maxCharge": null,
  "minDeposit": null,
  "maxDeposit": null,
  "documents": [],
  "blockchains": {},
  "liquidationBalances": {},
  "srn": "paymentmethod:PA_YHVNZLU266G"
};

